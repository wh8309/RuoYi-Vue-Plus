package com.ruoyi.common.core.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.Alias;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 工作单位对象 tors_work_unit
 *
 * @author ruoyi
 * @date 2021-05-14
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@Alias("TorsWorkUnit2")
public class TorsWorkUnit {

    private static final long serialVersionUID=1L;

    /** 工作单位id */
    private Long workUnitId;

    /** 单位代码 */
    @Excel(name = "单位代码")
    private String unitCode;

    /** 单位名称 */
    @Excel(name = "单位名称")
    private String unitName;

    /** 单位地址 */
    @Excel(name = "单位地址")
    private String unitAddress;

    /** 单位联系人 */
    @Excel(name = "单位联系人")
    private String unitContact;

    /** 单位办公电话 */
    @Excel(name = "单位办公电话")
    private String unitPhone;

    /** 是否删除 1 正常 -1 删除 */
    @Excel(name = "是否删除 1 正常 -1 删除")
    private Integer isDeleted;

    /** 排序字段 */
    @Excel(name = "排序字段")
    private Long orderNo;

    /** 创建者 */
    private String createBy;

    /** $column.columnComment */
    private Date createTime;

    /** 更新者 */
    private String updateBy;

    /** $column.columnComment */
    private Date updateTime;

    private String remark;

    @TableField(exist = false)
    private Map<String, Object> params = new HashMap<>();
}