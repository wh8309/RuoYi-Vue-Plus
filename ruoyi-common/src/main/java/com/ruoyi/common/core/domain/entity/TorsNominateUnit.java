package com.ruoyi.common.core.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.Alias;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 推荐单位对象 tors_nominate_unit
 * 
 * @author ruoyi
 * @date 2021-04-22
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@Alias("TorsNominateUnit2")
public class TorsNominateUnit implements Serializable {

    private static final long serialVersionUID=1L;


    /** 主键 */
    @TableId(value = "nominate_unit_id")
    private Long nominateUnitId;

    /** 单位名称 */
    @Excel(name = "单位名称")
    private String unitName;

    /** 单位地址 */
    @Excel(name = "单位地址")
    private String unitAddress;

    /** 单位联系人 */
    @Excel(name = "单位联系人")
    private String unitContact;

    /** 单位办公电话 */
    @Excel(name = "单位办公电话")
    private String unitPhone;

    /** 申报授权码 */
    @Excel(name = "申报授权码")
    private String unitAuthCode;

    /** 政务网账号 */
    @Excel(name = "政务网账号")
    private String account;

    /** 政务网密码 */
    @Excel(name = "政务网密码")
    private String password;

    /** 是否删除 1 正常 -1 删除 */
    @Excel(name = "是否删除 1 正常 -1 删除")
    private Integer isDeleted;

    /** 排序字段 */
    @Excel(name = "排序字段")
    private Long orderNo;

    /** 创建者 */
    private String createBy;

    /** $column.columnComment */
    private Date createTime;

    /** 更新者 */
    private String updateBy;

    /** $column.columnComment */
    private Date updateTime;

    private String remark;

    @TableField(exist = false)
    private Map<String, Object> params = new HashMap<>();
}
