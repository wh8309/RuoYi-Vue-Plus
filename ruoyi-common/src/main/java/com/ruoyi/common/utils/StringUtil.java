package com.ruoyi.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public abstract class StringUtil {
    
    public static final String EMPTY_STRING = "";
    
    public static final String[] EMPTY_STRING_ARR = new String[0];
    
    private static final Logger LOG = LoggerFactory.getLogger(StringUtil.class);
    
    private static Method propertiesSaveConvertMethod;
    private static Properties dummyProperties;
    
    static {
        init();
    }
    
    private static void init() {
        try {
            propertiesSaveConvertMethod = Properties.class.getDeclaredMethod("saveConvert", new Class[] {
                    String.class, Boolean.TYPE, Boolean.TYPE });
            propertiesSaveConvertMethod.setAccessible(true);
            dummyProperties = new Properties();
        } catch (Exception e) {
            LOG.error("init propertiesSaveConvertMethod failed", e);
        }
    }
    
    public static boolean isEmpty(String str) {
        return null == str || 0 == str.length();
    }
    
    public static boolean isBlank(String str) {
        if (null == str || 0 == str.length()) {
            return true;
        }
        int len = str.length();
        for (int i = 0; i < len; i++) {
            if (!Character.isWhitespace(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }
    
    public static String nullSafeTrim(String str) {
        return null == str ? null : str.trim();
    }
    
    public static String emptyToNull(Object value) {
        if (null == value) {
            return null;
        } else {
            String str = String.valueOf(value);
            if (str.isEmpty()) {
                return null;
            } else {
                return str;
            }
        }
    }
    
    public static String nullToEmpty(Object value) {
        return null == value ? EMPTY_STRING : String.valueOf(value);
    }
    
    public static String[] splitUsingIndex(String source, String sep) {
        return splitUsingIndex(source, sep, false);
    }
    
    public static String[] splitUsingIndex(String source, String sep, boolean keepLastEmpty) {
        if (null == source || null == sep || source.isEmpty()) {
            return EMPTY_STRING_ARR;
        }
        List<String> list = new ArrayList<String>();
        int sourceLen = source.length();
        int sepLen = sep.length();
        int fromIdx = 0;
        int idx = 0;
        while (-1 != (idx = source.indexOf(sep, fromIdx))) {
            list.add(source.substring(fromIdx, idx));
            fromIdx = idx + sepLen;
            if (fromIdx == sourceLen) {
                break;
            }
        }
        if (fromIdx == sourceLen) {
            if (keepLastEmpty) {
                list.add(EMPTY_STRING);
            }
        } else {
            list.add(source.substring(fromIdx));
        }
        return list.toArray(new String[list.size()]);
    }
    
    public static String[] splitUsingIndex(String source, char sep) {
        return splitUsingIndex(source, sep, false);
    }
    
    public static String[] splitUsingIndex(String source, char sep, boolean keepLastEmpty) {
        if (null == source) {
            return EMPTY_STRING_ARR;
        } else if (source.isEmpty()) {
            return new String[] { EMPTY_STRING };
        }
        List<String> list = new ArrayList<String>();
        int sourceLen = source.length();
        int sepLen = 1;
        int fromIdx = 0;
        int idx = 0;
        while (-1 != (idx = source.indexOf(sep, fromIdx))) {
            list.add(source.substring(fromIdx, idx));
            fromIdx = idx + sepLen;
            if (fromIdx == sourceLen) {
                break;
            }
        }
        if (fromIdx == sourceLen) {
            if (keepLastEmpty) {
                list.add(EMPTY_STRING);
            }
        } else {
            list.add(source.substring(fromIdx));
        }
        return list.toArray(new String[list.size()]);
    }
    
    public static String toPropertiesValue(String rawString) {
        if (null != propertiesSaveConvertMethod) {
            try {
                return (String) propertiesSaveConvertMethod.invoke(dummyProperties, rawString, false, true);
            } catch (Exception e) {
                LOG.error("call propertiesSaveConvertMethod failed", e);
            }
        }
        return simpleToPropertiesValue(rawString);
    }
    
    private static String simpleToPropertiesValue(String rawString) {
        if (null != rawString && !rawString.isEmpty()) {
            int len = rawString.length();
            StringBuilder sBuilder = new StringBuilder(len * 2);
            for (int i = 0; i < len; i++) {
                char c = rawString.charAt(i);
                if (0x007e < c) {
                    sBuilder.append('\\').append('u').append(Integer.toHexString(c));
                } else {
                    sBuilder.append(c);
                }
            }
            return sBuilder.toString();
        } else {
            return rawString;
        }
    }
    
}
