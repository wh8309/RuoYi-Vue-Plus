package com.ruoyi.common.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.entity.TorsNominateUnit;
import com.ruoyi.common.core.domain.entity.TorsWorkUnit;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.spring.SpringUtils;
import org.apache.poi.ss.formula.functions.T;

import java.util.*;
import java.util.stream.Collectors;

public class DropDownBoxUnit {

    public static void setCacheList(String key, Map data) {
        clearCache(key);
        RedisCache bean = SpringUtils.getBean(RedisCache.class);
        data.forEach((k, v) -> {
            bean.setCacheObject(key + k, v);
        });
    }

    public static void clearCache(String key) {
        Collection<String> keys = SpringUtils.getBean(RedisCache.class).keys(key + "*");
        SpringUtils.getBean(RedisCache.class).deleteObject(keys);
    }

    public static List<String> getCacheList(String key) {
        RedisCache bean = SpringUtils.getBean(RedisCache.class);
        Collection<String> keys = bean.keys(key + "*");
        if (keys.isEmpty()) {
            return Collections.emptyList();
        }
        List<String> list = new ArrayList<String>();
        for (String k : keys) {
            String name = bean.getCacheObject(k);
            list.add(name);
        }
        return list;
    }

    public static String getCache(String key, String type) {
        RedisCache bean = SpringUtils.getBean(RedisCache.class);
        return bean.getCacheObject(type + key);
    }

    public static void setCache(Object key, Object value, String type) {
        RedisCache bean = SpringUtils.getBean(RedisCache.class);
        bean.setCacheObject(type + key, value);
    }

    public static void deleteCache(Object key, String type) {
        RedisCache bean = SpringUtils.getBean(RedisCache.class);
        bean.deleteObject(type + key);
    }

}
