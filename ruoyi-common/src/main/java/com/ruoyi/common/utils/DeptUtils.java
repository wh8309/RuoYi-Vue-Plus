package com.ruoyi.common.utils;

import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.entity.SysDictData;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Stream;

/**
 * 部门工具类
 *
 * @author ruoyi
 */
public class DeptUtils {

    /**
     * 设置部门缓存
     *
     * @param key     参数键
     * @param sysDept 部门数据列表
     */
    public static void setDictCache(String key, SysDept sysDept) {
        SpringUtils.getBean(RedisCache.class).setCacheObject(getCacheKey(key), sysDept);
    }


    /**
     * 获取部门缓存
     *
     * @param key 参数键
     * @return dictDatas 部门数据列表
     */
    public static List<SysDept> getDictCache(String key) {
        RedisCache redisCache = SpringUtils.getBean(RedisCache.class);
        Object cacheObj = redisCache.keys(Constants.SYS_DEPT_KEY + key);
        if (StringUtils.isNotNull(cacheObj)) {
            LinkedHashSet<String> keyList = StringUtils.cast(cacheObj);
            List<SysDept> sysDeptList = new ArrayList<>();
            for (String o : keyList) {
                Object obj = redisCache.getCacheObject(o);
                if (StringUtils.isNotNull(obj)) {
                    SysDept sysDept = StringUtils.cast(obj);
                    sysDeptList.add(sysDept);
                }
            }
            return sysDeptList;
        }
        return null;
    }


    /**
     * 清空部门缓存
     */
    public static void clearDictCache() {
        Collection<String> keys = SpringUtils.getBean(RedisCache.class).keys(Constants.SYS_DEPT_KEY + "*");
        SpringUtils.getBean(RedisCache.class).deleteObject(keys);
    }

    /**
     * 清空部门缓存
     */
    public static void deleteDictCache(String key) {
        SpringUtils.getBean(RedisCache.class).deleteObject(getCacheKey(key));
    }

    /**
     * 设置cache key
     *
     * @param configKey 参数键
     * @return 缓存键key
     */
    public static String getCacheKey(String configKey) {
        return Constants.SYS_DEPT_KEY + configKey;
    }

    public static String getDeptId(String deptName) {
        if (StringUtils.isEmpty(deptName)) return null;
        List<SysDept> deptList = getDictCache("*");
        for (SysDept dept : deptList) {
            if (deptName.equals(dept.getDeptName())) {
                return dept.getDeptId().toString();
            }
        }
        return null;
    }
}
