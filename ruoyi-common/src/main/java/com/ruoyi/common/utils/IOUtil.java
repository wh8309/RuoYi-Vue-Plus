package com.ruoyi.common.utils;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public abstract class IOUtil {

	public static final String CHARSET_NAME_UTF8 = "UTF-8";

	public static final Charset CHARSET_UTF8 = Charset
			.forName(CHARSET_NAME_UTF8);

	public static final byte[] EMPTY_BYTES = new byte[0];

	private static final int DEFAULT_SIZE = 4096;

	private static final Logger LOG = LoggerFactory.getLogger(IOUtil.class);

	public static void close(Closeable closeable) {
		if (null != closeable) {
			try {
				closeable.close();
			} catch (IOException e) {
				LOG.error("Close closeable faild: " + closeable.getClass(), e);
			}
		}
	}

	public static byte[] base64StringToBytes(String base64String) {
		if (StringUtils.isNotEmpty(base64String)) {
			return Base64.decodeBase64(base64String);
		} else {
			return EMPTY_BYTES;
		}
	}

	public static byte[] fileToBytes(File file) {
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(file);
			return streamToBytes(fis);
		} catch (Exception e) {
			LOG.error("read bytes failed", e);
		} finally {
			close(fis);
		}
		return null;
	}

	public static byte[] streamToBytes(InputStream is) {
		return streamToBytes(is, false);
	}
	
	public static byte[] streamToBytes(InputStream is, boolean closeInputStream) {
		ByteArrayOutputStream baos = null;
		try {
			baos = new ByteArrayOutputStream();
			byte[] buff = new byte[DEFAULT_SIZE];
			int readedLen = -1;
			while (-1 != (readedLen = is.read(buff))) {
				baos.write(buff, 0, readedLen);
			}
			byte[] bytes = baos.toByteArray();
			return bytes;
		} catch (Exception e) {
			LOG.error("read bytes failed", e);
		} finally {
			close(baos);
			if (closeInputStream) {
				close(is);
			}
		}
		return null;
	}

	public static String bytesToBase64String(byte[] bytes) {
		if (null != bytes) {
			return Base64.encodeBase64String(bytes);
		} else {
			return StringUtil.EMPTY_STRING;
		}
	}

	public static String fileToBase64String(File file) {
		byte[] bytes = fileToBytes(file);
		return bytesToBase64String(bytes);
	}

	public static String streamToBase64String(InputStream is) {
		return streamToBase64String(is, false);
	}

	public static String streamToBase64String(InputStream is,
			boolean closeInputStream) {
		byte[] bytes = streamToBytes(is, closeInputStream);
		if (null != bytes) {
			return Base64.encodeBase64String(bytes);
		} else {
			return StringUtil.EMPTY_STRING;
		}
	}

	public static boolean bytesToFile(byte[] bytes, File file) {
		if (null != bytes && null != file) {
			ByteArrayInputStream bais = null;
			try {
				bais = new ByteArrayInputStream(bytes);
				return streamToFile(bais, file);
			} catch (Exception e) {
				LOG.error("write bytes failed", e);
			} finally {
				close(bais);
			}
		}
		return false;
	}

	/**
	 * 将zip文件解压缩(不支持带文件夹)
	 * 
	 * @param zipFile
	 *            待压缩文件的带后缀全路径
	 * 
	 * @param storePath
	 *            压缩后存放的目录
	 * */
	public static ArrayList<String> zipdepressFile(String zipFile,
			String storePath) {
		ArrayList<String> allFileName = new ArrayList<String>();
		try {
			// 先指定压缩档的位置和档名，建立FileInputStream对象
			FileInputStream fins = new FileInputStream(zipFile);
			// 将fins传入ZipInputStream中
			ZipInputStream zins = new ZipInputStream(fins);
			ZipEntry ze = null;
			byte[] ch = new byte[256];
			while ((ze = zins.getNextEntry()) != null) {
				File zfile = new File(storePath + ze.getName());
				File fpath = new File(zfile.getParentFile().getPath());
				if (ze.isDirectory()) {
					if (!zfile.exists())
						zfile.mkdirs();
					zins.closeEntry();
				} else {
					if (!fpath.exists())
						fpath.mkdirs();
					FileOutputStream fouts = new FileOutputStream(zfile);
					int i;
					allFileName.add(zfile.getAbsolutePath());
					while ((i = zins.read(ch)) != -1)
						fouts.write(ch, 0, i);
					zins.closeEntry();
					fouts.close();
				}
			}
			fins.close();
			zins.close();
		} catch (Exception e) {
			System.err.println("Extract error:" + e.getMessage());
		}
		return allFileName;
	}
	
	/**
	 * 将压缩文件进行解压,并将其中每个文件的内容转化成Base64字符串,返回其Base64字符串的List(不支持带文件夹)
	 * 
	 * @param zipFile
	 *            压缩文件
	 * */
	public static List<String> zipdepressFileToBase64(File zipFile , List<String> nameList) {
		ArrayList<String> allFilebase64List = new ArrayList<String>();
		try {
			// 先指定压缩档的位置和档名，建立FileInputStream对象
			FileInputStream fins = new FileInputStream(zipFile);
			// 将fins传入ZipInputStream中
			ZipInputStream zins = new ZipInputStream(fins);
			ZipEntry ze = null;
			while ((ze = zins.getNextEntry()) != null) {
				InputStream ips = zins;
				nameList.add(ze.getName());
				allFilebase64List.add(streamToBase64String(ips));
				zins.closeEntry();
			}
			fins.close();
			zins.close();
		} catch (Exception e) {
			System.err.println("Extract error:" + e.getMessage());
		}
		return allFilebase64List;
	}
	
	/**
	 * 以zip格式压缩文件(不支持带文件夹)
	 * 
	 * @param srcFileNames
	 *            带路径的文件名数组
	 * 
	 * @param zipname
	 *            带路径带后缀的生成zip文件
	 * */
	public static void zipCompressFiles(String[] srcFileNames, String zipname)
			throws IOException {
		String[] files = srcFileNames;
		OutputStream os = new BufferedOutputStream(
				new FileOutputStream(zipname));
		ZipOutputStream zos = new ZipOutputStream(os);
		byte[] buf = new byte[8192];
		int len;
		for (int i = 0; i < files.length; i++) {
			File file = new File(files[i]);

			if (!file.isFile())
				continue;
			ZipEntry ze = new ZipEntry(file.getName());
			zos.putNextEntry(ze);
			BufferedInputStream bis = new BufferedInputStream(
					new FileInputStream(file));
			while ((len = bis.read(buf)) > 0) {
				zos.write(buf, 0, len);
			}
			zos.closeEntry();
		}
		zos.closeEntry();
		zos.close();
	}

	public static boolean streamToFile(InputStream is, File file) {
		return streamToFile(is, file, false);
	}
	
	public static boolean streamToFile(InputStream is, File file,
			boolean closeInputStream) {
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(file);
			byte[] buff = new byte[DEFAULT_SIZE];
			int readedLen = -1;
			while (-1 != (readedLen = is.read(buff))) {
				fos.write(buff, 0, readedLen);
			}
			fos.flush();
			return true;
		} catch (Exception e) {
			LOG.error("write bytes failed", e);
		} finally {
			close(fos);
			if (closeInputStream) {
				close(is);
			}
		}
		return false;
	}

	public static boolean base64StringToFile(String base64String, File file) {
		if (null != base64String) {
			byte[] bytes = base64StringToBytes(base64String);
			return bytesToFile(bytes, file);
		}
		return false;
	}

	public static InputStream bytesToStream(byte[] bytes) {
		return new ByteArrayInputStream(bytes);
	}

	public static InputStream base64StringToStream(String base64String) {
		if (null != base64String) {
			byte[] bytes = base64StringToBytes(base64String);
			return new ByteArrayInputStream(bytes);
		}
		return null;
	}

	public static InputStream fileToStream(File file)
			throws FileNotFoundException {
		return new FileInputStream(file);
	}

	public static File createTmpFile(InputStream ips, String suffix , String tmpFilePath) {
		File newFile = new File(tmpFilePath
				+ UUID.randomUUID().toString().replace("-", "") + suffix);
		if (streamToFile(ips, newFile)) {
			return newFile;
		}
		return null;
	}

	public static String streamToString(InputStream ips) throws IOException {
		byte[] b = new byte[DEFAULT_SIZE];
		StringBuffer buffer = new StringBuffer();
		int len = 0;
		while ((len = ips.read(b)) != -1) {
			buffer.append(new String(b, 0, len));
		}
		return buffer.toString();
	}
	
	public static String streamToString(InputStream ips , boolean closeIps) throws IOException {
		try {
			byte[] b = new byte[DEFAULT_SIZE];
			StringBuffer buffer = new StringBuffer();
			int len = 0;
			while ((len = ips.read(b)) != -1) {
				buffer.append(new String(b, 0, len));
			}
			return buffer.toString();
		} finally {
			if( closeIps ) {
				ips.close();
			}
		}
	}

}
