package com.ruoyi.common.enums;

import com.baomidou.mybatisplus.annotation.IEnum;

import java.io.Serializable;

public enum ClientReportStatus implements IEnum {


    FILLING(0,"填报中"),

    TOAUDIT(10,"待审核"),

    AUDITPASS(20,"审核通过"),
    AUDITUNPASS(-20,"审核驳回"),

    RECHECKASS(30,"复核通过"),
    RECHECKUNPASS(-30,"复核驳回");

    private final int value;
    private final String desc;

    ClientReportStatus(final int value, final String desc) {
        this.value = value;
        this.desc = desc;
    }

    @Override
    public Integer getValue() {
        return this.value;
    }

    // Jackson 注解为 JsonValue 返回中文 json 描述
    public String getDesc() {
        return this.desc;
    }

}
