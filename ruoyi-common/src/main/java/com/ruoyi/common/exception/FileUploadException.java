package com.ruoyi.common.exception;

/**
 * 演示模式异常
 * 
 * @author ruoyi
 */
public class FileUploadException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public FileUploadException()
    {
    }

    public FileUploadException(String msg)
    {
        super(msg);
    }

}
