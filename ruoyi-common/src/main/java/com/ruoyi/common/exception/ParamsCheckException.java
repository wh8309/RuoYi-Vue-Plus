package com.ruoyi.common.exception;

public class ParamsCheckException extends RuntimeException {

    private String message;


    public ParamsCheckException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage(){
        return message;
    }
}
