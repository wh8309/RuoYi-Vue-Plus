package com.ruoyi.website.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.website.domain.ClientBreakRule;

/**
 * 破格条件说明Mapper接口
 *
 * @author ruoyi
 * @date 2021-03-25
 */
public interface ClientBreakRuleMapper extends BaseMapper<ClientBreakRule> {

}
