package com.ruoyi.website.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.website.domain.ClientSkillSummary;

/**
 * 任期内专业技术业绩与成果报告Mapper接口
 *
 * @author ruoyi
 * @date 2021-03-24
 */
public interface ClientSkillSummaryMapper extends BaseMapper<ClientSkillSummary> {

}
