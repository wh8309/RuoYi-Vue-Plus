package com.ruoyi.website.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.website.domain.ClientQualificationConfirm;

/**
 * 职称资格确认Mapper接口
 *
 * @author ruoyi
 * @date 2021-03-24
 */
public interface ClientQualificationConfirmMapper extends BaseMapper<ClientQualificationConfirm> {

}
