package com.ruoyi.website.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.website.domain.ClientEducation;

/**
 * 学历信息Mapper接口
 *
 * @author ruoyi
 * @date 2021-03-22
 */
public interface ClientEducationMapper extends BaseMapper<ClientEducation> {

}
