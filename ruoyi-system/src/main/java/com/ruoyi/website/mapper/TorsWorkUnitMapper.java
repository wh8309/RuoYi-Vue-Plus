package com.ruoyi.website.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.website.domain.TorsWorkUnit;

/**
 * 推荐单位Mapper接口
 *
 * @author ruoyi
 * @date 2021-05-14
 */
public interface TorsWorkUnitMapper extends BaseMapper<TorsWorkUnit> {
}
