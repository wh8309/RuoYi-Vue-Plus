package com.ruoyi.website.mapper;

import com.ruoyi.website.domain.TorsNominateUnit;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 推荐单位Mapper接口
 *
 * @author ruoyi
 * @date 2021-04-22
 */
public interface TorsNominateUnitMapper extends BaseMapper<TorsNominateUnit> {

}
