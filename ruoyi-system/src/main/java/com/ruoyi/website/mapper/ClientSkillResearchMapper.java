package com.ruoyi.website.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.website.domain.ClientSkillResearch;

/**
 * 任期内科研成果Mapper接口
 *
 * @author ruoyi
 * @date 2021-03-18
 */
public interface ClientSkillResearchMapper extends BaseMapper<ClientSkillResearch> {

}
