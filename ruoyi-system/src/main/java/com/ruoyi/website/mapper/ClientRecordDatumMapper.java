package com.ruoyi.website.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.website.domain.ClientRecordDatum;

/**
 * 评审申报材料Mapper接口
 *
 * @author ruoyi
 * @date 2021-03-18
 */
public interface ClientRecordDatumMapper extends BaseMapper<ClientRecordDatum> {

}
