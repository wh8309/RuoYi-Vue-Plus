package com.ruoyi.website.mapper;

import com.ruoyi.website.domain.TorsAuditResult;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 审核未通过Mapper接口
 *
 * @author admin
 * @date 2021-07-07
 */
public interface TorsAuditResultMapper extends BaseMapper<TorsAuditResult> {

}
