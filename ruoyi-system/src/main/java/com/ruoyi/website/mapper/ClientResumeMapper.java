package com.ruoyi.website.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.website.domain.ClientResume;

/**
 * 从事专业技术工作简历Mapper接口
 *
 * @author ruoyi
 * @date 2021-03-22
 */
public interface ClientResumeMapper extends BaseMapper<ClientResume> {

}
