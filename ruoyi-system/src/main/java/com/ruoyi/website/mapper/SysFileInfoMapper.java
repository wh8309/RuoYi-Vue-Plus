package com.ruoyi.website.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.SysConfig;
import com.ruoyi.website.domain.SysFileInfo;

import java.util.List;

/**
 * 文件信息Mapper接口
 *
 * @author ruoyi
 * @date 2021-03-18
 */
public interface SysFileInfoMapper extends BaseMapper<SysFileInfo> {


}
