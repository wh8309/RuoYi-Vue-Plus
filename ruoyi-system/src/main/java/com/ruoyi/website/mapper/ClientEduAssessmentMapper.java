package com.ruoyi.website.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.website.domain.ClientEduAssessment;

/**
 * 年度考核与继续教育Mapper接口
 *
 * @author ruoyi
 * @date 2021-03-22
 */
public interface ClientEduAssessmentMapper extends BaseMapper<ClientEduAssessment> {

}
