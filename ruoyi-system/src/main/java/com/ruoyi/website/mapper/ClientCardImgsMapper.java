package com.ruoyi.website.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.website.domain.ClientCardImgs;

/**
 * 证件电子图片Mapper接口
 *
 * @author ruoyi
 * @date 2021-03-18
 */
public interface ClientCardImgsMapper extends BaseMapper<ClientCardImgs> {

}
