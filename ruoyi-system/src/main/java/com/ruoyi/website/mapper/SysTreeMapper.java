package com.ruoyi.website.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.website.domain.SysTree;

/**
 * 类目Mapper接口
 *
 * @author ruoyi
 * @date 2021-03-23
 */
public interface SysTreeMapper extends BaseMapper<SysTree> {

}
