package com.ruoyi.website.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.website.domain.ClientProfile;
import com.ruoyi.website.domain.vo.request.ClientProfilePageRequestVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户信息Mapper接口
 *
 * @author ruoyi
 * @date 2021-04-01
 */
public interface ClientProfileMapper extends BaseMapper<ClientProfile> {

    List<ClientProfile> selectClientProfileList(ClientProfile clientProfile);

    /**
     * 校验手机号码是否唯一
     *
     * @param mobile 手机号码
     * @return 结果
     */
    public ClientProfile checkMobileUnique(String mobile);

    public ClientProfile checkMobileUniqueUnContainsThat(@Param("clientId") Long clientId, @Param("mobile") String mobile);

    /**
     * 校验身份证号码是否唯一
     *
     * @param idcard 身份证号码
     * @return 结果
     */
    public ClientProfile checkIdcardUnique(String idcard);

    public ClientProfile checkIdcardUniqueUnContainsThat(@Param("clientId") Long clientId, @Param("idcard") String idcard);

    /**
     * 批量删除客户信息
     *
     * @param clientIds 需要删除的客户id
     * @return 结果
     */
    public int deleteClientProfileByClientIds(Long[] clientIds);

    int resetUserPwd(@Param("userName") String userName, @Param("password") String password);
}
