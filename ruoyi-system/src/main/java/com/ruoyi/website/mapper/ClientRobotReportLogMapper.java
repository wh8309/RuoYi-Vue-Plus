package com.ruoyi.website.mapper;

import com.ruoyi.website.domain.ClientRobotReportLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 机器人上报流水Mapper接口
 *
 * @author ruoyi
 * @date 2021-06-11
 */
public interface ClientRobotReportLogMapper extends BaseMapper<ClientRobotReportLog> {

}
