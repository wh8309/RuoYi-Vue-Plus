package com.ruoyi.website.mapper;

import com.ruoyi.website.domain.ClientReportProcess;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 申报流程处理(流转意见)Mapper接口
 *
 * @author ruoyi
 * @date 2021-04-29
 */
public interface ClientReportProcessMapper extends BaseMapper<ClientReportProcess> {

}
