package com.ruoyi.website.mapper;

import com.ruoyi.website.domain.ClientRobotReportTag;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 机器人上报tagMapper接口
 *
 * @author ruoyi
 * @date 2022-04-14
 */
public interface ClientRobotReportTagMapper extends BaseMapper<ClientRobotReportTag> {

}
