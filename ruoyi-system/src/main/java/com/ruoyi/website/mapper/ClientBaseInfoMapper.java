package com.ruoyi.website.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.website.domain.ClientBaseInfo;
import com.ruoyi.website.domain.bo.FillInStatisticsBO;
import com.ruoyi.website.domain.vo.response.BasePositionApplyResponseVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 客户基本信息Mapper接口
 *
 * @author ruoyi
 * @date 2021-03-22
 */
public interface ClientBaseInfoMapper extends BaseMapper<ClientBaseInfo> {

    int selectTableCount(@Param("tableName") String tableName,@Param("clientId") Long clientId);

    //分页查询包括数据权限
    List<BasePositionApplyResponseVO> selectClientBaseInfoList(ClientBaseInfo clientBaseInfo);

    //人员填报统计
    List<FillInStatisticsBO> fillInStatistics(ClientBaseInfo clientBaseInfo);


}
