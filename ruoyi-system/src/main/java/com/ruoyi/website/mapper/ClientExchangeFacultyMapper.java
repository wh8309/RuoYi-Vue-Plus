package com.ruoyi.website.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.website.domain.ClientExchangeFaculty;

/**
 * 转换系列Mapper接口
 *
 * @author ruoyi
 * @date 2021-03-24
 */
public interface ClientExchangeFacultyMapper extends BaseMapper<ClientExchangeFaculty> {

}
