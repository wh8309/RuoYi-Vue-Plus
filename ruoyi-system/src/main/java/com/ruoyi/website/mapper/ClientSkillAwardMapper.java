package com.ruoyi.website.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.website.domain.ClientSkillAward;

/**
 * 任职期间奖励情况Mapper接口
 *
 * @author ruoyi
 * @date 2021-03-24
 */
public interface ClientSkillAwardMapper extends BaseMapper<ClientSkillAward> {

}
