package com.ruoyi.website.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.website.domain.ClientSkillPublishedPaper;

/**
 * 任期内发论文论著情况Mapper接口
 *
 * @author ruoyi
 * @date 2021-03-18
 */
public interface ClientSkillPublishedPaperMapper extends BaseMapper<ClientSkillPublishedPaper> {

}
