package com.ruoyi.website.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.website.domain.ClientReferralType;

/**
 * 转评条件说明Mapper接口
 *
 * @author ruoyi
 * @date 2021-03-24
 */
public interface ClientReferralTypeMapper extends BaseMapper<ClientReferralType> {

}
