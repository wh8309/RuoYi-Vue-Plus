package com.ruoyi.website.service;

import com.ruoyi.website.domain.TorsAuditResult;
import com.ruoyi.website.vo.TorsAuditResultVo;
import com.ruoyi.website.bo.TorsAuditResultQueryBo;
import com.ruoyi.website.bo.TorsAuditResultAddBo;
import com.ruoyi.website.bo.TorsAuditResultEditBo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Collection;
import java.util.List;

/**
 * 审核未通过Service接口
 *
 * @author admin
 * @date 2021-07-07
 */
public interface ITorsAuditResultService extends IService<TorsAuditResult> {
	/**
	 * 查询单个
	 * @return
	 */
	TorsAuditResultVo queryById(Long auditResultId);

	/**
	 * 查询列表
	 */
	List<TorsAuditResultVo> queryList(TorsAuditResultQueryBo bo);

	/**
	 * 根据新增业务对象插入审核未通过
	 * @param bo 审核未通过新增业务对象
	 * @return
	 */
	Boolean insertByAddBo(TorsAuditResultAddBo bo);

	/**
	 * 根据编辑业务对象修改审核未通过
	 * @param bo 审核未通过编辑业务对象
	 * @return
	 */
	Boolean updateByEditBo(TorsAuditResultEditBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

	//批量修改为:已处理
	Boolean updateDealStatusByIds(Collection<Long> ids);
}
