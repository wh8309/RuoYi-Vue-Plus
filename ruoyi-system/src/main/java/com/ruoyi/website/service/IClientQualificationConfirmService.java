package com.ruoyi.website.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.website.domain.ClientQualificationConfirm;
import com.ruoyi.website.domain.vo.request.ClientQualificationConfirmRequest;
import com.ruoyi.website.domain.vo.response.ClientQualificationConfirmResponse;


/**
 * 职称资格确认Service接口
 *
 * @author ruoyi
 * @date 2021-03-24
 */
public interface IClientQualificationConfirmService extends IService<ClientQualificationConfirm> {

    ClientQualificationConfirmResponse queryDetailById(Long clientId);

    int insertOrUpdate(ClientQualificationConfirmRequest qualificationConfirmRequest);

    //重置“职称资格确认表”PDF
    void resetDatumPDFUrl(Long clientId);

}
