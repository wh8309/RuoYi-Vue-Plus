package com.ruoyi.website.service.impl;

import com.ruoyi.website.domain.SysTree;
import com.ruoyi.website.mapper.SysTreeMapper;
import com.ruoyi.website.service.ISysTreeService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.apache.commons.lang3.StringUtils;


import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 类目Service业务层处理
 *
 * @author ruoyi
 * @date 2021-03-23
 */
@Service
public class SysTreeServiceImpl extends ServiceImpl<SysTreeMapper, SysTree> implements ISysTreeService {

    @Resource
    private SysTreeMapper sysTreeMapper;

    @Override
    public List<SysTree> queryNoParentListByType(String treeType) {
        LambdaQueryWrapper<SysTree> lqw = Wrappers.lambdaQuery();
        lqw.eq(SysTree::getTreeType, treeType);
        lqw.isNull(SysTree::getParentTreeId);
        lqw.eq(SysTree::getIsEnable,1);
        lqw.orderByDesc(SysTree::getCreateTime);
        List<SysTree> sysTreeList = sysTreeMapper.selectList(lqw);
        return sysTreeList;
    }

    @Override
    public List<SysTree> querySysTreeList(Long parentTreeId, String treeType, Integer Level) {
        LambdaQueryWrapper<SysTree> lqw = Wrappers.lambdaQuery();
        lqw.eq(parentTreeId != null, SysTree::getParentTreeId, parentTreeId);
        lqw.eq(Level != null, SysTree::getLvl, Level);
        lqw.eq(StringUtils.isNotEmpty(treeType), SysTree::getTreeType, treeType);
        List<SysTree> sysTreeList = sysTreeMapper.selectList(lqw);
        return sysTreeList;
    }

    @Override
    public SysTree querySysTreeByTreeId(Long treeId) {
        SysTree sysTree = sysTreeMapper.selectById(treeId);
        return sysTree;
    }
}
