package com.ruoyi.website.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.website.domain.ClientBaseInfo;
import com.ruoyi.website.domain.TorsNominateUnit;
import com.ruoyi.website.domain.vo.request.*;
import com.ruoyi.website.domain.vo.response.*;

import java.util.List;
import java.util.Map;

/**
 * 客户基本信息Service接口
 *
 * @author ruoyi
 * @date 2021-03-22
 */
public interface IClientBaseInfoService extends IService<ClientBaseInfo> {

    BaseInfoResponseVO queryBaseById(Long clientId);

    ClientBaseInfo queryBaseInfoById(Long clientId);

    List<BaseInfoResponseVO> queryBaseInfoList(BaseInfoQueryRequestVO baseInfoQueryRequestVO);

    int insertOrUpdate(BaseInfoRequestVO baseInfoRequestVO);

    int insertOrUpdateBase(ClientBaseInfo clientBaseInfo);

    List<ReviewNoticeResponse> queryReviewNoticeList(ReviewNoticeRequest reviewNoticeRequest);

    ReviewNoticeResponse queryReviewNoticeByTreeId(Long treeId);

    List<Map<Long, String>> queryProfessionalList(Long parentTreeId);

    List<Map<Long, String>> queryProfessionalNameList(Long parentTreeId);

    List<SysTreeResponse> queryEncodeCompanyTreeList();

    List<SysTreeResponse> queryPositionalTitlesTreeList();

    List<SysTreeResponse> querySpecialtyTreeList();

    int queryTableCount(String tableName, Long clientId);

    List<MenuResponseVO> queryMenuList(Long clientId);

    int updateStatusByClientId(Long clientId, Integer status,Long orderNo);

    //分页查询，包括数据权限
    List<BasePositionApplyResponseVO> queryPageList(ClientBaseInfo clientBaseInfo);

    public int fakeDeleteBaseByClientIdArr(Long[] clientIds);

    //批量设置推荐单位
    public boolean updateBatch(BatchSetNominateUnitVO batchSetNominateUnitVO);

    //批量设置工作单位
    public boolean updateBatch(BatchSetWorkUnitVO batchSetWorkUnitVO);

    //批量设置填报人
    public boolean updateBatch(BatchSetFillInUserVO batchSetFillInUserVO);

    //批量分配退回
    public boolean updateBatch(BatchDistributeBackVO batchDistributeBackVO);

    //批量流程退回
    public boolean updateBatch(BatchBackClientReportStatusVO batchBackClientReportStatusVO);

    //批量重置机器人上报
    public boolean updateBatch(BatchRestRobotReportVO batchRestRobotReportVO);

    //人员填报统计
    List<FillInStatisticsResponseVO> fillInStatistics(ClientBaseInfo clientBaseInfo);


    List<BaseInfoExcelImportResponseVO> exportListByParam(ClientBaseInfo baseInfo);

    String importBaseInfo(List<BaseInfoExcelImportResponseVO> excelData, String operName);

    //更新政务网账号和密码
    boolean updateGovAccount(GovAccountVO govAccountVO);

    //获取某个客户机器人上报日志
    List<RobotReportLogResponseVO> queryRobotReportLogByClientId(Long clientId);

    //根据推荐单位名称，修改授权码
    boolean updateByTorsNominateUnit(TorsNominateUnit torsNominateUnit);


}
