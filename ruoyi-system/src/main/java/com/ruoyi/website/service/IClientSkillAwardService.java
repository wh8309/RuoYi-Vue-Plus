package com.ruoyi.website.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.website.domain.ClientSkillAward;
import com.ruoyi.website.domain.vo.request.ClientSkillAwardRequest;
import com.ruoyi.website.domain.vo.response.ClientSkillAwardResponse;


/**
 * 任职期间奖励情况Service接口
 *
 * @author ruoyi
 * @date 2021-03-24
 */
public interface IClientSkillAwardService extends IService<ClientSkillAward> {


    ClientSkillAwardResponse queryDetailById(Long clientId);

    int insertOrUpdate(ClientSkillAwardRequest clientSkillAwardRequest);
}
