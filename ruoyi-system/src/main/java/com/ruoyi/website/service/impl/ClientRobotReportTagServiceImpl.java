package com.ruoyi.website.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.website.domain.ClientBaseInfo;
import com.ruoyi.website.domain.ClientQualificationConfirm;
import com.ruoyi.website.domain.ClientRobotReportTag;
import com.ruoyi.website.mapper.ClientBaseInfoMapper;
import com.ruoyi.website.mapper.ClientRobotReportTagMapper;
import com.ruoyi.website.service.IClientQualificationConfirmService;
import com.ruoyi.website.service.IClientRobotReportTagService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 机器人上报tagService业务层处理
 *
 * @author ruoyi
 * @date 2022-04-14
 */
@Service
public class ClientRobotReportTagServiceImpl extends ServiceImpl<ClientRobotReportTagMapper, ClientRobotReportTag> implements IClientRobotReportTagService {
    @Autowired
    private ClientBaseInfoMapper baseInfoMapper;
    @Autowired
    private IClientQualificationConfirmService iClientQualificationConfirmService;

    private final static List<String> resetGeneratedDatumUrlTagList = new ArrayList<>();//
    private final static Map<Integer,String> fileTypeReportTagMap = new HashMap();//文件类型与上报Tag对应Map
    static {
        fileTypeReportTagMap.put(1,"menu_photo");//照片

        fileTypeReportTagMap.put(10,"menu_picture,01a");//身份证
        fileTypeReportTagMap.put(11,"menu_picture,03a");//申报学历证
        fileTypeReportTagMap.put(12,"menu_picture,04a");//申报学位证
        fileTypeReportTagMap.put(13,"menu_picture,05a");//职称外语证书
        fileTypeReportTagMap.put(14,"menu_picture,06a");//职称计算机证书
        fileTypeReportTagMap.put(15,"menu_picture,07a");//二级公证员职称证书
        fileTypeReportTagMap.put(16,"menu_picture,12a");//职(执)业资格证书
        fileTypeReportTagMap.put(17,"menu_picture,13a");//水平能力测试合格证
        fileTypeReportTagMap.put(18,"menu_picture,59a");//职称资格确认证明材料
        fileTypeReportTagMap.put(19,"menu_picture,40a");//其他证明材料

        fileTypeReportTagMap.put(50,"menu_fileManager,10a");//申报专业技术任职资格诚信承诺书
        fileTypeReportTagMap.put(51,"menu_fileManager,10a");//任现职以来工作情况证明材料
        fileTypeReportTagMap.put(52,"menu_fileManager,10a");//机关调入、海外引进、非国有单位证明扫描件
        fileTypeReportTagMap.put(53,"menu_fileManager,20a");//专业论文论著照片
        fileTypeReportTagMap.put(54,"menu_fileManager,30a");//反映个人专业工作业绩的材料
        fileTypeReportTagMap.put(55,"menu_fileManager,40a");//任职以来获得的专业奖励证书
        fileTypeReportTagMap.put(56,"menu_fileManager,50a");//任职以来获得的其他奖励证书
        fileTypeReportTagMap.put(57,"menu_fileManager,60a");//任职以来参加继续教育培训证书或证明材料
        fileTypeReportTagMap.put(58,"menu_fileManager,122a");//年度考核材料

//        fileTypeReportTagMap.put(70,"menu_photo"); //破格申请表上传 暂时不处理

        fileTypeReportTagMap.put(80,"menu_workSummary");//任期内专业技术工作总结


        resetGeneratedDatumUrlTagList.add("menu_jbxx");//基本情况
        resetGeneratedDatumUrlTagList.add("menu_education");//学历
        resetGeneratedDatumUrlTagList.add("menu_experience");//从事专业技术工作简历
        resetGeneratedDatumUrlTagList.add("menu_msInfo4");//资格确认
    }

    @Override
    public List<ClientRobotReportTag> queryList(ClientRobotReportTag clientRobotReportTag) {
        LambdaQueryWrapper<ClientRobotReportTag> lqw = Wrappers.lambdaQuery();
        if (StringUtils.isNotBlank(clientRobotReportTag.getReportTagJson())){
            lqw.eq(ClientRobotReportTag::getReportTagJson ,clientRobotReportTag.getReportTagJson());
        }
        return this.list(lqw);
    }

    //重置某个上报标识
    @Override
   public void resetReportTag(Long clientId,String tag){
        if(clientId!=null && clientId>0 && StringUtils.isNotBlank(tag)) {
            ClientBaseInfo clientInfo = baseInfoMapper.selectById(clientId);
            String reportTagId = clientInfo.getClientId()+"-"+clientInfo.getBelongSeries();
            ClientRobotReportTag robotReportTag = getById(reportTagId);
            if (robotReportTag != null) {
                Map<String, Object> reportTagMap = JSONObject.parseObject(robotReportTag.getReportTagJson(), LinkedHashMap.class, Feature.OrderedField);
                reportTagMap.put(tag, false);
                String reportTagJsonString = JSON.toJSONString(reportTagMap);
                robotReportTag.setReportTagJson(reportTagJsonString);
                robotReportTag.setUpdateTime(new Date());
                updateById(robotReportTag);
            }
            //重新生成“职称资格确认表”
            resetGeneratedDatumUrl(clientId,tag,null);

            if("menu_qualityProject12".equals(tag)){//任期内科研成果 修改
                resetReportTag(clientId,54);
            }
            if("menu_paperBook".equals(tag)){//任期内发表论文论著情况 修改
                resetReportTag(clientId,53);
            }
        }
    }

    //判定“职称资格确认表”是否需要重新生成
    private void resetGeneratedDatumUrl(Long clientId,String tag,Integer fileType){
        ClientQualificationConfirm clientQualificationConfirm = iClientQualificationConfirmService.getById(clientId);
        if(clientQualificationConfirm != null ) {
            if (clientQualificationConfirm.getRobotReportExceptionCode()==null || clientQualificationConfirm.getRobotReportExceptionCode()!=1){//异常码等于1，账号密码错误，重置没有意义
                if (StringUtils.isNotBlank(tag)) {
                    if (resetGeneratedDatumUrlTagList.contains(tag)) {
                        iClientQualificationConfirmService.resetDatumPDFUrl(clientId);
                    }
                }
                if (fileType != null && fileType > 0) {
                    if (fileType == 1) {//照片
                        iClientQualificationConfirmService.resetDatumPDFUrl(clientId);
                    }
                }
            }
        }
    }


    //重置某个文件类型
    @Override
    public void resetReportTag(Long clientId,Integer fileType){
        if(clientId!=null && clientId>0 && fileType!=null && fileType>0) {
            ClientBaseInfo clientInfo = baseInfoMapper.selectById(clientId);
            String reportTagId = clientInfo.getClientId()+"-"+clientInfo.getBelongSeries();
            ClientRobotReportTag robotReportTag = getById(reportTagId);
            if(robotReportTag != null && StringUtils.isNotBlank(fileTypeReportTagMap.get(fileType))) {
                Map<String, Object> reportTagMap = JSONObject.parseObject(robotReportTag.getReportTagJson(), LinkedHashMap.class, Feature.OrderedField);
                for(String tag:fileTypeReportTagMap.get(fileType).split(",")) {
                    reportTagMap.put(tag, false);
                }
                String reportTagJsonString = JSON.toJSONString(reportTagMap);
                robotReportTag.setReportTagJson(reportTagJsonString);
                robotReportTag.setUpdateTime(new Date());
                updateById(robotReportTag);
            }
            //重新生成“职称资格确认表”
            resetGeneratedDatumUrl(clientId,null,fileType);
        }
    }

}
