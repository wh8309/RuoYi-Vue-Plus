package com.ruoyi.website.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.website.domain.ClientRecordDatum;
import com.ruoyi.website.domain.vo.response.RecordDatumResponseVO;
import com.ruoyi.website.mapper.ClientRecordDatumMapper;
import com.ruoyi.website.service.IClientRecordDatumService;
import com.ruoyi.website.service.ISysFileInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 评审申报材料Service业务层处理
 *
 * @author ruoyi
 * @date 2021-03-18
 */
@Service
public class ClientRecordDatumServiceImpl extends ServiceImpl<ClientRecordDatumMapper, ClientRecordDatum> implements IClientRecordDatumService {
    @Autowired
    private ISysFileInfoService sysFileInfoService;

    @Override
    public List<RecordDatumResponseVO> selectRecordDatumMenu(Long clientId) {
        LoginUser logUser = SecurityUtils.getLoginUser();
        SysUser sysUser = logUser.getUser();
        if (clientId == null) clientId = sysUser.getUserId();
        List<RecordDatumResponseVO> recordDatumResponseVOList = new ArrayList<RecordDatumResponseVO>();
        RecordDatumResponseVO recordDatumResponse1 = new RecordDatumResponseVO("各类表格,证明", "/fillInTableProve");
        recordDatumResponseVOList.add(recordDatumResponse1);
        RecordDatumResponseVO recordDatumResponseVO10 = new RecordDatumResponseVO("专业论文,论著(如被检索收录,请提交检索照片)", "/fillInThesis");
        recordDatumResponseVOList.add(recordDatumResponseVO10);
        RecordDatumResponseVO recordDatumResponseVO11 = new RecordDatumResponseVO("反映个人专业工作业绩的材料", "/fillInPerformanceMaterials");
        recordDatumResponseVOList.add(recordDatumResponseVO11);
        RecordDatumResponseVO recordDatumResponseVO12 = new RecordDatumResponseVO("任现职以来获得的专业奖励证书", "/fillInAwardCertificate");
        recordDatumResponseVOList.add(recordDatumResponseVO12);
        RecordDatumResponseVO recordDatumResponseVO13 = new RecordDatumResponseVO("任现职以来获得的其他奖励证书", "/fillInRestAward");
        recordDatumResponseVOList.add(recordDatumResponseVO13);
        RecordDatumResponseVO recordDatumResponseVO14 = new RecordDatumResponseVO("任现职以来参加继续教育培训证书", "/fillInEducational");
        recordDatumResponseVOList.add(recordDatumResponseVO14);
        RecordDatumResponseVO recordDatumResponseVO15 = new RecordDatumResponseVO("年度考核材料", "/fillInExamineMaterials");
        recordDatumResponseVOList.add(recordDatumResponseVO15);
//        RecordDatumResponseVO recordDatumResponseVO16 = new RecordDatumResponseVO("预览", "/previewed");
//        recordDatumResponseVOList.add(recordDatumResponseVO16);
        return recordDatumResponseVOList;
    }
}


