package com.ruoyi.website.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.Page;
import com.ruoyi.common.enums.ClientReportStatus;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.service.ISysDictDataService;
import com.ruoyi.website.domain.ClientBaseInfo;
import com.ruoyi.website.domain.ClientReportProcess;
import com.ruoyi.website.domain.vo.request.ClientReportProcessSubmitRequest;
import com.ruoyi.website.domain.vo.response.BaseInfoResponseVO;
import com.ruoyi.website.domain.vo.response.ClientBaseInfoReportResponseVO;
import com.ruoyi.website.domain.vo.response.ClientReportProcessResponseVO;
import com.ruoyi.website.domain.vo.response.ReviewNoticeResponse;
import com.ruoyi.website.mapper.ClientReportProcessMapper;
import com.ruoyi.website.service.IClientBaseInfoService;
import com.ruoyi.website.service.IClientReportProcessService;
import com.ruoyi.website.service.ISysFileInfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 申报流程处理(流转意见)Service业务层处理
 *
 * @author ruoyi
 * @date 2021-04-29
 */
@Service
public class ClientReportProcessServiceImpl extends ServiceImpl<ClientReportProcessMapper, ClientReportProcess> implements IClientReportProcessService {

    @Autowired
    private IClientBaseInfoService clientBaseInfoService;
    @Autowired
    private ISysFileInfoService sysFileInfoService;
    @Autowired
    private ISysDictDataService sysDictDataService;

    @Override
    public List<ClientReportProcess> queryList(ClientReportProcess clientReportProcess) {
        LambdaQueryWrapper<ClientReportProcess> lqw = Wrappers.lambdaQuery();
        if (clientReportProcess.getClientId() != null){
            lqw.eq(ClientReportProcess::getClientId ,clientReportProcess.getClientId());
        }
        if (clientReportProcess.getReportStatus() != null){
            lqw.eq(ClientReportProcess::getReportStatus ,clientReportProcess.getReportStatus());
        }
        if (clientReportProcess.getTransactorUserId() != null){
            lqw.eq(ClientReportProcess::getTransactorUserId ,clientReportProcess.getTransactorUserId());
        }
        if (StringUtils.isNotBlank(clientReportProcess.getTransactorSuggestion())){
            lqw.eq(ClientReportProcess::getTransactorSuggestion ,clientReportProcess.getTransactorSuggestion());
        }
        return this.list(lqw);
    }

    /**
     * 1.查询基本情况中的工作单位是否填写，填写完成，即可认为已提交
     * 2.查询 照片文件是否存在，如果存在则为已提交
     * 3.查询是否有数据：学历/查询年度考核与继续教育/从事专业技术工作简历/任职期间奖励情况/任职期内科研(业绩)成果/任期内发表论文论著情况/任期内专业技术与成果报告
     * 4.根据基础信息判断是否查询有数据：破格条件说明/转评条件说明/资格确认/转换系列
     * 5.根据必填项查询该类型的图片文件是否存在
     *
     * @param clientId
     * @return
     */
    @Override
    public List<String> checkSubmitCondition(Long clientId) {
        clientId = clientId == null ? SecurityUtils.getLoginUserId() : clientId;
        ArrayList<String> resultList = new ArrayList<String>();
        BaseInfoResponseVO clientBaseInfo = clientBaseInfoService.queryBaseById(clientId);
        String postPrefixName = "";
        if (clientBaseInfo != null && com.ruoyi.common.utils.StringUtils.isNotBlank(clientBaseInfo.getNowPostName())) {
            String nowPostName = clientBaseInfo.getNowPostName();
            if (nowPostName.indexOf("【") != -1) {
                postPrefixName = nowPostName.substring(0, nowPostName.indexOf("【"));
            }
        }
        String condConfirm = "2";//资格确认 1 是 2 否
        if (clientBaseInfo != null && com.ruoyi.common.utils.StringUtils.isNotBlank(clientBaseInfo.getCondConfirm())) {
            condConfirm = clientBaseInfo.getCondConfirm();
        }
        int count = 0;
        if (clientBaseInfo != null) {
            if (com.ruoyi.common.utils.StringUtils.isEmpty(clientBaseInfo.getJobUnit())) resultList.add("【基本情况】尚未填报！");
        }
        count = sysFileInfoService.selectFileCount(clientId, "1");
        if (count == 0) resultList.add("【照片】尚未上传！");
        count = clientBaseInfoService.queryTableCount("client_education", clientId);
        if (count == 0) resultList.add("【学历信息】尚未填报！");
        count = clientBaseInfoService.queryTableCount("client_edu_assessment", clientId);
        if (count == 0) resultList.add("【年度考核&继续教育】尚未填报！");
        count = clientBaseInfoService.queryTableCount("client_resume", clientId);
        if (count == 0) resultList.add("【从事专业技术工作简历】尚未填报！");
        count = clientBaseInfoService.queryTableCount("client_skill_award", clientId);
        if (count == 0) resultList.add("【任职期间奖励情况】尚未填报！");
        count = clientBaseInfoService.queryTableCount("client_skill_research", clientId);
        if (count == 0) resultList.add("【任职期内科研(业绩)成果】尚未填报！");
        count = clientBaseInfoService.queryTableCount("client_skill_published_paper", clientId);
        if (count == 0) resultList.add("【任期内发表论文论著情况】尚未填报！");
        count = clientBaseInfoService.queryTableCount("client_skill_summary", clientId);
        if (count == 0) resultList.add("【任期内专业技术与成果报告】尚未填报！");
        if (clientBaseInfo != null) {
            int showFlag15 = (com.ruoyi.common.utils.StringUtils.isNotEmpty(clientBaseInfo.getIsBreakRule()) && "1".equals(clientBaseInfo.getIsBreakRule())) ? 1 : 0;
            count = clientBaseInfoService.queryTableCount("client_break_rule", clientId);
            if (showFlag15 == 1 && count == 0) resultList.add("【破格条件说明】尚未填报！");
            int showFlag_10_12 = (com.ruoyi.common.utils.StringUtils.isNotEmpty(clientBaseInfo.getReferralType()) && "1".equals(clientBaseInfo.getReferralType())) ? 1 : 0;
            count = clientBaseInfoService.queryTableCount("client_referral_type", clientId);
            if (showFlag_10_12 == 1 && count == 0) resultList.add("【转评条件说明】尚未填报！");
            int showFlag11 = (com.ruoyi.common.utils.StringUtils.isNotEmpty(clientBaseInfo.getCondConfirm()) && "1".equals(clientBaseInfo.getCondConfirm())) ? 1 : 0;
            count = clientBaseInfoService.queryTableCount("client_qualification_confirm", clientId);
            if (showFlag11 == 1 && count == 0) resultList.add("【资格确认】尚未填报！");
            count = clientBaseInfoService.queryTableCount("client_exchange_faculty", clientId);
            if (showFlag_10_12 == 1 && count == 0) resultList.add("【转换系列】尚未填报！");
        }
        //拼接证券电子图片
        StringBuilder sb = new StringBuilder("【证件电子图片】- ");
        List<String> listStr = new ArrayList<>();
        count = sysFileInfoService.selectFileCount(clientId, "10");
        if (count == 0) listStr.add("身份证尚未上传");
        count = sysFileInfoService.selectFileCount(clientId, "11");
        if (count == 0) listStr.add("申报学历证图片尚未上传");
        if(StringUtils.isNotBlank(postPrefixName)) {
            count = sysFileInfoService.selectFileCount(clientId, "15");
            if (count == 0) listStr.add(postPrefixName + "职称证书尚未上传");
        }
        if ("1".equals(condConfirm)) {//资格确认
            count = sysFileInfoService.selectFileCount(clientId, "18");
            if (count == 0) listStr.add("职称资格确认证明材料尚未上传");
        }
        sb.append(String.join(";", listStr)).append("!");
        if (listStr.size() > 0) resultList.add(sb.toString());
        count = sysFileInfoService.selectFileCount(clientId, "50");
        if (count == 0) resultList.add("【评审申报材料】-《申报专业技术任职资格诚信承诺书》尚未上传！");
        return resultList;
    }

    @Override
    public ClientBaseInfoReportResponseVO queryBaseInfoByClientId(Long clientId) {
        clientId = clientId == null ? SecurityUtils.getLoginUserId() : clientId;
        ClientBaseInfo clientBaseInfo = clientBaseInfoService.queryBaseInfoById(clientId);
        ClientBaseInfoReportResponseVO responseVO = new ClientBaseInfoReportResponseVO();
        if (clientBaseInfo == null) {
            responseVO.setReportStatus(ClientReportStatus.FILLING.getValue());
            responseVO.setIsEdit(1);//
            return responseVO;
        }
        responseVO.setClientId(clientId);
        responseVO.setReportStatus(clientBaseInfo.getReportStatus());
        responseVO.setName(clientBaseInfo.getName());
        responseVO.setNominateUnit(clientBaseInfo.getNominateUnit());
        responseVO.setJudgesName(clientBaseInfo.getJudgesName());
        responseVO.setReportYear(clientBaseInfo.getReportYear());

        //显示破格按钮
        if (com.ruoyi.common.utils.StringUtils.isEmpty(clientBaseInfo.getIsBreakRule())) {
            responseVO.setIsBreakRule("0");
        } else {
            if ("0".equals(clientBaseInfo.getIsBreakRule())) {
                responseVO.setIsBreakRule("0");
            } else {
                responseVO.setIsBreakRule("1");
            }
        }
        //是否显示转评条件
        if (com.ruoyi.common.utils.StringUtils.isEmpty(clientBaseInfo.getReferralType())) {
            responseVO.setReferralType("0");
        } else {
            if ("0".equals(clientBaseInfo.getIsBreakRule())) {
                responseVO.setIsBreakRule("0");
            } else {
                responseVO.setReferralType("1");
            }
        }

        String reportStatusName = sysDictDataService.selectDictLabel("client_report_status", String.valueOf(clientBaseInfo.getReportStatus()));
        responseVO.setReportStatusName(reportStatusName);

        if (com.ruoyi.common.utils.StringUtils.isNotEmpty(clientBaseInfo.getBelongSeries())) {
            ReviewNoticeResponse reviewNoticeResponse = clientBaseInfoService.queryReviewNoticeByTreeId(Long.valueOf(clientBaseInfo.getBelongSeries()));
            if (reviewNoticeResponse != null) {
                responseVO.setBelongSeries(reviewNoticeResponse.getBelongSeries());
            }
        }
        if (com.ruoyi.common.utils.StringUtils.isNotEmpty(clientBaseInfo.getSpecialtyName())) {
            ReviewNoticeResponse reviewNoticeResponse = clientBaseInfoService.queryReviewNoticeByTreeId(Long.valueOf(clientBaseInfo.getSpecialtyName()));
            if (reviewNoticeResponse != null) {
                responseVO.setSpecialtyName(reviewNoticeResponse.getTreeName());
            }
        }
        if (com.ruoyi.common.utils.StringUtils.isNotEmpty(clientBaseInfo.getDeclarePost())) {
            String dictLabel = sysDictDataService.selectDictLabel("tors_declare_name", clientBaseInfo.getDeclarePost());
            responseVO.setDeclarePost(dictLabel);
        }
        // 待审核:10  审核通过：20 申报中:30 申报成功:100 不可以编辑
        if (null != clientBaseInfo.getReportStatus() && clientBaseInfo.getReportStatus().intValue() > 0) {
            responseVO.setIsEdit(0);
        } else {
            //申报异常:-100  审核驳回:-20  暂存:0 可以编辑
            responseVO.setIsEdit(1);
        }
        return responseVO;
    }

    @Override
    public List<ClientReportProcessResponseVO> queryListByClientId(Long clientId) {
        clientId = clientId == null ? SecurityUtils.getLoginUserId() : clientId;
        LambdaQueryWrapper<ClientReportProcess> lqw = Wrappers.lambdaQuery();
        lqw.eq(ClientReportProcess::getClientId, clientId);
        lqw.orderByDesc(ClientReportProcess::getCreateTime);
        return entity2Vo(baseMapper.selectList(lqw));
    }

    @Override
    @Transactional(
            rollbackFor = {Exception.class}
    )
    public int processSubmit(ClientReportProcessSubmitRequest clientReportProcessSubmitRequest) {
        if(clientReportProcessSubmitRequest.getClientId()==null){
            clientReportProcessSubmitRequest.setClientId(SecurityUtils.getLoginUserId());
        }
        Long orderNo=100l;//排序字段 默认100 已分配-80 待审核-60 审核通过-40 复核通过-20
        if(clientReportProcessSubmitRequest.getReportStatus()<=0){
            orderNo=100l;
        }else if(clientReportProcessSubmitRequest.getReportStatus()==10){
            orderNo=60l;
        }else if(clientReportProcessSubmitRequest.getReportStatus()==20){
            orderNo=40l;
        }else if(clientReportProcessSubmitRequest.getReportStatus()==30){
            orderNo=20l;
        }
        clientBaseInfoService.updateStatusByClientId(clientReportProcessSubmitRequest.getClientId(), clientReportProcessSubmitRequest.getReportStatus(),orderNo);
        ClientReportProcess clientReportProcess=new ClientReportProcess();
        BeanUtils.copyProperties(clientReportProcessSubmitRequest,clientReportProcess);
        clientReportProcess.setTransactorUserId(SecurityUtils.getLoginUserId());
        clientReportProcess.setCreateBy(SecurityUtils.getUsername());
        clientReportProcess.setCreateTime(new Date());
        return baseMapper.insert(clientReportProcess);
    }

    /**
     * 实体类转化成视图对象
     *
     * @param collection 实体类集合
     * @return
     */
    private List<ClientReportProcessResponseVO> entity2Vo(Collection<ClientReportProcess> collection) {
        List<ClientReportProcessResponseVO> voList = collection.stream()
                .map(any -> BeanUtil.toBean(any, ClientReportProcessResponseVO.class))
                .collect(Collectors.toList());
        if (collection instanceof Page) {
            Page<ClientReportProcess> page = (Page<ClientReportProcess>)collection;
            Page<ClientReportProcessResponseVO> pageVo = new Page<>();
            BeanUtil.copyProperties(page,pageVo);
            pageVo.addAll(voList);
            voList = pageVo;
        }
        return voList;
    }

}
