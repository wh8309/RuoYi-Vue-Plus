package com.ruoyi.website.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.website.domain.ClientSkillResearch;
import com.ruoyi.website.domain.vo.request.SkillResearchQueryRequestVO;
import com.ruoyi.website.domain.vo.request.SkillResearchRequestVO;
import com.ruoyi.website.domain.vo.response.SkillResearchResponseVO;

import java.util.List;

/**
 * 任期内科研成果Service接口
 *
 * @author ruoyi
 * @date 2021-03-18
 */
public interface IClientSkillResearchService extends IService<ClientSkillResearch> {

    /**
     * 查询列表
     */
    public List<SkillResearchResponseVO> queryList(SkillResearchQueryRequestVO skillResearchQueryRequestVO);

    public int insertOrUpdate(SkillResearchRequestVO skillResearchRequestVO);

}
