package com.ruoyi.website.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.website.domain.ClientResume;
import com.ruoyi.website.domain.vo.request.ClientResumeRequest;
import com.ruoyi.website.domain.vo.response.ClientResumeResponse;

/**
 * 从事专业技术工作简历Service接口
 *
 * @author ruoyi
 * @date 2021-03-22
 */
public interface IClientResumeService extends IService<ClientResume> {


    ClientResumeResponse queryDetailById(Long clientId);

    int insertOrUpdate(ClientResumeRequest clientResumeRequest);
}
