package com.ruoyi.website.service;

import com.ruoyi.website.domain.ClientRobotReportTag;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * 机器人上报tagService接口
 *
 * @author ruoyi
 * @date 2022-04-14
 */
public interface IClientRobotReportTagService extends IService<ClientRobotReportTag> {

    /**
     * 查询列表
     */
    List<ClientRobotReportTag> queryList(ClientRobotReportTag clientRobotReportTag);


    //重置某个上报标识
    void resetReportTag(Long clientId,String tag);

    //重置某个文件类型
    void resetReportTag(Long clientId,Integer fileType);

}
