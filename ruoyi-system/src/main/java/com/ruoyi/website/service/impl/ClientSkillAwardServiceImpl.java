package com.ruoyi.website.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.website.domain.ClientSkillAward;
import com.ruoyi.website.domain.vo.request.ClientSkillAwardRequest;
import com.ruoyi.website.domain.vo.response.ClientSkillAwardResponse;
import com.ruoyi.website.mapper.ClientSkillAwardMapper;
import com.ruoyi.website.service.IClientSkillAwardService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 任职期间奖励情况Service业务层处理
 *
 * @author ruoyi
 * @date 2021-03-24
 */
@Service
public class ClientSkillAwardServiceImpl extends ServiceImpl<ClientSkillAwardMapper, ClientSkillAward> implements IClientSkillAwardService {

    @Resource
    private ClientSkillAwardMapper clientSkillAwardMapper;

    @Override
    public ClientSkillAwardResponse queryDetailById(Long clientId) {
        LoginUser logUser = SecurityUtils.getLoginUser();
        SysUser sysUser = logUser.getUser();
        if (clientId == null) clientId = sysUser.getUserId();
        ClientSkillAward clientSkillAward = clientSkillAwardMapper.selectById(clientId);
        ClientSkillAwardResponse skillAwardResponse = new ClientSkillAwardResponse();
        if (null != clientSkillAward) BeanUtils.copyProperties(clientSkillAward, skillAwardResponse);
        return skillAwardResponse;
    }

    @Override
    public int insertOrUpdate(ClientSkillAwardRequest clientSkillAwardRequest) {

        Long clientId = clientSkillAwardRequest.getClientId();
        if (clientId == null) {
            LoginUser logUser = SecurityUtils.getLoginUser();
            SysUser sysUser = logUser.getUser();
            clientId = sysUser.getUserId();
        }
        ClientSkillAward clientSkillAward = clientSkillAwardMapper.selectById(clientId);
        if (clientSkillAward == null) {
            clientSkillAward = new ClientSkillAward();
            BeanUtils.copyProperties(clientSkillAwardRequest, clientSkillAward);
            clientSkillAward.setClientId(clientId);
            clientSkillAward.setCreateBy(SecurityUtils.getUsername());
            clientSkillAward.setCreateTime(new Date());
            return clientSkillAwardMapper.insert(clientSkillAward);
        } else {
            BeanUtils.copyProperties(clientSkillAwardRequest, clientSkillAward);
            clientSkillAward.setClientId(clientId);
            clientSkillAward.setUpdateBy(SecurityUtils.getUsername());
            clientSkillAward.setUpdateTime(new Date());
            return clientSkillAwardMapper.updateById(clientSkillAward);
        }

    }
}
