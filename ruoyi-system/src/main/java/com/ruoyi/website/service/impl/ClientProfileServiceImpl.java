package com.ruoyi.website.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.idcard.IdCardUtils;
import com.ruoyi.system.service.ISysConfigService;
import com.ruoyi.website.domain.ClientBaseInfo;
import com.ruoyi.website.domain.ClientProfile;
import com.ruoyi.website.domain.vo.request.ClientProfileRequestVO;
import com.ruoyi.website.domain.vo.request.ClientProfileStatusUpdateRequestVO;
import com.ruoyi.website.domain.vo.response.ClientProfileExcelExportResponseVO;
import com.ruoyi.website.domain.vo.response.ClientProfileExcelImportResponseVO;
import com.ruoyi.website.domain.vo.response.ClientProfileResponseVO;
import com.ruoyi.website.mapper.ClientProfileMapper;
import com.ruoyi.website.service.IClientBaseInfoService;
import com.ruoyi.website.service.IClientProfileService;
import com.ruoyi.website.service.IClientRobotReportTagService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 用户信息Service业务层处理
 *
 * @author ruoyi
 * @date 2021-04-01
 */
@Service
public class ClientProfileServiceImpl extends ServiceImpl<ClientProfileMapper, ClientProfile> implements IClientProfileService {

    @Autowired
    private ClientProfileMapper clientProfileMapper;

    @Autowired
    private  IClientBaseInfoService clientBaseInfoService;

    @Autowired
    private ISysConfigService configService;

    @Autowired
    private IClientRobotReportTagService clientRobotReportTagService;

    @Override
    @DataScope(deptAlias = "d", userAlias = "c", userType = "client_id")
    public List<ClientProfileResponseVO> queryListByParam(ClientProfile clientProfile) {
        List<ClientProfile> clientProfileList = clientProfileMapper.selectClientProfileList(clientProfile);
        return clientProfileList.stream().map(e -> {
            ClientProfileResponseVO responseVO = new ClientProfileResponseVO();
            BeanUtils.copyProperties(e, responseVO);
            return responseVO;
        }).collect(Collectors.toList());
    }


/*    @Override
    @DataScope(deptAlias = "d", userAlias = "u", userType = "client_id")
    public List<ClientProfile> selectClientProfileList(ClientProfile clientProfile) {
        return clientProfileMapper.selectClientProfileList(clientProfile);
    }*/

    @Override
    public ClientProfileResponseVO queryDetailById(Long clientId) {
        ClientProfile clientProfile = clientProfileMapper.selectById(clientId);
        ClientProfileResponseVO responseVO = new ClientProfileResponseVO();
        BeanUtils.copyProperties(clientProfile, responseVO);
        return responseVO;
    }

    /**
     * @param profileRequest
     * @return
     */
    @Override
    public int insertOrUpdate(ClientProfileRequestVO profileRequest) {
        Long clientId = profileRequest.getClientId();
        ClientProfile profile = clientId != null ? clientProfileMapper.selectById(clientId) : null;
        int count;
        if (profile == null) {
            profile = new ClientProfile();
            BeanUtils.copyProperties(profileRequest, profile);
            profile.setIsDeleted(1);
            profile.setStatus("0");
            profile.setCreateBy(SecurityUtils.getUsername());
            profile.setCreateTime(new Date());
            count = clientProfileMapper.insert(profile);
        } else {
            BeanUtils.copyProperties(profileRequest, profile);
            profile.setClientId(clientId);
            profile.setUpdateBy(SecurityUtils.getUsername());
            profile.setUpdateTime(new Date());
            count = clientProfileMapper.updateById(profile);
        }
        //添加用户的基本信息
        ClientBaseInfo clientBase = new ClientBaseInfo();
        clientBase.setClientId(profile.getClientId());
        clientBase.setDeptId(profileRequest.getDeptId());
        clientBase.setName(profileRequest.getName());
        clientBase.setIdcard(profileRequest.getIdcard());
        clientBase.setSex(profileRequest.getSex());
        clientBase.setMobile(profileRequest.getMobile());
        clientBaseInfoService.insertOrUpdateBase(clientBase);
        clientRobotReportTagService.resetReportTag(clientBase.getClientId(),"menu_jbxx");//重置上报Tag
        return count;
    }

    @Override
    public String checkMobileUnique(ClientProfile clientProfile) {
        Long clientId = com.ruoyi.common.utils.StringUtils.isNull(clientProfile.getClientId()) ? -1L : clientProfile.getClientId();
        ClientProfile clientProfileInfo = clientProfileMapper.checkMobileUnique(clientProfile.getMobile());
        if (com.ruoyi.common.utils.StringUtils.isNotNull(clientProfileInfo) && clientProfileInfo.getClientId().longValue() != clientId.longValue()) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    @Override
    public String checkMobileUniqueContainsThat(ClientProfile clientProfile) {
        Long clientId = com.ruoyi.common.utils.StringUtils.isNull(clientProfile.getClientId()) ? -1L : clientProfile.getClientId();
        ClientProfile clientProfileInfo = clientProfileMapper.checkMobileUniqueUnContainsThat(clientId, clientProfile.getMobile());
        if (com.ruoyi.common.utils.StringUtils.isNotNull(clientProfileInfo) && clientProfileInfo.getClientId().longValue() != clientId.longValue()) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }


    @Override
    public String checkIdcardUnique(ClientProfile clientProfile) {
        Long clientId = com.ruoyi.common.utils.StringUtils.isNull(clientProfile.getClientId()) ? -1L : clientProfile.getClientId();
        ClientProfile clientProfileInfo = clientProfileMapper.checkIdcardUnique(clientProfile.getIdcard());
        if (com.ruoyi.common.utils.StringUtils.isNotNull(clientProfileInfo) && clientProfileInfo.getClientId().longValue() != clientId.longValue()) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    @Override
    public String checkIdcardUniqueUnContainsThat(ClientProfile clientProfile) {
        Long clientId = com.ruoyi.common.utils.StringUtils.isNull(clientProfile.getClientId()) ? -1L : clientProfile.getClientId();
        ClientProfile clientProfileInfo = clientProfileMapper.checkIdcardUniqueUnContainsThat(clientProfile.getClientId(), clientProfile.getIdcard());
        if (com.ruoyi.common.utils.StringUtils.isNotNull(clientProfileInfo) && clientProfileInfo.getClientId().longValue() != clientId.longValue()) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }


    @Override
    public int insertOrUpdate(ClientProfile clientProfile) {
        String clientInitPassword = configService.selectConfigByKey("tors.client.initPassword");//客户初始密码
        int count = 0;
        if (clientProfile.getClientId() == null) {
            clientProfile.setLoginName(clientProfile.getIdcard());//登录名:身份证号码
            clientProfile.setPassword(SecurityUtils.encryptPassword(clientInitPassword));
            clientProfile.setCreateBy(SecurityUtils.getUsername());
            clientProfile.setCreateTime(new Date());
            count = clientProfileMapper.insert(clientProfile);
        } else {
            ClientProfile updateClientProfile = clientProfileMapper.selectById(clientProfile.getClientId());
            BeanUtils.copyProperties(clientProfile, updateClientProfile);
            clientProfile.setPassword(updateClientProfile.getPassword());
            clientProfile.setUpdateBy(SecurityUtils.getUsername());
            clientProfile.setUpdateTime(new Date());
            count = clientProfileMapper.updateById(clientProfile);
        }
        Map<String, String> idCardMap = IdCardUtils.getBirthdayAgeSex(clientProfile.getIdcard());
        ClientBaseInfo clientBaseInfo = new ClientBaseInfo();
        clientBaseInfo.setClientId(clientProfile.getClientId());
        clientBaseInfo.setDeptId(clientProfile.getDeptId());
        clientBaseInfo.setName(clientProfile.getName());
        clientBaseInfo.setIdcard(clientProfile.getIdcard());
        clientBaseInfo.setSex(clientProfile.getSex());
        clientBaseInfo.setMobile(clientProfile.getMobile());
        clientBaseInfo.setBirthday(idCardMap.get("birthday"));
        clientBaseInfoService.insertOrUpdateBase(clientBaseInfo);
        clientRobotReportTagService.resetReportTag(clientBaseInfo.getClientId(),"menu_jbxx");//重置上报Tag
        return count;
    }

    @Override
    public ClientProfile selectByLoginName(String loginName) {
        QueryWrapper<ClientProfile> queryWrapper = new QueryWrapper<ClientProfile>();
        queryWrapper.eq("login_name", loginName);
        return clientProfileMapper.selectOne(queryWrapper);
    }

    public ClientProfile selectByIdCard(String idCard) {
        QueryWrapper<ClientProfile> queryWrapper = new QueryWrapper<ClientProfile>();
        queryWrapper.eq("idcard", idCard);
        return clientProfileMapper.selectOne(queryWrapper);
    }

    @Override
    @Transactional
    public int deleteClientProfileByClientIds(Long[] clientIds) {
        clientBaseInfoService.fakeDeleteBaseByClientIdArr(clientIds);
        return clientProfileMapper.deleteClientProfileByClientIds(clientIds);
    }

    @Override
    public void checkUserAllowed(ClientProfile clientProfile) {
        if (StringUtils.isNull(clientProfile.getClientId())) {
            throw new CustomException("客户信息ID为空，不允许操作该客户数据");
        }
    }

    @Override
    public int resetPwd(Long clientId) {
        clientId = clientId == null ? SecurityUtils.getLoginUserId() : clientId;
        ClientProfile clientProfile = clientProfileMapper.selectById(clientId);
        if (clientProfile == null) {
            throw new CustomException("未查询到clientId为" + clientId + "的用户信息，请联系管理员！");
        }
        String clientInitPassword = configService.selectConfigByKey("tors.client.initPassword");//客户初始密码
        clientProfile.setPassword(SecurityUtils.encryptPassword(clientInitPassword));
        clientProfile.setUpdateBy(SecurityUtils.getUsername());
        clientProfile.setUpdateTime(new Date());
        return clientProfileMapper.updateById(clientProfile);
    }


    @Override
    public int updateUserStatus(ClientProfileStatusUpdateRequestVO requestVO) {
        if (requestVO.getClientId() == null) {
            throw new CustomException("客户信息ID为空，不允许操作该客户数据");
        }
        if (requestVO.getStatus() == null) {
            throw new CustomException("用户装态为空，不允许修改该客户状态");
        }
        ClientProfile clientProfile = clientProfileMapper.selectById(requestVO.getClientId());
        if (clientProfile == null) {
            throw new CustomException("该ID对应的用户不存在！");
        }
        clientProfile.setStatus(requestVO.getStatus().toString());
        clientProfile.setUpdateBy(SecurityUtils.getUsername());
        clientProfile.setUpdateTime(new Date());
        return clientProfileMapper.updateById(clientProfile);
    }

    @Override
    public void updateLogin(String username, String ipAddr) {
        ClientProfile clientProfile = selectByLoginName(username);
        clientProfile.setLoginDate(new Date());
        clientProfile.setLoginIp(ipAddr);
        clientProfile.setUpdateBy(username);
        clientProfile.setUpdateTime(new Date());
        clientProfileMapper.updateById(clientProfile);
    }

    @Override
    public int resetUserPwd(String userName, String password) {
        return clientProfileMapper.resetUserPwd(userName, password);
    }

    @Override
    public String importUser(List<ClientProfileExcelImportResponseVO> excelData, boolean isUpdateSupport, String operName) {
        if (StringUtils.isNull(excelData) || excelData.size() == 0) {
            throw new CustomException("导入用户数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (ClientProfileExcelImportResponseVO responseData : excelData) {
            try {
                // 验证是否存在这个用户
                ClientProfile clientProfile = selectByIdCard(responseData.getIdcard());
                if (StringUtils.isNull(clientProfile)) {
                    clientProfile = new ClientProfile();
                    BeanUtils.copyProperties(responseData, clientProfile);
                    clientProfile.setLoginName(responseData.getIdcard());
                    String clientInitPassword = configService.selectConfigByKey("tors.client.initPassword");//客户初始密码
                    clientProfile.setPassword(SecurityUtils.encryptPassword(clientInitPassword));
                    clientProfile.setCreateBy(operName);
                    clientProfileMapper.insert(clientProfile);
                    ClientBaseInfo clientBase = new ClientBaseInfo();
                    clientBase.setClientId(clientProfile.getClientId());
                    clientBase.setDeptId(clientProfile.getDeptId());
                    clientBase.setName(clientProfile.getName());
                    clientBase.setIdcard(clientProfile.getIdcard());
                    clientBase.setSex(clientProfile.getSex());
                    clientBase.setMobile(clientProfile.getMobile());
                    clientBase.setReportStatus(0);
                    clientBase.setIsDeleted(1);
                    clientBaseInfoService.insertOrUpdateBase(clientBase);
                    clientRobotReportTagService.resetReportTag(clientBase.getClientId(),"menu_jbxx");//重置上报Tag
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、账号 " + clientProfile.getName() + " 导入成功");
                } else if (isUpdateSupport) {
                    BeanUtils.copyProperties(responseData, clientProfile);
                    clientProfile.setUpdateBy(operName);
                    clientProfileMapper.updateById(clientProfile);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、账号 " + clientProfile.getName() + " 更新成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、账号 " + responseData.getName() + " 已存在");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、账号 " + responseData.getName() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new CustomException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();

    }


    @Override
    public List<ClientProfileExcelExportResponseVO> exportListByParam(ClientProfile clientProfile) {
        List<ClientProfile> clientProfileList = clientProfileMapper.selectClientProfileList(clientProfile);
        return clientProfileList.stream().map(e -> {
            ClientProfileExcelExportResponseVO responseVO = new ClientProfileExcelExportResponseVO();
            BeanUtils.copyProperties(e, responseVO);
            return responseVO;
        }).collect(Collectors.toList());
    }


}
