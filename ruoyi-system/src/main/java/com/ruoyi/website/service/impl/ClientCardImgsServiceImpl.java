package com.ruoyi.website.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.website.domain.ClientBaseInfo;
import com.ruoyi.website.domain.ClientCardImgs;
import com.ruoyi.website.domain.vo.response.CardImgsMenuResponseVO;
import com.ruoyi.website.mapper.ClientCardImgsMapper;
import com.ruoyi.website.service.IClientBaseInfoService;
import com.ruoyi.website.service.IClientCardImgsService;
import com.ruoyi.website.service.ISysFileInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 证件电子图片Service业务层处理
 *
 * @author ruoyi
 * @date 2021-03-18
 */
@Service
public class ClientCardImgsServiceImpl extends ServiceImpl<ClientCardImgsMapper, ClientCardImgs> implements IClientCardImgsService {

    @Autowired
    private ISysFileInfoService sysFileInfoService;
    @Autowired
    private IClientBaseInfoService clientBaseInfoService;

    public List<CardImgsMenuResponseVO>  selectCardImgsMenu(Long clientId){
        LoginUser logUser = SecurityUtils.getLoginUser();
        SysUser sysUser = logUser.getUser();
        if(clientId==null){
            clientId=sysUser.getUserId();
        }
        ClientBaseInfo clientBaseInfo=clientBaseInfoService.getById(clientId);
        String postPrefixName="";
        if(clientBaseInfo!=null&& StringUtils.isNotBlank(clientBaseInfo.getNowPostName()) ){
            String nowPostName=clientBaseInfo.getNowPostName();
            if(nowPostName.indexOf("【")!=-1){
                postPrefixName=nowPostName.substring(0,nowPostName.indexOf("【"));
            }
        }
        String condConfirm="2";//资格确认 1 是 2 否
        if(clientBaseInfo!=null && StringUtils.isNotBlank(clientBaseInfo.getCondConfirm()) ){
            condConfirm=clientBaseInfo.getCondConfirm();
        }

        List<CardImgsMenuResponseVO> cardImgsMenuResponseVOList=new ArrayList<CardImgsMenuResponseVO>();
        CardImgsMenuResponseVO cardImgsMenuResponseVOAll=new CardImgsMenuResponseVO("ALLCardImgs", "预览全部上传图片", 0, 0,"/fillUploadAll");
        cardImgsMenuResponseVOList.add(cardImgsMenuResponseVOAll);
        CardImgsMenuResponseVO cardImgsMenuResponseVO10=new CardImgsMenuResponseVO("10", "身份证（正、反面2张）", 1, sysFileInfoService.selectFileCount(clientId,"10")>0?1:0,"/fillInIdentity");
        cardImgsMenuResponseVOList.add(cardImgsMenuResponseVO10);
        CardImgsMenuResponseVO cardImgsMenuResponseVO11=new CardImgsMenuResponseVO("11", "申报学历证", 1, sysFileInfoService.selectFileCount(clientId,"11")>0?1:0,"/fillInDeclareEducation");
        cardImgsMenuResponseVOList.add(cardImgsMenuResponseVO11);
        CardImgsMenuResponseVO cardImgsMenuResponseVO12=new CardImgsMenuResponseVO("12", "申报学位证", 0, sysFileInfoService.selectFileCount(clientId,"12")>0?1:0,"/fillInCertificate");
        cardImgsMenuResponseVOList.add(cardImgsMenuResponseVO12);
        CardImgsMenuResponseVO cardImgsMenuResponseVO13=new CardImgsMenuResponseVO("13", "职称外语证书", 0, sysFileInfoService.selectFileCount(clientId,"13")>0?1:0,"/fillInForeign");
        cardImgsMenuResponseVOList.add(cardImgsMenuResponseVO13);
        CardImgsMenuResponseVO cardImgsMenuResponseVO14=new CardImgsMenuResponseVO("14", "职称计算机证书", 0, sysFileInfoService.selectFileCount(clientId,"14")>0?1:0,"/fillInComputers");
        cardImgsMenuResponseVOList.add(cardImgsMenuResponseVO14);
        if(StringUtils.isNotBlank(postPrefixName)) {
            CardImgsMenuResponseVO cardImgsMenuResponseVO15 = new CardImgsMenuResponseVO("15", postPrefixName + "职称证书", 1, sysFileInfoService.selectFileCount(clientId, "15") > 0 ? 1 : 0, "/fillInGreffier");
            cardImgsMenuResponseVOList.add(cardImgsMenuResponseVO15);
        }
        CardImgsMenuResponseVO cardImgsMenuResponseVO16=new CardImgsMenuResponseVO("16", "职(执)业资格证书", 0, sysFileInfoService.selectFileCount(clientId,"16")>0?1:0,"/fillInTeaching");
        cardImgsMenuResponseVOList.add(cardImgsMenuResponseVO16);
        CardImgsMenuResponseVO cardImgsMenuResponseVO17=new CardImgsMenuResponseVO("17", "水平能力测试合格证", 0, sysFileInfoService.selectFileCount(clientId,"17")>0?1:0,"/fillInStandard");
        cardImgsMenuResponseVOList.add(cardImgsMenuResponseVO17);
        if("1".equals(condConfirm)) {//资格确认
            CardImgsMenuResponseVO cardImgsMenuResponseVO18 = new CardImgsMenuResponseVO("18", "职称资格确认证明材料", 1, sysFileInfoService.selectFileCount(clientId, "18") > 0 ? 1 : 0, "/fillInCertificationTestify");
            cardImgsMenuResponseVOList.add(cardImgsMenuResponseVO18);
        }
        CardImgsMenuResponseVO cardImgsMenuResponseVO19=new CardImgsMenuResponseVO("19", "其他证明材料", 0, sysFileInfoService.selectFileCount(clientId,"19")>0?1:0,"/fillInRestType");
        cardImgsMenuResponseVOList.add(cardImgsMenuResponseVO19);
        return cardImgsMenuResponseVOList;
    }


}
