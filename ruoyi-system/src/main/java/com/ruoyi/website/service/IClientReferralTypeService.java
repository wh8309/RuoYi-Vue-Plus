package com.ruoyi.website.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.website.domain.ClientReferralType;
import com.ruoyi.website.domain.vo.request.ClientReferralTypeRequest;
import com.ruoyi.website.domain.vo.response.ClientReferralTypeResponse;


/**
 * 转评条件说明Service接口
 *
 * @author ruoyi
 * @date 2021-03-24
 */
public interface IClientReferralTypeService extends IService<ClientReferralType> {


    ClientReferralTypeResponse queryDetailById(Long clientId);

    int insertOrUpdate(ClientReferralTypeRequest referralTypeRequest);
}
