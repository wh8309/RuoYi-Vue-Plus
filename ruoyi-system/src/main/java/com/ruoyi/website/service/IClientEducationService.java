package com.ruoyi.website.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.website.domain.ClientEducation;
import com.ruoyi.website.domain.vo.request.ClientEducationRequestVO;
import com.ruoyi.website.domain.vo.response.ClientEducationResponseVO;

/**
 * 学历信息Service接口
 *
 * @author ruoyi
 * @date 2021-03-22
 */
public interface IClientEducationService extends IService<ClientEducation> {

    ClientEducationResponseVO queryDetailById(Long clientId);

    int insertOrUpdate(ClientEducationRequestVO clientEducationRequestVO);
}
