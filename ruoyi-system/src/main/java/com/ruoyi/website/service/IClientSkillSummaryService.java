package com.ruoyi.website.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.website.domain.ClientSkillSummary;
import com.ruoyi.website.domain.vo.request.ClientSkillSummaryRequest;
import com.ruoyi.website.domain.vo.response.ClientSkillSummaryResponse;

/**
 * 任期内专业技术业绩与成果报告Service接口
 *
 * @author ruoyi
 * @date 2021-03-24
 */
public interface IClientSkillSummaryService extends IService<ClientSkillSummary> {

    ClientSkillSummaryResponse queryDetailById(Long clientId);

    int insertOrUpdate(ClientSkillSummaryRequest clientSkillSummaryRequest);
}
