package com.ruoyi.website.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.website.domain.ClientExchangeFaculty;
import com.ruoyi.website.domain.vo.request.ClientExchangeFacultyRequest;
import com.ruoyi.website.domain.vo.response.ClientExchangeFacultyResponse;

import java.util.List;

/**
 * 转换系列Service接口
 *
 * @author ruoyi
 * @date 2021-03-24
 */
public interface IClientExchangeFacultyService extends IService<ClientExchangeFaculty> {

    ClientExchangeFacultyResponse queryDetailById(Long clientId);

    int insertOrUpdate(ClientExchangeFacultyRequest exchangeFacultyRequest);
}
