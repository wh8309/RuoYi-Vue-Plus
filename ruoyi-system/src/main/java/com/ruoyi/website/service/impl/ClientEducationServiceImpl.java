package com.ruoyi.website.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.website.domain.ClientEducation;
import com.ruoyi.website.domain.vo.request.ClientEducationRequestVO;
import com.ruoyi.website.domain.vo.response.ClientEducationResponseVO;
import com.ruoyi.website.mapper.ClientEducationMapper;
import com.ruoyi.website.service.IClientEducationService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 学历信息Service业务层处理
 *
 * @author ruoyi
 * @date 2021-03-22
 */
@Service
public class ClientEducationServiceImpl extends ServiceImpl<ClientEducationMapper, ClientEducation> implements IClientEducationService {

    @Resource
    private ClientEducationMapper clientEducationMapper;


    @Override
    public ClientEducationResponseVO queryDetailById(Long clientId) {
        LoginUser logUser = SecurityUtils.getLoginUser();
        SysUser sysUser = logUser.getUser();
        if (clientId == null) clientId = sysUser.getUserId();
        ClientEducation clientEducation = clientEducationMapper.selectById(clientId);
        ClientEducationResponseVO educationResponseVO = new ClientEducationResponseVO();
        if (null != clientEducation) BeanUtils.copyProperties(clientEducation, educationResponseVO);
        return educationResponseVO;
    }

    @Override
    public int insertOrUpdate(ClientEducationRequestVO clientEducationRequestVO) {
        Long clientId = clientEducationRequestVO.getClientId();
        if (clientId == null) {
            LoginUser logUser = SecurityUtils.getLoginUser();
            SysUser sysUser = logUser.getUser();
            clientId = sysUser.getUserId();
        }
        ClientEducation clientEducation = clientEducationMapper.selectById(clientId);
        if (clientEducation == null) {
            clientEducation = new ClientEducation();
            BeanUtils.copyProperties(clientEducationRequestVO, clientEducation);
            clientEducation.setClientId(clientId);
            clientEducation.setCreateBy(SecurityUtils.getUsername());
            clientEducation.setCreateTime(new Date());
            return clientEducationMapper.insert(clientEducation);
        } else {
            BeanUtils.copyProperties(clientEducationRequestVO, clientEducation);
            clientEducation.setClientId(clientId);
            clientEducation.setUpdateBy(SecurityUtils.getUsername());
            clientEducation.setUpdateTime(new Date());
            return clientEducationMapper.updateById(clientEducation);
        }
    }
}
