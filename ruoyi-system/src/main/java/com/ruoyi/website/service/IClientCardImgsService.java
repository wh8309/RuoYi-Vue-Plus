package com.ruoyi.website.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.website.domain.ClientCardImgs;
import com.ruoyi.website.domain.vo.response.CardImgsMenuResponseVO;

import java.util.List;

/**
 * 证件电子图片Service接口
 *
 * @author ruoyi
 * @date 2021-03-18
 */
public interface IClientCardImgsService extends IService<ClientCardImgs> {

    public List<CardImgsMenuResponseVO>  selectCardImgsMenu(Long clientId);

}
