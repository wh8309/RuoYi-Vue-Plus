package com.ruoyi.website.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.website.domain.ClientBaseInfo;
import com.ruoyi.website.domain.ClientExchangeFaculty;
import com.ruoyi.website.domain.ClientQualificationConfirm;
import com.ruoyi.website.domain.vo.request.ClientQualificationConfirmRequest;
import com.ruoyi.website.domain.vo.response.ClientQualificationConfirmResponse;
import com.ruoyi.website.mapper.ClientBaseInfoMapper;
import com.ruoyi.website.mapper.ClientExchangeFacultyMapper;
import com.ruoyi.website.mapper.ClientQualificationConfirmMapper;
import com.ruoyi.website.service.IClientQualificationConfirmService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 职称资格确认Service业务层处理
 *
 * @author ruoyi
 * @date 2021-03-24
 */
@Service
public class ClientQualificationConfirmServiceImpl extends ServiceImpl<ClientQualificationConfirmMapper, ClientQualificationConfirm> implements IClientQualificationConfirmService {

    @Resource
    private ClientQualificationConfirmMapper qualificationConfirmMapper;
    @Resource
    private ClientExchangeFacultyMapper exchangeFacultyMapper;
    @Resource
    private ClientBaseInfoMapper clientBaseInfoMapper;



    @Override
    public ClientQualificationConfirmResponse queryDetailById(Long clientId) {

        LoginUser logUser = SecurityUtils.getLoginUser();
        SysUser sysUser = logUser.getUser();
        if (clientId == null) clientId = sysUser.getUserId();
        ClientQualificationConfirm qualificationConfirm = qualificationConfirmMapper.selectById(clientId);
        ClientQualificationConfirmResponse qualificationConfirmResponse = new ClientQualificationConfirmResponse();
        if (null != qualificationConfirm) BeanUtils.copyProperties(qualificationConfirm,qualificationConfirmResponse );
        return qualificationConfirmResponse;
    }

    @Override
    @Transactional
    public int insertOrUpdate(ClientQualificationConfirmRequest qualificationConfirmRequest) {
        Long clientId = qualificationConfirmRequest.getClientId();
        if (clientId == null) {
            LoginUser logUser = SecurityUtils.getLoginUser();
            SysUser sysUser = logUser.getUser();
            clientId = sysUser.getUserId();
        }
        ClientQualificationConfirm qualificationConfirm = qualificationConfirmMapper.selectById(clientId);
        if (qualificationConfirm == null) {
            qualificationConfirm = new ClientQualificationConfirm();
            BeanUtils.copyProperties(qualificationConfirmRequest, qualificationConfirm);
            qualificationConfirm.setClientId(clientId);
            qualificationConfirm.setCreateBy(SecurityUtils.getUsername());
            ClientExchangeFaculty clientExchangeFaculty=new ClientExchangeFaculty();
            BeanUtils.copyProperties(qualificationConfirm,clientExchangeFaculty);
            clientExchangeFaculty.setCreateTime(new Date());
            exchangeFacultyMapper.insert(clientExchangeFaculty);
            qualificationConfirm.setCreateTime(new Date());
            return qualificationConfirmMapper.insert(qualificationConfirm);
        } else {
            BeanUtils.copyProperties(qualificationConfirmRequest, qualificationConfirm);
            qualificationConfirm.setClientId(clientId);
            qualificationConfirm.setUpdateBy(SecurityUtils.getUsername());
            ClientExchangeFaculty clientExchangeFaculty=new ClientExchangeFaculty();
            BeanUtils.copyProperties(qualificationConfirm,clientExchangeFaculty);
            clientExchangeFaculty.setUpdateTime(new Date());
            exchangeFacultyMapper.updateById(clientExchangeFaculty);
            qualificationConfirm.setUpdateTime(new Date());
            return qualificationConfirmMapper.updateById(qualificationConfirm);
        }
    }

    //重置“职称资格确认表”PDF
    @Override
    public void resetDatumPDFUrl(Long clientId){
        ClientBaseInfo clientBaseInfo = clientBaseInfoMapper.selectById(clientId);
        if(StringUtils.isNotBlank(clientBaseInfo.getCondConfirm()) && "1".equals(clientBaseInfo.getCondConfirm())){//资格确认
            ClientQualificationConfirm clientQualificationConfirm = qualificationConfirmMapper.selectById(clientId);
            if(clientQualificationConfirm!=null) {
                LambdaUpdateWrapper<ClientQualificationConfirm> updateWrapper = new LambdaUpdateWrapper<>();
                updateWrapper.set(ClientQualificationConfirm::getDatumUrl, null);
                updateWrapper.set(ClientQualificationConfirm::getIsGeneratedUrl, 0);
                updateWrapper.set(ClientQualificationConfirm::getRobotReportStatus, 0);
                updateWrapper.set(ClientQualificationConfirm::getRobotReportExceptionTimes, 0);
                updateWrapper.set(ClientQualificationConfirm::getRobotReportExceptionCode, null);
                updateWrapper.set(ClientQualificationConfirm::getRobotReportExceptionMsg, null);
                updateWrapper.set(ClientQualificationConfirm::getRobotReportTime, null);
                updateWrapper.eq(ClientQualificationConfirm::getClientId, Long.valueOf(clientId));
                qualificationConfirmMapper.update(null, updateWrapper);
            }
        }
    }

}
