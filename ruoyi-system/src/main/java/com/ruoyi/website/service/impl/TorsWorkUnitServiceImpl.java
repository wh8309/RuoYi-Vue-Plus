package com.ruoyi.website.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.Page;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.common.utils.DropDownBoxUnit;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.website.domain.TorsWorkUnit;
import com.ruoyi.website.domain.vo.response.*;
import com.ruoyi.website.mapper.TorsWorkUnitMapper;
import com.ruoyi.website.service.ITorsWorkUnitService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Service
public class TorsWorkUnitServiceImpl extends ServiceImpl<TorsWorkUnitMapper, TorsWorkUnit> implements ITorsWorkUnitService {

    @Override
    public List<TorsWorkUnitResponseVO> selectWorkUnitByPage(TorsWorkUnit torsWorkUnit) {
        LambdaQueryWrapper<TorsWorkUnit> lam = Wrappers.lambdaQuery();
        if (StringUtils.isNotBlank(torsWorkUnit.getUnitName())) {
            lam.like(TorsWorkUnit::getUnitName, torsWorkUnit.getUnitName());
        }
        if (StringUtils.isNotBlank(torsWorkUnit.getUnitCode())) {
            lam.like(TorsWorkUnit::getUnitCode, torsWorkUnit.getUnitCode());
        }
        lam.eq(TorsWorkUnit::getIsDeleted, "1");
        lam.orderByDesc(TorsWorkUnit::getCreateTime);
        List<TorsWorkUnit> list = this.list(lam);
        return getTorsNominateUnitQueryRequestVOList(list);
    }

    private List<TorsWorkUnitResponseVO> getTorsNominateUnitQueryRequestVOList(List<TorsWorkUnit> list) {
        List<TorsWorkUnitResponseVO> retData = list.stream()
                .map(data -> BeanUtil.toBean(data, TorsWorkUnitResponseVO.class))
                .collect(Collectors.toList());
        if (list instanceof Page) {
            Page<TorsWorkUnit> page = (Page<TorsWorkUnit>) list;
            Page<TorsWorkUnitResponseVO> pageVo = new Page<>();
            BeanUtil.copyProperties(page, pageVo);
            pageVo.addAll(retData);
            retData = pageVo;
        }
        return retData;
    }

    @Override
    public boolean saveWorkUnit(TorsWorkUnit torsWorkUnit) {
        torsWorkUnit.setCreateTime(new Date());
        LoginUser logUser = SecurityUtils.getLoginUser();
        SysUser sysUser = logUser.getUser();
        torsWorkUnit.setCreateBy(sysUser.getUserName());
        boolean save = this.save(torsWorkUnit);
        init();
        return save;

    }

    @Override
    public boolean updateWorkUnit(TorsWorkUnit torsWorkUnit) {
        torsWorkUnit.setUpdateTime(new Date());
        LoginUser logUser = SecurityUtils.getLoginUser();
        torsWorkUnit.setUpdateBy(logUser.getUsername());
        torsWorkUnit.setCreateBy(null);
        torsWorkUnit.setCreateTime(null);
        boolean update = this.updateById(torsWorkUnit);
        init();
        return update;
    }

    @Override
    public boolean deleteTorsWorkUnitByIds(Long[] workUnitIds) {
        List<TorsWorkUnit> list = new ArrayList<>();
        for (Long workUnitId : workUnitIds) {
            TorsWorkUnit torsWorkUnit = new TorsWorkUnit();
            torsWorkUnit.setWorkUnitId(workUnitId);
            torsWorkUnit.setIsDeleted(-1);
            list.add(torsWorkUnit);
        }
        boolean ret = this.saveOrUpdateBatch(list);
        init();
        return ret;
    }

    @Override
    public String importWorkUnit(List<TorsWorkUnitExcelImportResponseVO> excelData, boolean updateSupport, String operName) {
        if (com.ruoyi.common.utils.StringUtils.isNull(excelData) || excelData.size() == 0) {
            throw new CustomException("导入数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (TorsWorkUnitExcelImportResponseVO responseData : excelData) {
            try {
                // 验证是否存在
                TorsWorkUnit workUnit = exitsByUnitCode(responseData.getUnitCode());
                if (workUnit == null) {
                    workUnit = new TorsWorkUnit();
                    org.springframework.beans.BeanUtils.copyProperties(responseData, workUnit);
                    workUnit.setWorkUnitId(null);
                    workUnit.setCreateBy(operName);
                    workUnit.setCreateTime(new Date());
                    workUnit.setIsDeleted(1);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、工作单位 " + workUnit.getUnitName() + " 导入成功");
                } else if (updateSupport) {
                    Long workUnitId = workUnit.getWorkUnitId();
                    BeanUtils.copyProperties(responseData, workUnit);
                    workUnit.setWorkUnitId(workUnitId);
                    workUnit.setIsDeleted(1);
                    workUnit.setUpdateBy(operName);
                    workUnit.setUpdateTime(new Date());
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、工作单位 " + workUnit.getUnitName() + " 更新成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、工作单位 " + workUnit.getUnitName() + " 已存在");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、账号 " + responseData.getUnitName() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new CustomException(failureMsg.toString());
        } else {
            successMsg.insert(0, "导入成功！共 " + successNum + " 条，数据如下：");
        }
        init();
        return successMsg.toString();
    }

    private TorsWorkUnit exitsByUnitCode(String workUnitCode) {
        LambdaQueryWrapper<TorsWorkUnit> lam = Wrappers.lambdaQuery();
        lam.eq(TorsWorkUnit::getUnitCode, workUnitCode);
        List<TorsWorkUnit> list = this.list(lam);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public List<TorsWorkUnitExcelImportResponseVO> exportListByParam(TorsWorkUnit workUnit) {
        LambdaQueryWrapper<TorsWorkUnit> lam = Wrappers.lambdaQuery();
        if (StringUtils.isNotBlank(workUnit.getUnitName())) {
            lam.like(TorsWorkUnit::getUnitName, workUnit.getUnitName());
        }
        if (StringUtils.isNotBlank(workUnit.getUnitCode())) {
            lam.like(TorsWorkUnit::getUnitCode, workUnit.getUnitCode());
        }
        lam.eq(TorsWorkUnit::getIsDeleted, "1");
        lam.orderByAsc(TorsWorkUnit::getCreateTime);
        List<TorsWorkUnit> list = this.list(lam);
        AtomicLong atomicLong = new AtomicLong(1);
        return list.stream().map(e -> {
            TorsWorkUnitExcelImportResponseVO responseVO = new TorsWorkUnitExcelImportResponseVO();
            BeanUtils.copyProperties(e, responseVO);
            responseVO.setWorkUnitId(atomicLong.getAndIncrement());
            return responseVO;
        }).collect(Collectors.toList());
    }

    @PostConstruct
    public void init() {
        DropDownBoxUnit.clearCache(Constants.CACHE_JOB_UNIT);
        List<TorsWorkUnit> list = this.lambdaQuery().eq(TorsWorkUnit::getIsDeleted, "1").list();
        if (!list.isEmpty()) {
            Map<Long, String> collect = list.stream()
                    .collect(Collectors.toMap(TorsWorkUnit::getWorkUnitId, TorsWorkUnit::getUnitName));
            DropDownBoxUnit.setCacheList(Constants.CACHE_JOB_UNIT, collect);
        }
    }
}
