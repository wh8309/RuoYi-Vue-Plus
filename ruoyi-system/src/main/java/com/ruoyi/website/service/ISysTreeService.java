package com.ruoyi.website.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.website.domain.SysTree;

import java.util.List;

/**
 * 类目Service接口
 *
 * @author ruoyi
 * @date 2021-03-23
 */
public interface ISysTreeService extends IService<SysTree> {

    SysTree querySysTreeByTreeId(Long treeId);

    List<SysTree> queryNoParentListByType(String treeType);

    List<SysTree> querySysTreeList(Long parentTreeId, String treeType, Integer Level);
}
