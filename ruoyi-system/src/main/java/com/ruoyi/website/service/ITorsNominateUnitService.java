package com.ruoyi.website.service;

import com.ruoyi.website.domain.TorsNominateUnit;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.website.domain.vo.request.TorsNominateUnitSmsEnableRequestVO;
import com.ruoyi.website.domain.vo.response.TorsNominateUnitExcelImportResponseVO;
import com.ruoyi.website.domain.vo.response.TorsNominateUnitResponseVO;

import java.util.List;

/**
 * 推荐单位Service接口
 *
 * @author ruoyi
 * @date 2021-04-22
 */
public interface ITorsNominateUnitService extends IService<TorsNominateUnit> {

    /**
     * 查询列表
     */
    List<TorsNominateUnitResponseVO> queryList(TorsNominateUnit torsNominateUnit);

    boolean deleteBatch(List<Long> idList);

    List<TorsNominateUnitExcelImportResponseVO> exportListByParam(TorsNominateUnit torsNominateUnit);

    String importNominateUnit(List<TorsNominateUnitExcelImportResponseVO> excelData, boolean updateSupport, String operName);

    int updateSmsEnable(TorsNominateUnitSmsEnableRequestVO requestVO);
}
