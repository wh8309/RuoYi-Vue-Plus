package com.ruoyi.website.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.IdWorkerUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.cloud.CloudStorageService;
import com.ruoyi.system.cloud.OSSFactory;
import com.ruoyi.system.service.ISysConfigService;
import com.ruoyi.website.domain.SysFileInfo;
import com.ruoyi.website.domain.vo.request.FileQueryRequestVO;
import com.ruoyi.website.domain.vo.request.FileUploadRequestVO;
import com.ruoyi.website.mapper.SysFileInfoMapper;
import com.ruoyi.website.service.ISysFileInfoService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 文件信息Service业务层处理
 *
 * @author ruoyi
 * @date 2021-03-18
 */
@Service
public class SysFileInfoServiceImpl extends ServiceImpl<SysFileInfoMapper, SysFileInfo> implements ISysFileInfoService {

    @Autowired
    private SysFileInfoMapper sysFileInfoMapper;
    @Autowired
    private ISysConfigService sysConfigService;

    public SysFileInfo insertFile(Map<String,String> fileMap , FileUploadRequestVO fileUploadRequestVO,SysFileInfo sysFileInfo){
        String fileId = IdWorkerUtils.generateId().toString();
        Integer fileType=fileUploadRequestVO.getFileType();
        Integer businessType=null;
        if(53==fileType){//专业论文论著照片
            businessType=1;
        }else if(54==fileType){//反映个人专业工作业绩的材料
            businessType=2;
        }
        LoginUser logUser = SecurityUtils.getLoginUser();
        SysUser sysUser = logUser.getUser();
        Long clientId=sysUser.getUserId();
        if(fileUploadRequestVO.getClientId()!=null){
            clientId=fileUploadRequestVO.getClientId();
        }
//        SysFileInfo sysFileInfo = new SysFileInfo();
        sysFileInfo.setFileId(fileId);
        sysFileInfo.setFileType(fileUploadRequestVO.getFileType());
        sysFileInfo.setBusinessId(fileUploadRequestVO.getBusinessId());
        sysFileInfo.setBusinessType(businessType);
        sysFileInfo.setFileName(fileMap.get("actualStoreFileName"));
        sysFileInfo.setOriginalFileName(fileMap.get("originalFileName"));
        sysFileInfo.setFileExt(fileMap.get("fileExt"));
        sysFileInfo.setFileSize(Long.valueOf(fileMap.get("fileSize")));
        sysFileInfo.setFilePath(fileMap.get("fileUrl"));
        sysFileInfo.setMd5(fileMap.get("md5"));
        sysFileInfo.setRemark(fileUploadRequestVO.getRemark());
        sysFileInfo.setClientId(clientId);
        sysFileInfo.setCreateBy(SecurityUtils.getUsername());
        sysFileInfo.setCreateTime(new Date());
        baseMapper.insert(sysFileInfo);
        return sysFileInfo;
    }

    //更新文件信息
    public  SysFileInfo updateFile(Map<String,String> fileMap, SysFileInfo sysFileInfo){
        sysFileInfo.setFileName(fileMap.get("actualStoreFileName"));
        sysFileInfo.setOriginalFileName(fileMap.get("originalFileName"));
        sysFileInfo.setFileExt(fileMap.get("fileExt"));
        sysFileInfo.setFileSize(Long.valueOf(fileMap.get("fileSize")));
        sysFileInfo.setFilePath(fileMap.get("fileUrl"));
        sysFileInfo.setMd5(fileMap.get("md5"));
        sysFileInfo.setUpdateBy(SecurityUtils.getUsername());
        sysFileInfo.setUpdateTime(new Date());
        baseMapper.updateById(sysFileInfo);
        return sysFileInfo;
    }

    //删除文件
    public int deleteFileById(String fileId){
        LambdaUpdateWrapper<SysFileInfo> wrapper = Wrappers.lambdaUpdate();
        wrapper.set(SysFileInfo::getIsDeleted, -1);
        wrapper.set(SysFileInfo::getUpdateBy,SecurityUtils.getUsername() );
        wrapper.eq(SysFileInfo::getFileId, fileId);
        return  baseMapper.update(null,wrapper);
    }

    //根据业务business_id和file_type删除
    public int deleteFileByBusinessId(Long businessId,String fileType){
        LambdaUpdateWrapper<SysFileInfo> wrapper = Wrappers.lambdaUpdate();
        wrapper.set(SysFileInfo::getIsDeleted, -1);
        wrapper.set(SysFileInfo::getUpdateBy,SecurityUtils.getUsername() );
        wrapper.eq(SysFileInfo::getFileType, Integer.valueOf(fileType));
        wrapper.eq(SysFileInfo::getBusinessId,businessId);
        return  baseMapper.update(null,wrapper);
    }

    //查詢
    public List<SysFileInfo> selectFileList(FileQueryRequestVO fileQueryRequestVO){
        LoginUser logUser = SecurityUtils.getLoginUser();
        SysUser sysUser = logUser.getUser();
        Long clientId=sysUser.getUserId();
        if(fileQueryRequestVO.getClientId()!=null){
            clientId=fileQueryRequestVO.getClientId();
        }
        QueryWrapper<SysFileInfo> queryWrapper = new QueryWrapper<SysFileInfo>();
        queryWrapper.eq("client_id",clientId);
        if("ALLCardImgs".equals(fileQueryRequestVO.getFileType())){//
            queryWrapper.ge("file_type",10);
            queryWrapper.le("file_type",20);
        }else if("ALLRecordDatums".equals(fileQueryRequestVO.getFileType())){//
            queryWrapper.ge("file_type",50);
            queryWrapper.le("file_type",60);
        }else{
            queryWrapper.eq("file_type",Long.valueOf(fileQueryRequestVO.getFileType()));
        }
        if(fileQueryRequestVO.getBusinessId()!=null){
            queryWrapper.eq("business_id",fileQueryRequestVO.getBusinessId());
        }
        queryWrapper.eq("is_deleted",1);//正常文件，非删除的
        queryWrapper.orderByAsc("file_type");
        return baseMapper.selectList(queryWrapper);
    }

    public int selectFileCount(Long clientId,String fileType){
        QueryWrapper<SysFileInfo> queryWrapper = new QueryWrapper<SysFileInfo>();
        queryWrapper.eq("client_id",clientId);
        if("ALLCardImgs".equals(fileType)){//所有证件电子图片
            queryWrapper.ge("file_type",10);
            queryWrapper.le("file_type",20);
        }else if("ALLRecordDatums".equals(fileType)){//所有评审申报材料
            queryWrapper.ge("file_type",50);
            queryWrapper.le("file_type",60);
        }else{
            queryWrapper.eq("file_type",Long.valueOf(fileType));
        }
        queryWrapper.eq("is_deleted",1);//正常文件，非删除的
        return baseMapper.selectCount(queryWrapper);
    }

    @Override
    public void uploadFileToCloud(String key, String filename) {
        CloudStorageService storage = OSSFactory.getInstance();
        storage.uploadFile(key,filename);
    }

    //获取云上URL
    @Override
    public String getCloudVisitUrl(String cloudStoragePath){
        CloudStorageService storage = OSSFactory.getInstance();
        return storage.getCloudVisitUrl(cloudStoragePath);
    }

    /**
     * 清理磁盘文件
     */
    @Scheduled(cron ="0 */1 * * * ?")
    public void clearDistDelFile(){
        QueryWrapper<SysFileInfo> queryWrapper = new QueryWrapper<SysFileInfo>();
        queryWrapper.eq("is_deleted",-1);//查询逻辑删除的文件
        queryWrapper.orderByAsc("update_time");
        queryWrapper.last("limit 1000");
        List<SysFileInfo> needDistDelFileList = baseMapper.selectList(queryWrapper);
        List<SysFileInfo> needUpdateFileList = new ArrayList<>();//磁盘存在删除，更新状态为-2
        List<SysFileInfo> notExistFileList = new ArrayList<>();//磁盘上本来就不存在文件，更新状态为-3
        for(SysFileInfo diskDelFile:needDistDelFileList){
            //1.磁盘删除
            String localFilePath= RuoYiConfig.getProfile()+diskDelFile.getFilePath().replaceAll(Constants.RESOURCE_PREFIX,"");
            File localFile=new File(localFilePath);
            try {
                FileUtils.forceDelete(localFile);
                diskDelFile.setIsDeleted(-2);
                needUpdateFileList.add(diskDelFile);
            } catch (IOException e) {
                diskDelFile.setIsDeleted(-3);
                notExistFileList.add(diskDelFile);
//                e.printStackTrace();
            }
        }
        //2.修改删除状态-2
        if(needUpdateFileList.size()>0){
            updateBatchById(needUpdateFileList);
        }
        //3.磁盘上本事就不存在的文件，更新状态为-3
        if(notExistFileList.size()>0){
            updateBatchById(notExistFileList);
        }
    }


}
