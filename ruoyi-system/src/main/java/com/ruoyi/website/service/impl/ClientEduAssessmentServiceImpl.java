package com.ruoyi.website.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.ObjectUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.website.domain.ClientEduAssessment;
import com.ruoyi.website.domain.vo.request.ClientEduAssessmentRequestVO;
import com.ruoyi.website.domain.vo.response.ClientEduAssessmentResponseVO;
import com.ruoyi.website.mapper.ClientEduAssessmentMapper;
import com.ruoyi.website.service.IClientEduAssessmentService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 年度考核与继续教育Service业务层处理
 *
 * @author ruoyi
 * @date 2021-03-22
 */
@Service
public class ClientEduAssessmentServiceImpl extends ServiceImpl<ClientEduAssessmentMapper, ClientEduAssessment> implements IClientEduAssessmentService {

    @Resource
    private ClientEduAssessmentMapper clientEduAssessmentMapper;

    @Override
    public ClientEduAssessmentResponseVO queryDetailById(Long clientId) {

        LoginUser logUser = SecurityUtils.getLoginUser();
        SysUser sysUser = logUser.getUser();
        if (clientId == null) clientId = sysUser.getUserId();
        ClientEduAssessment clientEduAssessment = clientEduAssessmentMapper.selectById(clientId);
        ClientEduAssessmentResponseVO eduAssessmentResponse = new ClientEduAssessmentResponseVO();
        if (null != clientEduAssessment) BeanUtils.copyProperties(clientEduAssessment, eduAssessmentResponse);
        return eduAssessmentResponse;
    }

    @Override
    public int insertOrUpdate(ClientEduAssessmentRequestVO clientEducationRequest) {
        try {
            ObjectUtils.setEmptyString(clientEducationRequest);//如果为NULL,设置为空字符串
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        Long clientId = clientEducationRequest.getClientId();
        if (clientId == null) {
            LoginUser logUser = SecurityUtils.getLoginUser();
            SysUser sysUser = logUser.getUser();
            clientId = sysUser.getUserId();
        }
        ClientEduAssessment clientEduAssessment = clientEduAssessmentMapper.selectById(clientId);
        if (clientEduAssessment == null) {
            clientEduAssessment = new ClientEduAssessment();
            BeanUtils.copyProperties(clientEducationRequest, clientEduAssessment);
            clientEduAssessment.setClientId(clientId);
            clientEduAssessment.setCreateBy(SecurityUtils.getUsername());
            clientEduAssessment.setCreateTime(new Date());
            return clientEduAssessmentMapper.insert(clientEduAssessment);
        } else {
            BeanUtils.copyProperties(clientEducationRequest, clientEduAssessment);
            clientEduAssessment.setClientId(clientId);
            clientEduAssessment.setUpdateBy(SecurityUtils.getUsername());
            clientEduAssessment.setUpdateTime(new Date());
            return clientEduAssessmentMapper.updateById(clientEduAssessment);
        }
    }

}
