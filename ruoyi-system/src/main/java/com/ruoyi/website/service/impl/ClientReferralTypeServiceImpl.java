package com.ruoyi.website.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.website.domain.ClientReferralType;
import com.ruoyi.website.domain.vo.request.ClientReferralTypeRequest;
import com.ruoyi.website.domain.vo.response.ClientReferralTypeResponse;
import com.ruoyi.website.mapper.ClientReferralTypeMapper;
import com.ruoyi.website.service.IClientReferralTypeService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 转评条件说明Service业务层处理
 *
 * @author ruoyi
 * @date 2021-03-24
 */
@Service
public class ClientReferralTypeServiceImpl extends ServiceImpl<ClientReferralTypeMapper, ClientReferralType> implements IClientReferralTypeService {

    @Resource
    private ClientReferralTypeMapper referralTypeMapper;


    @Override
    public ClientReferralTypeResponse queryDetailById(Long clientId) {
        LoginUser logUser = SecurityUtils.getLoginUser();
        SysUser sysUser = logUser.getUser();
        if (clientId == null) clientId = sysUser.getUserId();
        ClientReferralType referralType = referralTypeMapper.selectById(clientId);
        ClientReferralTypeResponse referralTypeResponse = new ClientReferralTypeResponse();
        if (null != referralType) BeanUtils.copyProperties(referralType, referralTypeResponse);
        return referralTypeResponse;
    }

    @Override
    public int insertOrUpdate(ClientReferralTypeRequest referralTypeRequest) {
        Long clientId = referralTypeRequest.getClientId();
        if (clientId == null) {
            LoginUser logUser = SecurityUtils.getLoginUser();
            SysUser sysUser = logUser.getUser();
            clientId = sysUser.getUserId();
        }
        ClientReferralType referralType = referralTypeMapper.selectById(clientId);
        if (referralType == null) {
            referralType = new ClientReferralType();
            BeanUtils.copyProperties(referralTypeRequest, referralType);
            referralType.setClientId(clientId);
            referralType.setCreateBy(SecurityUtils.getUsername());
            referralType.setCreateTime(new Date());
            return referralTypeMapper.insert(referralType);
        } else {
            BeanUtils.copyProperties(referralTypeRequest, referralType);
            referralType.setClientId(clientId);
            referralType.setUpdateBy(SecurityUtils.getUsername());
            referralType.setUpdateTime(new Date());
            return referralTypeMapper.updateById(referralType);
        }
    }
}
