package com.ruoyi.website.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.website.domain.ClientSkillSummary;
import com.ruoyi.website.domain.vo.request.ClientSkillSummaryRequest;
import com.ruoyi.website.domain.vo.response.ClientSkillSummaryResponse;
import com.ruoyi.website.mapper.ClientSkillSummaryMapper;
import com.ruoyi.website.service.IClientSkillSummaryService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 任期内专业技术业绩与成果报告Service业务层处理
 *
 * @author ruoyi
 * @date 2021-03-24
 */
@Service
public class ClientSkillSummaryServiceImpl extends ServiceImpl<ClientSkillSummaryMapper, ClientSkillSummary> implements IClientSkillSummaryService {

    @Resource
    private ClientSkillSummaryMapper clientSkillSummaryMapper;

    @Override
    public ClientSkillSummaryResponse queryDetailById(Long clientId) {
        LoginUser logUser = SecurityUtils.getLoginUser();
        SysUser sysUser = logUser.getUser();
        if (clientId == null) clientId = sysUser.getUserId();
        ClientSkillSummary clientSkillSummary = clientSkillSummaryMapper.selectById(clientId);
        ClientSkillSummaryResponse clientSkillSummaryResponse = new ClientSkillSummaryResponse();
        if (null != clientSkillSummary) BeanUtils.copyProperties(clientSkillSummary, clientSkillSummaryResponse);
        return clientSkillSummaryResponse;
    }

    @Override
    public int insertOrUpdate(ClientSkillSummaryRequest clientSkillSummaryRequest) {
        Long clientId = clientSkillSummaryRequest.getClientId();
        if (clientId == null) {
            LoginUser logUser = SecurityUtils.getLoginUser();
            SysUser sysUser = logUser.getUser();
            clientId = sysUser.getUserId();
        }
        ClientSkillSummary clientSkillSummary = clientSkillSummaryMapper.selectById(clientId);
        if (clientSkillSummary == null) {
            clientSkillSummary = new ClientSkillSummary();
            BeanUtils.copyProperties(clientSkillSummaryRequest, clientSkillSummary);
            clientSkillSummary.setClientId(clientId);
            clientSkillSummary.setCreateBy(SecurityUtils.getUsername());
            clientSkillSummary.setCreateTime(new Date());
            return clientSkillSummaryMapper.insert(clientSkillSummary);
        } else {
            BeanUtils.copyProperties(clientSkillSummaryRequest, clientSkillSummary);
            clientSkillSummary.setClientId(clientId);
            clientSkillSummary.setUpdateBy(SecurityUtils.getUsername());
            clientSkillSummary.setUpdateTime(new Date());
            return clientSkillSummaryMapper.updateById(clientSkillSummary);
        }

    }
}
