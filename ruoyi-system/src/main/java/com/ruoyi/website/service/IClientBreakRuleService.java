package com.ruoyi.website.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.website.domain.ClientBreakRule;
import com.ruoyi.website.domain.vo.request.ClientBreakRuleRequest;
import com.ruoyi.website.domain.vo.response.ClientBreakRuleResponse;


/**
 * 破格条件说明Service接口
 *
 * @author ruoyi
 * @date 2021-03-25
 */
public interface IClientBreakRuleService extends IService<ClientBreakRule> {

    ClientBreakRuleResponse queryDetailById(Long clientId);

    int insertOrUpdate(ClientBreakRuleRequest clientBreakRuleRequest);
}
