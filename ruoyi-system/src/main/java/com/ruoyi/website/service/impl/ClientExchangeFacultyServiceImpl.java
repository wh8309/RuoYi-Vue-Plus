package com.ruoyi.website.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.website.domain.ClientExchangeFaculty;
import com.ruoyi.website.domain.ClientQualificationConfirm;
import com.ruoyi.website.domain.vo.request.ClientExchangeFacultyRequest;
import com.ruoyi.website.domain.vo.response.ClientExchangeFacultyResponse;
import com.ruoyi.website.mapper.ClientExchangeFacultyMapper;
import com.ruoyi.website.mapper.ClientQualificationConfirmMapper;
import com.ruoyi.website.service.IClientExchangeFacultyService;
import com.ruoyi.website.service.IClientQualificationConfirmService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 转换系列Service业务层处理
 *
 * @author ruoyi
 * @date 2021-03-24
 */
@Service
public class ClientExchangeFacultyServiceImpl extends ServiceImpl<ClientExchangeFacultyMapper, ClientExchangeFaculty> implements IClientExchangeFacultyService {

    @Resource
    private ClientExchangeFacultyMapper exchangeFacultyMapper;

    @Resource
    private ClientQualificationConfirmMapper qualificationConfirmMapper;

    @Override
    public ClientExchangeFacultyResponse queryDetailById(Long clientId) {
        LoginUser logUser = SecurityUtils.getLoginUser();
        SysUser sysUser = logUser.getUser();
        if (clientId == null) clientId = sysUser.getUserId();
        ClientExchangeFaculty exchangeFaculty = exchangeFacultyMapper.selectById(clientId);
        ClientExchangeFacultyResponse exchangeFacultyResponse = new ClientExchangeFacultyResponse();
        if (null != exchangeFaculty) BeanUtils.copyProperties(exchangeFaculty,exchangeFacultyResponse);
        return exchangeFacultyResponse;
    }

    @Override
    @Transactional
    public int insertOrUpdate(ClientExchangeFacultyRequest exchangeFacultyRequest) {
        Long clientId = exchangeFacultyRequest.getClientId();
        if (clientId == null) {
            LoginUser logUser = SecurityUtils.getLoginUser();
            SysUser sysUser = logUser.getUser();
            clientId = sysUser.getUserId();
        }
        ClientExchangeFaculty exchangeFaculty = exchangeFacultyMapper.selectById(clientId);
        if (exchangeFaculty == null) {
            exchangeFaculty = new ClientExchangeFaculty();
            BeanUtils.copyProperties(exchangeFacultyRequest, exchangeFaculty);
            exchangeFaculty.setClientId(clientId);
            exchangeFaculty.setCreateBy(SecurityUtils.getUsername());
            ClientQualificationConfirm clientQualificationConfirm=new ClientQualificationConfirm();
            BeanUtils.copyProperties(exchangeFaculty,clientQualificationConfirm);
            clientQualificationConfirm.setCreateTime(new Date());
            qualificationConfirmMapper.insert(clientQualificationConfirm);
            exchangeFaculty.setCreateTime(new Date());
            return exchangeFacultyMapper.insert(exchangeFaculty);
        } else {
            BeanUtils.copyProperties(exchangeFacultyRequest, exchangeFaculty);
            exchangeFaculty.setClientId(clientId);
            exchangeFaculty.setUpdateBy(SecurityUtils.getUsername());
            ClientQualificationConfirm clientQualificationConfirm=new ClientQualificationConfirm();
            BeanUtils.copyProperties(exchangeFaculty,clientQualificationConfirm);
            clientQualificationConfirm.setUpdateTime(new Date());
            qualificationConfirmMapper.updateById(clientQualificationConfirm);
            exchangeFaculty.setUpdateTime(new Date());
            return exchangeFacultyMapper.updateById(exchangeFaculty);
        }

    }
}
