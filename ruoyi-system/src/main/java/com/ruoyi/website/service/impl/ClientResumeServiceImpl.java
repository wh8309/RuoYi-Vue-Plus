package com.ruoyi.website.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.website.domain.ClientResume;
import com.ruoyi.website.domain.vo.request.ClientResumeRequest;
import com.ruoyi.website.domain.vo.response.ClientResumeResponse;
import com.ruoyi.website.mapper.ClientResumeMapper;
import com.ruoyi.website.service.IClientResumeService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 从事专业技术工作简历Service业务层处理
 *
 * @author ruoyi
 * @date 2021-03-22
 */
@Service
public class ClientResumeServiceImpl extends ServiceImpl<ClientResumeMapper, ClientResume> implements IClientResumeService {

    @Resource
    private ClientResumeMapper clientResumeMapper;

    @Override
    public ClientResumeResponse queryDetailById(Long clientId) {
        LoginUser logUser = SecurityUtils.getLoginUser();
        SysUser sysUser = logUser.getUser();
        if (clientId == null) clientId = sysUser.getUserId();
        ClientResume clientEducation = clientResumeMapper.selectById(clientId);
        ClientResumeResponse clientResumeResponse = new ClientResumeResponse();
        if (null != clientEducation) BeanUtils.copyProperties(clientEducation, clientResumeResponse);
        return clientResumeResponse;
    }

    @Override
    public int insertOrUpdate(ClientResumeRequest clientResumeRequest) {
        Long clientId = clientResumeRequest.getClientId();
        if (clientId == null) {
            LoginUser logUser = SecurityUtils.getLoginUser();
            SysUser sysUser = logUser.getUser();
            clientId = sysUser.getUserId();
        }
        ClientResume clientResume = clientResumeMapper.selectById(clientId);
        if (clientResume == null) {
            clientResume = new ClientResume();
            BeanUtils.copyProperties(clientResumeRequest, clientResume);
            clientResume.setClientId(clientId);
            clientResume.setCreateBy(SecurityUtils.getUsername());
            clientResume.setCreateTime(new Date());
            return clientResumeMapper.insert(clientResume);
        } else {
            BeanUtils.copyProperties(clientResumeRequest, clientResume);
            clientResume.setClientId(clientId);
            clientResume.setUpdateBy(SecurityUtils.getUsername());
            clientResume.setUpdateTime(new Date());
            return clientResumeMapper.updateById(clientResume);
        }
    }
}
