package com.ruoyi.website.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.website.domain.ClientBreakRule;
import com.ruoyi.website.domain.vo.request.ClientBreakRuleRequest;
import com.ruoyi.website.domain.vo.response.ClientBreakRuleResponse;
import com.ruoyi.website.mapper.ClientBreakRuleMapper;
import com.ruoyi.website.service.IClientBreakRuleService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 破格条件说明Service业务层处理
 *
 * @author ruoyi
 * @date 2021-03-25
 */
@Service
public class ClientBreakRuleServiceImpl extends ServiceImpl<ClientBreakRuleMapper, ClientBreakRule> implements IClientBreakRuleService {

    @Resource
    private ClientBreakRuleMapper clientBreakRuleMapper;

    @Override
    public ClientBreakRuleResponse queryDetailById(Long clientId) {
        LoginUser logUser = SecurityUtils.getLoginUser();
        SysUser sysUser = logUser.getUser();
        if (clientId == null) clientId = sysUser.getUserId();
        ClientBreakRule clientBreakRule = clientBreakRuleMapper.selectById(clientId);
        ClientBreakRuleResponse clientBreakRuleResponse = new ClientBreakRuleResponse();
        if (null != clientBreakRule) BeanUtils.copyProperties(clientBreakRule,clientBreakRuleResponse );
        return clientBreakRuleResponse;
    }

    @Override
    public int insertOrUpdate(ClientBreakRuleRequest clientBreakRuleRequest) {
        Long clientId = clientBreakRuleRequest.getClientId();
        if (clientId == null) {
            LoginUser logUser = SecurityUtils.getLoginUser();
            SysUser sysUser = logUser.getUser();
            clientId = sysUser.getUserId();
        }
        ClientBreakRule clientBreakRule = clientBreakRuleMapper.selectById(clientId);
        if (clientBreakRule == null) {
            clientBreakRule = new ClientBreakRule();
            BeanUtils.copyProperties(clientBreakRuleRequest, clientBreakRule);
            clientBreakRule.setClientId(clientId);
            clientBreakRule.setCreateBy(SecurityUtils.getUsername());
            clientBreakRule.setCreateTime(new Date());
            return clientBreakRuleMapper.insert(clientBreakRule);
        } else {
            BeanUtils.copyProperties(clientBreakRuleRequest, clientBreakRule);
            clientBreakRule.setClientId(clientId);
            clientBreakRule.setUpdateBy(SecurityUtils.getUsername());
            clientBreakRule.setUpdateTime(new Date());
            return clientBreakRuleMapper.updateById(clientBreakRule);
        }
    }
}
