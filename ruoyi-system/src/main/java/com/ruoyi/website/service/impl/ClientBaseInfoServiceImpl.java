package com.ruoyi.website.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.common.exception.ParamsCheckException;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.bean.BeanUtils;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.system.mapper.SysDeptMapper;
import com.ruoyi.system.mapper.SysUserMapper;
import com.ruoyi.system.service.ISysConfigService;
import com.ruoyi.system.service.ISysUserService;
import com.ruoyi.website.domain.*;
import com.ruoyi.website.domain.bo.FillInStatisticsBO;
import com.ruoyi.website.domain.vo.request.*;
import com.ruoyi.website.domain.vo.response.*;
import com.ruoyi.website.mapper.ClientBaseInfoMapper;
import com.ruoyi.website.service.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

/**
 * 客户基本信息Service业务层处理
 *
 * @author ruoyi
 * @date 2021-03-22
 */
@Service
public class ClientBaseInfoServiceImpl extends ServiceImpl<ClientBaseInfoMapper, ClientBaseInfo> implements IClientBaseInfoService {

    @Autowired
    private ClientBaseInfoMapper baseInfoMapper;
    @Autowired
    private ISysTreeService sysTreeService;
    @Autowired
    private ISysFileInfoService sysFileInfoService;
    @Autowired
    private ITorsNominateUnitService torsNominateUnitService;
    @Autowired
    private ISysUserService sysUserService;
    @Autowired
    private ITorsWorkUnitService torsWorkUnitService;
    @Autowired
    private ISysConfigService configService;
    @Autowired
    private IClientRobotReportLogService robotReportLogService;
    @Autowired
    private IClientRobotReportTagService clientRobotReportTagService;
    @Autowired
    private IClientQualificationConfirmService clientQualificationConfirmService;

    @Override
    public BaseInfoResponseVO queryBaseById(Long clientId) {
        LoginUser logUser = SecurityUtils.getLoginUser();
        SysUser sysUser = logUser.getUser();
        if (clientId == null) clientId = sysUser.getUserId();
        ClientBaseInfo clientBaseInfo = baseInfoMapper.selectById(clientId);
        BaseInfoResponseVO response = new BaseInfoResponseVO();
        if (clientBaseInfo == null) return response;
        BeanUtils.copyProperties(clientBaseInfo, response);
        if(StringUtils.isNotBlank(response.getAccount())){
            response.setAccount(clientBaseInfo.getIdcard());
        }
        String clientInitPassword = configService.selectConfigByKey("tors.client.initPassword");//客户初始密码
        if(StringUtils.isNotBlank(response.getPassword())){
            response.setPassword(clientInitPassword);
        }
        if (StringUtils.isNotEmpty(response.getBelongSeries())) {
            ReviewNoticeResponse response1 = queryReviewNoticeByTreeId(Long.valueOf(response.getBelongSeries()));
            response.setBelongSeriesName(response1.getBelongSeries());
        }
        return response;
    }

    @Override
    public ClientBaseInfo queryBaseInfoById(Long clientId) {
        LoginUser logUser = SecurityUtils.getLoginUser();
        SysUser sysUser = logUser.getUser();
        if (clientId == null) clientId = sysUser.getUserId();
        ClientBaseInfo clientBaseInfo = baseInfoMapper.selectById(clientId);
        return clientBaseInfo;
    }

    @Override
    public List<BaseInfoResponseVO> queryBaseInfoList(BaseInfoQueryRequestVO baseInfoQueryRequest) {
        LoginUser logUser = SecurityUtils.getLoginUser();
        SysUser sysUser = logUser.getUser();
        Long clientId = baseInfoQueryRequest.getClientId() == null ? sysUser.getUserId() : baseInfoQueryRequest.getClientId();
        QueryWrapper<ClientBaseInfo> queryWrapper = new QueryWrapper<ClientBaseInfo>();
        queryWrapper.eq("client_id", clientId);
        queryWrapper.eq("is_deleted", 1);
        List<ClientBaseInfo> clientBaseInfoList = baseInfoMapper.selectList(queryWrapper);
        List<BaseInfoResponseVO> responseList = clientBaseInfoList.stream().map(base -> {
            BaseInfoResponseVO baseInfoResponse = new BaseInfoResponseVO();
            BeanUtils.copyProperties(base, baseInfoResponse);
            return baseInfoResponse;
        }).collect(Collectors.toList());
        return responseList;
    }

    @Override
    public int insertOrUpdate(BaseInfoRequestVO baseInfoRequestVO) {
        Long clientId = baseInfoRequestVO.getClientId();
        if (null == clientId) {
            LoginUser logUser = SecurityUtils.getLoginUser();
            SysUser sysUser = logUser.getUser();
            clientId = sysUser.getUserId();
        }
        ClientBaseInfo clientBaseInfo = baseInfoMapper.selectById(clientId);
        if (clientBaseInfo == null) {
            clientBaseInfo = new ClientBaseInfo();
            org.springframework.beans.BeanUtils.copyProperties(baseInfoRequestVO, clientBaseInfo);
            clientBaseInfo.setClientId(clientId);
            clientBaseInfo.setCreateBy(SecurityUtils.getUsername());
            clientBaseInfo.setCreateTime(new Date());
            clientBaseInfo.setAccount(baseInfoRequestVO.getIdcard());//初始政务网账号
            String clientInitPassword = configService.selectConfigByKey("tors.client.initPassword");//客户初始密码
            clientBaseInfo.setPassword(clientInitPassword);//初始政务网密码
            return baseInfoMapper.insert(clientBaseInfo);
        } else {
            if(StringUtils.isBlank(baseInfoRequestVO.getAccount())) {//前端界面账号没有传递，则数据库账号密码赋值过来
                if(StringUtils.isBlank(clientBaseInfo.getAccount())){//数据库不存在
                    clientBaseInfo.setAccount(baseInfoRequestVO.getIdcard());//初始政务网账号
                    String clientInitPassword = configService.selectConfigByKey("tors.client.initPassword");//客户初始密码
                    clientBaseInfo.setPassword(clientInitPassword);//初始政务网密码
                }
                baseInfoRequestVO.setAccount(clientBaseInfo.getAccount());
                baseInfoRequestVO.setPassword(clientBaseInfo.getPassword());
            }
            org.springframework.beans.BeanUtils.copyProperties(baseInfoRequestVO, clientBaseInfo);
            if (StringUtils.isBlank(baseInfoRequestVO.getNowPostAuthDate())) {//批准时间前端传递为空值
                clientBaseInfo.setNowPostAuthDate("");
            }
            clientBaseInfo.setClientId(clientId);
            clientBaseInfo.setUpdateBy(SecurityUtils.getUsername());
            clientBaseInfo.setUpdateTime(new Date());
            if (clientBaseInfo.getFillInTime() == null) {
                clientBaseInfo.setFillInTime(new Date());
            }
            return baseMapper.updateById(clientBaseInfo);
        }

    }


    @Override
    @Transactional
    public int insertOrUpdateBase(ClientBaseInfo clientBaseInfoRequest) {
        Long clientId = clientBaseInfoRequest.getClientId();
        ClientBaseInfo clientBaseInfo = baseInfoMapper.selectById(clientId);
        if (clientBaseInfo == null) {
            clientBaseInfo = new ClientBaseInfo();
            org.springframework.beans.BeanUtils.copyProperties(clientBaseInfoRequest, clientBaseInfo);
            clientBaseInfo.setClientId(clientId);
            clientBaseInfo.setCreateBy(SecurityUtils.getUsername());
            return baseInfoMapper.insert(clientBaseInfo);
        } else {
            org.springframework.beans.BeanUtils.copyProperties(clientBaseInfoRequest, clientBaseInfo);
            clientBaseInfo.setClientId(clientId);
            clientBaseInfo.setUpdateBy(SecurityUtils.getUsername());
            return baseMapper.updateById(clientBaseInfo);
        }

    }


    //{"report_year":"2020","belong_series":"工程","judges_panel_name":"评委会名称","review_notice_scope":"评审范围",
    // "apply_start_time":"2021-03-15","apply_end_time":"2021-04-02"}
    @Override
    public List<ReviewNoticeResponse> queryReviewNoticeList(ReviewNoticeRequest reviewNoticeRequest) {
        List<SysTree> sysTreeList = sysTreeService.queryNoParentListByType("A");
        List<ReviewNoticeResponse> responseList = new ArrayList<>();
        for (SysTree sysTree : sysTreeList) {
            ReviewNoticeResponse response = new ReviewNoticeResponse();
            BeanUtils.copyProperties(sysTree, response);
            responseList.add(response);
            if (StringUtils.isNotEmpty(sysTree.getExpandContent())) {
                Map<String, String> paramMap = new HashMap<>();
                paramMap = JSON.parseObject(sysTree.getExpandContent(), paramMap.getClass());
                response.setReportYear(paramMap.get("report_year"));
                response.setBelongSeries(paramMap.get("belong_series"));
                response.setJudgesPanelName(paramMap.get("judges_panel_name"));
                response.setReviewNoticeScope(paramMap.get("review_notice_scope"));
                response.setApplyStartTime(paramMap.get("apply_start_time"));
                response.setApplyEndTime(paramMap.get("apply_end_time"));
            }
        }
        //对搜索条件进行筛选
        responseList = responseList.stream().filter(response -> {
            Boolean flag = true;
            if (StringUtils.isNotEmpty(reviewNoticeRequest.getReportYear())) {
                flag = reviewNoticeRequest.getReportYear().equals(response.getReportYear());
            }
            if (!flag) return false;
            if (StringUtils.isNotEmpty(reviewNoticeRequest.getBelongSeries())) {
                flag = reviewNoticeRequest.getBelongSeries().equals(response.getBelongSeries());
            }
            if (!flag) return false;
            if (StringUtils.isNotEmpty(reviewNoticeRequest.getTreeName())) {
                if (StringUtils.isEmpty(response.getTreeName())) return false;
                flag = response.getTreeName().contains(reviewNoticeRequest.getTreeName());
            }
            return flag;
        }).collect(Collectors.toList());
        return responseList;
    }

    @Override
    public ReviewNoticeResponse queryReviewNoticeByTreeId(Long treeId) {
        SysTree sysTree = sysTreeService.querySysTreeByTreeId(treeId);
        ReviewNoticeResponse response = new ReviewNoticeResponse();
        BeanUtils.copyProperties(sysTree, response);
        if (StringUtils.isNotEmpty(sysTree.getExpandContent())) {
            Map<String, String> paramMap = new HashMap<>();
            paramMap = JSON.parseObject(sysTree.getExpandContent(), paramMap.getClass());
            response.setReportYear(paramMap.get("report_year"));
            response.setBelongSeries(paramMap.get("belong_series"));
            response.setJudgesPanelName(paramMap.get("judges_panel_name"));
            response.setReviewNoticeScope(paramMap.get("review_notice_scope"));
            response.setApplyStartTime(paramMap.get("apply_start_time"));
            response.setApplyEndTime(paramMap.get("apply_end_time"));
        }
        return response;
    }

    @Override
    public List<Map<Long, String>> queryProfessionalList(Long parentTreeId) {
        List<SysTree> sysTreeList = sysTreeService.querySysTreeList(parentTreeId, "A", 2);
        List<Professional> professionalList = new ArrayList<>();
        for (SysTree sysTree : sysTreeList) {
            professionalList.add(new Professional(sysTree.getTreeId(), sysTree.getTreeName()));
        }
        String professionalListStr = JSON.toJSONString(professionalList);
        List<Map<Long, String>> listMap = new ArrayList<>();
        listMap = JSON.parseObject(professionalListStr, listMap.getClass());
        return listMap;
    }

    @Override
    public List<Map<Long, String>> queryProfessionalNameList(Long parentTreeId) {
        List<SysTree> sysTreeList = sysTreeService.querySysTreeList(parentTreeId, "A", 3);
        List<Professional> professionalList = new ArrayList<>();
        for (SysTree sysTree : sysTreeList) {
            professionalList.add(new Professional(sysTree.getTreeId(), sysTree.getTreeName()));
        }
        String professionalListStr = JSON.toJSONString(professionalList);
        List<Map<Long, String>> listMap = new ArrayList<>();
        listMap = JSON.parseObject(professionalListStr, listMap.getClass());
        return listMap;
    }

    @Override
    public List<SysTreeResponse> queryEncodeCompanyTreeList() {
        List<SysTree> sysTreeList = sysTreeService.querySysTreeList(null, "B", null);
        List<SysTreeResponse> encodeCompanyList = new ArrayList<>();
        sysTreeList.forEach(sysTree -> encodeCompanyList.add(new SysTreeResponse(sysTree.getTreeId(), sysTree.getParentTreeId(), sysTree.getTreeName())));
        List<SysTreeResponse> treeModelList = new ArrayList<>();
        for (SysTreeResponse treeModel : encodeCompanyList) {
            if (null != treeModel.getParentTreeId()) continue;
            List<SysTreeResponse> childrenMenuList = getChildrenTreeModelList(encodeCompanyList, treeModel.getTreeId());
            treeModel.setChildren(childrenMenuList);
            treeModelList.add(treeModel);
        }
        return treeModelList;
    }

    @Override
    public List<SysTreeResponse> queryPositionalTitlesTreeList() {
        List<SysTree> sysTreeList = sysTreeService.querySysTreeList(null, "C", null);
        List<SysTreeResponse> encodeCompanyList = new ArrayList<>();
        sysTreeList.forEach(sysTree -> encodeCompanyList.add(new SysTreeResponse(sysTree.getTreeId(), sysTree.getParentTreeId(), sysTree.getTreeName())));
        List<SysTreeResponse> treeModelList = new ArrayList<>();
        for (SysTreeResponse treeModel : encodeCompanyList) {
            if (null == treeModel.getParentTreeId()) {
                List<SysTreeResponse> childrenMenuList = getChildrenTreeModelList(encodeCompanyList, treeModel.getTreeId());
                treeModel.setChildren(childrenMenuList);
                treeModelList.add(treeModel);
            }
        }
        return treeModelList;
    }

    @Override
    public List<SysTreeResponse> querySpecialtyTreeList() {
        List<SysTree> sysTreeList = sysTreeService.querySysTreeList(null, "A", null);
        List<SysTreeResponse> encodeCompanyList = new ArrayList<>();
        sysTreeList.forEach(sysTree -> encodeCompanyList.add(new SysTreeResponse(sysTree.getTreeId(), sysTree.getParentTreeId(), sysTree.getTreeName())));
        List<SysTreeResponse> treeModelList = new ArrayList<>();
        for (SysTreeResponse treeModel : encodeCompanyList) {
            if (null != treeModel.getParentTreeId()) continue;
            List<SysTreeResponse> childrenMenuList = getChildrenTreeModelList(encodeCompanyList, treeModel.getTreeId());
            treeModel.setChildren(childrenMenuList);
            treeModelList.add(treeModel);
        }
        return treeModelList;
    }


    /**
     * 递归组装树结构
     *
     * @param encodeCompanyList 总数据集合
     * @param parentTreeId      父级ID
     * @return
     */
    private List<SysTreeResponse> getChildrenTreeModelList(List<SysTreeResponse> encodeCompanyList, Long parentTreeId) {
        List<SysTreeResponse> childrenTreeModel = new ArrayList<>();
        for (SysTreeResponse treeModel : encodeCompanyList) {
            if (null == treeModel.getParentTreeId() || !parentTreeId.equals(treeModel.getParentTreeId())) continue;
            List<SysTreeResponse> iterateTreeModel = getChildrenTreeModelList(encodeCompanyList, treeModel.getTreeId());
            treeModel.setChildren(iterateTreeModel.size() == 0 ? null : iterateTreeModel);
            childrenTreeModel.add(treeModel);
        }
        return childrenTreeModel;
    }


    @Override
    public List<MenuResponseVO> queryMenuList(Long clientId) {
        LoginUser logUser = SecurityUtils.getLoginUser();
        SysUser sysUser = logUser.getUser();
        if (clientId == null) clientId = sysUser.getUserId();
        List<MenuResponseVO> responseList = new ArrayList<>();
        ClientBaseInfo clientBaseInfo = baseInfoMapper.selectById(clientId);
        int showFlag11 = 0;
        int showFlag15 = 0;
        int showFlag_10_12 = 0;
        if (clientBaseInfo != null) {
            if (StringUtils.isNotEmpty(clientBaseInfo.getCondConfirm()) && "1".equals(clientBaseInfo.getCondConfirm()))
                showFlag11 = 1;
            if (StringUtils.isNotEmpty(clientBaseInfo.getIsBreakRule()) && (!"0".equals(clientBaseInfo.getIsBreakRule())))
                showFlag15 = 1;
            if (StringUtils.isNotEmpty(clientBaseInfo.getReferralType()) && "1".equals(clientBaseInfo.getReferralType()))
                showFlag_10_12 = 1;
        }
        //根据clientId和表名称查询当前菜单是否存在
        String tableName = "client_base_info";
        int count = baseInfoMapper.selectTableCount(tableName, clientId);
        MenuResponseVO menuResponse1 = new MenuResponseVO("基本情况", "/basicMessage", 1, count > 0 ? 1 : 0);
        responseList.add(menuResponse1);
        count = sysFileInfoService.selectFileCount(clientId, "1");
        MenuResponseVO menuResponse2 = new MenuResponseVO("照片", "/photograph", 1, count > 0 ? 1 : 0);
        responseList.add(menuResponse2);
        tableName = "client_education";
        count = baseInfoMapper.selectTableCount(tableName, clientId);
        MenuResponseVO menuResponse3 = new MenuResponseVO("学历", "/education", 1, count > 0 ? 1 : 0);
        responseList.add(menuResponse3);
        tableName = "client_edu_assessment";
        count = baseInfoMapper.selectTableCount(tableName, clientId);
        MenuResponseVO menuResponse4 = new MenuResponseVO("年度考核&继续教育", "/fillInYearExamine", 1, count > 0 ? 1 : 0);
        responseList.add(menuResponse4);
        tableName = "client_resume";
        count = baseInfoMapper.selectTableCount(tableName, clientId);
        MenuResponseVO menuResponse5 = new MenuResponseVO("从事专业技术工作简历", "/fillInResume", 1, count > 0 ? 1 : 0);
        responseList.add(menuResponse5);
        tableName = "client_skill_award";
        count = baseInfoMapper.selectTableCount(tableName, clientId);
        MenuResponseVO menuResponse6 = new MenuResponseVO("任职期间奖励情况", "/fillInTakeOffice", 1, count > 0 ? 1 : 0);
        responseList.add(menuResponse6);
        tableName = "client_skill_research";
        count = baseInfoMapper.selectTableCount(tableName, clientId);
        MenuResponseVO menuResponse7 = new MenuResponseVO("任职期内科研(业绩)成果", "/fillInTakeAchievement", 1, count > 0 ? 1 : 0);
        responseList.add(menuResponse7);
        tableName = "client_skill_published_paper";
        count = baseInfoMapper.selectTableCount(tableName, clientId);
        MenuResponseVO menuResponse8 = new MenuResponseVO("任期内发表论文论著情况", "/fillInTakeThesis", 1, count > 0 ? 1 : 0);
        responseList.add(menuResponse8);
        tableName = "client_skill_summary";
        count = baseInfoMapper.selectTableCount(tableName, clientId);
        MenuResponseVO menuResponse9 = new MenuResponseVO("任期内专业技术与成果报告", "/fillInTakeReport", 1, count > 0 ? 1 : 0);
        responseList.add(menuResponse9);
        tableName = "client_break_rule";
        count = baseInfoMapper.selectTableCount(tableName, clientId);
        MenuResponseVO menuResponse15 = new MenuResponseVO("破格条件说明", "/fillInConditionBreak", showFlag15, count > 0 ? 1 : 0);
        responseList.add(menuResponse15);
        tableName = "client_referral_type";
        count = baseInfoMapper.selectTableCount(tableName, clientId);
        MenuResponseVO menuResponse10 = new MenuResponseVO("转评条件说明", "/fillInConditionTreatment", showFlag_10_12, count > 0 ? 1 : 0);
        responseList.add(menuResponse10);
        tableName = "client_qualification_confirm";
        count = baseInfoMapper.selectTableCount(tableName, clientId);
        MenuResponseVO menuResponse11 = new MenuResponseVO("资格确认", "/fillInQualificationOK", showFlag11, count > 0 ? 1 : 0);
        responseList.add(menuResponse11);
        tableName = "client_exchange_faculty";
        count = baseInfoMapper.selectTableCount(tableName, clientId);
        MenuResponseVO menuResponse12 = new MenuResponseVO("转换系列", "/fillInTransitionSeries", showFlag_10_12, count > 0 ? 1 : 0);
        responseList.add(menuResponse12);
        MenuResponseVO menuResponse13 = new MenuResponseVO("证件电子图片", "/fillUpload", 1, sysFileInfoService.selectFileCount(clientId, "ALLCardImgs") > 0 ? 1 : 0);
        responseList.add(menuResponse13);
        MenuResponseVO menuResponse14 = new MenuResponseVO("评审申报材料", "/fillInMaterialsFileUpload", 1, sysFileInfoService.selectFileCount(clientId, "ALLRecordDatums") > 0 ? 1 : 0);
        responseList.add(menuResponse14);
        /*后端新增菜单*/
        MenuResponseVO menuResponse16 = new MenuResponseVO("完成并送审", "/fillInAppraisalOK", 1, 0);
        responseList.add(menuResponse16);
//        MenuResponseVO menuResponse17 = new MenuResponseVO("其他", "/fillInRest", 1, 0);
//        responseList.add(menuResponse17);
        /*后端新增菜单*/
        return responseList;
    }

    @Data
    @NoArgsConstructor
    class Professional implements Serializable {
        private Long treeId;
        private String treeName;

        public Professional(Long treeId, String treeName) {
            this.treeId = treeId;
            this.treeName = treeName;
        }
    }

    @Override
    public int queryTableCount(String tableName, Long clientId) {
        int count = baseInfoMapper.selectTableCount(tableName, clientId);
        return count;
    }


    /**
     * 根据用户clientId修改状态
     *
     * @param clientId
     * @param status
     * @return
     */
    @Override
    public int updateStatusByClientId(Long clientId, Integer status, Long orderNo) {
        LambdaUpdateWrapper<ClientBaseInfo> luw = Wrappers.lambdaUpdate();
        luw.eq(ClientBaseInfo::getClientId, clientId);
        luw.set(ClientBaseInfo::getReportStatus, status);
        luw.set(ClientBaseInfo::getOrderNo, orderNo);
        return baseInfoMapper.update(null, luw);
    }

    @Override
    @DataScope(deptAlias = "d", userAlias = "c", userType = "client_id")
    public List<BasePositionApplyResponseVO> queryPageList(ClientBaseInfo clientBaseInfo) {
        clientBaseInfo.getParams().put("orderColumn", " c.update_time ");
        clientBaseInfo.getParams().put("orderTurn", " DESC ");
        if (2 == clientBaseInfo.getModuleType()) {//申报填写
            clientBaseInfo.setFillInUserId(SecurityUtils.getLoginUser().getUser().getUserId());
        } else if (3 == clientBaseInfo.getModuleType()) {//申报审核
            clientBaseInfo.setReportStatus(10);
        } else if (4 == clientBaseInfo.getModuleType()) {//申报复核
            clientBaseInfo.setReportStatus(20);
        } else if (5 == clientBaseInfo.getModuleType()) {//申报查询
            clientBaseInfo.getParams().put("defaultOrderColumn", " c.order_no ");
            clientBaseInfo.getParams().put("defaultOrderTurn", " ASC ");
        }else if(6 == clientBaseInfo.getModuleType()){//机器人申报查询
            clientBaseInfo.getParams().put("defaultOrderColumn", " c.robot_report_time ");
            clientBaseInfo.getParams().put("defaultOrderTurn", " DESC ");
        }
        List<BasePositionApplyResponseVO> positionApplyResponseList = baseInfoMapper.selectClientBaseInfoList(clientBaseInfo);
        return positionApplyResponseList;
    }

    @Override
    public int fakeDeleteBaseByClientIdArr(Long[] clientIds) {
        if (clientIds == null && clientIds.length == 0) {
            return 0;
        }
        LambdaUpdateWrapper<ClientBaseInfo> luw = Wrappers.lambdaUpdate();
        luw.set(ClientBaseInfo::getIsDeleted, -1);
        luw.in(ClientBaseInfo::getClientId, clientIds);
        return baseInfoMapper.update(null, luw);
    }

    @Override
    public boolean updateBatch(BatchSetNominateUnitVO batchSetNominateUnitVO) {
        TorsNominateUnit torsNominateUnit = torsNominateUnitService.getById(batchSetNominateUnitVO.getNominateUnitId());
        List<ClientBaseInfo> batchSetNominateUnitClientList = new ArrayList<>();
        for (String clientId : batchSetNominateUnitVO.getClientIds().split(",")) {
            ClientBaseInfo clientBaseInfo = new ClientBaseInfo();
            clientBaseInfo.setClientId(Long.valueOf(clientId));
            clientBaseInfo.setNominateUnit(torsNominateUnit.getUnitName());
            clientBaseInfo.setUnitAuthCode(torsNominateUnit.getUnitAuthCode());
            batchSetNominateUnitClientList.add(clientBaseInfo);
        }
        return saveOrUpdateBatch(batchSetNominateUnitClientList);
    }

    //批量设置工作单位
    @Override
    public boolean updateBatch(BatchSetWorkUnitVO batchSetWorkUnitVO) {
        TorsWorkUnit torsWorkUnit = torsWorkUnitService.getById(batchSetWorkUnitVO.getWorkUnitId());
        List<ClientBaseInfo> batchSetWorkUnitClientList = new ArrayList<>();
        for (String clientId : batchSetWorkUnitVO.getClientIds().split(",")) {
            ClientBaseInfo clientBaseInfo = new ClientBaseInfo();
            clientBaseInfo.setClientId(Long.valueOf(clientId));
            clientBaseInfo.setJobUnit(torsWorkUnit.getUnitName());
            batchSetWorkUnitClientList.add(clientBaseInfo);
        }
        return saveOrUpdateBatch(batchSetWorkUnitClientList);
    }

    @Override
    public boolean updateBatch(BatchSetFillInUserVO batchSetFillInUserVO) {
        SysUser fillInUser = sysUserService.selectUserById(batchSetFillInUserVO.getFillInUserId());
        List<ClientBaseInfo> batchSetFillInUserList = new ArrayList<>();
        for (String clientId : batchSetFillInUserVO.getClientIds().split(",")) {
            ClientBaseInfo clientBaseInfo = new ClientBaseInfo();
            clientBaseInfo.setClientId(Long.valueOf(clientId));
            clientBaseInfo.setDistributeStatus("1");//分配状态 0 未分配 1 已分配 -1 退回
            clientBaseInfo.setFillInUserId(fillInUser.getUserId());
            clientBaseInfo.setFillInUserName(fillInUser.getNickName());
            clientBaseInfo.setOrderNo(80l);//设置排序
            batchSetFillInUserList.add(clientBaseInfo);
        }
        return saveOrUpdateBatch(batchSetFillInUserList);
    }

    @Override
    public boolean updateBatch(BatchDistributeBackVO batchDistributeBackVO) {
        boolean retBoolean = false;
        for (String clientId : batchDistributeBackVO.getClientIds().split(",")) {
            ClientBaseInfo clientBaseInfo = new ClientBaseInfo();
            LambdaUpdateWrapper<ClientBaseInfo> updateWrapper = new LambdaUpdateWrapper<>();
            updateWrapper.set(ClientBaseInfo::getDistributeStatus, "-1");//分配状态 0 未分配 1 已分配 -1 退回
            updateWrapper.set(ClientBaseInfo::getFillInUserId, null);
            updateWrapper.set(ClientBaseInfo::getFillInUserName, null);
            updateWrapper.set(ClientBaseInfo::getDistributeBackReason, batchDistributeBackVO.getDistributeBackReason());
            updateWrapper.set(ClientBaseInfo::getOrderNo, 100l);//设置排序
            updateWrapper.eq(ClientBaseInfo::getClientId, Long.valueOf(clientId));
            retBoolean = update(clientBaseInfo, updateWrapper);
            if (!retBoolean) {
                return false;
            }
        }
        return retBoolean;
    }

    @Override
    public boolean updateBatch(BatchBackClientReportStatusVO batchBackClientReportStatusVO){
        boolean retBoolean = false;
        for (String clientId : batchBackClientReportStatusVO.getClientIds().split(",")) {
            ClientBaseInfo clientBaseInfo = new ClientBaseInfo();
            LambdaUpdateWrapper<ClientBaseInfo> updateWrapper = new LambdaUpdateWrapper<>();
            updateWrapper.set(ClientBaseInfo::getRobotReportStatus,0);
            updateWrapper.set(ClientBaseInfo::getRobotReportExceptionTimes, 0);
            updateWrapper.set(ClientBaseInfo::getRobotReportExceptionCode, null);
            updateWrapper.set(ClientBaseInfo::getRobotReportExceptionMsg, null);
            updateWrapper.set(ClientBaseInfo::getRobotReportTime, null);
            updateWrapper.set(ClientBaseInfo::getReportStatus, batchBackClientReportStatusVO.getReportStatus());
            if(batchBackClientReportStatusVO.getReportStatus()==0){
                updateWrapper.set(ClientBaseInfo::getOrderNo, 80l);
            }else if(batchBackClientReportStatusVO.getReportStatus()==10){
                updateWrapper.set(ClientBaseInfo::getOrderNo, 60l);
            }else if(batchBackClientReportStatusVO.getReportStatus()==20){
                updateWrapper.set(ClientBaseInfo::getOrderNo, 40l);
            }
            updateWrapper.eq(ClientBaseInfo::getClientId, Long.valueOf(clientId));
            retBoolean = update(clientBaseInfo, updateWrapper);
            if (!retBoolean) {
                return false;
            }
        }
        return retBoolean;
    }

    //批量重置机器人上报
    @Override
    public boolean updateBatch(BatchRestRobotReportVO batchRestRobotReportVO){
        boolean retBoolean = false;
        for (String clientId : batchRestRobotReportVO.getClientIds().split(",")) {
            ClientBaseInfo clientBaseInfo = new ClientBaseInfo();
            LambdaUpdateWrapper<ClientBaseInfo> updateWrapper = new LambdaUpdateWrapper<>();
            updateWrapper.set(ClientBaseInfo::getRobotReportStatus,0);
            updateWrapper.set(ClientBaseInfo::getRobotReportExceptionTimes, 0);
            updateWrapper.set(ClientBaseInfo::getRobotReportExceptionCode, null);
            updateWrapper.set(ClientBaseInfo::getRobotReportExceptionMsg, null);
            updateWrapper.set(ClientBaseInfo::getRobotReportTime, null);
            updateWrapper.set(ClientBaseInfo::getOrderNo, 20l);
            updateWrapper.eq(ClientBaseInfo::getClientId, Long.valueOf(clientId));
            retBoolean = update(clientBaseInfo, updateWrapper);
            if(retBoolean){//删除机器人上报tag
                LambdaUpdateWrapper<ClientRobotReportTag> deleteWrapper = new LambdaUpdateWrapper<>();
                deleteWrapper.eq(ClientRobotReportTag::getClientId,Long.valueOf(clientId));
                clientRobotReportTagService.remove(deleteWrapper);
            }
            if (!retBoolean) {
                return false;
            }
        }
        return retBoolean;
    };

    @Override
    public boolean updateGovAccount(GovAccountVO govAccountVO){
        ClientBaseInfo clientBaseInfo = new ClientBaseInfo();
        LambdaUpdateWrapper<ClientBaseInfo> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.set(ClientBaseInfo::getAccount, govAccountVO.getAccount());
        updateWrapper.set(ClientBaseInfo::getPassword, govAccountVO.getPassword());
        updateWrapper.eq(ClientBaseInfo::getClientId, govAccountVO.getClientId());
        boolean retBoolean = update(clientBaseInfo, updateWrapper);
        clientQualificationConfirmService.resetDatumPDFUrl(govAccountVO.getClientId());
        return retBoolean;
    }

    @Override
    public List<FillInStatisticsResponseVO> fillInStatistics(ClientBaseInfo clientBaseInfo) {
        List<FillInStatisticsResponseVO> retFillInStatisticsResponseVOList = new ArrayList<>();
        Set<Long> fillInUserIdSet = new HashSet();
        Map<String, Integer> statisticsDataMap = new HashMap<>();
        List<FillInStatisticsBO> fillInStatisticsList = baseInfoMapper.fillInStatistics(clientBaseInfo);
        for (FillInStatisticsBO fillInStatisticsBO : fillInStatisticsList) {
            fillInUserIdSet.add(fillInStatisticsBO.getFillInUserId());
            String mapKey = fillInStatisticsBO.getFillInUserId() + ":" + fillInStatisticsBO.getFillInDate();
            statisticsDataMap.put(mapKey, fillInStatisticsBO.getCount());
        }
        //获取日期区间
        List<String> betweenDays = com.ruoyi.common.utils.DateUtils.getBetweenDays(clientBaseInfo.getFillBeginDate(), clientBaseInfo.getFillEndDate());
        Collections.reverse(betweenDays);//倒序排列
        for (Long fillInUserId : fillInUserIdSet) {
            FillInStatisticsResponseVO fillInStatisticsResponseVO = new FillInStatisticsResponseVO();
            SysUser fillInUser = sysUserService.selectUserById(fillInUserId);
            if (fillInUser != null) {
                fillInStatisticsResponseVO.setFillInUserId(fillInUserId);
                fillInStatisticsResponseVO.setFillInUser(fillInUser);
                fillInStatisticsResponseVO.setPhonenumber(fillInUser.getPhonenumber());
                fillInStatisticsResponseVO.setNickName(fillInUser.getNickName());
                fillInStatisticsResponseVO.setUserName(fillInUser.getUserName());
                if (fillInUser.getDept() != null) {
                    fillInStatisticsResponseVO.setDeptName(fillInUser.getDept().getDeptName());
                }
            }
            int sum=0;
            List<FillInStatisticsItemResponseVO> itemList = new ArrayList<>();
            for (String statisticsDay : betweenDays) {
                String key = fillInUserId + ":" + statisticsDay;
                FillInStatisticsItemResponseVO item = new FillInStatisticsItemResponseVO(statisticsDay, statisticsDataMap.get(key) != null ? statisticsDataMap.get(key) : 0);
                itemList.add(item);
                sum+=item.getCount();
            }
            fillInStatisticsResponseVO.setItems(itemList);
            fillInStatisticsResponseVO.setSum(sum);
            retFillInStatisticsResponseVOList.add(fillInStatisticsResponseVO);
        }
        return retFillInStatisticsResponseVOList;
    }

    @Override
    public List<BaseInfoExcelImportResponseVO> exportListByParam(ClientBaseInfo baseInfo) {
        LambdaQueryWrapper<ClientBaseInfo> lam = Wrappers.lambdaQuery();
        if (StringUtils.isNotBlank(baseInfo.getName())) {
            lam.like(ClientBaseInfo::getName, baseInfo.getName());
        }
        if (StringUtils.isNotBlank(baseInfo.getMobile())) {
            lam.like(ClientBaseInfo::getMobile, baseInfo.getMobile());
        }
        if (StringUtils.isNotBlank(baseInfo.getIdcard())) {
            lam.like(ClientBaseInfo::getIdcard, baseInfo.getIdcard());
        }
        if (StringUtils.isNotBlank(baseInfo.getDistributeStatus())) {
            lam.like(ClientBaseInfo::getDistributeStatus, baseInfo.getDistributeStatus());
        }
        if (baseInfo.getDeptId() != null && baseInfo.getDeptId() != 0) {
            lam.eq(ClientBaseInfo::getDeptId, baseInfo.getDeptId());
        }
        if (StringUtils.isNotBlank(baseInfo.getJobUnit())) {
            lam.like(ClientBaseInfo::getJobUnit, baseInfo.getJobUnit());
        }
        if (StringUtils.isNotBlank(baseInfo.getNominateUnit())) {
            lam.like(ClientBaseInfo::getNominateUnit, baseInfo.getNominateUnit());
        }
        if (baseInfo.getFillInUserId() != null && baseInfo.getFillInUserId() != 0) {
            lam.eq(ClientBaseInfo::getFillInUserId, baseInfo.getFillInUserId());
        }
        lam.le(ClientBaseInfo::getRobotReportStatus,0);
        lam.eq(ClientBaseInfo::getIsDeleted, "1");
        lam.orderByAsc(ClientBaseInfo::getCreateTime);
        List<ClientBaseInfo> list = this.list(lam);
        Map<Long, String> map = new HashMap<>();
        SysDeptMapper bean = SpringUtils.getBean(SysDeptMapper.class);
        List<SysDept> sysDepts = bean.selectDeptList(new SysDept());
        sysDepts.forEach(dept -> {
            map.put(dept.getDeptId(), dept.getDeptName());
        });
        AtomicLong atomicLong = new AtomicLong(1);
        return list.stream().map(e -> {
            BaseInfoExcelImportResponseVO responseVO = new BaseInfoExcelImportResponseVO();
            org.springframework.beans.BeanUtils.copyProperties(e, responseVO);
            responseVO.setDeptId(map.get(e.getDeptId()));
            responseVO.setClientId(atomicLong.getAndIncrement());
            return responseVO;
        }).collect(Collectors.toList());
    }

    @Override
    public String importBaseInfo(List<BaseInfoExcelImportResponseVO> excelData, String operName) {
        if (com.ruoyi.common.utils.StringUtils.isNull(excelData) || excelData.size() == 0) {
            throw new CustomException("导入数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (BaseInfoExcelImportResponseVO responseData : excelData) {
            try {
                if (responseData == null) {
                    continue;
                }
                String idcard = responseData.getIdcard();
                if (StringUtils.isBlank(idcard)) {
                    throw new ParamsCheckException("身份证号码不能为空");
                }
                // 验证身份证是否存在
                ClientBaseInfo baseInfo = exitsByIdCard(responseData.getIdcard());
                if (baseInfo != null) {
                    baseInfo.setName(StringUtils.isBlank(responseData.getName()) ? baseInfo.getName() : responseData.getName());
                    baseInfo.setMobile(responseData.getMobile());
                    baseInfo.setDeptId(StringUtils.isBlank(responseData.getDeptId()) ? baseInfo.getDeptId() : Long.parseLong(responseData.getDeptId()));
                    TorsNominateUnit torsNominateUnit = conversionUnit(responseData.getNominateUnit());
                    baseInfo.setNominateUnit(torsNominateUnit == null ? baseInfo.getNominateUnit() : torsNominateUnit.getUnitName());
                    baseInfo.setUnitAuthCode(torsNominateUnit == null ? baseInfo.getUnitAuthCode() : torsNominateUnit.getUnitAuthCode());
                    baseInfo.setJobUnit(StringUtils.isBlank(responseData.getJobUnit()) ? baseInfo.getJobUnit() : responseData.getJobUnit());
                    SysUser user = conversionUser(responseData.getFillInUserName());
                    baseInfo.setFillInUserId(user == null ? baseInfo.getFillInUserId() : user.getUserId());
                    baseInfo.setFillInUserName(user == null ? baseInfo.getFillInUserName() : user.getNickName());
                    baseInfo.setDistributeStatus(user == null ? baseInfo.getDistributeStatus() : "1");//分配状态 0 未分配 1 已分配 -1 退回
                    baseInfo.setOrderNo(user==null ? baseInfo.getOrderNo() : 80l);//设置排序
                    baseInfo.setIsDeleted(1);
                    baseInfo.setUpdateBy(operName);
                    baseInfo.setUpdateTime(new Date());
                    this.updateById(baseInfo);
                    clientRobotReportTagService.resetReportTag(baseInfo.getClientId(),"menu_jbxx");//重置上报Tag
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、基础信息 " + baseInfo.getName() + " 更新成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、基础信息 " + responseData.getName() + " 不存在");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、信息 " + responseData.getName() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new CustomException(failureMsg.toString());
        } else {
            successMsg.insert(0, "导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

    private SysUser conversionUser(String fillInUserName) {
        SysUserMapper bean = SpringUtils.getBean(SysUserMapper.class);
        SysUser user = new SysUser();
        user.setNickName(fillInUserName);
        List<SysUser> sysUser = bean.selectUserByNickName(fillInUserName);
        if (sysUser.isEmpty()) {
            return null;
        }
        return sysUser.get(0);
    }

    private String conversionJobUnit(String jobUnit, String old) {
        return null;
    }

    private TorsNominateUnit conversionUnit(String nominateUnit) {
        ITorsNominateUnitService bean = SpringUtils.getBean(ITorsNominateUnitService.class);
        List<TorsNominateUnit> list = bean.lambdaQuery()
                .eq(TorsNominateUnit::getUnitName, nominateUnit)
                .eq(TorsNominateUnit::getIsDeleted, "1")
                .list();
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    private Long conversionDept(String deptId, Long old) {
        if (StringUtils.isBlank(deptId)) {
            return old;
        }
        SysDept dept = new SysDept();
        dept.setDeptName(deptId);
        SysDeptMapper bean = SpringUtils.getBean(SysDeptMapper.class);
        List<SysDept> sysDepts = bean.selectDeptList(dept);
        if (sysDepts.isEmpty()) {
            return old;
        }
        return sysDepts.get(0).getDeptId();
    }

    private ClientBaseInfo exitsByIdCard(String idcard) {
        LambdaQueryWrapper<ClientBaseInfo> lam = Wrappers.lambdaQuery();
        lam.eq(ClientBaseInfo::getIdcard, idcard);
        List<ClientBaseInfo> list = this.list(lam);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    //获取某个客户机器人上报日志
    @Override
    public List<RobotReportLogResponseVO> queryRobotReportLogByClientId(Long clientId){
        ClientRobotReportLog clientRobotReportLog=new ClientRobotReportLog();
        clientRobotReportLog.setClientId(clientId);
        List<ClientRobotReportLog>  clientRobotReportLogList = robotReportLogService.queryList(clientRobotReportLog);
        List<RobotReportLogResponseVO> voList = clientRobotReportLogList.stream()
                .map(any -> BeanUtil.toBean(any, RobotReportLogResponseVO.class))
                .collect(Collectors.toList());
        return voList;
    }

    //根据推荐单位名称，修改授权码
    public boolean updateByTorsNominateUnit(TorsNominateUnit torsNominateUnit){
        boolean retBoolean = false;
        LambdaUpdateWrapper<ClientBaseInfo> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.set(ClientBaseInfo::getUnitAuthCode,torsNominateUnit.getUnitAuthCode());
        updateWrapper.eq(ClientBaseInfo::getNominateUnit, torsNominateUnit.getUnitName());
        retBoolean = update(null, updateWrapper);
        if (!retBoolean) {
            return false;
        }
        return retBoolean;
    }
}
