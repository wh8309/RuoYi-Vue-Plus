package com.ruoyi.website.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.website.domain.ClientSkillPublishedPaper;
import com.ruoyi.website.domain.vo.request.SkillPublishedQueryRequestVO;
import com.ruoyi.website.domain.vo.request.SkillPublishedRequestVO;
import com.ruoyi.website.domain.vo.response.SkillPublishedResponseVO;

import java.util.List;

/**
 * 任期内发论文论著情况Service接口
 *
 * @author ruoyi
 * @date 2021-03-18
 */
public interface IClientSkillPublishedPaperService extends IService<ClientSkillPublishedPaper> {


    List<SkillPublishedResponseVO> queryList(SkillPublishedQueryRequestVO skillPublishedQueryRequestVO);

    int insertOrUpdate(SkillPublishedRequestVO skillPublishedRequestVO);
}
