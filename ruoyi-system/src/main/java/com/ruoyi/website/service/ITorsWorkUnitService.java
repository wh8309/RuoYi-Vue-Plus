package com.ruoyi.website.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.website.domain.TorsWorkUnit;
import com.ruoyi.website.domain.vo.response.TorsWorkUnitExcelImportResponseVO;
import com.ruoyi.website.domain.vo.response.TorsWorkUnitResponseVO;

import java.util.List;

/**
 * 推荐单位Service接口
 *
 * @author ruoyi
 * @date 2021-04-22
 */
public interface ITorsWorkUnitService extends IService<TorsWorkUnit> {

    List<TorsWorkUnitResponseVO> selectWorkUnitByPage(TorsWorkUnit torsWorkUnit);

    boolean saveWorkUnit(TorsWorkUnit torsWorkUnit);

    boolean updateWorkUnit(TorsWorkUnit torsWorkUnit);

    boolean deleteTorsWorkUnitByIds(Long[] workUnitIds);


    String importWorkUnit(List<TorsWorkUnitExcelImportResponseVO> excelData, boolean updateSupport, String operName);

    List<TorsWorkUnitExcelImportResponseVO> exportListByParam(TorsWorkUnit workUnit);
}
