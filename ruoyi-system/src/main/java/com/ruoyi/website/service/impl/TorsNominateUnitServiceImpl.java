package com.ruoyi.website.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.github.pagehelper.Page;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.common.utils.DropDownBoxUnit;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.website.domain.ClientProfile;
import com.ruoyi.website.domain.vo.request.ClientProfileStatusUpdateRequestVO;
import com.ruoyi.website.domain.vo.request.TorsNominateUnitSmsEnableRequestVO;
import com.ruoyi.website.domain.vo.response.TorsNominateUnitExcelImportResponseVO;
import com.ruoyi.website.domain.vo.response.TorsNominateUnitResponseVO;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.apache.commons.lang3.StringUtils;
import com.ruoyi.website.mapper.TorsNominateUnitMapper;
import com.ruoyi.website.domain.TorsNominateUnit;
import com.ruoyi.website.service.ITorsNominateUnitService;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

/**
 * 推荐单位Service业务层处理
 *
 * @author ruoyi
 * @date 2021-04-22
 */
@Service
public class TorsNominateUnitServiceImpl extends ServiceImpl<TorsNominateUnitMapper, TorsNominateUnit> implements ITorsNominateUnitService {

    @Override
    public List<TorsNominateUnitResponseVO> queryList(TorsNominateUnit torsNominateUnit) {
        LambdaQueryWrapper<TorsNominateUnit> lqw = Wrappers.lambdaQuery();
        if (StringUtils.isNotBlank(torsNominateUnit.getUnitName())) {
            lqw.like(TorsNominateUnit::getUnitName, torsNominateUnit.getUnitName());
        }
        if (StringUtils.isNotBlank(torsNominateUnit.getUnitAddress())) {
            lqw.like(TorsNominateUnit::getUnitAddress, torsNominateUnit.getUnitAddress());
        }
        if (StringUtils.isNotBlank(torsNominateUnit.getUnitContact())) {
            lqw.like(TorsNominateUnit::getUnitContact, torsNominateUnit.getUnitContact());
        }
        if (StringUtils.isNotBlank(torsNominateUnit.getUnitPhone())) {
            lqw.like(TorsNominateUnit::getUnitPhone, torsNominateUnit.getUnitPhone());
        }
        if (StringUtils.isNotBlank(torsNominateUnit.getUnitAuthCode())) {
            lqw.like(TorsNominateUnit::getUnitAuthCode, torsNominateUnit.getUnitAuthCode());
        }
        if (StringUtils.isNotBlank(torsNominateUnit.getAccount())) {
            lqw.like(TorsNominateUnit::getAccount, torsNominateUnit.getAccount());
        }
        if(torsNominateUnit.getSmsEnable()!=null){
            lqw.eq(TorsNominateUnit::getSmsEnable,torsNominateUnit.getSmsEnable());
        }
        lqw.eq(TorsNominateUnit::getIsDeleted, 1);
        lqw.orderByDesc(TorsNominateUnit::getCreateTime);
        return entity2Vo(this.list(lqw));
    }

    @Override
    public boolean save(TorsNominateUnit entity) {
        boolean ret = super.save(entity);
        init();
        return ret;
    }

    @Override
    public boolean updateById(TorsNominateUnit entity) {
        boolean ret = super.updateById(entity);
        init();
        return ret;
    }

    @Override
    public boolean deleteBatch(List<Long> idList) {
        List<TorsNominateUnit> torsNominateUnitList = new ArrayList<>();
        for (Long nominateUnitId : idList) {
            TorsNominateUnit torsNominateUnit = new TorsNominateUnit();
            torsNominateUnit.setNominateUnitId(nominateUnitId);
            torsNominateUnit.setIsDeleted(-1);
            torsNominateUnitList.add(torsNominateUnit);
            DropDownBoxUnit.deleteCache(nominateUnitId, Constants.CACHE_NOMINATE_UNIT);
        }
        boolean ret = this.saveOrUpdateBatch(torsNominateUnitList);
        init();
        return ret;
    }

    @Override
    public List<TorsNominateUnitExcelImportResponseVO> exportListByParam(TorsNominateUnit torsNominateUnit) {
        LambdaQueryWrapper<TorsNominateUnit> lam = Wrappers.lambdaQuery();
        if (StringUtils.isNotBlank(torsNominateUnit.getUnitName())) {
            lam.like(TorsNominateUnit::getUnitName, torsNominateUnit.getUnitName());
        }
        if (StringUtils.isNotBlank(torsNominateUnit.getUnitAddress())) {
            lam.like(TorsNominateUnit::getUnitAddress, torsNominateUnit.getUnitAddress());
        }
        if (StringUtils.isNotBlank(torsNominateUnit.getUnitContact())) {
            lam.like(TorsNominateUnit::getUnitContact, torsNominateUnit.getUnitContact());
        }
        if (StringUtils.isNotBlank(torsNominateUnit.getUnitPhone())) {
            lam.like(TorsNominateUnit::getUnitPhone, torsNominateUnit.getUnitPhone());
        }
        if (StringUtils.isNotBlank(torsNominateUnit.getUnitAuthCode())) {
            lam.like(TorsNominateUnit::getUnitAuthCode, torsNominateUnit.getUnitAuthCode());
        }
        if (StringUtils.isNotBlank(torsNominateUnit.getAccount())) {
            lam.like(TorsNominateUnit::getAccount, torsNominateUnit.getAccount());
        }
        if(torsNominateUnit.getSmsEnable()!=null){
            lam.eq(TorsNominateUnit::getSmsEnable,torsNominateUnit.getSmsEnable());
        }
        lam.eq(TorsNominateUnit::getIsDeleted, "1");
        lam.orderByAsc(TorsNominateUnit::getCreateTime);
        List<TorsNominateUnit> list = this.list(lam);
        AtomicLong atomicLong = new AtomicLong(1);
        return list.stream().map(e -> {
            TorsNominateUnitExcelImportResponseVO responseVO = new TorsNominateUnitExcelImportResponseVO();
            BeanUtils.copyProperties(e, responseVO);
            responseVO.setNominateUnitId(atomicLong.getAndIncrement());
            return responseVO;
        }).collect(Collectors.toList());
    }

    @Override
    public String importNominateUnit(List<TorsNominateUnitExcelImportResponseVO> excelData, boolean updateSupport, String operName) {
        if (com.ruoyi.common.utils.StringUtils.isNull(excelData) || excelData.size() == 0) {
            throw new CustomException("导入数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (TorsNominateUnitExcelImportResponseVO responseData : excelData) {
            try {
                // 验证是否存在
                TorsNominateUnit torsNominateUnit = exitsByUnitAuthCode(responseData.getUnitAuthCode());
                if (torsNominateUnit == null) {
                    torsNominateUnit = new TorsNominateUnit();
                    org.springframework.beans.BeanUtils.copyProperties(responseData, torsNominateUnit);
                    torsNominateUnit.setNominateUnitId(null);
                    torsNominateUnit.setCreateBy(operName);
                    torsNominateUnit.setCreateTime(new Date());
                    torsNominateUnit.setIsDeleted(1);
                    this.save(torsNominateUnit);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、推荐单位 " + torsNominateUnit.getUnitName() + " 导入成功");
                } else if (updateSupport) {
                    Long nominateUnitId = torsNominateUnit.getNominateUnitId();
                    BeanUtils.copyProperties(responseData, torsNominateUnit);
                    torsNominateUnit.setNominateUnitId(nominateUnitId);
                    torsNominateUnit.setIsDeleted(1);
                    torsNominateUnit.setUpdateBy(operName);
                    torsNominateUnit.setUpdateTime(new Date());
                    this.updateById(torsNominateUnit);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、推荐单位 " + torsNominateUnit.getUnitName() + " 更新成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、推荐单位 " + torsNominateUnit.getUnitName() + " 已存在");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、账号 " + responseData.getUnitName() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new CustomException(failureMsg.toString());
        } else {
            successMsg.insert(0, "导入成功！共 " + successNum + " 条，数据如下：");
        }
        init();
        return successMsg.toString();
    }

    private TorsNominateUnit exitsByUnitAuthCode(String unitAuthCode) {
        LambdaQueryWrapper<TorsNominateUnit> lam = Wrappers.lambdaQuery();
        lam.eq(TorsNominateUnit::getUnitAuthCode, unitAuthCode);
        List<TorsNominateUnit> list = this.list(lam);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    /**
     * 实体类转化成视图对象
     *
     * @param collection 实体类集合
     * @return
     */
    private List<TorsNominateUnitResponseVO> entity2Vo(Collection<TorsNominateUnit> collection) {
        List<TorsNominateUnitResponseVO> voList = collection.stream()
                .map(any -> BeanUtil.toBean(any, TorsNominateUnitResponseVO.class))
                .collect(Collectors.toList());
        if (collection instanceof Page) {
            Page<TorsNominateUnit> page = (Page<TorsNominateUnit>) collection;
            Page<TorsNominateUnitResponseVO> pageVo = new Page<>();
            BeanUtil.copyProperties(page, pageVo);
            pageVo.addAll(voList);
            voList = pageVo;
        }
        return voList;
    }

    @PostConstruct
    public void init() {
        DropDownBoxUnit.clearCache(Constants.CACHE_NOMINATE_UNIT);
        List<TorsNominateUnit> list = this.lambdaQuery().eq(TorsNominateUnit::getIsDeleted, "1").list();
        if (!list.isEmpty()) {
            Map<Long, String> collect = list.stream()
                    .collect(Collectors.toMap(TorsNominateUnit::getNominateUnitId, TorsNominateUnit::getUnitName));
            DropDownBoxUnit.setCacheList(Constants.CACHE_NOMINATE_UNIT, collect);
        }
    }

    @Override
    public int updateSmsEnable(TorsNominateUnitSmsEnableRequestVO requestVO) {
        TorsNominateUnit torsNominateUnit = getById(requestVO.getNominateUnitId());
        torsNominateUnit.setSmsEnable(requestVO.getSmsEnable());
        torsNominateUnit.setUpdateBy(SecurityUtils.getUsername());
        torsNominateUnit.setUpdateTime(new Date());
        if(updateById(torsNominateUnit)){
            return 1;
        }
        return 0;
    }

}
