package com.ruoyi.website.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.apache.commons.lang3.StringUtils;
import com.ruoyi.website.mapper.ClientRobotReportLogMapper;
import com.ruoyi.website.domain.ClientRobotReportLog;
import com.ruoyi.website.service.IClientRobotReportLogService;

import java.util.List;
import java.util.Map;

/**
 * 机器人上报流水Service业务层处理
 *
 * @author ruoyi
 * @date 2021-06-11
 */
@Service
public class ClientRobotReportLogServiceImpl extends ServiceImpl<ClientRobotReportLogMapper, ClientRobotReportLog> implements IClientRobotReportLogService {

    @Override
    public List<ClientRobotReportLog> queryList(ClientRobotReportLog clientRobotReportLog) {
        LambdaQueryWrapper<ClientRobotReportLog> lqw = Wrappers.lambdaQuery();
        if (clientRobotReportLog.getClientId() != null){
            lqw.eq(ClientRobotReportLog::getClientId ,clientRobotReportLog.getClientId());
        }
        if (StringUtils.isNotBlank(clientRobotReportLog.getName())){
            lqw.like(ClientRobotReportLog::getName ,clientRobotReportLog.getName());
        }
        if (StringUtils.isNotBlank(clientRobotReportLog.getIdcard())){
            lqw.eq(ClientRobotReportLog::getIdcard ,clientRobotReportLog.getIdcard());
        }
        if (StringUtils.isNotBlank(clientRobotReportLog.getOperModule())){
            lqw.eq(ClientRobotReportLog::getOperModule ,clientRobotReportLog.getOperModule());
        }
        if (StringUtils.isNotBlank(clientRobotReportLog.getOperDecription())){
            lqw.eq(ClientRobotReportLog::getOperDecription ,clientRobotReportLog.getOperDecription());
        }
        lqw.orderByDesc(ClientRobotReportLog::getCreateTime);
        return this.list(lqw);
    }
}
