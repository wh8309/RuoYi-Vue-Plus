package com.ruoyi.website.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.website.domain.SysFileInfo;
import com.ruoyi.website.domain.vo.request.FileQueryRequestVO;
import com.ruoyi.website.domain.vo.request.FileUploadRequestVO;
import org.springframework.scheduling.annotation.Async;

import java.util.List;
import java.util.Map;

/**
 * 文件信息Service接口
 *
 * @author ruoyi
 * @date 2021-03-18
 */
public interface ISysFileInfoService extends IService<SysFileInfo> {


    //查询文件
    public List<SysFileInfo> selectFileList(FileQueryRequestVO fileQueryRequestVO);

    //根据文件类型查询上传数量
    public int  selectFileCount(Long clientId,String fileType);

    //上传文件
    public SysFileInfo insertFile(Map<String,String> fileMap, FileUploadRequestVO fileUploadRequestVO,SysFileInfo sysFileInfo);

    //编辑文件
    public  SysFileInfo updateFile(Map<String,String> fileMap, SysFileInfo sysFileInfo);

    //删除文件
    public int deleteFileById(String fileId);

    //根据业务business_id和file_type删除
    public int deleteFileByBusinessId(Long businessId,String fileType);

    //异步上传云存储上
    @Async("threadPoolTaskExecutor")
    public void uploadFileToCloud(String key, String filename);

    //获取云上URL
    public String getCloudVisitUrl(String cloudStoragePath);





}
