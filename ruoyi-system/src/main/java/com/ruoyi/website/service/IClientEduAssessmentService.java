package com.ruoyi.website.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.website.domain.ClientEduAssessment;
import com.ruoyi.website.domain.vo.request.ClientEduAssessmentRequestVO;
import com.ruoyi.website.domain.vo.response.ClientEduAssessmentResponseVO;

import java.util.List;

/**
 * 年度考核与继续教育Service接口
 *
 * @author ruoyi
 * @date 2021-03-22
 */
public interface IClientEduAssessmentService extends IService<ClientEduAssessment> {


    ClientEduAssessmentResponseVO queryDetailById(Long clientId);

    int insertOrUpdate(ClientEduAssessmentRequestVO clientEducationRequest);
}
