package com.ruoyi.website.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.github.pagehelper.Page;
import com.ruoyi.website.bo.TorsAuditResultAddBo;
import com.ruoyi.website.bo.TorsAuditResultQueryBo;
import com.ruoyi.website.bo.TorsAuditResultEditBo;
import com.ruoyi.website.domain.TorsAuditResult;
import com.ruoyi.website.mapper.TorsAuditResultMapper;
import com.ruoyi.website.vo.TorsAuditResultVo;
import com.ruoyi.website.service.ITorsAuditResultService;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 审核未通过Service业务层处理
 *
 * @author admin
 * @date 2021-07-07
 */
@Service
public class TorsAuditResultServiceImpl extends ServiceImpl<TorsAuditResultMapper, TorsAuditResult> implements ITorsAuditResultService {

    @Override
    public TorsAuditResultVo queryById(Long auditResultId){
        TorsAuditResult db = this.baseMapper.selectById(auditResultId);
        return BeanUtil.toBean(db, TorsAuditResultVo.class);
    }

    @Override
    public List<TorsAuditResultVo> queryList(TorsAuditResultQueryBo bo) {
        QueryWrapper<TorsAuditResult> wrapper = new QueryWrapper<>();
        if(StrUtil.isNotBlank(bo.getClientName())) {
            wrapper.like("client_name", bo.getClientName());
        }
        if(StrUtil.isNotBlank(bo.getUnitName())){
            wrapper.like("unit_name", bo.getUnitName());
        }
        if(bo.getDealStatus() != null){
            wrapper.eq("deal_status",bo.getDealStatus());
        }
        if( StrUtil.isNotBlank(bo.getAuditStartDate()) && StrUtil.isNotBlank(bo.getAuditEndDate())){
            wrapper.between("  date_format( audit_time,'%Y-%m-%d') ",bo.getAuditStartDate(),bo.getAuditEndDate());
        }
        if(StrUtil.isNotBlank(bo.getAuditUnit())){
            wrapper.like("audit_unit", bo.getAuditUnit());
        }
        if(bo.getBusinessType()!=null){
            wrapper.eq("business_type",bo.getBusinessType());//业务类型 1-审核进展通知 2-补充资料
        }
        if(StrUtil.isNotBlank(bo.getAuditResult())) {
            wrapper.like("audit_result", bo.getAuditResult());
        }
        if(StrUtil.isNotBlank(bo.getAuditOpinion())) {
            wrapper.like("audit_opinion", bo.getAuditOpinion());
        }
        wrapper.orderByDesc("audit_time");
        return entity2Vo(this.list(wrapper));
    }

    /**
    * 实体类转化成视图对象
    *
    * @param collection 实体类集合
    * @return
    */
    private List<TorsAuditResultVo> entity2Vo(Collection<TorsAuditResult> collection) {
        List<TorsAuditResultVo> voList = collection.stream()
                .map(any -> BeanUtil.toBean(any, TorsAuditResultVo.class))
                .collect(Collectors.toList());
        if (collection instanceof Page) {
            Page<TorsAuditResult> page = (Page<TorsAuditResult>)collection;
            Page<TorsAuditResultVo> pageVo = new Page<>();
            BeanUtil.copyProperties(page,pageVo);
            pageVo.addAll(voList);
            voList = pageVo;
        }
        return voList;
    }

    @Override
    public Boolean insertByAddBo(TorsAuditResultAddBo bo) {
        TorsAuditResult add = BeanUtil.toBean(bo, TorsAuditResult.class);
        validEntityBeforeSave(add);
        return this.save(add);
    }

    @Override
    public Boolean updateByEditBo(TorsAuditResultEditBo bo) {
        TorsAuditResult update = BeanUtil.toBean(bo, TorsAuditResult.class);
        validEntityBeforeSave(update);
        return this.updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(TorsAuditResult entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return this.removeByIds(ids);
    }

    @Override
    public Boolean updateDealStatusByIds(Collection<Long> ids){
        List<TorsAuditResult> torsAuditResultList=new ArrayList<>();
        for(Long auditResultId:ids){
            TorsAuditResult torsAuditResult=new TorsAuditResult();
            torsAuditResult.setAuditResultId(auditResultId);
            torsAuditResult.setDealStatus(1);
            torsAuditResult.setUpdateBy(SecurityUtils.getUsername());
            torsAuditResult.setUpdateTime(new Date());
            torsAuditResultList.add(torsAuditResult);
        }
        if(torsAuditResultList.size()>0){
            return this.updateBatchById(torsAuditResultList);
        }
        return false;
    }

}
