package com.ruoyi.website.service;

import com.ruoyi.website.domain.ClientRobotReportLog;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * 机器人上报流水Service接口
 *
 * @author ruoyi
 * @date 2021-06-11
 */
public interface IClientRobotReportLogService extends IService<ClientRobotReportLog> {

    /**
     * 查询列表
     */
    List<ClientRobotReportLog> queryList(ClientRobotReportLog clientRobotReportLog);
}
