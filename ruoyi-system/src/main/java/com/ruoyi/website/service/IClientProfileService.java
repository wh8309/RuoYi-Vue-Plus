package com.ruoyi.website.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.website.domain.ClientProfile;
import com.ruoyi.website.domain.vo.request.ClientProfileRequestVO;
import com.ruoyi.website.domain.vo.request.ClientProfileStatusUpdateRequestVO;
import com.ruoyi.website.domain.vo.response.ClientProfileExcelExportResponseVO;
import com.ruoyi.website.domain.vo.response.ClientProfileExcelImportResponseVO;
import com.ruoyi.website.domain.vo.response.ClientProfileResponseVO;

import java.util.List;

/**
 * 用户信息Service接口
 *
 * @author ruoyi
 * @date 2021-04-01
 */
public interface IClientProfileService extends IService<ClientProfile> {

    //客户信息列表查询，包括数据权限
    List<ClientProfileResponseVO> queryListByParam(ClientProfile clientProfile);

    ClientProfileResponseVO queryDetailById(Long clientId);

    int insertOrUpdate(ClientProfileRequestVO clientProfileRequestVO);

    /**
     * 校验手机号码是否唯一
     *
     * @param clientProfile 客户信息
     * @return 结果
     */
    public String checkMobileUnique(ClientProfile clientProfile);

    public String checkMobileUniqueContainsThat(ClientProfile clientProfile);

    /**
     * 校验身份证号码是否唯一
     *
     * @param clientProfile 客户信息
     * @return 结果
     */
    public String checkIdcardUnique(ClientProfile clientProfile);

    public String checkIdcardUniqueUnContainsThat(ClientProfile clientProfile);

    int insertOrUpdate(ClientProfile clientProfile);

    //网站登录，根据登录名获取实体对象
    public ClientProfile selectByLoginName(String loginName);

    /**
     * 批量删除客户信息
     *
     * @param clientIds 需要删除的客户id
     * @return 结果
     */
    public int deleteClientProfileByClientIds(Long[] clientIds);

    public void checkUserAllowed(ClientProfile clientProfile);

    public int resetPwd(Long clientId);

    int updateUserStatus(ClientProfileStatusUpdateRequestVO clientProfileStatusUpdateRequestVO);

    void updateLogin(String username, String ipAddr);

    public int resetUserPwd(String userName, String password);

    String importUser(List<ClientProfileExcelImportResponseVO> excelData, boolean updateSupport, String operName);

    List<ClientProfileExcelExportResponseVO> exportListByParam(ClientProfile clientProfile);
}
