package com.ruoyi.website.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.ObjectUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.website.domain.ClientSkillPublishedPaper;
import com.ruoyi.website.domain.vo.request.SkillPublishedQueryRequestVO;
import com.ruoyi.website.domain.vo.request.SkillPublishedRequestVO;
import com.ruoyi.website.domain.vo.response.SkillPublishedResponseVO;
import com.ruoyi.website.mapper.ClientSkillPublishedPaperMapper;
import com.ruoyi.website.service.IClientSkillPublishedPaperService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * 任期内发论文论著情况Service业务层处理
 *
 * @author ruoyi
 * @date 2021-03-18
 */
@Service
public class ClientSkillPublishedPaperServiceImpl extends ServiceImpl<ClientSkillPublishedPaperMapper, ClientSkillPublishedPaper> implements IClientSkillPublishedPaperService {


    @Resource
    private ClientSkillPublishedPaperMapper clientSkillPublishedPaperMapper;


    @Override
    public List<SkillPublishedResponseVO> queryList(SkillPublishedQueryRequestVO skillPublishedQueryRequestVO) {
        List<SkillPublishedResponseVO> skillPublishedResponseVOList = new ArrayList<SkillPublishedResponseVO>();
        LoginUser logUser = SecurityUtils.getLoginUser();
        SysUser sysUser = logUser.getUser();
        Long clientId = skillPublishedQueryRequestVO.getClientId() != null ? skillPublishedQueryRequestVO.getClientId() : sysUser.getUserId();
        QueryWrapper<ClientSkillPublishedPaper> queryWrapper = new QueryWrapper<ClientSkillPublishedPaper>();
        queryWrapper.eq("client_id", clientId);
        queryWrapper.eq("is_deleted", 1);//正常文件，非删除的
        List<ClientSkillPublishedPaper> clientSkillPublishedPaperList = clientSkillPublishedPaperMapper.selectList(queryWrapper);
        for (ClientSkillPublishedPaper publishedPaper : clientSkillPublishedPaperList) {
            SkillPublishedResponseVO publishedResponseVO = new SkillPublishedResponseVO();
            BeanUtils.copyProperties(publishedPaper, publishedResponseVO);
            skillPublishedResponseVOList.add(publishedResponseVO);
        }
        return skillPublishedResponseVOList;
    }

    @Override
    public int insertOrUpdate(SkillPublishedRequestVO skillPublishedRequestVO) {
        try {
            ObjectUtils.setEmptyString(skillPublishedRequestVO);//如果为NULL,设置为空字符串
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        ClientSkillPublishedPaper clientSkillPublishedPaper = new ClientSkillPublishedPaper();
        BeanUtils.copyProperties(skillPublishedRequestVO, clientSkillPublishedPaper);
        if (clientSkillPublishedPaper.getSkillPublishedPapersId() == null) {
            if (clientSkillPublishedPaper.getClientId() == null) {
                LoginUser logUser = SecurityUtils.getLoginUser();
                SysUser sysUser = logUser.getUser();
                Long clientId = sysUser.getUserId();
                clientSkillPublishedPaper.setClientId(clientId);
            }
            clientSkillPublishedPaper.setCreateBy(SecurityUtils.getUsername());
            clientSkillPublishedPaper.setCreateTime(new Date());
            return clientSkillPublishedPaperMapper.insert(clientSkillPublishedPaper);
        } else {
            clientSkillPublishedPaper.setUpdateBy(SecurityUtils.getUsername());
            clientSkillPublishedPaper.setUpdateTime(new Date());
            return clientSkillPublishedPaperMapper.updateById(clientSkillPublishedPaper);
        }
    }


}
