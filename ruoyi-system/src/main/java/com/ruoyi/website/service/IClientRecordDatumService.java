package com.ruoyi.website.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.website.domain.ClientRecordDatum;
import com.ruoyi.website.domain.vo.response.RecordDatumResponseVO;

import java.util.List;

/**
 * 评审申报材料Service接口
 *
 * @author ruoyi
 * @date 2021-03-18
 */
public interface IClientRecordDatumService extends IService<ClientRecordDatum> {


    List<RecordDatumResponseVO> selectRecordDatumMenu(Long clientId);
}
