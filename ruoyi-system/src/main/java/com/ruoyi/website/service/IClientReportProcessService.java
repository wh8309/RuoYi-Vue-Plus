package com.ruoyi.website.service;

import com.ruoyi.website.domain.ClientReportProcess;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.website.domain.vo.request.ClientReportProcessSubmitRequest;
import com.ruoyi.website.domain.vo.response.ClientBaseInfoReportResponseVO;
import com.ruoyi.website.domain.vo.response.ClientReportProcessResponseVO;

import java.util.List;

/**
 * 申报流程处理(流转意见)Service接口
 *
 * @author ruoyi
 * @date 2021-04-29
 */
public interface IClientReportProcessService extends IService<ClientReportProcess> {

    /**
     * 查询列表
     */
    List<ClientReportProcess> queryList(ClientReportProcess clientReportProcess);


    /**
     * 验证是否填写完毕可以提交
     */
    List<String> checkSubmitCondition(Long clientId);

    /**
     * 查询客户基本申报信息(完成并送审)
     */
    ClientBaseInfoReportResponseVO queryBaseInfoByClientId(Long clientId);

    /**
     *流转意见
     */
    List<ClientReportProcessResponseVO> queryListByClientId(Long clientId);

    /**
     * 提交流程
     */
    int processSubmit(ClientReportProcessSubmitRequest clientReportProcessSubmitRequest);







}
