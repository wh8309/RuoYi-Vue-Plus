package com.ruoyi.website.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.website.domain.ClientSkillResearch;
import com.ruoyi.website.domain.vo.request.SkillResearchQueryRequestVO;
import com.ruoyi.website.domain.vo.request.SkillResearchRequestVO;
import com.ruoyi.website.domain.vo.response.SkillResearchResponseVO;
import com.ruoyi.website.mapper.ClientSkillResearchMapper;
import com.ruoyi.website.service.IClientSkillResearchService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 任期内科研成果Service业务层处理
 *
 * @author ruoyi
 * @date 2021-03-18
 */
@Service
public class ClientSkillResearchServiceImpl extends ServiceImpl<ClientSkillResearchMapper, ClientSkillResearch> implements IClientSkillResearchService {

    @Resource
    private ClientSkillResearchMapper clientSkillResearchMapper;

    public List<SkillResearchResponseVO> queryList(SkillResearchQueryRequestVO skillResearchQueryRequestVO){
        List<SkillResearchResponseVO> skillResearchResponseVOList=new ArrayList<SkillResearchResponseVO>();
        LoginUser logUser = SecurityUtils.getLoginUser();
        SysUser sysUser = logUser.getUser();
        Long clientId=sysUser.getUserId();
        if(skillResearchQueryRequestVO.getClientId()!=null){
            clientId=skillResearchQueryRequestVO.getClientId();
        }
        QueryWrapper<ClientSkillResearch> queryWrapper = new QueryWrapper<ClientSkillResearch>();
        queryWrapper.eq("client_id",clientId);
        queryWrapper.eq("is_deleted",1);//正常文件，非删除的
        List<ClientSkillResearch> clientSkillResearchList=baseMapper.selectList(queryWrapper);
        for(ClientSkillResearch clientSkillResearch:clientSkillResearchList){
            SkillResearchResponseVO skillResearchResponseVO=new SkillResearchResponseVO();
            BeanUtils.copyProperties(clientSkillResearch,skillResearchResponseVO);
            skillResearchResponseVOList.add(skillResearchResponseVO);
        }
        return skillResearchResponseVOList;
    }


    public int insertOrUpdate(SkillResearchRequestVO skillResearchRequestVO){
        ClientSkillResearch clientSkillResearch=new ClientSkillResearch();
        BeanUtils.copyProperties(skillResearchRequestVO,clientSkillResearch);
        if(clientSkillResearch.getSkillResearchId()==null){
            if(clientSkillResearch.getClientId()==null){
                LoginUser logUser = SecurityUtils.getLoginUser();
                SysUser sysUser = logUser.getUser();
                Long clientId=sysUser.getUserId();
                clientSkillResearch.setClientId(clientId);
            }
            clientSkillResearch.setCreateBy(SecurityUtils.getUsername());
            clientSkillResearch.setCreateTime(new Date());
           return baseMapper.insert(clientSkillResearch);
        }else{
            clientSkillResearch.setUpdateBy(SecurityUtils.getUsername());
            clientSkillResearch.setUpdateTime(new Date());
            return baseMapper.updateById(clientSkillResearch);
        }

    }

}
