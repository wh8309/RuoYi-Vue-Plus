package com.ruoyi.website.domain.vo.request;

import com.ruoyi.common.core.domain.AbstractEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class TorsWorkUnitQueryRequestVo extends AbstractEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "单位名称")
    private String unitName;

    @ApiModelProperty(value = "单位编码")
    private String unitCode;
}
