package com.ruoyi.website.domain.vo.response;

import com.ruoyi.common.core.domain.BaseResponseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
@ApiModel(value = "文件查询返回对象" )
@Data
public class FileResponseVO extends BaseResponseVO {

    public FileResponseVO() {

    }

    public FileResponseVO(String fileId, String filePath, String fileName, Integer fileType,String remark) {
        this.fileId = fileId;
        this.filePath = filePath;
        this.fileName = fileName;
        this.fileType = fileType;
        this.remark = remark;
    }
    @ApiModelProperty(value = "文件主键id")
    private String fileId;
    @ApiModelProperty(value = "访问该文件的url,如果该文件是图片的话，src指向地址")
    private String filePath;
    @ApiModelProperty(value = "上传的文件名称")
    private String fileName;
    @ApiModelProperty(value = "文件类型")
    private Integer fileType;
    @ApiModelProperty(value = "文件备注说明")
    private String remark;


}
