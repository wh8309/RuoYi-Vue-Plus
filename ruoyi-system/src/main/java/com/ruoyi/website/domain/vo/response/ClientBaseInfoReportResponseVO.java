package com.ruoyi.website.domain.vo.response;


import com.ruoyi.common.core.domain.BaseResponseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@ApiModel(value = "基础信息数据模型")
@NoArgsConstructor
public class ClientBaseInfoReportResponseVO extends BaseResponseVO {

    private static final long serialVersionUID = 1839987880463639422L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "client_id")
    private Long clientId;

    /**
     * 姓名
     */
    @ApiModelProperty(value = "姓名")
    private String name;
    /**
     * 推荐单位
     */
    @ApiModelProperty(value = "推荐单位")
    private String nominateUnit;
    /**
     * 评委会名称
     */
    @ApiModelProperty(value = "评委会名称")
    private String judgesName;

    /**
     * 申报年度
     */
    @ApiModelProperty(value = "申报年度")
    private String reportYear;
    /**
     * 所属系列
     */
    @ApiModelProperty(value = "所属系列")
    private String belongSeries;

    /**
     * 专业名称
     */
    @ApiModelProperty(value = "专业名称")
    private String specialtyName;

    /**
     * 申报职称
     */
    @ApiModelProperty(value = "申报职称")
    private String declarePost;

    /**
     * 申报状态
     */
    @ApiModelProperty(value = "申报状态：-30-复核驳回 -20-审核驳回 0-填报中 10-待审核 20-审核通过 30-复核通过 ")
    private Integer reportStatus;

    @ApiModelProperty(value = "送审状态名称")
    private String reportStatusName;


    @ApiModelProperty(value = "是否允许编辑：1-可以编辑 0-不可以编辑")
    private Integer isEdit;//填报是否可以编辑

    /**
     * 送审时间
     */
    @ApiModelProperty(value = "送审时间")
    private Date applyTime;

    /**
     * 退回时间
     */
    @ApiModelProperty(value = "送审时间")
    private Date goBackTime;

    /** 是否破格 */
    @ApiModelProperty(value = "是否显示是否破格申请表下载：1-显示；0-不显示")
    private String isBreakRule;

    /** 转评类型 */
    @ApiModelProperty(value = "是否显示转评申请下载：1-显示；0-不显示")
    private String referralType;

}
