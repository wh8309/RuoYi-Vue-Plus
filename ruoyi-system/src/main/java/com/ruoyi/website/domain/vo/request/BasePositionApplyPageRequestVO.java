package com.ruoyi.website.domain.vo.request;

import com.ruoyi.common.core.domain.AbstractEntity;
import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "客户职称申报数据模型")
public class BasePositionApplyPageRequestVO extends AbstractEntity {

    @ApiModelProperty(value = "当前页数")
    private Integer pageNum;

    /** 每页显示记录数 */
    @ApiModelProperty(value = "每页显示记录数")
    private Integer pageSize;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "移动电话")
    private String mobile;

    @ApiModelProperty(value = "状态：复合驳回:-30 审核驳回:-20  暂存:0  待审核:10  审核通过：20 复核通过：30  对应字典'client_report_status'")
    private Integer reportStatus;

    @ApiModelProperty(value = "模块类型：申报分配:1  申报填写:2  申报审核:3  申报复核：4  申报查询：5   机器人申报查询:6")
    private Integer moduleType;

    @ApiModelProperty(value = "部门编号")
    private Long deptId;

    @ApiModelProperty(value = "身份证号码")
    private String idcard;

    @ApiModelProperty(value = "推荐单位")
    private String nominateUnit;

    @ApiModelProperty(value = "工作单位")
    private String jobUnit;

    @ApiModelProperty(value = "填报开始日期")
    private String fillBeginDate;

    @ApiModelProperty(value = "填报结束日期")
    private String fillEndDate;

    @ApiModelProperty(value = "分配状态 0 未分配 1 已分配 -1 退回 对应字典'client_distribute_status'")
    private String distributeStatus;

    @ApiModelProperty(value = "填报人userId")
    private Long fillInUserId;

    @ApiModelProperty(value = "上报状态：上报失败:-100 待上报:0  上报中:10  上报成功：100  对应字典'robot_report_status'")
    private Integer robotReportStatus;

    @ApiModelProperty(value = "上报异常类型：账户异常:1 验证码输入错误超过限定次数:2  申报异常超过限定次数:3  其它异常：4  对应字典'robot_report_exception_code'")
    private Integer robotReportExceptionCode;

    @ApiModelProperty(value = "上报开始日期")
    private String robotReportStartDate;

    @ApiModelProperty(value = "上报结束日期")
    private String robotReportEndDate;


}
