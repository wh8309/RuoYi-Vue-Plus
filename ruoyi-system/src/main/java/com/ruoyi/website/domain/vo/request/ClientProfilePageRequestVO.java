package com.ruoyi.website.domain.vo.request;

import com.ruoyi.common.core.domain.AbstractEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;


/**
 * 用户信息对象 client_profile
 *
 * @author ruoyi
 * @date 2021-04-01
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
public class ClientProfilePageRequestVO extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "当前页数")
    private Integer pageNum;

    /**
     * 每页显示记录数
     */
    @ApiModelProperty(value = "每页显示记录数")
    private Integer pageSize;

    /**
     * 姓名
     */
    @ApiModelProperty(value = "姓名")
    private String name;

    /**
     * 部门ID
     */
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号")
    private String mobile;

    /**
     * 微信号
     */
    @ApiModelProperty(value = "微信号")
    private String wechat;

    @ApiModelProperty(value = "身份证号码")
    private String idcard;

    /**
     * 帐号状态（0正常 1停用）
     */
    @ApiModelProperty(value = "帐号状态")
    private String status;


}
