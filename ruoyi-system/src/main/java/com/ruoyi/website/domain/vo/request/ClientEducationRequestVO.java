package com.ruoyi.website.domain.vo.request;

import com.baomidou.mybatisplus.annotation.TableId;
import com.ruoyi.common.core.domain.AbstractEntity;
import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
 * 学历信息对象 client_education
 *
 * @author ruoyi
 * @date 2021-03-22
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
//@ApiModel(value = "学历信息对象(新增/修改)")
public class ClientEducationRequestVO extends AbstractEntity {

    private static final long serialVersionUID = 1L;


    /**
     * 主键
     */
    @TableId(value = "client_id")
    private Long clientId;

    /**
     * 第一学历
     */
    @NotBlank(message = "第一学历不能为空")
    @ApiModelProperty(value = "第一学历")
    private String firstEduName;

    /**
     * 毕业时间
     */
    @NotBlank(message = "毕业时间不能为空")
    @ApiModelProperty(value = "毕业时间")
    private String firstEduGraduateDate;

    /**
     * 毕业学校
     */
    @NotBlank(message = "毕业学校不能为空")
    @ApiModelProperty(value = "毕业学校")
    private String firstEduGraduateSchool;

    /**
     * 专业
     */
    @NotBlank(message = "专业不能为空")
    @ApiModelProperty(value = "专业")
    private String firstEduMajorName;

    /**
     * 学位
     */
    @NotBlank(message = "学位不能为空")
    @ApiModelProperty(value = "学位")
    private String firstEduDegree;

    /**
     * 培养方式
     */
    @NotBlank(message = "培养方式不能为空")
    @ApiModelProperty(value = "培养方式")
    private String firstEduStudyWay;

    /**
     * 最高学历
     */
    @NotBlank(message = "最高学历不能为空")
    @ApiModelProperty(value = "最高学历")
    private String topEduName;

    /**
     * 毕业时间
     */
    @NotBlank(message = "毕业时间不能为空")
    @ApiModelProperty(value = "毕业时间")
    private String topEduGraduateDate;

    /**
     * 毕业学校
     */
    @NotBlank(message = "毕业学校不能为空")
    @ApiModelProperty(value = "毕业学校")
    private String topEduGraduateSchool;

    /**
     * 专业
     */
    @NotBlank(message = "专业不能为空")
    @ApiModelProperty(value = "专业")
    private String topEduMajorName;

    /**
     * 学位
     */
    @NotBlank(message = "学位不能为空")
    @ApiModelProperty(value = "学位")
    private String topEduDegree;

    /**
     * 培养方式
     */
    @NotBlank(message = "培养方式不能为空")
    @ApiModelProperty(value = "培养方式")
    private String topEduStudyWay;


}
