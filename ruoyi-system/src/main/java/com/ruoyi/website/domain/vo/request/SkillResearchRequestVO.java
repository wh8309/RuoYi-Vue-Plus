package com.ruoyi.website.domain.vo.request;

import com.ruoyi.common.core.domain.BaseRequestVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@ApiModel(value = "任期内科研成果(新增/修改)VO")
@Data
public class SkillResearchRequestVO extends BaseRequestVO {

    @ApiModelProperty(value = "主键id")
    private Long skillResearchId;
    @ApiModelProperty(value = "科研年度")
    private String researchYear;
    @NotBlank(message = "成果名称不能为空")
    @Size(min = 0, max = 200, message = "成果名称不能超过200个字符")
    @ApiModelProperty(value = "成果名称")
    private String researchResult;
    @ApiModelProperty(value = "来源")
    private String researchSource;
    @ApiModelProperty(value = "经费")
    private String researchFunding;
    @ApiModelProperty(value = "承担的具体任务及排名")
    private String researchResponsibility;
    @ApiModelProperty(value = "状态或鉴定、时间")
    private String researchStatusTime;
    @ApiModelProperty(value = "客户Id,前端网站可以不传，默认取当前登录的用户id;管理端该参数必填")
    private Long clientId;

}
