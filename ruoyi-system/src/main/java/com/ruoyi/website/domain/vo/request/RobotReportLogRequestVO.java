package com.ruoyi.website.domain.vo.request;

import com.ruoyi.common.core.domain.AbstractEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Data
@NoArgsConstructor
//@ApiModel(value = "机器人上报VO")
public class RobotReportLogRequestVO extends AbstractEntity {

    @ApiModelProperty(value = "客户id")
    @NotNull(message = "客户id不能为空")
    private Long clientId;

}
