package com.ruoyi.website.domain.vo.response;

import com.ruoyi.common.core.domain.BaseResponseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@ApiModel(value = "菜单查询对象" )
public class MenuResponseVO extends BaseResponseVO {

    @ApiModelProperty(value = "菜单名称")
    private String menuName;
    @ApiModelProperty(value = "菜单链接地址")
    private String menuUrl;
    @ApiModelProperty(value = "是否显示 1-显示,0-不显示")
    private int showMenu = 0 ;
    @ApiModelProperty(value = "是否有数据 1-有数据,0-没有数据")
    private int hasData;

    public MenuResponseVO(String menuName, String menuUrl, int showMenu, int hasData) {
        this.menuName = menuName;
        this.menuUrl = menuUrl;
        this.showMenu = showMenu;
        this.hasData = hasData;
    }
}
