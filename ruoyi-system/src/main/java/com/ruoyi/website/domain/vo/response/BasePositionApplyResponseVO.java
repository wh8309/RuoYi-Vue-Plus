package com.ruoyi.website.domain.vo.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseResponseVO;
import com.ruoyi.common.core.domain.entity.SysDept;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@Data
@ApiModel(value = "客户职称申报数据模型")
@NoArgsConstructor
public class BasePositionApplyResponseVO extends BaseResponseVO {

    private Long clientId;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "政务网账号")
    private String account;

    /** 政务网密码 */
    @ApiModelProperty(value = "政务网密码")
    private String password;

    /**
     * 移动电话
     */
    @ApiModelProperty(value = "移动电话")
    private String mobile;

    @ApiModelProperty(value = "状态：申报异常:-100  审核驳回:-20  暂存:0  待审核:10  审核通过：20 申报中:30 申报成功:100")
    private Integer reportStatus;

    @ApiModelProperty(value = "部门信息")
    private SysDept dept;

    @ApiModelProperty(value = "身份证号码")
    private String idcard;

    @ApiModelProperty(value = "推荐单位")
    private String nominateUnit;

    @ApiModelProperty(value = "工作单位")
    private String jobUnit;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /** 分配状态 0 未分配 1 已分配 -1 退回  */
    @ApiModelProperty(value = "分配状态 0 未分配 1 已分配 -1 退回")
    private String distributeStatus;

    /** 退回原由 */
    @ApiModelProperty(value = "退回原由")
    private String distributeBackReason;

    /** 填表人id */
    @ApiModelProperty(value = "填表人id")
    private Long fillInUserId;

    /** 填报人姓名 */
    @ApiModelProperty(value = "填报人姓名")
    private String fillInUserName;

    /** 填报时间 */
    @ApiModelProperty(value = "填报时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date fillInTime;

    @ApiModelProperty(value = "上报状态：上报失败:-100 待上报:0  上报中:10  上报成功：100  对应字典'robot_report_status'")
    private Integer robotReportStatus;

    @ApiModelProperty(value = "上报异常类型：账户异常:1 验证码输入错误超过限定次数:2  申报异常超过限定次数:3  其它异常：4  对应字典'robot_report_exception_code'")
    private Integer robotReportExceptionCode;

    @ApiModelProperty(value = "异常信息")
    private String robotReportExceptionMsg;

    @ApiModelProperty(value = "上报时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date robotReportTime;




}
