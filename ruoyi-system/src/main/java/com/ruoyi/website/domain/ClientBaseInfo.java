package com.ruoyi.website.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 客户基本信息对象 client_base_info
 *
 * @author ruoyi
 * @date 2021-03-22
 */
@Data
@TableName("client_base_info")
public class ClientBaseInfo extends BaseEntity {

private static final long serialVersionUID=1L;

    public ClientBaseInfo() {
    }

    /** 主键 */
    @TableId(value = "client_id",type = IdType.INPUT )
    private Long clientId;

    /** 政务网账号 */
    @Excel(name = "政务网账号")
    private String account;

    /** 政务网密码 */
    @Excel(name = "政务网密码")
    private String password;

    /** 推荐单位 */
    @Excel(name = "推荐单位")
    private String nominateUnit;

    /** 推荐单位授权码 */
    @Excel(name = "推荐单位授权码")
    private String unitAuthCode;

    /** 评审通知 */
    @Excel(name = "评审通知")
    private String reviewNotice;

    /** 申报年度 */
    @Excel(name = "申报年度")
    private String reportYear;

    /** 评委会名称 */
    @Excel(name = "评委会名称")
    private String judgesName;

    /** 所属系列 */
    @Excel(name = "所属系列")
    private String belongSeries;

    /** 申报专业 */
    @Excel(name = "申报专业")
    private String reportSpecialty;

    /** 专业名称 */
    @Excel(name = "专业名称")
    private String specialtyName;

    /** 本专业工作年限 */
    @Excel(name = "本专业工作年限")
    private Integer reportSpecialtyJobYear;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 曾用名 */
    @Excel(name = "曾用名")
    private String beforeUsedName;

    /** 身份证号码 */
    @Excel(name = "身份证号码")
    private String idcard;

    /** 性别 */
    @Excel(name = "性别")
    private String sex;

    /** 出生日期 */
    @Excel(name = "出生日期")
    private String birthday;

    /** 移动电话 */
    @Excel(name = "移动电话")
    private String mobile;

    /** 工作单位 */
    @Excel(name = "工作单位")
    private String jobUnit;

    /** 参加工作日期 */
    @Excel(name = "参加工作日期")
    private String firstJobDate;

    /** 编码单位 */
    @Excel(name = "编码单位")
    private String codeUnit;

    /** 持何职业资格(或一体化)证书日期 */
    @Excel(name = "持何职业资格(或一体化)证书日期")
    private String professionDate;

    /** 持何职业资格(或一体化)证书名称 */
    @Excel(name = "持何职业资格(或一体化)证书名称")
    private String professionName;

    /** 岗位及行政职务 */
    @Excel(name = "岗位及行政职务")
    private String staffPost;

    /** 现职称 */
    @Excel(name = "现职称")
    private String nowPostName;

    /** 批准时间 */
    @Excel(name = "批准时间")
    private String nowPostAuthDate;

    @Excel(name = "批准文号")
    private String nowPostAuthNumber;

    /** 现专业技术职称审批机关 */
    @Excel(name = "现专业技术职称审批机关")
    private String nowPostCheckUnit;

    /** 是否破格 */
    @Excel(name = "是否破格")
    private String isBreakRule;

    /** 政治面貌 */
    @Excel(name = "政治面貌")
    private String politicalStatus;

    /** 申报职称 */
    @Excel(name = "申报职称")
    private String declarePost;

    /** 转评类型 */
    @Excel(name = "转评类型")
    private String referralType;

    /** 资格确认 */
    @Excel(name = "资格确认")
    private String condConfirm;

    /** 认定类型 */
    @Excel(name = "认定类型")
    private String reportTypeConfirm;

    /** 是否贫困县 */
    @Excel(name = "是否贫困县")
    private String isPoorCounty;

    /** 是否基层 */
    @Excel(name = "是否基层")
    private String isBasicLevel;

    /** 特殊贡献情况 */
    @Excel(name = "特殊贡献情况")
    private String specialDevote;

    /** 申报异常:-100  审核驳回:-20  暂存:0  待审核:10  审核通过：20 申报中:30 申报成功:100 */
    @Excel(name = "申报异常:-100  审核驳回:-20  暂存:0  待审核:10  审核通过：20 申报中:30 申报成功:100")
    private Integer reportStatus;

    /** 排序字段 */
    @Excel(name = "排序字段")
    private Long orderNo;

    /** 目录名称 */
    @Excel(name = "目录名称")
    private String directoryName;

    /** 编码单位分类 */
    @Excel(name = "编码单位分类")
    private String codeClassify;

    /** 职称级别 */
    @Excel(name = "职称级别")
    private String postNameRank;

    @Excel(name = "部门ID")
    private Long deptId;

    private Integer isDeleted;

    /** 分配状态 0 未分配 1 已分配 -1 退回  */
    @Excel(name = "分配状态 0 未分配 1 已分配 -1 退回 ")
    private String distributeStatus;

    /** 退回原由 */
    @Excel(name = "退回原由")
    private String distributeBackReason;

    /** 填表人id */
    @Excel(name = "填表人id")
    private Long fillInUserId;

    /** 填报人姓名 */
    @Excel(name = "填报人姓名")
    private String fillInUserName;

    @TableField(exist = false)
    private String fillBeginDate;

    @TableField(exist = false)
    private String fillEndDate;

    //模块类型：申报分配:1  申报填写:2  申报审核:3  申报复核：4  申报查询：5  机器人申报查询:6
    @TableField(exist = false)
    private Integer moduleType;

    /** 填报时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date fillInTime;

    /** 机器人申报状态: -100-申报异常 0-未申报  10-申报中 100-申报成功 */
    private Integer robotReportStatus;

    //异常code
    private Integer robotReportExceptionCode;

    /**  申报异常次数 **/
    private Integer robotReportExceptionTimes;

    /** 异常信息  **/
    private String robotReportExceptionMsg;

    //上报时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date robotReportTime;

    @TableField(exist = false)
    private String robotReportStartDate;

    @TableField(exist = false)
    private String robotReportEndDate;

    /** 审核结果监控标识 1-监控 0-不监控  **/
    private Integer auditResultMonitorFlag;

    /** 清除填报数据 1-清除 0-不清除  **/
    private Integer cleanDataFlag;

    /** 清除填报数据时间  **/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date cleanDataTime;

    //民族
    private String nation;

    //证件类型
    private String certType;

    //工作单位层级
    private String jobUnitLevel;

    //现职称ID
    private String nowPostTreeId;

    //编码单位id
    private String codeUnitTreeId;

    //工作单位类型
    private String jobUnitType;

}
