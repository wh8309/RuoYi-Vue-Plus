package com.ruoyi.website.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import com.ruoyi.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * 任期内科研成果对象 client_skill_research
 *
 * @author ruoyi
 * @date 2021-03-18
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("client_skill_research")
public class ClientSkillResearch implements Serializable {

private static final long serialVersionUID=1L;


    /** 主键 */
    @TableId(value = "skill_research_id")
    private Long skillResearchId;

    /** 主键 */
    @Excel(name = "主键")
    private Long clientId;

    /** 科研年度 */
    @Excel(name = "科研年度")
    private String researchYear;

    /** 成果名称 */
    @Excel(name = "成果名称")
    private String researchResult;

    /** 来源 */
    @Excel(name = "来源")
    private String researchSource;

    /** 经费 */
    @Excel(name = "经费")
    private String researchFunding;

    /** 承担的集体任务及排名 */
    @Excel(name = "承担的具体任务及排名")
    private String researchResponsibility;

    /** 状态/鉴定/时间 */
    @Excel(name = "状态/鉴定/时间")
    private String researchStatusTime;

    /** 相关材料,一个JSON结构的数组 */
    @Excel(name = "相关材料,一个JSON结构的数组")
    private String researchMaterials;

    /** 申报异常:-100  审核驳回:-20  暂存:0  待审核:10  审核通过：20 申报中:30 申报成功:100 */
    @Excel(name = "申报异常:-100  审核驳回:-20  暂存:0  待审核:10  审核通过：20 申报中:30 申报成功:100")
    private Integer reportStatus;

    private Integer isDeleted;

    /** 创建者 */
    private String createBy;

    /** 创建时间 */
    private Date createTime;

    /** 更新者 */
    private String updateBy;

    /** 修改时间 */
    private Date updateTime;

}
