package com.ruoyi.website.domain.vo.response;


import com.ruoyi.common.core.domain.BaseResponseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@ApiModel(value = "基础信息数据模型")
@NoArgsConstructor
public class BaseInfoResponseVO extends BaseResponseVO {

    private static final long serialVersionUID = 1839987880463639422L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "client_id")
    private Long clientId;

    /**
     * 政务网账号
     */
    @ApiModelProperty(value = "政务网账号")
    private String account;

    /**
     * 政务网密码
     */
    @ApiModelProperty(value = "政务网密码")
    private String password;

    /**
     * 推荐单位
     */
    @ApiModelProperty(value = "推荐单位")
    private String nominateUnit;

    /**
     * 推荐单位授权码
     */
    @ApiModelProperty(value = "推荐单位授权码")
    private String unitAuthCode;

    /**
     * 评审通知
     */
    @ApiModelProperty(value = "评审通知")
    private String reviewNotice;

    /**
     * 申报年度
     */
    @ApiModelProperty(value = "申报年度")
    private String reportYear;

    /**
     * 评委会名称
     */
    @ApiModelProperty(value = "评委会名称")
    private String judgesName;

    /**
     * 所属系列
     */
    @ApiModelProperty(value = "所属系列")
    private String belongSeries;

    /**
     * 所属系列名称
     */
    @ApiModelProperty(value = "所属系列名称")
    private String belongSeriesName;


    /**
     * 申报专业
     */
    @ApiModelProperty(value = "申报专业")
    private String reportSpecialty;

    /**
     * 专业名称
     */
    @ApiModelProperty(value = "专业名称")
    private String specialtyName;

    /**
     * 本专业工作年限
     */
    @ApiModelProperty(value = "本专业工作年限")
    private Integer reportSpecialtyJobYear;

    /**
     * 姓名
     */
    @ApiModelProperty(value = "姓名")
    private String name;

    /**
     * 曾用名
     */
    @ApiModelProperty(value = "曾用名")
    private String beforeUsedName;

    /**
     * 身份证号码
     */
    @ApiModelProperty(value = "身份证号码")
    private String idcard;

    /**
     * 性别
     */
    @ApiModelProperty(value = "性别")
    private String sex;

    /**
     * 出生日期
     */
    @ApiModelProperty(value = "出生日期")
    private String birthday;

    /**
     * 移动电话
     */
    @ApiModelProperty(value = "移动电话")
    private String mobile;

    /**
     * 工作单位
     */
    @ApiModelProperty(value = "工作单位")
    private String jobUnit;

    /**
     * 参加工作日期
     */
    @ApiModelProperty(value = "参加工作日期")
    private String firstJobDate;

    /**
     * 编码单位
     */
    @ApiModelProperty(value = "编码单位")
    private String codeUnit;

    /**
     * 持何职业资格(或一体化)证书日期
     */
    @ApiModelProperty(value = "持何职业资格(或一体化)证书日期")
    private String professionDate;

    /**
     * 持何职业资格(或一体化)证书名称
     */
    @ApiModelProperty(value = "持何职业资格(或一体化)证书名称")
    private String professionName;

    /**
     * 岗位及行政职务
     */
    @ApiModelProperty(value = "岗位及行政职务")
    private String staffPost;

    /**
     * 现职称
     */
    @ApiModelProperty(value = "现职称")
    private String nowPostName;

    /**
     * 批准时间
     */
    @ApiModelProperty(value = "批准时间")
    private String nowPostAuthDate;

    /**
     * 批准文号
     */
    @ApiModelProperty("批准文号")
    private String nowPostAuthNumber;

    /**
     * 现专业技术职称审批机关
     */
    @ApiModelProperty(value = "现专业技术职称审批机关")
    private String nowPostCheckUnit;

    /**
     * 是否破格
     */
    @ApiModelProperty(value = "是否破格")
    private String isBreakRule;

    /**
     * 政治面貌
     */
    @ApiModelProperty(value = "政治面貌")
    private String politicalStatus;

    /**
     * 申报职称
     */
    @ApiModelProperty(value = "申报职称")
    private String declarePost;

    /**
     * 转评类型
     */
    @ApiModelProperty(value = "转评类型")
    private String referralType;

    /**
     * 资格确认
     */
    @ApiModelProperty(value = "资格确认")
    private String condConfirm;

    /**
     * 认定类型
     */
    @ApiModelProperty(value = "认定类型")
    private String reportTypeConfirm;

    /**
     * 是否贫困县
     */
    @ApiModelProperty(value = "是否贫困县")
    private String isPoorCounty;

    /**
     * 是否基层
     */
    @ApiModelProperty(value = "是否基层")
    private String isBasicLevel;

    /**
     * 特殊贡献情况
     */
    @ApiModelProperty(value = "特殊贡献情况")
    private String specialDevote;

    /**
     * 申报异常:-100  审核驳回:-20  暂存:0  待审核:10  审核通过：20 申报中:30 申报成功:100
     */
    @ApiModelProperty(value = "申报异常:-100  审核驳回:-20  暂存:0  待审核:10  审核通过：20 申报中:30 申报成功:100")
    private Integer reportStatus;

    /**
     * 排序字段
     */
    @ApiModelProperty(value = "排序字段")
    private Long orderNo;

    /**
     * 目录名称
     */
    @ApiModelProperty(value = "目录名称")
    private String directoryName;

    /**
     * 编码单位分类
     */
    @ApiModelProperty(value = "编码单位分类")
    private String codeClassify;

    /**
     * 职称级别
     */
    @ApiModelProperty(value = "职称级别")
    private String postNameRank;

    @ApiModelProperty(value = "民族，对应数据字典:tors_nation")
    private String nation;

    @ApiModelProperty(value = "证件类型，对应数据字典:tors_cert_type")
    private String certType;

    @ApiModelProperty(value = "工作单位层级，对应数据字典:tors_job_unit_level")
    private String jobUnitLevel;

    @ApiModelProperty(value = "现职称ID")
    private String nowPostTreeId;

    @ApiModelProperty(value = "编码单位id")
    private String codeUnitTreeId;

    @ApiModelProperty(value = "工作单位类型，对应数据字典:tors_job_unit_type")
    private String jobUnitType;
}
