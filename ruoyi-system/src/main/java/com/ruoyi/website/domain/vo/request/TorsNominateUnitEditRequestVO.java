package com.ruoyi.website.domain.vo.request;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.AbstractEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class TorsNominateUnitEditRequestVO extends AbstractEntity {

    private static final long serialVersionUID=1L;

    @NotNull(message = "主键id不能为空")
    private Long nominateUnitId;

    @ApiModelProperty(value = "单位名称")
    @NotBlank(message = "单位名称不能为空")
    private String unitName;

    @ApiModelProperty(value = "单位地址")
    private String unitAddress;

    @ApiModelProperty(value = "单位联系人")
    private String unitContact;

    @ApiModelProperty(value = "单位办公电话")
    private String unitPhone;

    @ApiModelProperty(value = "申报授权码")
    @NotBlank(message = "申报授权码不能为空")
    private String unitAuthCode;

    @ApiModelProperty(value = "政务网账号")
    @NotBlank(message = "政务网账号不能为空")
    private String account;
    
    @ApiModelProperty(value = "政务网密码")
    @NotBlank(message = "政务网密码不能为空")
    private String password;

    /** 排序字段 */
    @ApiModelProperty(value = "排序")
    private Long orderNo;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "短信通知 1-启用 0-禁用")
    private Integer smsEnable;

    @ApiModelProperty(value = "短信接收号码")
    private String smsReceiveMobiles;


}
