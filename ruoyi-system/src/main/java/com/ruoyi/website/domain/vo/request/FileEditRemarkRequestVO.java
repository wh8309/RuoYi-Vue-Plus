package com.ruoyi.website.domain.vo.request;

import com.ruoyi.common.core.domain.BaseRequestVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value = "文件编辑备注VO")
@Data
public class FileEditRemarkRequestVO extends BaseRequestVO {
    @ApiModelProperty(value = "文件id",required = true)
    private String fileId;
    @ApiModelProperty(value = "文件备注说明")
    private String remark;

}
