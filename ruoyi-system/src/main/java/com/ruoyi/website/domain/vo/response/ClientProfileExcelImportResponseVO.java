package com.ruoyi.website.domain.vo.response;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.annotation.Excels;
import com.ruoyi.common.core.domain.BaseResponseVO;
import com.ruoyi.common.core.domain.entity.SysDept;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 用户信息对象 client_profile
 *
 * @author ruoyi
 * @date 2021-04-01
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
public class ClientProfileExcelImportResponseVO extends BaseResponseVO {

    private static final long serialVersionUID = 1L;

    @Excel(name = "用户序号", cellType = Excel.ColumnType.NUMERIC, prompt = "用户编号")
    private Long serialNumber;

    @Excel(name = "姓名")
    private String name;

    @ApiModelProperty(value = "用户昵称")
    @Excel(name = "用户昵称")
    private String nickName;

//    @Excel(name = "用户性别", dictType = "tors_gender" )
    @Excel(name = "用户性别", readConverterExp = "1=男,2=女", combo = {"男", "女"})
    private String sex;

    @Excel(name = "手机号")
    private String mobile;


    @Excel(name = "身份证号码",cellType = Excel.ColumnType.TEXT)
    private String idcard;

    @Excel(name = "微信号")
    private String wechat;

    @Excel(name = "QQ号码")
    private String qq;

    @Excel(name = "用户邮箱")
    private String email;

    @Excel(name = "帐号状态", readConverterExp = "0=正常,1=停用", combo = {"正常", "停用"})
    private String status;
//
//    @Excel(name = "审批状态", dictType = "client_audit_status")
//    private String clientAuditStatus;

    @Excel(name = "部门",dept = true)
    private Long deptId;

    @Excel(name = "备注信息")
    private String remark;

}
