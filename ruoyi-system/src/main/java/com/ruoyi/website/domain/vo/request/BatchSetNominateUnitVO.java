package com.ruoyi.website.domain.vo.request;

import com.ruoyi.common.core.domain.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
//@ApiModel(value = "批量设置推荐单位VO")
public class BatchSetNominateUnitVO extends AbstractEntity {

    @ApiModelProperty(value = "所选客户:多个以英文逗号分隔")
    @NotBlank(message = "所选客户不能为空")
    private String clientIds;

    @ApiModelProperty(value = "推荐单位")
    @NotNull(message = "推荐单位不能为空")
    private Long nominateUnitId;

}
