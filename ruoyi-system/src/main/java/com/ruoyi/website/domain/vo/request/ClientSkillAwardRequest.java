package com.ruoyi.website.domain.vo.request;

import com.ruoyi.common.core.domain.AbstractEntity;
import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * 任职期间奖励情况对象 client_skill_award
 *
 * @author ruoyi
 * @date 2021-03-24
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
//@ApiModel(value = "任职期间奖励情况对象(新增/修改)")
public class ClientSkillAwardRequest extends AbstractEntity {

    private static final long serialVersionUID = 1L;


    /**
     * 主键
     */
    @ApiModelProperty(value = "client_id")
    private Long clientId;

    /**
     * 任职期间奖励情况
     */
    @NotBlank(message = "任职期间奖励情况不能为空")
    @Size(min = 1, max = 200, message = "任职期间奖励情况最多不能超过200个字符")
    @ApiModelProperty(value = "任职期间奖励情况")
    private String inSkillAward;


}
