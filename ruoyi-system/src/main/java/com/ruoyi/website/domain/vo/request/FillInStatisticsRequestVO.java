package com.ruoyi.website.domain.vo.request;

import com.ruoyi.common.core.domain.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "人员填报统计")
public class FillInStatisticsRequestVO extends AbstractEntity {

    @ApiModelProperty(value = "开始日期")
    private String fillBeginDate;

    @ApiModelProperty(value = "结束日期")
    private String fillEndDate;




}
