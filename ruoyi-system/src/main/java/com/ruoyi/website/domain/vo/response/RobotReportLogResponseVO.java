package com.ruoyi.website.domain.vo.response;


import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseResponseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@ApiModel(value = "机器人上报日志")
@NoArgsConstructor
public class RobotReportLogResponseVO extends BaseResponseVO {

    private static final long serialVersionUID = 1839987880463639422L;

    /** 日志主键 */
    @ApiModelProperty(value = "主键id")
    private Long reportLogId;

    /** 客户id */
    @ApiModelProperty(value = "客户id")
    private Long clientId;

    /** 客户姓名 */
    @ApiModelProperty(value = "客户姓名")
    private String name;

    /** 身份证号码 */
    @ApiModelProperty(value = "身份证号码")
    private String idcard;

    /** 操作模块 */
    @ApiModelProperty(value = "操作模块")
    private String operModule;

    /** 操作描述 */
    @ApiModelProperty(value = "操作描述")
    private String operDecription;

    /** $column.columnComment */
    @ApiModelProperty(value = "操作时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;


}
