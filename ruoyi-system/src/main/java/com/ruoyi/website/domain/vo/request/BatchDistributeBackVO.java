package com.ruoyi.website.domain.vo.request;

import com.ruoyi.common.core.domain.AbstractEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
//@ApiModel(value = "批量退回分配")
public class BatchDistributeBackVO extends AbstractEntity {

    @ApiModelProperty(value = "所选客户:多个以英文逗号分隔")
    @NotBlank(message = "所选客户不能为空")
    private String clientIds;

    @ApiModelProperty(value = "退回原因")
    @NotBlank(message = "退回原因不能为空")
    private String distributeBackReason;

}
