package com.ruoyi.website.domain.vo.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.annotation.Excels;
import com.ruoyi.common.core.domain.BaseResponseVO;
import com.ruoyi.common.core.domain.entity.SysDept;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 用户信息对象 client_profile
 *
 * @author ruoyi
 * @date 2021-04-01
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "用户信息查询返回模型")
public class ClientProfileResponseVO extends BaseResponseVO {

    private static final long serialVersionUID = 1L;

    /**
     * 客户id
     */
    @Excel(name = "用户序号", cellType = Excel.ColumnType.NUMERIC, prompt = "用户编号")
    @ApiModelProperty(value = "client_id")
    private Long clientId;

    /**
     * 姓名
     */
    @Excel(name = "姓名")
    @ApiModelProperty(value = "姓名")
    private String name;

    /**
     * 性别
     */
    @Excel(name = "用户性别", readConverterExp = "1=男,2=女")
    @ApiModelProperty(value = "性别")
    private String sex;

    /**
     * 身份证号码
     */
    @Excel(name = "身份证号码")
    @ApiModelProperty(value = "身份证号码")
    private String idcard;

    /**
     * 手机号
     */
    @Excel(name = "手机号")
    @ApiModelProperty(value = "手机号")
    private String mobile;

    /**
     * 微信号
     */
    @Excel(name = "微信号")
    @ApiModelProperty(value = "微信号")
    private String wechat;

    /**
     * QQ号码
     */
    @Excel(name = "QQ号码")
    @ApiModelProperty(value = "QQ号码")
    private String qq;

    /**
     * 登录名
     */
    @Excel(name = "登录名")
    @ApiModelProperty(value = "登录名")
    private String loginName;

    /**
     * 客户类型
     */
    @ApiModelProperty(value = "客户类型")
    private String clientType;

    /**
     * 销售负责人
     */
    @ApiModelProperty(value = "销售负责人")
    private Long salesChargeUserId;

    /**
     * 客服负责人
     */
    @ApiModelProperty(value = "客服负责人")
    private Long serviceChargeUserId;

    /**
     * 部门ID
     */
    @ApiModelProperty(value = "部门ID")
    private Long deptId;


    @Excels({
            @Excel(name = "部门名称", targetAttr = "deptName", type = Excel.Type.EXPORT),
            @Excel(name = "部门负责人", targetAttr = "leader", type = Excel.Type.EXPORT)
    })
    @ApiModelProperty(value = "部门信息")
    private SysDept dept;
    /**
     * 用户昵称
     */
    @Excel(name = "用户昵称")
    @ApiModelProperty(value = "用户昵称")
    private String nickName;

    /**
     * 用户邮箱
     */
    @Excel(name = "用户邮箱")
    @ApiModelProperty(value = "用户邮箱")
    private String email;

    /**
     * 头像地址
     */
    @ApiModelProperty(value = "头像地址")
    @Excel(name = "头像地址")
    private String avatar;

    /**
     * 帐号状态（0正常 1停用）
     */
    @ApiModelProperty(value = "帐号状态")
    @Excel(name = "帐号状态" , readConverterExp = "0=正常,1=停用")
    private String status;

    /**
     * 最后登录IP
     */
    @ApiModelProperty(value = "最后登录IP")
    @Excel(name = "最后登录IP")
    private String loginIp;

    /**
     * 最后登录时间
     */
    @ApiModelProperty(value = "最后登录时间")
    @Excel(name = "最后登录时间" , width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date loginDate;

    /**
     * 是否删除 1 正常 -1 删除
     */
    @Excel(name = "是否删除", readConverterExp = "1=正常,-1=删除,2=未知")
    @ApiModelProperty(value = "是否删除 1 正常 -1 删除")
    private Integer isDeleted;

    /**
     * 备注
     */
    @Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String remark;


}
