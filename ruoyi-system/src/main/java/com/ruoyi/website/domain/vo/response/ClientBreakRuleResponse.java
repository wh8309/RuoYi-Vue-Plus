package com.ruoyi.website.domain.vo.response;

import com.ruoyi.common.core.domain.BaseResponseVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 破格条件说明对象 client_break_rule
 *
 * @author ruoyi
 * @date 2021-03-25
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
public class ClientBreakRuleResponse extends BaseResponseVO {

    private static final long serialVersionUID = 1L;


    /**
     * 主键
     */
    @ApiModelProperty(value = "client_id")
    private Long clientId;

    /**
     * 破格条件
     */
    @ApiModelProperty(value = "破格条件")
    private String breakRuleDesc;

}
