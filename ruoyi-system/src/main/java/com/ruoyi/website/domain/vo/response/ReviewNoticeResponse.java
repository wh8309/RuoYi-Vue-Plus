package com.ruoyi.website.domain.vo.response;

import com.ruoyi.common.core.domain.BaseResponseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@ApiModel(value = "评审通知数据模型")
public class ReviewNoticeResponse extends BaseResponseVO {

    @ApiModelProperty(value = "主键")
    private Long treeId;

    @ApiModelProperty(value = "父id")
    private Long parentTreeId;

    @ApiModelProperty(value = "编码")
    private String treeCode;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "年度")
    private String reportYear;

    @ApiModelProperty(value = "系列")
    private String belongSeries;

    @ApiModelProperty(value = "评审通知名称")
    private String treeName;

    @ApiModelProperty(value = "评委会名称")
    private String judgesPanelName;

    @ApiModelProperty(value = "评审范围")
    private String reviewNoticeScope;

    @ApiModelProperty(value = "申报开始时间")
    private String applyStartTime;

    @ApiModelProperty(value = "申报结束时间")
    private String applyEndTime;

}

