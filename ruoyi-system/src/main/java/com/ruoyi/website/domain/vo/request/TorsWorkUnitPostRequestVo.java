package com.ruoyi.website.domain.vo.request;

import com.ruoyi.common.core.domain.AbstractEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@EqualsAndHashCode(callSuper = true)
@Data
public class TorsWorkUnitPostRequestVo extends AbstractEntity {

    private static final long serialVersionUID=1L;


    @ApiModelProperty(value = "单位名称")
    @NotBlank(message = "单位名称不能为空")
    private String unitName;

    @ApiModelProperty(value = "单位编号")
    @NotBlank(message = "单位编号不能为空")
    private String unitCode;

    @ApiModelProperty(value = "单位联系人")
    private String unitContact;

    @ApiModelProperty(value = "单位办公电话")
    private String unitPhone;

    @ApiModelProperty(value = "单位地址")
    private String unitAddress;

    @ApiModelProperty(value = "备注")
    private String remark;

}
