package com.ruoyi.website.domain.vo.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseResponseVO;
import com.ruoyi.common.core.domain.entity.SysUser;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


@Data
@ApiModel(value = "填报统计返回结果")
@NoArgsConstructor
public class FillInStatisticsResponseVO extends BaseResponseVO {

    @ApiModelProperty(value = "部门名称")
    private String deptName;
    @ApiModelProperty(value = "用户名称")
    private String userName;
    @ApiModelProperty(value = "昵称")
    private String nickName;
    @ApiModelProperty(value = "手机号码")
    private String phonenumber;
    @ApiModelProperty(value = "填表人id")
    private Long fillInUserId;
    @ApiModelProperty(value = "填报用户信息")
    private SysUser fillInUser;
    @ApiModelProperty(value = "填报总数")
    private Integer sum=0;

    @ApiModelProperty(value = "填报日期及数量数组")
    private List<FillInStatisticsItemResponseVO> items=new ArrayList<>();



}
