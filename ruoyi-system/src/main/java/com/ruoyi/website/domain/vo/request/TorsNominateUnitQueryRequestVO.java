package com.ruoyi.website.domain.vo.request;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.AbstractEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class TorsNominateUnitQueryRequestVO extends AbstractEntity {

    private static final long serialVersionUID=1L;


    @ApiModelProperty(value = "单位名称")
    private String unitName;

    @ApiModelProperty(value = "申报授权码")
    private String unitAuthCode;

    @ApiModelProperty(value = "政务网账号")
    private String account;

    @ApiModelProperty(value = "短信通知 1-启用 0-禁用")
    private Integer smsEnable;




}
