package com.ruoyi.website.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 破格条件说明对象 client_break_rule
 *
 * @author ruoyi
 * @date 2021-03-25
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("client_break_rule")
public class ClientBreakRule implements Serializable {

private static final long serialVersionUID=1L;


    /** 主键 */
    @TableId(value = "client_id", type = IdType.INPUT)
    private Long clientId;

    /** 破格条件 */
    @Excel(name = "破格条件")
    private String breakRuleDesc;

    /** 申报异常:-100|审核驳回:-20|暂存:0|待审核:10|审核通过:20|申报中:30|申报成功:100 */
    @Excel(name = "申报异常:-100|审核驳回:-20|暂存:0|待审核:10|审核通过:20|申报中:30|申报成功:100")
    private Integer reportStatus;

    /** 创建者 */
    private String createBy;

    /** 创建时间 */
    private Date createTime;

    /** 更新者 */
    private String updateBy;

    /** 修改时间 */
    private Date updateTime;

    @TableField(exist = false)
    private Map<String, Object> params = new HashMap<>();
}
