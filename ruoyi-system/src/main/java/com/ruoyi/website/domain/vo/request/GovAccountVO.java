package com.ruoyi.website.domain.vo.request;

import com.ruoyi.common.core.domain.AbstractEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Data
@NoArgsConstructor
//@ApiModel(value = "更新政务网账号信息")
public class GovAccountVO extends AbstractEntity {

    @ApiModelProperty(value = "客户id")
    @NotNull(message = "客户id不能为空")
    private Long clientId;

    /**
     * 政务网账号
     */
    @ApiModelProperty(value = "政务网账号")
    @NotBlank(message = "政务网账号不能为空")
    private String account;

    /**
     * 政务网密码
     */
    @ApiModelProperty(value = "政务网密码")
    @NotBlank(message = "政务网密码不能为空")
    private String password;

}
