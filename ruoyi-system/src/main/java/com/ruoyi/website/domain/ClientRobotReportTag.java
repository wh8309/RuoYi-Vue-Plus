package com.ruoyi.website.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import com.ruoyi.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * 机器人上报tag对象 client_robot_report_tag
 * 
 * @author ruoyi
 * @date 2022-04-14
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("client_robot_report_tag")
public class ClientRobotReportTag implements Serializable {

private static final long serialVersionUID=1L;


    /** 主键(clientId-belongSeries) */
    @TableId(value = "report_tag_id",type = IdType.INPUT)
    private String reportTagId;

    /** 客户id */
    private Long clientId;

    /** 上报json */
    @Excel(name = "上报json")
    private String reportTagJson;

    /** $column.columnComment */
    private Date createTime;

    /** $column.columnComment */
    private Date updateTime;

    @TableField(exist = false)
    private Map<String, Object> params = new HashMap<>();
}
