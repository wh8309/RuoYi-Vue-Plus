package com.ruoyi.website.domain.vo.request;

import com.ruoyi.common.core.domain.AbstractEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class ClientReportProcessSubmitRequest extends AbstractEntity {

    @ApiModelProperty(value = "客户id")
    private Long clientId;

    @NotNull(message = "申报状态不能为空")
    @ApiModelProperty(value = "申报状态：-30-复核驳回 -20-审核驳回 0-填报中 10-待审核 20-审核通过 30-复核通过")
    private Integer reportStatus;

    /** 处理意见 */
    @ApiModelProperty(value = "处理意见")
    private String transactorSuggestion;

    @ApiModelProperty(value = "备注信息")
    private String remark;

}
