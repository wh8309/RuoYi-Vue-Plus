package com.ruoyi.website.domain.vo.response;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.domain.BaseResponseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 转换系列对象 client_exchange_faculty
 *
 * @author ruoyi
 * @date 2021-03-24
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "转换系列对象" )
public class ClientExchangeFacultyResponse extends BaseResponseVO {

private static final long serialVersionUID=1L;


    /** 主键 */
    @ApiModelProperty(value ="client_id")
    private Long clientId;

    /** 原工作单位及从事专业 */
    @ApiModelProperty(value = "原工作单位及从事专业")
    private String originalUnitSkill;

    /** 证书号码 */
    @ApiModelProperty(value = "证书号码")
    private String cerNo;

    /** 取得资格方式 */
    @ApiModelProperty(value = "取得资格方式")
    private String fetchWay;

    /** 任职资格批准文号 */
    @ApiModelProperty(value = "任职资格批准文号")
    private String authNo;

    /** 现工作单位及进入时间 */
    @ApiModelProperty(value = "现工作单位及进入时间")
    private String nowJobEnterTime;

    /** 申请转换系列(专业)时间 */
    @ApiModelProperty(value = "申请转换系列(专业)时间")
    private String applyTransTime;

}
