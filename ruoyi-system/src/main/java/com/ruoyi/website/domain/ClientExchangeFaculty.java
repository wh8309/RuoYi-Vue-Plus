package com.ruoyi.website.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 转换系列对象 client_exchange_faculty
 *
 * @author ruoyi
 * @date 2021-03-24
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("client_exchange_faculty")
public class ClientExchangeFaculty implements Serializable {

private static final long serialVersionUID=1L;


    /** 主键 */
    @TableId(value = "client_id",type = IdType.INPUT)
    private Long clientId;

    /** 原工作单位及从事专业 */
    @Excel(name = "原工作单位及从事专业")
    private String originalUnitSkill;

    /** 证书号码 */
    @Excel(name = "证书号码")
    private String cerNo;

    /** 取得资格方式 */
    @Excel(name = "取得资格方式")
    private String fetchWay;

    /** 任职资格批准文号 */
    @Excel(name = "任职资格批准文号")
    private String authNo;

    /** 现工作单位及进入时间 */
    @Excel(name = "现工作单位及进入时间")
    private String nowJobEnterTime;

    /** 申请转换系列(专业)时间 */
    @Excel(name = "申请转换系列(专业)时间")
    private String applyTransTime;

    /** 申报异常:-100|审核驳回:-20|暂存:0|待审核:10|审核通过:20|申报中:30|申报成功:100 */
    @Excel(name = "申报异常:-100|审核驳回:-20|暂存:0|待审核:10|审核通过:20|申报中:30|申报成功:100")
    private Integer reportStatus;

    /** 创建者 */
    private String createBy;

    /** 创建时间 */
    private Date createTime;

    /** 更新者 */
    private String updateBy;

    /** 修改时间 */
    private Date updateTime;

    @TableField(exist = false)
    private Map<String, Object> params = new HashMap<>();
}
