package com.ruoyi.website.domain.vo.response;

import com.ruoyi.common.core.domain.BaseResponseVO;
import com.ruoyi.common.core.domain.entity.SysUser;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.LinkedHashMap;
import java.util.Map;


@Data
@ApiModel(value = "填报返回数据项")
public class FillInStatisticsItemResponseVO extends BaseResponseVO {

    public FillInStatisticsItemResponseVO() {

    }

    public FillInStatisticsItemResponseVO(String fillInDate, Integer count) {
        this.fillInDate = fillInDate;
        this.count = count;
    }

    @ApiModelProperty(value = "填报日期")
    private String fillInDate;

    @ApiModelProperty(value = "填报日期")
    private Integer count=0;//填报数



}
