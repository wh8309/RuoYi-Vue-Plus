package com.ruoyi.website.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import com.ruoyi.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * 文件信息对象 sys_file_info
 *
 * @author ruoyi
 * @date 2021-03-21
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("sys_file_info")
public class SysFileInfo implements Serializable {

    private static final long serialVersionUID=1L;


    /** 主键Id */
    @TableId(value = "file_id",type = IdType.INPUT)
    private String fileId;

    /** $column.columnComment */
    @Excel(name = "${comment}" , readConverterExp = "$column.readConverterExp()")
    private Integer fileType;

    /** 业务ID */
    @Excel(name = "业务ID")
    private Long businessId;

    /** $column.columnComment */
    @Excel(name = "业务ID")
    private Integer businessType;

    /** 附件名称(存储名称) */
    @Excel(name = "附件名称(存储名称)")
    private String fileName;

    /** 源文件名称 */
    @Excel(name = "源文件名称")
    private String originalFileName;

    /** 附件扩展名 */
    @Excel(name = "附件扩展名")
    private String fileExt;

    /** 附件大小 */
    @Excel(name = "附件大小")
    private Long fileSize;

    /** 附件存放路径 */
    @Excel(name = "附件存放路径")
    private String filePath;

    /** $column.columnComment */
    @Excel(name = "附件存放路径")
    private String md5;

    /** 文件备注 */
    @Excel(name = "文件备注")
    private String remark;

    /** 客户id */
    @Excel(name = "客户id")
    private Long clientId;

    /** 是否删除 1 正常 -1 删除 */
    @Excel(name = "是否删除 1 正常 -1 删除")
    private Integer isDeleted;

    /** 创建者 */
    private String createBy;

    /** 创建时间 */
    private Date createTime;

    /** 更新者 */
    private String updateBy;

    /** 修改时间 */
    private Date updateTime;

    /** 文件来源类型 0-用户上传 1-伙伴云同步 */
    private Integer fileOriginType;

    /** 云存储类型: 1-七牛云 2-阿里云 3-腾讯云 */
    private Integer cloudStorageType;

    /** 云存储路径(Key) */
    private String cloudStoragePath;

    /** 源文件id */
    private Long originFileId;

    /** 机器人执行状态 0-未执行 1-已经执行 */
    private Integer robotExecuteStatus;

    @TableField(exist = false)
    private Map<String, Object> params = new HashMap<>();
}