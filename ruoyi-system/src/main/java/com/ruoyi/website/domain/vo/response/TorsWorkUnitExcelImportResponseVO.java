package com.ruoyi.website.domain.vo.response;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseResponseVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 用户信息对象 client_profile
 *
 * @author ruoyi
 * @date 2021-04-01
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
public class TorsWorkUnitExcelImportResponseVO extends BaseResponseVO {

    private static final long serialVersionUID = 1L;

    @Excel(name = "序号", cellType = Excel.ColumnType.NUMERIC, prompt = "序号")
    private Long workUnitId;

    @Excel(name = "单位代码")
    private String unitCode;

    @ApiModelProperty(value = "单位名称")
    @Excel(name = "单位名称")
    private String unitName;

    @Excel(name = "单位地址")
    private String unitAddress;

    @Excel(name = "单位联系人")
    private String unitContact;

    @Excel(name = "单位办公电话")
    private String unitPhone;

    @Excel(name = "备注")
    private String remark;

}
