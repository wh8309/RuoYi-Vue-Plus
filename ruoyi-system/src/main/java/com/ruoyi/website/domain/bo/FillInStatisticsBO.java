package com.ruoyi.website.domain.bo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.AbstractEntity;
import lombok.Data;

@Data
public class FillInStatisticsBO extends AbstractEntity {

    /** 填表人id */
    private Long fillInUserId;

    /** 填报人姓名 */
    private String fillInUserName;

    /** 填报时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private String fillInDate;

    private Integer count=0;//填报数

}
