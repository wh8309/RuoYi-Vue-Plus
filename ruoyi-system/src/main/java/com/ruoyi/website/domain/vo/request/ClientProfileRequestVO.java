package com.ruoyi.website.domain.vo.request;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.AbstractEntity;
import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.Size;

/**
 * 用户信息对象 client_profile
 *
 * @author ruoyi
 * @date 2021-04-01
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
public class ClientProfileRequestVO extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 客户id
     */
    @ApiModelProperty(value = "client_id")
    private Long clientId;

    /**
     * 姓名
     */
    @ApiModelProperty(value = "姓名")
    private String name;

    /**
     * 性别
     */
    @ApiModelProperty(value = "性别")
    private String sex;

    /**
     * 身份证号码
     */
    @ApiModelProperty(value = "身份证号码")
    private String idcard;

    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号")
    @Size(min = 0, max = 11, message = "手机号码长度不能超过11个字符")
    private String mobile;

    /**
     * 微信号
     */
    @ApiModelProperty(value = "微信号")
    private String wechat;

    /**
     * QQ号码
     */
    @ApiModelProperty(value = "QQ号码")
    private String qq;

    /**
     * 登录名
     */
    @ApiModelProperty(value = "登录名")
    private String loginName;

    /**
     * 登录密码
     */
    @ApiModelProperty(value = "登录密码")
    private String password;

    /**
     * 客户类型
     */
    @ApiModelProperty(value = "客户类型")
    private String clientType;

    /**
     * 销售负责人
     */
    @ApiModelProperty(value = "销售负责人")
    private Long salesChargeUserId;

    /**
     * 客服负责人
     */
    @ApiModelProperty(value = "客服负责人")
    private Long serviceChargeUserId;

    /**
     * 部门ID
     */
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /**
     * 用户昵称
     */
    @ApiModelProperty(value = "用户昵称")
    private String nickName;

    /**
     * 用户邮箱
     */
    @ApiModelProperty(value = "用户邮箱")
    private String email;

    /**
     * 头像地址
     */
    @ApiModelProperty(value = "头像地址")
    private String avatar;

    /** 帐号状态（0正常 1停用） */
    @ApiModelProperty(value = "帐号状态（0正常 1停用）")
    private String status;

    /**
     * 是否删除 1 正常 -1 删除
     */
    @ApiModelProperty(value = "是否删除 1 正常 -1 删除")
    private Integer isDeleted;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;


}
