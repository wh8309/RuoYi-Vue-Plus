package com.ruoyi.website.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import com.ruoyi.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * 评审申报材料对象 client_record_datum
 *
 * @author ruoyi
 * @date 2021-03-18
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("client_record_datum")
public class ClientRecordDatum implements Serializable {

private static final long serialVersionUID=1L;


    /** 主键 */
    @TableId(value = "client_id" ,type = IdType.INPUT)
    private Long clientId;

    /** 《申报专业技术任职资格诚信承诺书》 */
    @Excel(name = "《申报专业技术任职资格诚信承诺书》")
    private String honestImgs;

    /** 任现职以来工作情况证明材料(含教学) */
    @Excel(name = "任现职以来工作情况证明材料(含教学)")
    private String jobMaterielImgs;

    /** 机关调入、海外引进、非国有单位证明扫描件 */
    @Excel(name = "机关调入、海外引进、非国有单位证明扫描件")
    private String unitMaterielImgs;

    /** 任职以来获得的专业奖励证书 */
    @Excel(name = "任职以来获得的专业奖励证书")
    private String skillAwardImgs;

    /** 任职以来获得的其他奖励证书 */
    @Excel(name = "任职以来获得的其他奖励证书")
    private String skillAwardOtherImgs;

    /** 任职以来参加继续教育培训证书或证明材料 */
    @Excel(name = "任职以来参加继续教育培训证书或证明材料")
    private String keepEduImgs;

    /** 年度考核材料 */
    @Excel(name = "年度考核材料")
    private String recentExamImgs;

    /** 申报异常:-100|审核驳回:-20|暂存:0|待审核:10|审核通过:20|申报中:30|申报成功:100 */
    @Excel(name = "申报异常:-100|审核驳回:-20|暂存:0|待审核:10|审核通过:20|申报中:30|申报成功:100")
    private Integer reportStatus;

    /** 创建者 */
    private String createBy;

    /** 创建时间 */
    private Date createTime;

    /** 更新者 */
    private String updateBy;

    /** 修改时间 */
    private Date updateTime;

    @TableField(exist = false)
    private Map<String, Object> params = new HashMap<>();
}
