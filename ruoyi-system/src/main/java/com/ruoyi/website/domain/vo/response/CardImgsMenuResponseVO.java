package com.ruoyi.website.domain.vo.response;

import com.ruoyi.common.core.domain.BaseResponseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@ApiModel(value = "证件电子图片菜单" )
public class CardImgsMenuResponseVO extends BaseResponseVO {

    public CardImgsMenuResponseVO() {

    }

    public CardImgsMenuResponseVO(String fileType, String menuName, int required, int uploaded,String menuUrl) {
        this.fileType = fileType;
        this.menuName = menuName;
        this.required = required;
        this.uploaded = uploaded;
        this.menuUrl = menuUrl;
    }
    @ApiModelProperty(value = "文件类型")
    private String fileType;//类型
    @ApiModelProperty(value = "菜单名称")
    private String menuName;//菜单名称
    @ApiModelProperty(value = "是否必填 0 非必填  1 必填")
    private int required=0;//是否必填 0 非必填  1 必填
    @ApiModelProperty(value = "是否上传过 0 没有上传过 1 上传过")
    private int uploaded=0;//是否上传过 0 没有上传过 1 上传过
    @ApiModelProperty(value = "菜单链接地址")
    private String menuUrl;


}
