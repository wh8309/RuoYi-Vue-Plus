package com.ruoyi.website.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 证件电子图片对象 client_card_imgs
 *
 * @author ruoyi
 * @date 2021-03-18
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("client_card_imgs")
public class ClientCardImgs implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 主键
     */
    @TableId(value = "client_id", type = IdType.INPUT)
    private Long clientId;

    /**
     * 身份证（正、反面2张）
     */
    @Excel(name = "身份证", readConverterExp = "正=、反面2张")
    private String idcardImgs;

    /**
     * 申报学历证
     */
    @Excel(name = "申报学历证")
    private String eduImgs;

    /**
     * 申报学位证
     */
    @Excel(name = "申报学位证")
    private String degreeImgs;

    /**
     * 职称外语证书
     */
    @Excel(name = "职称外语证书")
    private String forLangImgs;

    /**
     * 职称计算机证书
     */
    @Excel(name = "职称计算机证书")
    private String computeImgs;

    /**
     * 二级公证员职称证书
     */
    @Excel(name = "二级公证员职称证书")
    private String level2CivilServantImgs;

    /**
     * 职(执)业资格证书
     */
    @Excel(name = "职(执)业资格证书")
    private String jobCredentialImgs;

    /**
     * 水平能力测试合格证
     */
    @Excel(name = "水平能力测试合格证")
    private String abilityImgs;

    /**
     * 职称资格确认证明材料
     */
    @Excel(name = "职称资格确认证明材料")
    private String postConfirmMateriel;

    /**
     * 其他证明材料
     */
    @Excel(name = "其他证明材料")
    private String otherProveMaterielImgs;

    /**
     * 申报异常:-100|审核驳回:-20|暂存:0|待审核:10|审核通过:20|申报中:30|申报成功:100
     */
    @Excel(name = "申报异常:-100|审核驳回:-20|暂存:0|待审核:10|审核通过:20|申报中:30|申报成功:100")
    private Integer reportStatus;

    /**
     * 创建者
     */
    private String createBy;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新者
     */
    private String updateBy;

    /**
     * 修改时间
     */
    private Date updateTime;

    @TableField(exist = false)
    private Map<String, Object> params = new HashMap<>();
}
