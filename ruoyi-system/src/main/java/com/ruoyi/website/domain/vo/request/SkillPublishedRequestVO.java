package com.ruoyi.website.domain.vo.request;

import com.ruoyi.common.core.domain.BaseRequestVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

//@ApiModel(value = "任期内发表论文论著(新增/修改)VO")
@Data
public class SkillPublishedRequestVO extends BaseRequestVO {

    @ApiModelProperty(value = "主键id")
    private Long skillPublishedPapersId;
    @ApiModelProperty(value = "客户Id,前端网站可以不传，默认取当前登录的用户id;管理端该参数必填")
    private Long clientId;
    @ApiModelProperty(value = "出版年月")
    private String publishedPaperTime;

    @NotBlank(message = "论文、论著名称不能为空")
    @Size(min = 0, max = 200, message = "论文、论著名称不能超过200个字符")
    @ApiModelProperty(value = "论文名称")
    private String publishedPaperName;
    @ApiModelProperty(value = "作者序")
    private String publishedAuthorName;
    @ApiModelProperty(value = "刊物(出版社)名称")
    private String publicationsName;
    @ApiModelProperty(value = "刊号(ISSN/CN/ISBN)")
    private String publicationsNumb;
    @ApiModelProperty(value = "刊物级别")
    private String publicationsLevel;


}
