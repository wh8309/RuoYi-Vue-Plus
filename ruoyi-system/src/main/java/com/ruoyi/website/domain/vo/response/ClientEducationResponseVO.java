package com.ruoyi.website.domain.vo.response;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.domain.BaseResponseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 学历信息对象 client_education
 *
 * @author ruoyi
 * @date 2021-03-22
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "学历信息对象" )
public class ClientEducationResponseVO extends BaseResponseVO {

private static final long serialVersionUID=1L;


    /** 主键 */
    @TableId(value = "client_id")
    private Long clientId;

    /** 第一学历 */
    @ApiModelProperty(value = "第一学历")
    private String firstEduName;

    /** 毕业时间 */
    @ApiModelProperty(value = "毕业时间")
    private String firstEduGraduateDate;

    /** 毕业学校 */
    @ApiModelProperty(value = "毕业学校")
    private String firstEduGraduateSchool;

    /** 专业 */
    @ApiModelProperty(value = "专业")
    private String firstEduMajorName;

    /** 学位 */
    @ApiModelProperty(value = "学位")
    private String firstEduDegree;

    /** 培养方式 */
    @ApiModelProperty(value = "培养方式")
    private String firstEduStudyWay;

    /** 最高学历 */
    @ApiModelProperty(value = "最高学历")
    private String topEduName;

    /** 毕业时间 */
    @ApiModelProperty(value = "毕业时间")
    private String topEduGraduateDate;

    /** 毕业学校 */
    @ApiModelProperty(value = "毕业学校")
    private String topEduGraduateSchool;

    /** 专业 */
    @ApiModelProperty(value = "专业")
    private String topEduMajorName;

    /** 学位 */
    @ApiModelProperty(value = "学位")
    private String topEduDegree;

    /** 培养方式 */
    @ApiModelProperty(value = "培养方式")
    private String topEduStudyWay;

    /** 申报异常:-100  审核驳回:-20  暂存:0  待审核:10  审核通过：20 申报中:30 申报成功:100 */
    @ApiModelProperty(value = "申报异常:-100  审核驳回:-20  暂存:0  待审核:10  审核通过：20 申报中:30 申报成功:100")
    private Integer reportStatus;

    /** 创建者 */
    private String createBy;

    /** 创建时间 */
    private Date createTime;

    /** 更新者 */
    private String updateBy;

    /** 修改时间 */
    private Date updateTime;

}
