package com.ruoyi.website.domain.vo.request;

import com.ruoyi.common.core.domain.AbstractEntity;
import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * 任期内专业技术业绩与成果报告对象 client_skill_summary
 *
 * @author ruoyi
 * @date 2021-03-24
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
//@ApiModel(value = "任期内专业技术业绩与成果报告对象(新增/修改)")
public class ClientSkillSummaryRequest extends AbstractEntity {

    private static final long serialVersionUID = 1L;


    /**
     * 主键
     */
    @ApiModelProperty(value = "client_id")
    private Long clientId;

    /**
     * 任期内专业技术工作总结
     */
    @NotBlank(message = "任期内专业技术工作总结不能为空")
    @Size(min = 1, max = 1500, message = "任期内专业技术工作总结最多不能超过1500个字符")
    @ApiModelProperty(value = "任期内专业技术工作总结")
    private String inSkillReportContent;

    /**
     * 任期内专业技术工作总结附件
     */
    @ApiModelProperty(value = "任期内专业技术工作总结附件")
    private String inSkillReportAttach;


}
