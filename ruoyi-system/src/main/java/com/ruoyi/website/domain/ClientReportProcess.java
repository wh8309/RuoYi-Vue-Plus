package com.ruoyi.website.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import com.ruoyi.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * 申报流程处理(流转意见)对象 client_report_process
 * 
 * @author ruoyi
 * @date 2021-04-29
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("client_report_process")
public class ClientReportProcess implements Serializable {

private static final long serialVersionUID=1L;


    /** 主键id */
    @TableId(value = "report_process_id")
    private Long reportProcessId;

    /** 客户id */
    @Excel(name = "客户id")
    private Long clientId;

    /** 申报状态：-100-申报异常 -30-复核驳回 -20-审核驳回 0-填报中 10-待审核 20-审核通过 30-复核通过 100-申报成功 */
    @Excel(name = "申报状态：-100-申报异常 -30-复核驳回 -20-审核驳回 0-填报中 10-待审核 20-审核通过 30-复核通过 100-申报成功")
    private Integer reportStatus;

    /** 处理人 */
    @Excel(name = "处理人")
    private Long transactorUserId;

    /** 处理意见 */
    @Excel(name = "处理意见")
    private String transactorSuggestion;

    /** 备注 */
    @Excel(name = "备注")
    private String remark;

    /** 创建者 */
    private String createBy;

    /** 创建时间 */
    private Date createTime;


}
