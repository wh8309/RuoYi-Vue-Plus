package com.ruoyi.website.domain.vo.response;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.domain.BaseResponseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 转评条件说明对象 client_referral_type
 *
 * @author ruoyi
 * @date 2021-03-24
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "转评条件说明对象" )
public class ClientReferralTypeResponse extends BaseResponseVO {

private static final long serialVersionUID=1L;

    /** 主键 */
    @ApiModelProperty(value =  "client_id")
    private Long clientId;

    /** 转评条件 */
     @ApiModelProperty(value =  "转评条件")
    private String referralTypeDesc;


}
