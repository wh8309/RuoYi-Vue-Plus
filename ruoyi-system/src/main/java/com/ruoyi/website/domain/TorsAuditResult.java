package com.ruoyi.website.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import com.ruoyi.common.annotation.Excel;

/**
 * 审核未通过对象 tors_audit_result
 * 
 * @author admin
 * @date 2021-07-07
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("tors_audit_result")
public class TorsAuditResult implements Serializable {

private static final long serialVersionUID=1L;


    /** 主键 */
    @TableId(value = "audit_result_id")
    private Long auditResultId;

    private Long clientId;

    /** 客户姓名 */
    private String clientName;

    /** 推荐单位id */
    private Long nominateUnitId;

    /** 单位名称 */
    private String unitName;

    /** 送达时间(接收时间) */
    private Date receiveTime;

    /** 审核单位 */
    private String auditUnit;

    /** 流程状态描述 */
    private String flowStatusDesc;

    /** 审核时间 */
    private Date auditTime;

    /** 审核结果 */
    private String auditResult;

    /** 审核意见 */
    private String auditOpinion;

    /** 短信轮询结束日期 */
    private Date smsLoopEndTime;

    /** 业务类型 1-进展通知 2-补充资料 */
    private Integer businessType;

    /** 处理状态 1-已处理 0-未处理 */
    private Integer dealStatus;

    /**  */
    private String remark;

    /**  */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /** 更新者 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /**  */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

}
