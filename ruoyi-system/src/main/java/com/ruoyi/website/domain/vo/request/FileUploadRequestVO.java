package com.ruoyi.website.domain.vo.request;

import com.ruoyi.common.core.domain.BaseRequestVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@ApiModel(value = "文件上传VO")
@Data
public class FileUploadRequestVO extends BaseRequestVO {
    @ApiModelProperty(value = "客户Id,前端网站可以不传，默认取当前登录的用户id;管理端该参数必填")
    private Long clientId;
    @ApiModelProperty(value = "业务id('专业论文论著'和'个人专业工作业绩的材料'列表数据id")
    private Long businessId;
    @ApiModelProperty(value = "文件备注说明")
    private String remark;
    private MultipartFile file;
    @ApiModelProperty(value = "文件类型")
    private Integer fileType;
}
