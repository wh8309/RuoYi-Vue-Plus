package com.ruoyi.website.domain.vo.request;

import com.ruoyi.common.core.domain.BaseRequestVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

//@ApiModel(value = "任期内发表论文论著查询条件VO")
@Data
public class SkillPublishedQueryRequestVO extends BaseRequestVO {

    @ApiModelProperty(value = "客户Id,前端网站可以不传，默认取当前登录的用户id;管理端该参数必填")
    private Long clientId;

}
