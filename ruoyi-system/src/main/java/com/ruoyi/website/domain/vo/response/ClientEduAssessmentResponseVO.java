package com.ruoyi.website.domain.vo.response;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.domain.BaseResponseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;



/**
 * 年度考核与继续教育对象 client_edu_assessment
 *
 * @author ruoyi
 * @date 2021-03-22
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "年度考核与继续教育对象" )
public class ClientEduAssessmentResponseVO extends BaseResponseVO {


    /** 主键 */
    @ApiModelProperty(value =  "主键")
    private Long clientId;

    /** 考核年度 */
    @ApiModelProperty(value =  "考核年度")
    private String recentExamYear1;

    /** 考核结果 */
    @ApiModelProperty(value =  "考核结果")
    private String recentExamResult1;

    /** 考核年度 */
    @ApiModelProperty(value =  "考核年度")
    private String recentExamYear2;

    /** 考核结果 */
    @ApiModelProperty(value =  "考核结果")
    private String recentExamResult2;

    /** 考核年度 */
    @ApiModelProperty(value =  "考核年度")
    private String recentExamYear3;

    /** 考核结果 */
    @ApiModelProperty(value =  "考核结果")
    private String recentExamResult3;

    /** 考核年度 */
    @ApiModelProperty(value =  "考核年度")
    private String recentExamYear4;

    /** 考核结果 */
    @ApiModelProperty(value =  "考核结果")
    private String recentExamResult4;

    /** 考核年度 */
    @ApiModelProperty(value =  "考核年度")
    private String recentExamYear5;

    /** 考核结果 */
    @ApiModelProperty(value =  "考核结果")
    private String recentExamResult5;

    /** 外语年度 */
    @ApiModelProperty(value =  "外语年度")
    private String forLangYear;

    /** 考试结果 */
    @ApiModelProperty(value =  "考试结果")
    private String forLangExamResult;

    /** 计算机情况 */
    @ApiModelProperty(value =  "计算机情况")
    private String computeYear;

    /** 考试结果 */
    @ApiModelProperty(value =  "考试结果")
    private String computeResult;

    /** 水平能力测试 */
    @ApiModelProperty(value =  "水平能力测试")
    private String abilityYear;

    /** 考试结果 */
    @ApiModelProperty(value =  "考试结果")
    private String abilityExamReuslt;

    /** 继续教育 开始年度 */
    @ApiModelProperty(value =  "继续教育 开始年度")
    private String keepEduStartYear;

    /** 继续教育 结束年度 */
    @ApiModelProperty(value =  "继续教育 结束年度")
    private String keepEduEndYear;

    /** 学时 */
    @ApiModelProperty(value =  "学时")
    private String keepEduStudyHour;


}

