package com.ruoyi.website.domain.vo.response;

import com.ruoyi.common.core.domain.BaseResponseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "评审申报材料菜单" )
public class RecordDatumResponseVO extends BaseResponseVO {


    @ApiModelProperty(value = "是否必填 0 非必填  1 必填")
    private int required=0;//是否必填 0 非必填  1 必填
    @ApiModelProperty(value = "是否上传过 0 没有上传过 1 上传过")
    private int uploaded=0;//是否上传过 0 没有上传过 1 上传过
    @ApiModelProperty(value = "菜单名称")
    private String menuName;//菜单名称
    @ApiModelProperty(value = "菜单链接地址")
    private String menuUrl;
    public RecordDatumResponseVO() {

    }
    public RecordDatumResponseVO( String menuName, String menuUrl) {
        this.menuName = menuName;
        this.menuUrl = menuUrl;
    }

}
