package com.ruoyi.website.domain.vo.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class TorsWorkUnitResponseVO {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "单位id")
    private Long workUnitId;

    @ApiModelProperty(value = "单位名称")
    private String unitName;

    @ApiModelProperty(value = "单位编码")
    private String unitCode;

    @ApiModelProperty(value = "联系人")
    private String unitContact;

    @ApiModelProperty(value = "办公电话")
    private String unitPhone;

}
