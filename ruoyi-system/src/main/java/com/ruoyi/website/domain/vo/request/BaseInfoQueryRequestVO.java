package com.ruoyi.website.domain.vo.request;

import com.ruoyi.common.core.domain.AbstractEntity;
import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
//@ApiModel(value = "基础信息查询对象" )
public class BaseInfoQueryRequestVO extends AbstractEntity {

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键clientId")
    private Long clientId;

}
