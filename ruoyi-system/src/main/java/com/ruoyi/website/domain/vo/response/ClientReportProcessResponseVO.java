package com.ruoyi.website.domain.vo.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseResponseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 客户申报审核记录对象 client_report_audit
 *
 * @author ruoyi
 * @date 2021-04-01
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "流转意见")
public class ClientReportProcessResponseVO extends BaseResponseVO {

    private static final long serialVersionUID = 1L;

    /** 主键id */
    @ApiModelProperty(value = "report_process_id")
    private Long reportProcessId;

    /** 客户id */
    @ApiModelProperty(value = "客户id")
    private Long clientId;

    /** 申报状态：-100-申报异常 -30-复核驳回 -20-审核驳回 0-填报中 10-待审核 20-审核通过 30-复核通过 100-申报成功 */
    @ApiModelProperty(value = "申报状态：-30-复核驳回 -20-审核驳回 0-填报中 10-待审核 20-审核通过 30-复核通过 ")
    private Integer reportStatus;

    /** 处理人 */
    @ApiModelProperty(value = "处理人ID")
    private Long transactorUserId;

    /** 处理意见 */
    @ApiModelProperty(value = "处理意见")
    private String transactorSuggestion;

    /** 备注 */
    @Excel(name = "备注")
    private String remark;

    /** 创建者 */
    @ApiModelProperty(value = "创建者")
    private String createBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

}
