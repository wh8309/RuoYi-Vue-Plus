package com.ruoyi.website.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import com.ruoyi.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * 任期内发论文论著情况对象 client_skill_published_papers
 *
 * @author ruoyi
 * @date 2021-03-18
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("client_skill_published_paper")
public class ClientSkillPublishedPaper implements Serializable {

private static final long serialVersionUID=1L;


    /** 主键 */
    @TableId(value = "skill_published_papers_id")
    private Long skillPublishedPapersId;

    /** 主键 */
    @Excel(name = "主键")
    private Long clientId;

    /** 出版年月 */
    @Excel(name = "出版年月")
    private String publishedPaperTime;

    /** 论文名称 */
    @Excel(name = "论文名称")
    private String publishedPaperName;

    /** 作者序 */
    @Excel(name = "作者序")
    private String publishedAuthorName;

    /** 刊物(出版社)名称 */
    @Excel(name = "刊物(出版社)名称")
    private String publicationsName;

    /** 刊号(ISSN/CN/ISBN) */
    @Excel(name = "刊号(ISSN/CN/ISBN)")
    private String publicationsNumb;

    /** 刊物级别 */
    @Excel(name = "刊物级别")
    private String publicationsLevel;

    /** 相关材料,一个JSON结构的数组 */
    @Excel(name = "相关材料,一个JSON结构的数组")
    private String publicationsMaterials;

    /** 申报异常:-100  审核驳回:-20  暂存:0  待审核:10  审核通过：20 申报中:30 申报成功:100 */
    @Excel(name = "申报异常:-100  审核驳回:-20  暂存:0  待审核:10  审核通过：20 申报中:30 申报成功:100")
    private Integer reportStatus;


    private Integer isDeleted;

    /** 创建者 */
    private String createBy;

    /** 创建时间 */
    private Date createTime;

    /** 更新者 */
    private String updateBy;

    /** 修改时间 */
    private Date updateTime;

}
