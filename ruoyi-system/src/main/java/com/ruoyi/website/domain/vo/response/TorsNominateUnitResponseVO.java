package com.ruoyi.website.domain.vo.response;


import com.ruoyi.common.core.domain.BaseResponseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TorsNominateUnitResponseVO extends BaseResponseVO {

    private static final long serialVersionUID = 1839987880463639422L;

    @ApiModelProperty(value = "主键id")
    private Long nominateUnitId;

    @ApiModelProperty(value = "单位名称")
    private String unitName;

    @ApiModelProperty(value = "单位地址")
    private String unitAddress;

    @ApiModelProperty(value = "单位联系人")
    private String unitContact;

    @ApiModelProperty(value = "单位办公电话")
    private String unitPhone;

    @ApiModelProperty(value = "申报授权码")
    private String unitAuthCode;

    @ApiModelProperty(value = "政务网账号")
    private String account;

    @ApiModelProperty(value = "政务网密码")
    private String password;

    @ApiModelProperty(value = "排序")
    private Long orderNo;

    @ApiModelProperty(value = "短信通知 1-启用 0-禁用")
    private Integer smsEnable;

    @ApiModelProperty(value = "短信接收号码")
    private String smsReceiveMobiles;

}
