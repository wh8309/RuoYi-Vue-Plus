package com.ruoyi.website.domain.vo.request;

import com.ruoyi.common.core.domain.AbstractEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Data
@NoArgsConstructor
//@ApiModel(value = "基础信息操作(新增/修改)VO")
public class BaseInfoRequestVO extends AbstractEntity {

    @ApiModelProperty(value = "主键")
    private Long clientId;

    /**
     * 政务网账号
     */
    @ApiModelProperty(value = "政务网账号")
    private String account;

    /**
     * 政务网密码
     */
    @ApiModelProperty(value = "政务网密码")
    private String password;

    /**
     * 推荐单位
     */
    @ApiModelProperty(value = "推荐单位")
    private String nominateUnit;

    /**
     * 推荐单位授权码
     */
    @ApiModelProperty(value = "推荐单位授权码")
    private String unitAuthCode;

    /**
     * 评审通知
     */
    @ApiModelProperty(value = "评审通知")
    private String reviewNotice;

    /**
     * 申报年度
     */
    @ApiModelProperty(value = "申报年度")
    private String reportYear;

    /**
     * 评委会名称
     */
    @ApiModelProperty(value = "评委会名称")
    private String judgesName;

    /**
     * 所属系列
     */
    @ApiModelProperty(value = "所属系列")
    private String belongSeries;

    /**
     * 申报专业
     */
    @ApiModelProperty(value = "申报专业")
    @NotBlank(message = "申报专业不能为空")
    private String reportSpecialty;

    /**
     * 专业名称
     */
    @ApiModelProperty(value = "专业名称")
    @NotBlank(message = "专业名称不能为空")
    private String specialtyName;

    /**
     * 本专业工作年限
     */
    @ApiModelProperty(value = "本专业工作年限")
    @NotNull(message = "本专业工作年限不能为空")
    private Integer reportSpecialtyJobYear;

    /**
     * 姓名
     */
    @NotBlank(message = "姓名不能为空")
    @ApiModelProperty(value = "姓名")
    private String name;

    /**
     * 曾用名
     */
    @ApiModelProperty(value = "曾用名")
    private String beforeUsedName;

    /**
     * 身份证号码
     */
    @ApiModelProperty(value = "身份证号码")
    @NotBlank(message = "身份证号码不能为空")
    private String idcard;

    /**
     * 性别
     */
    @NotBlank(message = "性别不能为空")
    @ApiModelProperty(value = "性别")
    private String sex;

    /**
     * 出生日期
     */
    @NotBlank(message = "出生日期不能为空")
    @ApiModelProperty(value = "出生日期")
    private String birthday;

    /**
     * 移动电话
     */
    @ApiModelProperty(value = "移动电话")
    @NotBlank(message = "移动电话不能为空")
    private String mobile;

    /**
     * 工作单位
     */
    @ApiModelProperty(value = "工作单位")
    @NotBlank(message = "工作单位不能为空")
    private String jobUnit;

    /**
     * 参加工作日期
     */
    @NotBlank(message = "参加工作日期不能为空")
    @ApiModelProperty(value = "参加工作日期")
    private String firstJobDate;

    /**
     * 编码单位
     */
    @NotBlank(message = "编码单位不能为空")
    @ApiModelProperty(value = "编码单位")
    private String codeUnit;

    /**
     * 持何职业资格(或一体化)证书日期
     */
    @ApiModelProperty(value = "持何职业资格(或一体化)证书日期")
    private String professionDate;

    /**
     * 持何职业资格(或一体化)证书名称
     */
    @ApiModelProperty(value = "持何职业资格(或一体化)证书名称")
    private String professionName;

    /**
     * 岗位及行政职务
     */
    @NotBlank(message = "岗位及行政职务不能为空")
    @ApiModelProperty(value = "岗位及行政职务")
    private String staffPost;

    /**
     * 现职称
     */
    @ApiModelProperty(value = "现职称")
    @NotBlank(message = "现职称不能为空")
    private String nowPostName;

    /**
     * 批准时间
     */
    @ApiModelProperty(value = "批准时间")
    private String nowPostAuthDate;

    /**
     * 批准文号
     */
    @ApiModelProperty("批准文号")
    private String nowPostAuthNumber;

    /**
     * 现专业技术职称审批机关
     */
    @ApiModelProperty(value = "现专业技术职称审批机关")
    private String nowPostCheckUnit;

    /**
     * 是否破格
     */
    @ApiModelProperty(value = "是否破格")
    private String isBreakRule;

    /**
     * 政治面貌
     */
    @ApiModelProperty(value = "政治面貌")
    private String politicalStatus;

    /**
     * 申报职称
     */
    @NotBlank(message = "申报职称不能为空")
    @ApiModelProperty(value = "申报职称")
    private String declarePost;

    /**
     * 转评类型
     */
    @ApiModelProperty(value = "转评类型")
    private String referralType;

    /**
     * 资格确认
     */
    @ApiModelProperty(value = "资格确认")
    private String condConfirm;

    /**
     * 认定类型
     */
    @ApiModelProperty(value = "认定类型")
    private String reportTypeConfirm;

    /**
     * 是否贫困县
     */
    @ApiModelProperty(value = "是否贫困县")
    private String isPoorCounty;

    /**
     * 是否基层
     */
    @ApiModelProperty(value = "是否基层")
    private String isBasicLevel;

    /**
     * 特殊贡献情况
     */
    @ApiModelProperty(value = "特殊贡献情况")
    private String specialDevote;

    /**
     * 申报异常:-100  审核驳回:-20  暂存:0  待审核:10  审核通过：20 申报中:30 申报成功:100
     */
    @ApiModelProperty(value = "申报异常:-100  审核驳回:-20  暂存:0  待审核:10  审核通过：20 申报中:30 申报成功:100")
    private Integer reportStatus;

    /**
     * 排序字段
     */
    @ApiModelProperty(value = "排序字段")
    private Long orderNo;

    /**
     * 目录名称
     */
    @ApiModelProperty(value = "目录名称")
    private String directoryName;

    /**
     * 编码单位分类
     */
    @ApiModelProperty(value = "编码单位分类")
    private String codeClassify;

    /**
     * 职称级别
     */
    @ApiModelProperty(value = "职称级别")
    private String postNameRank;

    @ApiModelProperty(value = "民族")
    private String nation;

    @ApiModelProperty(value = "证件类型")
    private String certType;

    @ApiModelProperty(value = "工作单位层级")
    private String jobUnitLevel;

    @ApiModelProperty(value = "现职称ID")
    private String nowPostTreeId;

    @ApiModelProperty(value = "编码单位id")
    private String codeUnitTreeId;

    @ApiModelProperty(value = "工作单位类型")
    private String jobUnitType;
}
