package com.ruoyi.website.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import com.ruoyi.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * 推荐单位对象 tors_nominate_unit
 * 
 * @author ruoyi
 * @date 2021-04-22
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("tors_nominate_unit")
public class TorsNominateUnit implements Serializable {

private static final long serialVersionUID=1L;


    /** 主键 */
    @TableId(value = "nominate_unit_id")
    private Long nominateUnitId;

    /** 单位名称 */
    @Excel(name = "单位名称")
    private String unitName;

    /** 单位地址 */
    @Excel(name = "单位地址")
    private String unitAddress;

    /** 单位联系人 */
    @Excel(name = "单位联系人")
    private String unitContact;

    /** 单位办公电话 */
    @Excel(name = "单位办公电话")
    private String unitPhone;

    /** 申报授权码 */
    @Excel(name = "申报授权码")
    private String unitAuthCode;

    /** 政务网账号 */
    @Excel(name = "政务网账号")
    private String account;

    /** 政务网密码 */
    @Excel(name = "政务网密码")
    private String password;

    /** 是否删除 1 正常 -1 删除 */
    @Excel(name = "是否删除 1 正常 -1 删除")
    private Integer isDeleted;

    /** 排序字段 */
    @Excel(name = "排序字段")
    private Long orderNo;

    /** 创建者 */
    private String createBy;

    /** $column.columnComment */
    private Date createTime;

    /** 更新者 */
    private String updateBy;

    /** $column.columnComment */
    private Date updateTime;

    private String remark;

    /** 是否启用短信通知 0-不启用  1-启用 */
    private Integer smsEnable;

    /** 短信接收手机(以逗号分隔) */
    private String smsReceiveMobiles;

    @TableField(exist = false)
    private Map<String, Object> params = new HashMap<>();
}
