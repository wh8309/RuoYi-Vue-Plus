package com.ruoyi.website.domain.vo.response;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.domain.BaseResponseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 任期内专业技术业绩与成果报告对象 client_skill_summary
 *
 * @author ruoyi
 * @date 2021-03-24
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "任期内专业技术业绩与成果报告对象" )
public class ClientSkillSummaryResponse extends BaseResponseVO {

private static final long serialVersionUID=1L;


    /** 主键 */
    @ApiModelProperty(value =  "client_id")
    private Long clientId;

    /** 任期内专业技术工作总结 */
    @ApiModelProperty(value =  "任期内专业技术工作总结")
    private String inSkillReportContent;

    /** 任期内专业技术工作总结附件 */
    @ApiModelProperty(value = "任期内专业技术工作总结附件")
    private String inSkillReportAttach;


}
