package com.ruoyi.website.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 年度考核与继续教育对象 client_edu_assessment
 *
 * @author ruoyi
 * @date 2021-03-22
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("client_edu_assessment")
public class ClientEduAssessment implements Serializable {

private static final long serialVersionUID=1L;


    /** 主键 */
    @TableId(value = "client_id",type = IdType.INPUT)
    private Long clientId;

    /** 考核年度 */
    @ApiModelProperty(value =  "考核年度")
    private String recentExamYear1;

    /** 考核结果 */
    @ApiModelProperty(value =  "考核结果")
    private String recentExamResult1;

    /** 考核年度 */
    @ApiModelProperty(value =  "考核年度")
    private String recentExamYear2;

    /** 考核结果 */
    @ApiModelProperty(value =  "考核结果")
    private String recentExamResult2;

    /** 考核年度 */
    @ApiModelProperty(value =  "考核年度")
    private String recentExamYear3;

    /** 考核结果 */
    @ApiModelProperty(value =  "考核结果")
    private String recentExamResult3;

    /** 考核年度 */
    @ApiModelProperty(value =  "考核年度")
    private String recentExamYear4;

    /** 考核结果 */
    @ApiModelProperty(value =  "考核结果")
    private String recentExamResult4;

    /** 考核年度 */
    @ApiModelProperty(value =  "考核年度")
    private String recentExamYear5;

    /** 考核结果 */
    @ApiModelProperty(value =  "考核结果")
    private String recentExamResult5;

    /** 外语年度 */
    @ApiModelProperty(value =  "外语年度")
    private String forLangYear;

    /** 考试结果 */
    @ApiModelProperty(value =  "考试结果")
    private String forLangExamResult;

    /** 计算机情况 */
    @ApiModelProperty(value =  "计算机情况")
    private String computeYear;

    /** 考试结果 */
    @ApiModelProperty(value =  "考试结果")
    private String computeResult;

    /** 水平能力测试 */
    @ApiModelProperty(value =  "水平能力测试")
    private String abilityYear;

    /** 考试结果 */
    @ApiModelProperty(value =  "考试结果")
    private String abilityExamReuslt;

    /** 继续教育 开始年度 */
    @ApiModelProperty(value =  "继续教育 开始年度")
    private String keepEduStartYear;

    /** 继续教育 结束年度 */
    @ApiModelProperty(value =  "继续教育 结束年度")
    private String keepEduEndYear;

    /** 学时 */
    @ApiModelProperty(value =  "学时")
    private String keepEduStudyHour;

    /** 申报异常:-100  审核驳回:-20  暂存:0  待审核:10  审核通过：20 申报中:30 申报成功:100 */
    @ApiModelProperty(value =  "申报异常:-100  审核驳回:-20  暂存:0  待审核:10  审核通过：20 申报中:30 申报成功:100")
    private Integer reportStatus;

    /** 创建者 */
    private String createBy;

    /** 创建时间 */
    private Date createTime;

    /** 更新者 */
    private String updateBy;

    /** 修改时间 */
    private Date updateTime;

    @TableField(exist = false)
    private Map<String, Object> params = new HashMap<>();
}
