package com.ruoyi.website.domain.vo.request;

import com.ruoyi.common.core.domain.AbstractEntity;
import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
 * 从事专业技术工作简历对象 client_resume
 *
 * @author ruoyi
 * @date 2021-03-22
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
//@ApiModel(value = "从事专业技术工作简历对象(新增/修改)")
public class ClientResumeRequest extends AbstractEntity {

    private static final long serialVersionUID = 1L;


    /**
     * 主键
     */
    @ApiModelProperty(value = "client_id")
    private Long clientId;

    /**
     * 从事专业技术工作简历
     */
    @NotBlank(message = "从事专业技术工作简历不能为空")
    @ApiModelProperty(value = "从事专业技术工作简历")
    private String skillJobResume;


}
