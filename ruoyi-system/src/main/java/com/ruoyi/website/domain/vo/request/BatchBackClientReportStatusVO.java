package com.ruoyi.website.domain.vo.request;

import com.ruoyi.common.core.domain.AbstractEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
//@ApiModel(value = "批量流程退回")
public class BatchBackClientReportStatusVO extends AbstractEntity {

    @ApiModelProperty(value = "所选客户:多个以英文逗号分隔")
    @NotBlank(message = "所选客户不能为空")
    private String clientIds;

    @ApiModelProperty(value = "流程节点,对应数据字典:client_report_rollback_status")
    @NotNull(message = "流程节点不能为空")
    private Integer reportStatus;

}
