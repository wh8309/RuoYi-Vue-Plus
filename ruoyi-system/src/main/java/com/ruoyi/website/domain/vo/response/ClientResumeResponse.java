package com.ruoyi.website.domain.vo.response;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.domain.BaseResponseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 从事专业技术工作简历对象 client_resume
 *
 * @author ruoyi
 * @date 2021-03-22
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "从事专业技术工作简历对象" )
public class ClientResumeResponse extends BaseResponseVO {

private static final long serialVersionUID=1L;


    /** 主键 */
    @ApiModelProperty(value = "client_id")
    private Long clientId;

    /** 从事专业技术工作简历 */
    @ApiModelProperty(value = "从事专业技术工作简历")
    private String skillJobResume;


}
