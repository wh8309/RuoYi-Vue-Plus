package com.ruoyi.website.domain.vo.request;

import com.ruoyi.common.core.domain.AbstractEntity;
import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * 用户信息对象 client_profile
 *
 * @author ruoyi
 * @date 2021-04-01
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
public class ClientProfileAddRequestVO extends AbstractEntity {

    private static final long serialVersionUID = 1L;


    /**
     * 姓名
     */
    @ApiModelProperty(value = "姓名")
    @NotBlank(message = "姓名不能为空")
    private String name;

    /**
     * 性别
     */
    @ApiModelProperty(value = "用户性别 1=男,2=女")
    private String sex;

    /**
     * 身份证号码
     */
    @ApiModelProperty(value = "身份证号码")
    @NotBlank(message = "身份证号码不能为空")
    private String idcard;

    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号")
    @Size(min = 0, max = 11, message = "手机号码长度不能超过11个字符")
    private String mobile;

    /**
     * 微信号
     */
    @ApiModelProperty(value = "微信号")
    private String wechat;

    /**
     * QQ号码
     */
    @ApiModelProperty(value = "QQ号码")
    private String qq;

    /**
     * 部门ID
     */
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /**
     * 用户昵称
     */
    @ApiModelProperty(value = "用户昵称")
    @Size(min = 0, max = 30, message = "用户昵称长度不能超过30个字符")
    private String nickName;

    /**
     * 用户邮箱
     */
    @ApiModelProperty(value = "用户邮箱")
    @Email(message = "邮箱格式不正确")
    private String email;

    @ApiModelProperty(value = "帐号状态 0=正常,1=停用")
    private String status;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;


}
