package com.ruoyi.website.domain.vo.response;

import com.ruoyi.common.core.domain.BaseResponseVO;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class SysTreeResponse extends BaseResponseVO {

    private Long treeId;
    private Long parentTreeId;
    private String treeName;
    private List<SysTreeResponse> children;

    public SysTreeResponse(Long treeId, Long parentTreeId, String treeName) {
        this.treeId = treeId;
        this.parentTreeId = parentTreeId;
        this.treeName = treeName;
    }

}
