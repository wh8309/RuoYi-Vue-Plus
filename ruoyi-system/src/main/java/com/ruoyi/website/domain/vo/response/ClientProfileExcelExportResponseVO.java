package com.ruoyi.website.domain.vo.response;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.annotation.Excels;
import com.ruoyi.common.core.domain.AbstractEntity;
import com.ruoyi.common.core.domain.BaseResponseVO;
import com.ruoyi.common.core.domain.entity.SysDept;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 用户信息对象 client_profile
 *
 * @author ruoyi
 * @date 2021-04-01
 */
@Data
@Accessors(chain = true)
public class ClientProfileExcelExportResponseVO extends BaseResponseVO {

    private static final long serialVersionUID = 1L;

    @Excel(name = "姓名")
    private String name;

    @ApiModelProperty(value = "用户昵称")
    @Excel(name = "用户昵称")
    private String nickName;

    @Excel(name = "登录名", cellType = Excel.ColumnType.TEXT)
    private String loginName;

    @Excel(name = "用户性别", dictType = "sys_user_sex")
    private String sex;

    @Excel(name = "手机号")
    private String mobile;


    @Excel(name = "身份证号码", cellType = Excel.ColumnType.TEXT)
    private String idcard;

    @Excel(name = "微信号")
    private String wechat;

    @Excel(name = "QQ号码")
    private String qq;

    @Excel(name = "用户邮箱")
    private String email;

    @Excel(name = "帐号状态", readConverterExp = "0=正常,1=停用", combo = {"正常", "停用"})
    private String status;

//    @Excel(name = "审批状态", dictType = "client_audit_status" )
//    private String clientAuditStatus;
    /**
     * 部门ID
     */
//    @Excel(name = "部门编号")
//    private Long deptId;

    /**
     * 部门对象
     */
    @Excels(
            @Excel(name = "部门名称", targetAttr = "deptName", type = Excel.Type.EXPORT)
    )
    private SysDept dept;


    /**
     * 备注
     */
    @Excel(name = "备注信息")
    private String remark;

    public ClientProfileExcelExportResponseVO() {
    }
}
