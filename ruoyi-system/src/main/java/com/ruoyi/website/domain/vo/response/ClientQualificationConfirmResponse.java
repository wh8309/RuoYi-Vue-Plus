package com.ruoyi.website.domain.vo.response;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseResponseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 职称资格确认对象 client_qualification_confirm
 *
 * @author ruoyi
 * @date 2021-03-24
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "职称资格确认对象" )
public class ClientQualificationConfirmResponse extends BaseResponseVO {

private static final long serialVersionUID=1L;


    /** 主键 */
    @ApiModelProperty(value = "client_id")
    private Long clientId;

    /** 原工作单位及从事专业 */
    @ApiModelProperty(value =  "原工作单位及从事专业")
    private String originalUnitSkill;

    /** 证书号码 */
    @ApiModelProperty(value =  "证书号码")
    private String cerNo;

    /** 取得资格方式 */
    @ApiModelProperty(value =  "取得资格方式")
    private String fetchWay;

    /** 任职资格批准文号 */
    @ApiModelProperty(value =  "任职资格批准文号")
    private String authNo;

    /** 现工作单位及进入时间 */
    @ApiModelProperty(value =  "现工作单位及进入时间")
    private String nowJobEnterTime;

    /** 政务网查看PDF材料URL */
    @ApiModelProperty(value =  "政务网查看PDF材料URL")
    private String datumUrl;

    /** 是否生成过材料URL 0-未生成 1-生成成功 -1-生成失败 */
    @ApiModelProperty(value = "是否生成过材料URL 0-未生成 1-生成成功 -1-生成失败")
    private Integer isGeneratedUrl;

    /** 机器人申报状态: -100-申报异常 0-待申报 10-申报中  100-申报成功 */
    @ApiModelProperty(value = "机器人申报状态: -100-申报异常 0-待申报 10-申报中  100-申报成功")
    private Integer robotReportStatus;

    /** 异常次数 */
    @ApiModelProperty(value = "异常次数")
    private Integer robotReportExceptionTimes;

    /** 异常码 */
    @ApiModelProperty(value = "异常码")
    private Integer robotReportExceptionCode;

    /** 申报异常信息 */
    @ApiModelProperty(value = "申报异常信息")
    private String robotReportExceptionMsg;
    

}
