package com.ruoyi.website.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 职称资格确认对象 client_qualification_confirm
 *
 * @author ruoyi
 * @date 2021-03-24
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("client_qualification_confirm")
public class ClientQualificationConfirm implements Serializable {

private static final long serialVersionUID=1L;


    /** 主键 */
    @TableId(value = "client_id",type = IdType.INPUT)
    private Long clientId;

    /** 原工作单位及从事专业 */
    @Excel(name = "原工作单位及从事专业")
    private String originalUnitSkill;

    /** 证书号码 */
    @Excel(name = "证书号码")
    private String cerNo;

    /** 取得资格方式 */
    @Excel(name = "取得资格方式")
    private String fetchWay;

    /** 任职资格批准文号 */
    @Excel(name = "任职资格批准文号")
    private String authNo;

    /** 现工作单位及进入时间 */
    @Excel(name = "现工作单位及进入时间")
    private String nowJobEnterTime;

    /** 申报异常:-100|审核驳回:-20|暂存:0|待审核:10|审核通过:20|申报中:30|申报成功:100 */
    @Excel(name = "申报异常:-100|审核驳回:-20|暂存:0|待审核:10|审核通过:20|申报中:30|申报成功:100")
    private Integer reportStatus;

    /** 创建者 */
    private String createBy;

    /** 创建时间 */
    private Date createTime;

    /** 更新者 */
    private String updateBy;

    /** 修改时间 */
    private Date updateTime;

    /** 政务网查看PDF材料URL */
    @Excel(name = "政务网查看PDF材料URL")
    private String datumUrl;

    /** 是否生成过材料URL 0-未生成 1-生成成功 -1-生成失败 */
    @Excel(name = "是否生成过材料URL 0-未生成 1-生成成功 -1-生成失败")
    private Integer isGeneratedUrl;

    /** 机器人申报状态: -100-申报异常 0-待申报 10-申报中  100-申报成功 */
    @Excel(name = "机器人申报状态: -100-申报异常 0-待申报 10-申报中  100-申报成功")
    private Integer robotReportStatus;

    /** 异常次数 */
    @Excel(name = "异常次数")
    private Integer robotReportExceptionTimes;

    /** 异常码 */
    @Excel(name = "异常码")
    private Integer robotReportExceptionCode;

    /** 申报异常信息 */
    @Excel(name = "申报异常信息")
    private String robotReportExceptionMsg;

    /** 上报时间 */
    @Excel(name = "上报时间" , width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date robotReportTime;

    @TableField(exist = false)
    private Map<String, Object> params = new HashMap<>();
}
