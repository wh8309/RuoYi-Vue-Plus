package com.ruoyi.website.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import com.ruoyi.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * 机器人上报流水对象 client_robot_report_log
 * 
 * @author ruoyi
 * @date 2021-06-11
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("client_robot_report_log")
public class ClientRobotReportLog implements Serializable {

private static final long serialVersionUID=1L;


    /** 日志主键 */
    @TableId(value = "report_log_id")
    private Long reportLogId;

    /** 客户id */
    @Excel(name = "客户id")
    private Long clientId;

    /** 客户姓名 */
    @Excel(name = "客户姓名")
    private String name;

    /** 身份证号码 */
    @Excel(name = "身份证号码")
    private String idcard;

    /** 操作模块 */
    @Excel(name = "操作模块")
    private String operModule;

    /** 操作描述 */
    @Excel(name = "操作描述")
    private String operDecription;

    /** $column.columnComment */
    private Date createTime;

    @TableField(exist = false)
    private Map<String, Object> params = new HashMap<>();
}
