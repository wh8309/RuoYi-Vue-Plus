package com.ruoyi.website.domain.vo.response;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.domain.BaseResponseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 任职期间奖励情况对象 client_skill_award
 *
 * @author ruoyi
 * @date 2021-03-24
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "任职期间奖励情况对象" )
public class ClientSkillAwardResponse extends BaseResponseVO {

private static final long serialVersionUID=1L;


    /** 主键 */
    @ApiModelProperty(value ="client_id")
    private Long clientId;

    /** 任职期间奖励情况 */
    @ApiModelProperty(value ="任职期间奖励情况")
    private String inSkillAward;


}
