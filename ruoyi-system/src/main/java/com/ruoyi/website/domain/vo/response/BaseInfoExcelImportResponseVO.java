package com.ruoyi.website.domain.vo.response;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseResponseVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 用户信息对象 client_profile
 *
 * @author ruoyi
 * @date 2021-04-01
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
public class BaseInfoExcelImportResponseVO extends BaseResponseVO {

    private static final long serialVersionUID = 1L;

    @Excel(name = "序号", cellType = Excel.ColumnType.NUMERIC, prompt = "序号")
    private Long clientId;

    @Excel(name = "客户姓名")
    private String name;

    @Excel(name = "手机号码")
    private String mobile;

    @Excel(name = "身份证号码", cellType = Excel.ColumnType.TEXT)
    private String idcard;

    @Excel(name = "部门", dept = true)
    private String deptId;

    @Excel(name = "推荐单位", nominateUnit = true)
    private String nominateUnit;

    @Excel(name = "工作单位", jobUnit = true)
    private String jobUnit;

    @Excel(name = "填报人", fillInUser = true)
    private String fillInUserName;

}
