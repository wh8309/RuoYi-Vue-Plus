package com.ruoyi.website.domain.vo.response;

import com.ruoyi.common.core.domain.BaseResponseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value = "任期内科研成果VO")
@Data
public class SkillResearchResponseVO extends BaseResponseVO {

    public SkillResearchResponseVO() {

    }

    @ApiModelProperty(value = "主键id")
    private Long skillResearchId;
    @ApiModelProperty(value = "科研年度")
    private String researchYear;
    @ApiModelProperty(value = "成果名称")
    private String researchResult;
    @ApiModelProperty(value = "来源")
    private String researchSource;
    @ApiModelProperty(value = "经费")
    private String researchFunding;
    @ApiModelProperty(value = "承担的具体任务及排名")
    private String researchResponsibility;
    @ApiModelProperty(value = "状态或鉴定、时间")
    private String researchStatusTime;




}
