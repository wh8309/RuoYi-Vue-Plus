package com.ruoyi.website.domain.vo.request;

import com.ruoyi.common.core.domain.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
//@ApiModel(value = "重审列表请求查询请求")
public class ReviewNoticeRequest extends AbstractEntity {

    @ApiModelProperty(value = "年度")
    private String reportYear;

    @ApiModelProperty(value = "系列")
    private String belongSeries;

    @ApiModelProperty(value = "评审通知名称")
    private String treeName;

}
