package com.ruoyi.website.domain.vo.response;

import com.ruoyi.common.core.domain.BaseResponseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value = "任期内发表论文论著情况")
@Data
public class SkillPublishedResponseVO extends BaseResponseVO {

    public SkillPublishedResponseVO() {
    }

    @ApiModelProperty(value = "主键id")
    private Long skillPublishedPapersId;
    @ApiModelProperty(value = "出版年月")
    private String publishedPaperTime;
    @ApiModelProperty(value = "论文名称")
    private String publishedPaperName;
    @ApiModelProperty(value = "作者序")
    private String publishedAuthorName;
    @ApiModelProperty(value = "刊物(出版社)名称")
    private String publicationsName;
    @ApiModelProperty(value = "刊号(ISSN/CN/ISBN)")
    private String publicationsNumb;
    @ApiModelProperty(value = "刊物级别")
    private String publicationsLevel;

}
