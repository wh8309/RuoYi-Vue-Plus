package com.ruoyi.website.domain.vo.request;

import com.ruoyi.common.core.domain.AbstractEntity;
import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)

public class ClientProfileStatusUpdateRequestVO extends AbstractEntity {

    @ApiModelProperty(value = "用户Id")
    private Long clientId;

    @ApiModelProperty(value = "状态")
    private Long status;

}
