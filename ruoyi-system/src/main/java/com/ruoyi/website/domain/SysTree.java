package com.ruoyi.website.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * 类目对象 sys_tree
 *
 * @author ruoyi
 * @date 2021-03-23
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("sys_tree")
public class SysTree implements Serializable {

private static final long serialVersionUID=1L;


    /** 主键 */
    @TableId(value = "tree_id")
    private Long treeId;

    /** 父id */
    @Excel(name = "父id")
    private Long parentTreeId;

    /** 编码 */
    @Excel(name = "编码")
    private String treeCode;

    /** 编码名称 */
    @Excel(name = "编码名称")
    private String treeName;

    /** 层级 1顶级节点 */
    @Excel(name = "层级 1顶级节点")
    private Long lvl;

    /** 是否禁用 0 禁用 1 启用 */
    @Excel(name = "是否禁用 0 禁用 1 启用")
    private Integer isEnable;

    @Excel(name = "定义码 A:申报专业 B:编码单位 C:现职称 D:推荐单位")
    private String treeType;

    /** 排序 */
    @Excel(name = "排序")
    private Long orderNo;

    /** 备注 */
    @Excel(name = "备注")
    private String remark;

    /** 创建时间 */
    private Date createTime;

    /** 修改时间 */
    private Date updateTime;

    /** 扩展内容，通常为json字符串 */
    @Excel(name = "扩展内容，通常为json字符串")
    private String expandContent;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SysTree sysTree = (SysTree) o;
        return Objects.equals(treeId, sysTree.treeId) &&
                Objects.equals(parentTreeId, sysTree.parentTreeId) &&
                Objects.equals(treeCode, sysTree.treeCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(treeId, parentTreeId, treeCode);
    }
}
