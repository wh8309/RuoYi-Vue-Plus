package com.ruoyi.website.domain.vo.request;

import com.ruoyi.common.core.domain.AbstractEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@Accessors(chain = true)

public class TorsNominateUnitSmsEnableRequestVO extends AbstractEntity {

    @ApiModelProperty(value = "推荐单位id")
    @NotNull(message = "主键id不能为空")
    private Long nominateUnitId;

    @ApiModelProperty(value = "短信通知 1-启用 0-禁用")
    @NotNull(message = "短信通知状态不能为空")
    private Integer smsEnable;

}
