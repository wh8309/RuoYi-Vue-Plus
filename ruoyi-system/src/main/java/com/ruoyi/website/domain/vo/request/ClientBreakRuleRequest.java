package com.ruoyi.website.domain.vo.request;

import com.ruoyi.common.core.domain.AbstractEntity;
import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
 * 破格条件说明对象 client_break_rule
 *
 * @author ruoyi
 * @date 2021-03-25
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
public class ClientBreakRuleRequest extends AbstractEntity {

    private static final long serialVersionUID = 1L;


    /**
     * 主键
     */
    @ApiModelProperty(value = "client_id")
    private Long clientId;

    /**
     * 破格条件
     */
    @NotBlank(message = "破格条件不能为空")
    @ApiModelProperty(value = "破格条件")
    private String breakRuleDesc;

}
