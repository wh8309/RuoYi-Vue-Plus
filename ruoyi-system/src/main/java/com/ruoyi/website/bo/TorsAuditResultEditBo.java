package com.ruoyi.website.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 审核未通过编辑对象 tors_audit_result
 *
 * @author admin
 * @date 2021-07-07
 */
@Data
@ApiModel("审核未通过编辑对象")
public class TorsAuditResultEditBo {


    /** 主键 */
    @ApiModelProperty("主键")
    private Long auditResultId;

    /** 客户姓名 */
    @ApiModelProperty("客户姓名")
    private String clientName;

    /** 推荐单位id */
    @ApiModelProperty("推荐单位id")
    private Long nominateUnitId;

    /** 单位名称 */
    @ApiModelProperty("单位名称")
    private String unitName;

    /** 审核时间 */
    @ApiModelProperty("审核时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date auditTime;

    /** 审核意见 */
    @ApiModelProperty("审核意见")
    private String auditOpinion;

    /** 短信轮询结束日期 */
    @ApiModelProperty("短信轮询结束日期")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date smsLoopEndTime;

    /** 业务类型 1-进展通知 2-补充资料 */
    @ApiModelProperty("业务类型 1-进展通知 2-补充资料")
    private Integer businessType;

    /** 处理状态 1-已处理 0-未处理 */
    @ApiModelProperty("处理状态 1-已处理 0-未处理")
    private Integer dealStatus;

    /**  */
    @ApiModelProperty("")
    private String remark;

    /** 更新者 */
    @ApiModelProperty("更新者")
    private String updateBy;

    /**  */
    @ApiModelProperty("")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
}
