package com.ruoyi.website.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.Map;
import java.util.HashMap;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 审核未通过分页查询对象 tors_audit_result
 *
 * @author admin
 * @date 2021-07-07
 */
@Data
@ApiModel("审核未通过分页查询对象")
public class TorsAuditResultQueryBo  {

	/** 分页大小 */
	@ApiModelProperty("分页大小")
	private Integer pageSize;
	/** 当前页数 */
	@ApiModelProperty("当前页数")
	private Integer pageNum;

	/** 客户姓名 */
	@ApiModelProperty("客户姓名")
	private String clientName;
	/** 单位名称 */
	@ApiModelProperty("单位名称")
	private String unitName;
	/** 业务类型 1-进展通知 2-补充资料 */
	@ApiModelProperty("业务类型 1-进展通知 2-补充资料")
	private Integer businessType;
	/** 处理状态 1-已处理 0-未处理 */
	@ApiModelProperty("处理状态 1-已处理 0-未处理")
	private Integer dealStatus;
	@ApiModelProperty(value = "审核开始日期")
	private String auditStartDate;
	@ApiModelProperty(value = "审核结束日期")
	private String auditEndDate;

	@ApiModelProperty(value = "审核单位")
	private String auditUnit;

	@ApiModelProperty(value = "审核结果")
	private String auditResult;

	@ApiModelProperty(value = "审核意见")
	private String auditOpinion;

}
