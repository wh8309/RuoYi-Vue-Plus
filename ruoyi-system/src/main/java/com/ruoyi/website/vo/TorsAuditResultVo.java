package com.ruoyi.website.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;



/**
 * 审核未通过视图对象 mall_package
 *
 * @author admin
 * @date 2021-07-07
 */
@Data
@ApiModel("资料补充视图对象")
public class TorsAuditResultVo {
	private static final long serialVersionUID = 1L;

	/** 主键 */
	@ApiModelProperty("主键")
	private Long auditResultId;

	/** 客户姓名 */
	@Excel(name = "姓名" , align = Excel.Align.LEFT)
	@ApiModelProperty("姓名")
	private String clientName;

	/** 推荐单位id */
	@ApiModelProperty("推荐单位id")
	private Long nominateUnitId;

	/** 单位名称 */
	@Excel(name = "推荐单位",width = 30 , align = Excel.Align.LEFT )
	@ApiModelProperty("推荐单位")
	private String unitName;

	/** 送审时间 */
	@Excel(name = "送审时间" , width = 20, dateFormat = "yyyy-MM-dd HH:mm:ss" , align = Excel.Align.LEFT)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("送审时间")
	private Date receiveTime;

	/** 审核单位 */
	@Excel(name = "审核单位",width = 30 , align = Excel.Align.LEFT )
	@ApiModelProperty("审核单位")
	private String auditUnit;

	/** 审核单位 */
	@Excel(name = "流程状态",width = 30 , align = Excel.Align.LEFT )
	@ApiModelProperty("流程状态")
	private String flowStatusDesc;

	/** 审核时间 */
	@Excel(name = "审核时间" , width = 20, dateFormat = "yyyy-MM-dd HH:mm:ss" , align = Excel.Align.LEFT)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("审核时间")
	private Date auditTime;

	/** 短信轮询结束日期 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("短信轮询结束日期")
	private Date smsLoopEndTime;

	/** 业务类型 1-进展通知 2-补充资料 */
	@ApiModelProperty("业务类型 1-审核进展通知 2-补充资料")
	@Excel(name = "业务类型", readConverterExp = "1=审核进展通知,2=补充资料", combo = {"审核进展通知", "补充资料"} , align = Excel.Align.LEFT)
	private Integer businessType;

	@Excel(name = "采集时间" , width = 20, dateFormat = "yyyy-MM-dd HH:mm:ss" , align = Excel.Align.LEFT)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	/** 处理状态 1-已处理 0-未处理 */
	@Excel(name = "处理状态", readConverterExp = "1=已处理,0=未处理", combo = {"已处理", "未处理"} ,align = Excel.Align.LEFT)
	@ApiModelProperty("处理状态 1-已处理 0-未处理")
	private Integer dealStatus;

	/** 审核结果 */
	@Excel(name = "审核结果", width = 30 ,align = Excel.Align.LEFT)
	@ApiModelProperty("审核结果")
	private String auditResult;

	/** 审核意见 */
	@Excel(name = "审核意见", width = 80 ,align = Excel.Align.LEFT)
	@ApiModelProperty("审核意见")
	private String auditOpinion;

	@ApiModelProperty("")
	private String remark;

}
