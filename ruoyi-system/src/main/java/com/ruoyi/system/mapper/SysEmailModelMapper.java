package com.ruoyi.system.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.SysEmailModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


/**
 * 邮件模板Mapper接口
 *
 * @author ruoyi
 * @date 2021-06-24
 */
@Mapper
public interface SysEmailModelMapper extends BaseMapper<SysEmailModel> {
    Integer emailByIdMax();
}
