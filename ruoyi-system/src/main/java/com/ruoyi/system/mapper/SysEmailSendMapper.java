package com.ruoyi.system.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.SysEmailSend;

/**
 * 邮件发送记录Mapper接口
 *
 * @author ruoyi
 * @date 2021-06-24
 */

public interface SysEmailSendMapper extends BaseMapper<SysEmailSend> {

}
