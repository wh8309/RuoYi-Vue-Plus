package com.ruoyi.system.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.SysSmsSend;

/**
 * 短信发送记录Mapper接口
 *
 * @author ruoyi
 * @date 2021-06-24
 */

public interface SysSmsSendMapper extends BaseMapper<SysSmsSend> {

}
