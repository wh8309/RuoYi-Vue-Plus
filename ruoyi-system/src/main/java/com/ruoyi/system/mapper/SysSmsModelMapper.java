package com.ruoyi.system.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.SysSmsModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * 短信模版Mapper接口
 *
 * @author ruoyi
 * @date 2021-06-24
 */
@Mapper
public interface SysSmsModelMapper extends BaseMapper<SysSmsModel> {
    Integer smsByIdMax();
}
