package com.ruoyi.system.domain.vo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;
import javax.validation.constraints.*;


/**
 * 短信模版编辑对象 sys_sms_model
 *
 * @author ruoyi
 * @date 2021-06-24
 */
@Data
@ApiModel("短信模版编辑对象")
public class SysSmsModelEditBo {


    /** 主键 */
    @ApiModelProperty("主键")
    @NotNull(message = "主键不能为空")
    private Long smsModelId;

    /** 短信模版code */
    @ApiModelProperty("短信模版code")
    @NotBlank(message = "短信模版code不能为空")
    private String smsCode;

    /** 短信分类 */
    @ApiModelProperty("短信分类")
    @NotBlank(message = "短信分类不能为空")
    private String smsType;

    /** 短信模版内容 */
    @ApiModelProperty("短信模版内容")
    @NotBlank(message = "短信模版内容不能为空")
    private String smsContent;

    /** 状态:1-启用，-1-禁用 */
    @ApiModelProperty("状态:1-启用，-1-禁用")
    @NotNull(message = "状态不能为空")
    private Integer status;

    /** 备注 */
    @ApiModelProperty("备注")
    private String remark;

}
