package com.ruoyi.system.domain.vo.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@Data
public class SysEmailModelQueryResponseVo {

    /** 主键 */
    @ApiModelProperty(value =  "主键")
    private Long emailModelId;

    /** 邮件模版code */
    @ApiModelProperty(value =  "邮件模版code")
    private String emailCode;

    /** 邮件分类 */
    @ApiModelProperty(value =  "邮件分类")
    private String emailType;

    /** 邮件模版内容 */
    @ApiModelProperty(value =  "邮件模版内容")
    private String emailContent;

    /** 状态:1-启用，-1-禁用 */
    @ApiModelProperty(value =  "状态:1-启用，-1-禁用")
    private Integer status;

    /** 备注 */
    @ApiModelProperty(value =  "备注")
    private String remark;
}
