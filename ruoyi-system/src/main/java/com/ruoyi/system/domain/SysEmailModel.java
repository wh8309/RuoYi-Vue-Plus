package com.ruoyi.system.domain;


import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;


/**
 * 邮件模板对象 sys_email_model
 *
 * @author ruoyi
 * @date 2021-06-24
 */
@Data
@TableName("sys_email_model")
public class SysEmailModel extends BaseEntity {

    private static final long serialVersionUID=1L;

    /** 主键 */
    @TableId(value = "email_model_id",type = IdType.INPUT)
    private Long emailModelId;

    /** 邮件模版code */
    private String emailCode;

    /** 邮件分类 */
    private String emailType;

    /** 邮件模版内容 */
    private String emailContent;

    /** 状态:1-启用，-1-禁用 */
    private Integer status;

    /** 备注 */
    private String remark;

    /** 创建者 */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /** 修改者 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /** 修改时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

}
