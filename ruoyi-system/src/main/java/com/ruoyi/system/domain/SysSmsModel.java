package com.ruoyi.system.domain;


import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 短信模版对象 sys_sms_model
 *
 * @author ruoyi
 * @date 2021-06-24
 */
@Data
@TableName("sys_sms_model")
public class SysSmsModel extends BaseEntity {

    private static final long serialVersionUID=1L;


    /** 主键 */
    @TableId(value = "sms_model_id")
    private Long smsModelId;

    /** 短信模版code */
    private String smsCode;

    /** 短信分类 */
    private String smsType;

    /** 短信模版内容 */
    private String smsContent;

    /** 状态:1-启用，-1-禁用 */
    private Integer status;

    /** 备注 */
    private String remark;

    /** 创建者 */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /** 更新者 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /** 修改时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

}
