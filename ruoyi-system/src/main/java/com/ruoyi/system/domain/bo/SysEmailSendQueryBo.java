package com.ruoyi.system.domain.bo;

import com.ruoyi.common.core.domain.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 邮件发送记录分页查询对象 sys_email_send
 *
 * @author ruoyi
 * @date 2021-06-24
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("邮件发送记录分页查询对象")
public class SysEmailSendQueryBo extends AbstractEntity {

	/** 分页大小 */
	@ApiModelProperty("分页大小")
	private Integer pageSize;

	/** 当前页数 */
	@ApiModelProperty("当前页数")
	private Integer pageNum;

	/** 业务类型 */
	@ApiModelProperty("业务类型")
	private String businessType;

	/** 是否已发送 1已发送 0未发送 */
	@ApiModelProperty("是否已发送 1已发送 0未发送")
	private String isTransmit;

	/** 邮件发送地址 *//*
	@ApiModelProperty("邮件发送地址")
	private String emailSendAddress;
	*//** 邮件接受地址 *//*
	@ApiModelProperty("邮件接受地址")
	private String emailReciveAddress;*/

	/** 发送状态 */
/*	@ApiModelProperty("发送状态")
	private String sendStatus;*/
}
