package com.ruoyi.system.domain.vo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;
import javax.validation.constraints.*;



/**
 * 邮件发送记录添加对象 sys_email_send
 *
 * @author ruoyi
 * @date 2021-06-24
 */
@Data
@ApiModel("邮件发送记录添加对象")
public class SysEmailSendAddBo {


    /** 邮件发送地址 */
    @Email(message = "邮箱格式不正确")
    @Size(min = 0, max = 50, message = "邮箱长度不能超过50个字符")
    @ApiModelProperty("邮件发送地址")
    @NotBlank(message = "邮件发送地址不能为空")
    private String emailSendAddress;

    /** 邮件接受地址 */
    @Email(message = "邮箱格式不正确")
    @Size(min = 0, max = 50, message = "邮箱长度不能超过50个字符")
    @ApiModelProperty("邮件接受地址")
    @NotBlank(message = "邮件接受地址不能为空")
    private String emailReciveAddress;

    /** 业务类型 */
    @ApiModelProperty("业务类型")
    @NotBlank(message = "业务类型不能为空")
    private String businessType;

    /** 发送状态 */
    @ApiModelProperty("发送状态")
    @NotBlank(message = "发送状态不能为空")
    private String sendStatus;

    /** 是否已发送 1已发送 0未发送 */
    @ApiModelProperty("是否已发送 1已发送 0未发送")
    private String isTransmit;

    /** 失败原因 */
    @ApiModelProperty("失败原因")
    private String failReason;

    /** 邮件标题内容 */
    @ApiModelProperty("邮件标题内容")
    @NotBlank(message = "邮件标题内容不能为空")
    private String emailTitle;

    /** 邮件模版内容 */
    @ApiModelProperty("邮件模版内容")
    @NotBlank(message = "邮件模版内容不能为空")
    private String emailContent;

    /** 邮件附件id */
    @ApiModelProperty("邮件附件id")
    private String emailAttachUuid;

    /** 邮件附件名称 */
    @ApiModelProperty("邮件附件名称")
    private String emailAttachName;

}
