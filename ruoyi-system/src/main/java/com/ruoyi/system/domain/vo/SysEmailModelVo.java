package com.ruoyi.system.domain.vo;

import com.ruoyi.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 邮件模板视图对象 sys_email_model
 *
 * @author ruoyi
 * @date 2021-06-24
 */
@Data
@ApiModel("邮件模板视图对象")
public class SysEmailModelVo {

	private static final long serialVersionUID = 1L;

	/** 主键 */
	@ApiModelProperty("主键")
	private Long emailModelId;

	/** 邮件模版code */
	@Excel(name = "邮件模版code")
	@ApiModelProperty("邮件模版code")
	private String emailCode;

	/** 邮件分类 */
	@Excel(name = "邮件分类")
	@ApiModelProperty("邮件分类")
	private String emailType;

	/** 邮件模版内容 */
	@Excel(name = "邮件模版内容")
	@ApiModelProperty("邮件模版内容")
	private String emailContent;

	/** 状态:1-启用，-1-禁用 */
	@Excel(name = "状态:1-启用，-1-禁用")
	@ApiModelProperty("状态:1-启用，-1-禁用")
	private Integer status;

	/** $column.columnComment */
	@Excel(name = "状态:1-启用，-1-禁用")
	@ApiModelProperty("$column.columnComment")
	private String remark;


}
