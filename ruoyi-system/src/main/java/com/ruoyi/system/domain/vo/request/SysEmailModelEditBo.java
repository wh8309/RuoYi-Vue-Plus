package com.ruoyi.system.domain.vo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import javax.validation.constraints.*;


/**
 * 邮件模板编辑对象 sys_email_model
 *
 * @author ruoyi
 * @date 2021-06-24
 */
@Data
@ApiModel("邮件模板编辑对象")
public class SysEmailModelEditBo {


    /** 主键 */
    @ApiModelProperty("主键")
    @NotNull(message = "主键不能为空")
    private Long emailModelId;

    /** 邮件模版code */
    @ApiModelProperty("邮件模版code")
    private String emailCode;

    /** 邮件分类 */
    @ApiModelProperty("邮件分类")
    private String emailType;

    /** 邮件模版内容 */
    @ApiModelProperty("邮件模版内容")
    private String emailContent;

    /** 状态:1-启用，-1-禁用 */
    @ApiModelProperty("状态:1-启用，-1-禁用")
    private Integer status;

    @ApiModelProperty("备注")
    private String remark;

}
