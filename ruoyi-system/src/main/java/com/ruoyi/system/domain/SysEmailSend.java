package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import com.ruoyi.common.annotation.Excel;

/**
 * 邮件发送记录对象 sys_email_send
 *
 * @author ruoyi
 * @date 2021-06-24
 */
@Data
@TableName("sys_email_send")
public class SysEmailSend  {

    private static final long serialVersionUID=1L;


    /** 主键 */
    @TableId(value = "email_send_id")
    private Long emailSendId;

    /** 邮件发送地址 */
    private String emailSendAddress;

    /** 邮件接受地址 */
    private String emailReciveAddress;

    /** 业务类型 */
    private String businessType;

    /** 发送状态 */
    private String sendStatus;

    /** 是否已发送 1已发送 0未发送 */
    private String isTransmit;

    /** 失败原因 */
    private String failReason;

    /** 邮件标题内容 */
    private String emailTitle;

    /** 邮件模版内容 */
    private String emailContent;

    /** 邮件附件id */
    private String emailAttachUuid;

    /** 邮件附件名称 */
    private String emailAttachName;

    /** 创建者 */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

}
