package com.ruoyi.system.domain;


import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 短信发送记录对象 sys_sms_send
 *
 * @author ruoyi
 * @date 2021-06-24
 */
@Data
@TableName("sys_sms_send")
public class SysSmsSend  {

   /* private static final long serialVersionUID=1L;*/

    /** 主键 */
    @TableId(value = "sms_send_id")
    private Long smsSendId;

    /** 短信发送手机号 */
    private String mobile;

    /** 业务类型 */
    private String businessType;

    /** 发送状态 */
    private String sendStatus;

    /** 是否已发送 */
    private String isTransmit;

    /** 失败原因 */
    private String failReason;

    /** 短信内容 */
    private String smsContent;

    /** 创建者 */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

}
