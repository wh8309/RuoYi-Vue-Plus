package com.ruoyi.system.domain.vo;

import com.ruoyi.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;




/**
 * 短信发送记录视图对象 sys_sms_send
 *
 * @author ruoyi
 * @date 2021-06-24
 */
@Data
@ApiModel("短信发送记录视图对象")
public class SysSmsSendVo {

	private static final long serialVersionUID = 1L;

	/** 主键 */
	@ApiModelProperty("主键")
	private Long smsSendId;

	/** 短信发送手机号 */
	@Excel(name = "短信发送手机号")
	@ApiModelProperty("短信发送手机号")
	private String mobile;

	/** 业务类型 */
	@Excel(name = "业务类型")
	@ApiModelProperty("业务类型")
	private String businessType;

	/** 发送状态 */
	@Excel(name = "发送状态")
	@ApiModelProperty("发送状态")
	private String sendStatus;

	/** 是否已发送 */
	@Excel(name = "是否已发送")
	@ApiModelProperty("是否已发送")
	private String isTransmit;

	/** 失败原因 */
	@Excel(name = "失败原因")
	@ApiModelProperty("失败原因")
	private String failReason;

	/** 短信内容 */
	@Excel(name = "短信内容")
	@ApiModelProperty("短信内容")
	private String smsContent;


}
