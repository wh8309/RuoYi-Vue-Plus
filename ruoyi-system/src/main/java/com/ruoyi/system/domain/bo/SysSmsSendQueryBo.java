package com.ruoyi.system.domain.bo;

import com.ruoyi.common.core.domain.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 短信发送记录分页查询对象 sys_sms_send
 *
 * @author ruoyi
 * @date 2021-06-24
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("短信发送记录分页查询对象")
public class SysSmsSendQueryBo extends AbstractEntity {

	/** 分页大小 */
	@ApiModelProperty("分页大小")
	private Integer pageSize;
	/** 当前页数 */
	@ApiModelProperty("当前页数")
	private Integer pageNum;
	/** 排序列 */

	/** 短信发送手机号 */
	@ApiModelProperty("短信发送手机号")
	private String mobile;
	/** 业务类型 */
	@ApiModelProperty("业务类型")
	private String businessType;

	/** 是否已发送 */
	@ApiModelProperty("是否已发送")
	private String isTransmit;

/*	*//** 发送状态 *//*
	@ApiModelProperty("发送状态")
	private String sendStatus;*/
}
