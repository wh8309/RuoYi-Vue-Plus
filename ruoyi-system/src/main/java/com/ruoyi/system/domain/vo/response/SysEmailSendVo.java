package com.ruoyi.system.domain.vo.response;

import com.ruoyi.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;




/**
 * 邮件发送记录视图对象 sys_email_send
 *
 * @author ruoyi
 * @date 2021-06-24
 */
@Data
@ApiModel("邮件发送记录视图对象")
public class SysEmailSendVo {

	private static final long serialVersionUID = 1L;

	/** 主键 */
	@ApiModelProperty("主键")
	private Long emailSendId;

	/** 邮件发送地址 */
	@Excel(name = "邮件发送地址")
	@ApiModelProperty("邮件发送地址")
	private String emailSendAddress;

	/** 邮件接受地址 */
	@Excel(name = "邮件接受地址")
	@ApiModelProperty("邮件接受地址")
	private String emailReciveAddress;

	/** 业务类型 */
	@Excel(name = "业务类型")
	@ApiModelProperty("业务类型")
	private String businessType;

	/** 发送状态 */
	@Excel(name = "发送状态")
	@ApiModelProperty("发送状态")
	private String sendStatus;

	/** 是否已发送 1已发送 0未发送 */
	@Excel(name = "是否已发送 1已发送 0未发送")
	@ApiModelProperty("是否已发送 1已发送 0未发送")
	private String isTransmit;

	/** 失败原因 */
	@Excel(name = "失败原因")
	@ApiModelProperty("失败原因")
	private String failReason;

	/** 邮件标题内容 */
	@Excel(name = "邮件标题内容")
	@ApiModelProperty("邮件标题内容")
	private String emailTitle;

	/** 邮件模版内容 */
	@Excel(name = "邮件模版内容")
	@ApiModelProperty("邮件模版内容")
	private String emailContent;

	/** 邮件附件id */
	@Excel(name = "邮件附件id")
	@ApiModelProperty("邮件附件id")
	private String emailAttachUuid;

	/** 邮件附件名称 */
	@Excel(name = "邮件附件名称")
	@ApiModelProperty("邮件附件名称")
	private String emailAttachName;


}
