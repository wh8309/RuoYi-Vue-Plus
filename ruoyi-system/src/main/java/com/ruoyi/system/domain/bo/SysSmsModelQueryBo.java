package com.ruoyi.system.domain.bo;

import com.ruoyi.common.core.domain.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 短信模版分页查询对象 sys_sms_model
 *
 * @author ruoyi
 * @date 2021-06-24
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("短信模版分页查询对象")
public class SysSmsModelQueryBo extends AbstractEntity {

	/** 分页大小 */
	@ApiModelProperty("分页条数")
	private Integer pageSize;

	/** 当前页数 */
	@ApiModelProperty("当前页数")
	private Integer pageNum;

	/** 短信模版code */
	@ApiModelProperty("邮件模版code")
	private String smsCode;

	/** 短信分类 */
	@ApiModelProperty("邮件分类")
	private String smsType;

	/** 短信模版内容 *//*
	@ApiModelProperty("邮件模版内容")
	private String smsContent;*/

}
