package com.ruoyi.system.domain.vo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;
import javax.validation.constraints.*;



/**
 * 短信发送记录添加对象 sys_sms_send
 *
 * @author ruoyi
 * @date 2021-06-24
 */
@Data
@ApiModel("短信发送记录添加对象")
public class SysSmsSendAddBo {


    /** 短信发送手机号 */
    @ApiModelProperty("短信发送手机号")
    @NotBlank(message = "短信发送手机号不能为空")
    @Size(min = 0, max = 11, message = "短信手机号不能超过11个字符")
    private String mobile;

    /** 业务类型 */
    @ApiModelProperty("业务类型")
    @NotBlank(message = "业务类型不能为空")
    private String businessType;

    /** 发送状态 */
    @ApiModelProperty("发送状态")
    @NotBlank(message = "发送状态不能为空")
    private String sendStatus;

    /** 是否已发送 */
    @ApiModelProperty("是否已发送")
    @NotBlank(message = "是否已发送不能为空")
    private String isTransmit;

    /** 失败原因 */
    @ApiModelProperty("失败原因")
    private String failReason;

    /** 短信内容 */
    @ApiModelProperty("短信内容")
    @NotBlank(message = "短信内容不能为空")
    private String smsContent;

}
