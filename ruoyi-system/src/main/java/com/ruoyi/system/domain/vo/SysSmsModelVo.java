package com.ruoyi.system.domain.vo;

import com.ruoyi.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;



/**
 * 短信模版视图对象 sys_sms_model
 *
 * @author ruoyi
 * @date 2021-06-24
 */
@Data
@ApiModel("短信模版视图对象")
public class SysSmsModelVo {

	private static final long serialVersionUID = 1L;

	/** 主键 */
	@ApiModelProperty("主键")
	private Long smsModelId;

	/** 短信模版code */
	@Excel(name = "短信模版code")
	@ApiModelProperty("短信模版code")
	private String smsCode;

	/** 短信分类 */
	@Excel(name = "短信分类")
	@ApiModelProperty("短信分类")
	private String smsType;

	/** 短信模版内容 */
	@Excel(name = "短信模版内容")
	@ApiModelProperty("短信模版内容")
	private String smsContent;

	/** 状态:1-启用，-1-禁用 */
	@Excel(name = "状态:1-启用，-1-禁用")
	@ApiModelProperty("状态:1-启用，-1-禁用")
	private Integer status;

	/** 备注 */
	@Excel(name = "备注")
	@ApiModelProperty("备注")
	private String remark;


}
