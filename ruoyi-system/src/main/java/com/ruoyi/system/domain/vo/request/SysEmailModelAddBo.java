package com.ruoyi.system.domain.vo.request;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@ApiModel("邮箱模版添加对象")
public class SysEmailModelAddBo {

    /** 邮件分类 */
    @ApiModelProperty("邮件分类")
    @NotBlank(message = "邮件分类不能为空")
    private String emailType;

    @ApiModelProperty("邮件模版")
    @NotBlank(message = "邮件模版不能为空")
    private String emailCode;

    /** 邮件模版内容 */
    @ApiModelProperty("邮件模版内容")
    @NotBlank(message = "邮件模版内容不能为空")
    private String emailContent;

    /** 状态:1-启用，-1-禁用 */
    @ApiModelProperty("状态:1-启用，-1-禁用")
    @NotNull(message = "状态不能为空")
    private Integer status;

    /** 备注 */
    @ApiModelProperty("备注")
    private String remark;


}
