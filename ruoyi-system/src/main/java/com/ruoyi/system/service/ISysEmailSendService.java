package com.ruoyi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.SysEmailSend;
import com.ruoyi.system.domain.vo.request.SysEmailSendAddBo;

import com.ruoyi.system.domain.bo.SysEmailSendQueryBo;
import java.util.List;


/**
 * 邮件发送记录Service接口
 *
 * @author ruoyi
 * @date 2021-06-24
 */
public interface ISysEmailSendService extends IService<SysEmailSend> {

	/*SysEmailSendVo queryById(Long emailSendId);*/


	List<SysEmailSend> queryList(SysEmailSendQueryBo bo);

	/**
	 * 根据新增业务对象插入邮件发送记录
	 * @param bo 邮件发送记录新增业务对象
	 * @return
	 */
	Boolean insertByAddBo(SysEmailSendAddBo bo);


}
