package com.ruoyi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.SysSmsSend;
import com.ruoyi.system.domain.vo.request.SysSmsSendEditBo;
import com.ruoyi.system.domain.vo.SysSmsSendVo;
import com.ruoyi.system.domain.vo.request.SysSmsSendAddBo;
import com.ruoyi.system.domain.bo.SysSmsSendQueryBo;

import java.util.Collection;
import java.util.List;

/**
 * 短信发送记录Service接口
 *
 * @author ruoyi
 * @date 2021-06-24
 */
public interface ISysSmsSendService extends IService<SysSmsSend> {


	/*SysSmsSendVo queryById(Long smsSendId);*/


	List<SysSmsSend> queryList(SysSmsSendQueryBo bo);


	Boolean insertByAddBo(SysSmsSendAddBo bo);


}
