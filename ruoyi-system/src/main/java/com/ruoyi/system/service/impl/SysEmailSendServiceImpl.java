package com.ruoyi.system.service.impl;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.SysEmailModel;
import com.ruoyi.system.domain.SysEmailSend;
import com.ruoyi.system.domain.vo.request.SysEmailSendAddBo;
import com.ruoyi.system.domain.vo.request.SysEmailSendEditBo;
import com.ruoyi.system.domain.bo.SysEmailSendQueryBo;
import com.ruoyi.system.domain.vo.response.SysEmailSendVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.ruoyi.system.mapper.SysEmailSendMapper;

import com.ruoyi.system.service.ISysEmailSendService;

import javax.annotation.Resource;

import java.util.Collection;
import java.util.List;

/**
 * 邮件发送记录Service业务层处理
 *
 * @author ruoyi
 * @date 2021-06-24
 */
@Service
public class SysEmailSendServiceImpl extends ServiceImpl<SysEmailSendMapper, SysEmailSend> implements ISysEmailSendService {

    @Resource
    private SysEmailSendMapper sysEmailSendMapper;

    /*@Override
    public SysEmailSendVo queryById(Long emailSendId) {
        return null;
    }*/



    @Override
    public List<SysEmailSend> queryList(SysEmailSendQueryBo bo) {
        QueryWrapper<SysEmailSend> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq(!StrUtil.isEmpty(bo.getBusinessType()),"business_type",bo.getBusinessType());
        queryWrapper.eq(!StrUtil.isEmpty(bo.getIsTransmit()),"is_transmit",bo.getIsTransmit());
      /*  queryWrapper.eq(!StrUtil.isEmpty(bo.getSendStatus()),"send_status",bo.getSendStatus());*/
        List<SysEmailSend> sysEmailSends = sysEmailSendMapper.selectList(queryWrapper);
        return sysEmailSends;
    }

    @Override
    public Boolean insertByAddBo(SysEmailSendAddBo bo) {
        SysEmailSend sysEmailSend=new SysEmailSend();
        BeanUtils.copyProperties(bo,sysEmailSend);
        sysEmailSend.setCreateBy(SecurityUtils.getUsername());
        sysEmailSend.setCreateTime(new DateTime());
        boolean flog=false;
        if(sysEmailSendMapper.insert(sysEmailSend)>0){
            flog=true;
        }
        return  flog;
    }


}
