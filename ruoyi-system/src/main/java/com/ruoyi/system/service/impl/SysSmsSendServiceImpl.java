package com.ruoyi.system.service.impl;


import cn.hutool.core.date.DateTime;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.SysEmailSend;
import com.ruoyi.system.domain.SysSmsSend;
import com.ruoyi.system.domain.vo.request.SysSmsSendEditBo;
import com.ruoyi.system.domain.vo.SysSmsSendVo;
import com.ruoyi.system.domain.vo.request.SysSmsSendAddBo;
import com.ruoyi.system.domain.bo.SysSmsSendQueryBo;
import com.ruoyi.system.mapper.SysSmsSendMapper;
import com.ruoyi.system.service.ISysSmsSendService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 短信发送记录Service业务层处理
 *
 * @author ruoyi
 * @date 2021-06-24
 */
@Service
public class SysSmsSendServiceImpl extends ServiceImpl<SysSmsSendMapper, SysSmsSend> implements ISysSmsSendService {

    @Resource
    private SysSmsSendMapper sysSmsSendMapper;



    @Override
    public List<SysSmsSend> queryList(SysSmsSendQueryBo bo) {
        QueryWrapper<SysSmsSend> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq(!StrUtil.isEmpty(bo.getBusinessType()),"business_type",bo.getBusinessType());
        queryWrapper.eq(!StrUtil.isEmpty(bo.getIsTransmit()),"is_transmit",bo.getIsTransmit());
       /* queryWrapper.eq(!StrUtil.isEmpty(bo.getSendStatus()),"send_status",bo.getSendStatus());*/
        queryWrapper.eq(!StrUtil.isEmpty(bo.getMobile()),"mobile",bo.getMobile());
        List<SysSmsSend> sysEmailSends = sysSmsSendMapper.selectList(queryWrapper);
        return sysEmailSends;
    }

    @Override
    public Boolean insertByAddBo(SysSmsSendAddBo bo) {
        SysSmsSend sysSmsSend=new SysSmsSend();
        BeanUtils.copyProperties(bo,sysSmsSend);
        sysSmsSend.setCreateBy(SecurityUtils.getUsername());
        sysSmsSend.setCreateTime(new DateTime());
        sysSmsSend.setCreateTime(new DateTime());
        boolean flog=false;
        if(sysSmsSendMapper.insert(sysSmsSend)>0){
            flog=true;
        }
        return  flog;
    }


}
