package com.ruoyi.system.service.impl;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.SysEmailModel;
import com.ruoyi.system.domain.vo.request.SysEmailModelAddBo;
import com.ruoyi.system.domain.vo.request.SysEmailModelEditBo;
import com.ruoyi.system.domain.bo.SysEmailModelQueryBo;
import com.ruoyi.system.mapper.SysEmailModelMapper;
import com.ruoyi.system.service.ISysEmailModelService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 邮件模板Service业务层处理
 *
 * @author ruoyi
 * @date 2021-06-24
 */
@Service
public class SysEmailModelServiceImpl extends ServiceImpl<SysEmailModelMapper, SysEmailModel> implements ISysEmailModelService {

    @Resource
    private SysEmailModelMapper sysEmailModelMapper ;

    @Override
    public SysEmailModel queryById(Long emailModelId){
        return sysEmailModelMapper.selectById(emailModelId);
    }

    @Override
    public List<SysEmailModel> queryList(SysEmailModelQueryBo bo) {
        QueryWrapper<SysEmailModel> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq(!StrUtil.isEmpty(bo.getEmailCode()),"email_code",bo.getEmailCode());
        queryWrapper.eq(!StrUtil.isEmpty(bo.getEmailType()),"email_type",bo.getEmailType());
        queryWrapper.eq("status","1");
        List<SysEmailModel> sysEmailModels = sysEmailModelMapper.selectList(queryWrapper);
        return sysEmailModels;
    }


    @Override
    public int insertByAddBo(SysEmailModelAddBo bo) {
        SysEmailModel sysEmailModel=new SysEmailModel();
        BeanUtils.copyProperties(bo,sysEmailModel);
        sysEmailModel.setCreateBy(SecurityUtils.getUsername());
        sysEmailModel.setCreateTime(new DateTime());
        int row = sysEmailModelMapper.insert(sysEmailModel);
        return row;
    }


    @Override
    public int updateByEditBo(SysEmailModelEditBo bo) {
        SysEmailModel sysEmailModel=new SysEmailModel();
        BeanUtils.copyProperties(bo,sysEmailModel);
        sysEmailModel.setUpdateBy(SecurityUtils.getUsername());
        sysEmailModel.setUpdateTime(new DateTime());
        int row = sysEmailModelMapper.updateById(sysEmailModel);
        return row;
    }

    @Override
    public String checkByColdUnique(SysEmailModelAddBo bo) {
        QueryWrapper<SysEmailModel> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq(!StrUtil.isEmpty(bo.getEmailCode()),"email_code",bo.getEmailCode());
        SysEmailModel sysEmailModel = sysEmailModelMapper.selectOne(queryWrapper);
        if(StringUtils.isNotNull(sysEmailModel)&&bo.getEmailCode().equals(sysEmailModel.getEmailCode())){
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    @Override
    public String checkByColdUnique(SysEmailModelEditBo bo) {
        QueryWrapper<SysEmailModel> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq(!StrUtil.isEmpty(bo.getEmailCode()),"email_code",bo.getEmailCode());
        SysEmailModel sysEmailModel = sysEmailModelMapper.selectOne(queryWrapper);
        if(StringUtils.isNotNull(sysEmailModel)&&bo.getEmailCode().equals(sysEmailModel.getEmailCode())){
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

}
