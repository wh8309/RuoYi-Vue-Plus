package com.ruoyi.system.service.impl;


import cn.hutool.core.date.DateTime;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.utils.DictUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.SysEmailModel;
import com.ruoyi.system.domain.SysSmsModel;
import com.ruoyi.system.domain.bo.*;
import com.ruoyi.system.domain.vo.SysSmsModelVo;
import com.ruoyi.system.domain.vo.request.SysSmsModelAddBo;
import com.ruoyi.system.domain.vo.request.SysSmsModelEditBo;
import com.ruoyi.system.mapper.SysSmsModelMapper;
import com.ruoyi.system.service.ISysSmsModelService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 短信模版Service业务层处理
 *
 * @author ruoyi
 * @date 2021-06-24
 */
@Service
public class SysSmsModelServiceImpl extends ServiceImpl<SysSmsModelMapper, SysSmsModel> implements ISysSmsModelService {

    @Resource
    private  SysSmsModelMapper sysSmsModelMapper;

    @Override
    public SysSmsModel queryById(Long smsModelId) {
        return sysSmsModelMapper.selectById(smsModelId);
    }

    @Override
    public List<SysSmsModel> queryList(SysSmsModelQueryBo bo) {
        QueryWrapper<SysSmsModel> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq(!StrUtil.isEmpty(bo.getSmsCode()),"sms_code",bo.getSmsCode());
        queryWrapper.eq(!StrUtil.isEmpty(bo.getSmsType()),"sms_type",bo.getSmsType());
        queryWrapper.eq("status","1");
        List<SysSmsModel> sysEmailModels = sysSmsModelMapper.selectList(queryWrapper);
        return sysEmailModels;
    }

    @Override
    public int insertByAddBo(SysSmsModelAddBo bo) {
        SysSmsModel sysSmsModel=new SysSmsModel();
        BeanUtils.copyProperties(bo,sysSmsModel);
        sysSmsModel.setCreateBy(SecurityUtils.getUsername());
        sysSmsModel.setCreateTime(new DateTime());
        int row = sysSmsModelMapper.insert(sysSmsModel);
        return row;
    }

    @Override
    public int updateByEditBo(SysSmsModelEditBo bo) {
        SysSmsModel sysSmsModel=new SysSmsModel();
        BeanUtils.copyProperties(bo,sysSmsModel);
        sysSmsModel.setUpdateBy(SecurityUtils.getUsername());
        sysSmsModel.setUpdateTime(new DateTime());
        int row = sysSmsModelMapper.updateById(sysSmsModel);
        return row;
    }

    @Override
    public String checkByColdUnique(SysSmsModelAddBo bo) {
        QueryWrapper<SysSmsModel> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq(!StrUtil.isEmpty(bo.getSmsCode()),"sms_code",bo.getSmsCode());
        SysSmsModel sysSmsModel = sysSmsModelMapper.selectOne(queryWrapper);
        if(StringUtils.isNotNull(sysSmsModel)&&bo.getSmsCode().equals(sysSmsModel.getSmsCode())){
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    @Override
    public String checkByColdUnique(SysSmsModelEditBo bo) {
        QueryWrapper<SysSmsModel> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq(!StrUtil.isEmpty(bo.getSmsCode()),"sms_code",bo.getSmsCode());
        SysSmsModel sysSmsModel = sysSmsModelMapper.selectOne(queryWrapper);
        if(StringUtils.isNotNull(sysSmsModel)&&bo.getSmsCode().equals(sysSmsModel.getSmsCode())){
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

}
