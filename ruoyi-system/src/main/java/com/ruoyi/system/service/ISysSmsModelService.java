package com.ruoyi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.SysSmsModel;
import com.ruoyi.system.domain.vo.request.SysEmailModelAddBo;
import com.ruoyi.system.domain.vo.request.SysEmailModelEditBo;
import com.ruoyi.system.domain.vo.request.SysSmsModelAddBo;
import com.ruoyi.system.domain.vo.request.SysSmsModelEditBo;
import com.ruoyi.system.domain.bo.SysSmsModelQueryBo;
import com.ruoyi.system.domain.vo.SysSmsModelVo;


import java.util.List;

/**
 * 短信模版Service接口
 *
 * @author ruoyi
 * @date 2021-06-24
 */
public interface ISysSmsModelService extends IService<SysSmsModel> {
	/**
	 * 查询单个
	 * @return
	 */
	SysSmsModel queryById(Long smsModelId);



	List<SysSmsModel> queryList(SysSmsModelQueryBo bo);

	/**
	 * 根据新增业务对象插入短信模版
	 * @param bo 短信模版新增业务对象
	 * @return
	 */
	int insertByAddBo(SysSmsModelAddBo bo);

	/**
	 * 根据编辑业务对象修改短信模版
	 * @param bo 短信模版编辑业务对象
	 * @return
	 */
	int updateByEditBo(SysSmsModelEditBo bo);

	public String checkByColdUnique(SysSmsModelAddBo bo);
	public String checkByColdUnique(SysSmsModelEditBo bo);

}
