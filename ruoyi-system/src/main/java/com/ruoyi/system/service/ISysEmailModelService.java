package com.ruoyi.system.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.common.core.domain.entity.SysDictType;
import com.ruoyi.system.domain.SysEmailModel;
import com.ruoyi.system.domain.vo.request.SysEmailModelAddBo;
import com.ruoyi.system.domain.vo.request.SysEmailModelEditBo;
import com.ruoyi.system.domain.bo.SysEmailModelQueryBo;
import java.util.List;

/**
 * 邮件模板Service接口
 *
 * @author ruoyi
 * @date 2021-06-24
 */
public interface ISysEmailModelService extends IService<SysEmailModel> {
	/**
	 * 查询单个
	 * @return
	 */
	public SysEmailModel queryById(Long emailModelId);

	/**
	 * 查询列表
	 */
	public List<SysEmailModel> queryList(SysEmailModelQueryBo bo);

	/**
	 * 根据新增业务对象插入邮件模板
	 * @param bo 邮件模板新增业务对象
	 * @return
	 */
	public int insertByAddBo(SysEmailModelAddBo bo);

	/**
	 * 根据编辑业务对象修改邮件模板
	 * @param bo 邮件模板编辑业务对象
	 * @return
	 */
	public int updateByEditBo(SysEmailModelEditBo bo);

	/**
	 * 查询新增模板code是否存在
	 *
	 */
	public String checkByColdUnique(SysEmailModelAddBo bo);
	public String checkByColdUnique(SysEmailModelEditBo bo);
}
