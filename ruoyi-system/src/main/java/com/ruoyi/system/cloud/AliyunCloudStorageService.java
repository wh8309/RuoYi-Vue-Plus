package com.ruoyi.system.cloud;

import com.alibaba.fastjson.JSON;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.GeneratePresignedUrlRequest;
import com.aliyun.oss.model.ObjectMetadata;
import com.ruoyi.common.exception.user.OssException;
import com.ruoyi.common.utils.IOUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;

/**
 * 阿里云存储
 */
public class AliyunCloudStorageService extends CloudStorageService
{
    private static Logger logger = LoggerFactory.getLogger(AliyunCloudStorageService.class);
    // endpoint是访问OSS的域名。如果您已经在OSS的控制台上 创建了Bucket，请在控制台上查看域名。
    // 如果您还没有创建Bucket，endpoint选择请参看文档中心的“开发人员指南 > 基本概念 > 访问域名”，
    // 链接地址是：https://help.aliyun.com/document_detail/oss/user_guide/oss_concept/endpoint.html?spm=5176.docoss/user_guide/endpoint_region
    // endpoint的格式形如“http://oss-cn-hangzhou.aliyuncs.com/”，注意http://后不带bucket名称，
    // 比如“http://bucket-name.oss-cn-hangzhou.aliyuncs.com”，是错误的endpoint，请去掉其中的“bucket-name”。
    private  String endpoint = "http://oss-cn-hangzhou.aliyuncs.com";

    // accessKeyId和accessKeySecret是OSS的访问密钥，您可以在控制台上创建和查看，
    // 创建和查看访问密钥的链接地址是：https://ak-console.aliyun.com/#/。
    // 注意：accessKeyId和accessKeySecret前后都没有空格，从控制台复制时请检查并去除多余的空格。
    private  String accessKeyId = "LTAI4Fy8xSFf6WMY6NhEjjYu";
    private  String accessKeySecret = "A6VGXek47VfKgKe6u5OPEw4GwfbFYK";

    // Bucket用来管理所存储Object的存储空间，详细描述请参看“开发人员指南 > 基本概念 > OSS基本概念介绍”。
    // Bucket命名规范如下：只能包括小写字母，数字和短横线（-），必须以小写字母或者数字开头，长度必须在3-63字节之间。
    private  String bucketName = "online-report-store";

    private OSS client;

    public AliyunCloudStorageService(CloudStorageConfig config)
    {
        this.config = config;
        // 初始化
        init();
    }

    private void init()
    {
        endpoint=config.getAliyunEndPoint();
        accessKeyId=config.getAliyunAccessKeyId();
        accessKeySecret=config.getAliyunAccessKeySecret();
        bucketName=config.getAliyunBucketName();
        client = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        // 判断Bucket是否存在。详细请参看“SDK手册 > Java-SDK > 管理Bucket”。
        // 链接地址是：https://help.aliyun.com/document_detail/oss/sdk/java-sdk/manage_bucket.html?spm=5176.docoss/sdk/java-sdk/init
        if (client.doesBucketExist(bucketName)) {
            System.out.println("您已经创建Bucket：" + bucketName + "。");
        } else {
            System.out.println("您的Bucket不存在，创建Bucket：" + bucketName + "。");
            // 创建Bucket。详细请参看“SDK手册 > Java-SDK > 管理Bucket”。
            // 链接地址是：https://help.aliyun.com/document_detail/oss/sdk/java-sdk/manage_bucket.html?spm=5176.docoss/sdk/java-sdk/init
            client.createBucket(bucketName);
        }
    }

    @Override
    public String upload(byte[] data, String path)
    {
        return upload(new ByteArrayInputStream(data), path);
    }

    @Override
    public String upload(InputStream inputStream, String path)
    {
        try
        {
            client.putObject(config.getAliyunBucketName(), path, inputStream);
        }
        catch (Exception e)
        {
            throw new OssException("上传文件失败，请检查配置信息");
        }
        return config.getAliyunDomain() + "/" + path;
    }

    @Override
    public String uploadSuffix(byte[] data, String suffix)
    {
        return upload(data, getPath(config.getAliyunPrefix(), suffix));
    }

    @Override
    public String uploadSuffix(InputStream inputStream, String suffix)
    {
        return upload(inputStream, getPath(config.getAliyunPrefix(), suffix));
    }

    @Override
    public String uploadFile(String key, String filename)  {
        try {
            File file = new File(filename);
            return uploadFile(key, file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String uploadFile(String key, File file) throws Exception {
        return uploadFile(key, new FileInputStream(file), file.length(), true);
    }

    public String uploadFile(String key, InputStream in, long contentLength, boolean closeIn) throws Exception {
        try{
            ObjectMetadata objectMeta = new ObjectMetadata();
            objectMeta.setContentLength(contentLength);
            client.putObject(bucketName, key, in, objectMeta);
            return key;
        }finally{
            if (closeIn) {
                IOUtil.close(in);
            }
        }
    }

    /**
     * 获取云上访问路径
     * @param key 云上存储路径
     * @return
     */
    @Override
    public String getCloudVisitUrl(String key) {
        // 设置URL过期时间为10年 3600l* 1000*24*365*10
//		Date expiration = new Date(new Date().getTime() + 3600l * 1000 * 24 ); //* 365 * 10
        // 生成URL
        Date expiration = new Date(new Date().getTime() + 3600 * 2 * 1000);
        GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName, key);
        generatePresignedUrlRequest.setExpiration(expiration);
//        //大图缩略
//        larger_img_style: image/resize,l_720
//        //小图缩略
//        thumbnail_img_style: image/resize,l_198
//        //视频缩略
//        video_style: video/snapshot,t_50000,f_jpg,w_800,h_600
//        generatePresignedUrlRequest.setProcess("image/resize,l_720");
        URL url = client.generatePresignedUrl(generatePresignedUrlRequest);
        return url.toString();
    }




}
