package com.ruoyi.framework.web.service;

import com.ruoyi.website.domain.ClientBaseInfo;
import com.ruoyi.website.domain.ClientProfile;
import com.ruoyi.website.service.IClientBaseInfoService;
import com.ruoyi.website.service.IClientProfileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enums.UserStatus;
import com.ruoyi.common.exception.BaseException;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.service.ISysUserService;

/**
 * 用户验证处理
 *
 * @author ruoyi
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private static final Logger log = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

    @Autowired
    private ISysUserService userService;

    @Autowired
    private IClientProfileService clientProfileService;

    @Autowired
    private IClientBaseInfoService clientBaseInfoService;

    @Autowired
    private SysPermissionService permissionService;


    @Override
    public UserDetails loadUserByUsername(String loginNameAndLoginType) throws UsernameNotFoundException {
        String username = loginNameAndLoginType.substring(0, loginNameAndLoginType.length() - 2);
        Integer loginType = Integer.valueOf(loginNameAndLoginType.substring(loginNameAndLoginType.length() - 1));
        SysUser user = null;
        if (1 == (loginType)) {//管理端登录
            user = userService.selectUserByUserName(username);
        } else if (2 == loginType) {//网站
            ClientProfile clientProfile = clientProfileService.selectByLoginName(username);
            if (clientProfile != null) {
                ClientBaseInfo clientBaseInfo = clientBaseInfoService.getById(clientProfile.getClientId());
                user = new SysUser();
                BeanUtils.copyProperties(clientProfile, user);
                user.setLoginType(2);
                user.setUserId(clientProfile.getClientId());
                user.setUserName(clientProfile.getName());
                user.setLoginName(clientProfile.getLoginName());
                user.setPhonenumber(clientProfile.getMobile() + "");
                user.setDelFlag(String.valueOf(clientProfile.getIsDeleted()));
                if (clientBaseInfo.getReportStatus() <= 0) {
                    user.setIsEdit(1);
                } else {
                    user.setIsEdit(0);
                }
            }
        }
        if (StringUtils.isNull(user)) {
            log.info("登录用户：{} 不存在.", username);
            throw new UsernameNotFoundException("登录用户：" + username + " 不存在");
        } else if (UserStatus.DELETED.getCode().equals(user.getDelFlag())) {
            log.info("登录用户：{} 已被删除.", username);
            throw new BaseException("对不起，您的账号：" + username + " 已被删除");
        } else if (2 == user.getLoginType().intValue() && UserStatus.DELETED_CLIENT.getCode().equals(user.getDelFlag())) {
            log.info("登录用户：{} 已被删除.", username);
            throw new BaseException("对不起，您的账号：" + username + " 已被删除");
        } else if (UserStatus.DISABLE.getCode().equals(user.getStatus())) {
            log.info("登录用户：{} 已被停用.", username);
            throw new BaseException("对不起，您的账号：" + username + " 已停用");
        }
        return createLoginUser(user);
    }

    public UserDetails createLoginUser(SysUser user) {
        return new LoginUser(user, permissionService.getMenuPermission(user));
    }
}
