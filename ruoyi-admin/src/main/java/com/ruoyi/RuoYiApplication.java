package com.ruoyi;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.util.StringUtils;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * 启动程序
 *
 * @author ruoyi
 */
@Slf4j
@EnableFeignClients
@EnableScheduling
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class RuoYiApplication {
    public static void main(String[] args) throws UnknownHostException {
        System.setProperty("spring.devtools.restart.enabled", "false");
        ConfigurableApplicationContext application = SpringApplication.run(RuoYiApplication.class, args);
        Environment env = application.getEnvironment();
        String ip = InetAddress.getLocalHost().getHostAddress();
        String port = env.getProperty("server.port");
        String path = env.getProperty("server.servlet.context-path");
        if (StringUtils.isEmpty(path)) path = "";
        log.info("\n----------------------------------------------------------\n\t"
                + "(♥◠‿◠)ﾉﾞ  若依启动成功!   ლ(´ڡ`ლ)ﾞ\n\t"
                + "Application  is running! Access URLs:\n\t"
                + "Local访问网址: \t\thttp://localhost:" + port + path + "\n\t"
                + "External访问网址: \thttp://" + ip + ":" + port + path + "\n\t"
                + "Swagger访问网址: \thttp://" + ip + ":" + port + path + "swagger-ui.html\n"
                + "----------------------------------------------------------");

    }
}
