package com.ruoyi.web.controller.system;


import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.vo.request.SysEmailSendAddBo;
import com.ruoyi.system.domain.bo.SysEmailSendQueryBo;
import com.ruoyi.system.domain.vo.response.SysEmailSendVo;
import com.ruoyi.system.service.ISysEmailSendService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 邮件发送记录Controller
 *
 * @author ruoyi
 * @date 2021-06-24
 */
@Slf4j
@RestController
@RequestMapping("/system/emailSend")
@Api(value = "邮件发送记录控制器", description = "邮件发送记录控制器")
public class SysEmailSendController extends BaseController {

    @Resource
    private ISysEmailSendService iSysEmailSendService;

    @GetMapping(value = "/querySysEmailSendList")
    @ApiOperation(value = "查询邮件发送记录", notes = "查询邮件发送记录")
    @ApiResponses(@ApiResponse(code = 200, message = "OK", response = SysEmailSendVo.class))
    public AjaxResult querySysEmailSendList(@Validated SysEmailSendQueryBo bo) {
        startPage();
        return  AjaxResult.success(iSysEmailSendService.queryList(bo));
    }

    /**
     * 新增邮件模板
     */
    @PostMapping("/addEmailSend")
    @Log(title = "邮箱状态新增", businessType = BusinessType.INSERT)
    @ApiOperation(value = "新增邮件发送记录", notes = "新增邮件发送记录")
    public AjaxResult addEmailSend(@Validated @RequestBody SysEmailSendAddBo bo) {
        return toAjax(iSysEmailSendService.insertByAddBo(bo) ? 1 : 0);
    }

//    //修改邮件模板
//    @PostMapping("/updateEmailSend")
//    @ApiOperation(value = "修改邮箱模板", notes = "修改邮箱模板")
//    public AjaxResult updateEmailSend(@Validated @RequestBody SysEmailSendEditBo bo) {
//        return toAjax(iSysEmailSendService.updateByEditBo(bo) ? 1 : 0);
//    }

}
