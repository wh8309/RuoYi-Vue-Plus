package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;

import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.bo.*;
import com.ruoyi.system.domain.vo.request.SysSmsModelAddBo;
import com.ruoyi.system.domain.vo.request.SysSmsModelEditBo;
import com.ruoyi.system.domain.vo.response.SysEmailModelQueryResponseVo;
import com.ruoyi.system.service.ISysSmsModelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


/**
 * 短信模版Controller
 *
 * @author ruoyi
 * @date 2021-06-24
 */
@Slf4j
@RestController
@RequestMapping("/system/smsModel")
@Api(value = "短信模版控制器", description = "短信模版控制器")
public class SysSmsModelController extends BaseController {
    @Resource
    private  ISysSmsModelService iSysSmsModelService;

    @GetMapping(value = "/querySmsList")
    @ApiOperation(value = "查询短信模板", notes = "查询短信模板")
    @ApiResponses(@ApiResponse(code = 200, message = "OK", response = SysEmailModelQueryResponseVo.class))
    public AjaxResult querySmsList(@Validated  SysSmsModelQueryBo bo) {
        startPage();
        return  AjaxResult.success(iSysSmsModelService.queryList(bo));
    }

    /**
     * 新增邮件模板
     */
    @PostMapping("/addSms")
    @Log(title = "短信模板新增", businessType = BusinessType.INSERT)
    @ApiOperation(value = "短信模板新增", notes = "短信模板新增")
    public AjaxResult addSms(@Validated @RequestBody SysSmsModelAddBo bo) {
        if (UserConstants.NOT_UNIQUE.equals(iSysSmsModelService.checkByColdUnique(bo)))
        {
            return AjaxResult.error("新增邮件模板code'" + bo.getSmsCode() + "'失败，模板code已存在");
        }
        return toAjax(iSysSmsModelService.insertByAddBo(bo));
    }

    //修改邮件模板
    @PostMapping("/updateSms")
    @Log(title = "短信模板修改", businessType = BusinessType.INSERT)
    @ApiOperation(value = "修改短信模板", notes = "修改短信模板")
    public AjaxResult updateSms(@Validated @RequestBody SysSmsModelEditBo bo) {
        if (UserConstants.NOT_UNIQUE.equals(iSysSmsModelService.checkByColdUnique(bo)))
        {
            return AjaxResult.error("新增邮件模板code'" + bo.getSmsCode() + "'失败，模板code已存在");
        }
        return toAjax(iSysSmsModelService.updateByEditBo(bo));
    }

    @GetMapping(value = "/querySysSmsModelOne")
    @ApiOperation(value = "查询单条邮箱模板", notes = "查询单条邮箱模板")
    @ApiResponses(@ApiResponse(code = 200, message = "OK", response = SysEmailModelQueryResponseVo.class))
    public AjaxResult querySysSmsModelOne(@RequestParam(value = "id") Long id) {
        return  AjaxResult.success(iSysSmsModelService.queryById(id));
    }

}
