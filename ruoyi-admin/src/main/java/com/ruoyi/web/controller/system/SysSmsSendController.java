package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.vo.request.SysSmsSendAddBo;
import com.ruoyi.system.domain.bo.SysSmsSendQueryBo;
import com.ruoyi.system.domain.vo.response.SysEmailModelQueryResponseVo;
import com.ruoyi.system.service.ISysSmsSendService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.annotation.Resource;

/**
 * 短信发送记录Controller
 *
 * @author ruoyi
 * @date 2021-06-24
 */
@Slf4j
@RestController
@RequestMapping("/system/smsSend")
@Api(value = "短信发送记录控制器", description = "短信发送记录控制器")
public class SysSmsSendController extends BaseController {
    @Resource
    private  ISysSmsSendService iSysSmsSendService;

    @GetMapping(value = "/queryList")
    @ApiOperation(value = "查询短信发送记录", notes = "查询短信发送记录")
    @ApiResponses(@ApiResponse(code = 200, message = "OK", response = SysEmailModelQueryResponseVo.class))
    public AjaxResult queryList(@Validated SysSmsSendQueryBo bo) {
        startPage();
        return  AjaxResult.success(iSysSmsSendService.queryList(bo));
    }

    /**
     * 新增邮件模板
     */
    @PostMapping("/addEmail")
    @Log(title = "短信状态新增", businessType = BusinessType.INSERT)
    @ApiOperation(value = "新增短信发送记录", notes = "新增短信发送记录")
    public AjaxResult addEmail(@Validated @RequestBody SysSmsSendAddBo bo) {
        return toAjax(iSysSmsSendService.insertByAddBo(bo) ? 1 : 0);
    }



}
