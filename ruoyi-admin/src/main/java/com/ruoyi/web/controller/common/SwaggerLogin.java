package com.ruoyi.web.controller.common;

import com.google.code.kaptcha.Producer;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.framework.web.service.SysLoginService;
import com.ruoyi.framework.web.service.SysPermissionService;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.service.ISysMenuService;
import com.ruoyi.website.service.IClientBaseInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@Slf4j
@Api(value = "用户登录-swagger专用", description = "用户登录-swagger专用")
@RestController
@RequestMapping("/swagger")
public class SwaggerLogin {
    @Autowired
    private SysLoginService loginService;

    @Autowired
    private ISysMenuService menuService;

    @Autowired
    private SysPermissionService permissionService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private IClientBaseInfoService iClientBaseInfoService;

    @Resource(name = "captchaProducer")
    private Producer captchaProducer;

    @Resource(name = "captchaProducerMath")
    private Producer captchaProducerMath;

    @Autowired
    private RedisCache redisCache;

    // 验证码类型
    @Value("${ruoyi.captchaType}")
    private String captchaType;

    /**
     * 登录方法
     */
    @ApiOperation(value = "用户登录-swagger专用", notes = "用户登录")
    @RequestMapping(value = "/login", method = {RequestMethod.POST})
    public AjaxResult login(
            @RequestParam(name = "userName",value = "userName", required = false, defaultValue = "admin") String userName,
            @RequestParam(name = "passWord",value = "passWord", required = false, defaultValue = "admin123") String passWord,
            @RequestParam(name = "loginType",value = "loginType", required = true, defaultValue = "1") Integer loginType
    ) {
        // 执行验证码操作
        String uuid = IdUtils.simpleUUID();
        String verifyKey = Constants.CAPTCHA_CODE_KEY + uuid;
        String capStr = null, code = null;
        // 生成验证码
        if ("math".equals(captchaType)) {
            String capText = captchaProducerMath.createText();
            capStr = capText.substring(0, capText.lastIndexOf("@"));
            code = capText.substring(capText.lastIndexOf("@") + 1);
            log.info("验证码：{},答案：{}", capStr, code);
        } else if ("char".equals(captchaType)) {
            capStr = code = captchaProducer.createText();
            log.info("验证码：{},答案：{}", capStr, code);
        }
        redisCache.setCacheObject(verifyKey, code, Constants.CAPTCHA_EXPIRATION, TimeUnit.MINUTES);
        //执行登陆操作
        AjaxResult ajax = AjaxResult.success();
        LoginUser loginUser = loginService.login(userName, passWord, code, uuid,loginType);
        // 生成令牌
        String token = tokenService.createToken(loginUser);
        SysUser sysUser = loginUser.getUser();
        ajax.put(Constants.TOKEN, token);
        ajax.put("loginType", loginType);
        if(2==loginType){//网站登录
            ajax.put("isEdit",sysUser.getIsEdit());//1可以编辑 0 不可以编辑
        }
        //登录完成后创建基本信息表数据
        log.info("用户登录成功！");
        log.info("token：  {}", token);
        return ajax;
    }

}
