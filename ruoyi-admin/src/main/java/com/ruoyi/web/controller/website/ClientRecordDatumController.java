package com.ruoyi.web.controller.website;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.website.domain.vo.response.CardImgsMenuResponseVO;
import com.ruoyi.website.domain.vo.response.RecordDatumResponseVO;
import com.ruoyi.website.service.IClientRecordDatumService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 评审申报材料Controller
 *
 * @author ruoyi
 * @date 2021-03-18
 */
@RestController
@RequestMapping("/recordDatum")
@Api(value = "评审申报材料", description = "评审申报材料")
public class ClientRecordDatumController extends BaseController {

    @Autowired
    private IClientRecordDatumService iClientRecordDatumService;

    @GetMapping("/menu")
    @ApiOperation(value="评审申报材料",notes="获取评审申报材料菜单，目的标识哪些已经填写过")
    @ApiImplicitParams(@ApiImplicitParam(name = "clientId", value = "客户Id,前端网站可以不传，默认取当前登录的用户id;管理端该参数必填", example = "", paramType = "query", dataType = "Long", required = false))
    @ApiResponses(@ApiResponse(code = 200,message = "OK",response = CardImgsMenuResponseVO.class))
    public TableDataInfo list(Long clientId) {
        List<RecordDatumResponseVO> list = iClientRecordDatumService.selectRecordDatumMenu(clientId);
        return getDataTable(list);
    }


}
