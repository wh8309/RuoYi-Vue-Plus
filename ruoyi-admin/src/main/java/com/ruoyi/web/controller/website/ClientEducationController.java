package com.ruoyi.web.controller.website;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.website.domain.vo.request.ClientEducationRequestVO;
import com.ruoyi.website.domain.vo.response.ClientEducationResponseVO;
import com.ruoyi.website.service.IClientEducationService;
import com.ruoyi.website.service.IClientRobotReportTagService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 学历信息Controller
 *
 * @author ruoyi
 * @date 2021-03-22
 */
@RestController
@RequestMapping("/education")
@Api(value = "学历", description = "学历")
public class ClientEducationController extends BaseController {

    @Autowired
    private IClientEducationService iClientEducationService;
    @Autowired
    private  IClientRobotReportTagService clientRobotReportTagService;

    /**
     * 根据主键查询详情
     */
    @GetMapping(value = "/queryDetailById")
    @ApiOperation(value = "根据主键查询详情", notes = "根据主键查询详情")
    @ApiResponses(@ApiResponse(code = 200, message = "OK", response = ClientEducationResponseVO.class))
    @ApiImplicitParams(@ApiImplicitParam(name = "clientId", value = "主键ID", required = false, dataType = "Long", paramType = "query"))
    public AjaxResult queryDetailById(Long clientId) {
        ClientEducationResponseVO clientEducationResponse = iClientEducationService.queryDetailById(clientId);
        return AjaxResult.success(clientEducationResponse);
    }

    /**
     * 新增新增
     */
    @PostMapping("/add")
    @ApiOperation(value = "新增学历信息", notes = "新增学历信息")
    public AjaxResult add(@Validated @RequestBody ClientEducationRequestVO clientEducationRequestVO) {
        int rows = iClientEducationService.insertOrUpdate(clientEducationRequestVO);
        clientRobotReportTagService.resetReportTag(clientEducationRequestVO.getClientId(),"menu_education");//重置上报Tag
        return toAjax(rows);
    }

    /**
     * 编辑
     */
    @PostMapping("/edit")
    @ApiOperation(value = "编辑学历信息", notes = "编辑学历信息")
    public AjaxResult edit(@Validated @RequestBody ClientEducationRequestVO clientEducationRequestVO) {
        int rows = iClientEducationService.insertOrUpdate(clientEducationRequestVO);
        clientRobotReportTagService.resetReportTag(clientEducationRequestVO.getClientId(),"menu_education");//重置上报Tag
        return toAjax(rows);
    }


}
