package com.ruoyi.web.controller.website;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.website.domain.vo.request.ClientReferralTypeRequest;
import com.ruoyi.website.domain.vo.response.ClientReferralTypeResponse;
import com.ruoyi.website.service.IClientReferralTypeService;
import com.ruoyi.website.service.IClientRobotReportTagService;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 转评条件说明Controller
 *
 * @author ruoyi
 * @date 2021-03-24
 */
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/referralType" )
@Api(value = "转评条件说明", description = "转评条件说明")
public class ClientReferralTypeController extends BaseController {

    @Autowired
    private IClientReferralTypeService iClientReferralTypeService;
    @Autowired
    private IClientRobotReportTagService clientRobotReportTagService;
    /**
     * 根据主键查询详情
     */
    @GetMapping(value = "/queryDetailById")
    @ApiOperation(value = "根据主键查询详情", notes = "根据主键查询详情")
    @ApiResponses(@ApiResponse(code = 200, message = "OK", response = ClientReferralTypeResponse.class))
    @ApiImplicitParams(@ApiImplicitParam(name = "clientId", value = "主键ID", required = false, dataType = "Long", paramType = "query"))
    public AjaxResult queryDetailById(Long clientId) {
        ClientReferralTypeResponse referralTypeResponse = iClientReferralTypeService.queryDetailById(clientId);
        return AjaxResult.success(referralTypeResponse);
    }

    /**
     * 新增新增
     */
    @PostMapping("/add")
    @ApiOperation(value = "新增转评条件说明", notes = "新增转评条件说明")
    public AjaxResult add(@Validated @RequestBody ClientReferralTypeRequest referralTypeRequest) {
        int rows = iClientReferralTypeService.insertOrUpdate(referralTypeRequest);
        clientRobotReportTagService.resetReportTag(referralTypeRequest.getClientId(),"menu_zpInfo");//重置上报Tag
        return toAjax(rows);
    }

    /**
     * 编辑
     */
    @PostMapping("/edit")
    @ApiOperation(value = "编辑转评条件说明", notes = "编辑转评条件说明")
    public AjaxResult edit(@Validated @RequestBody ClientReferralTypeRequest referralTypeRequest) {
        int rows = iClientReferralTypeService.insertOrUpdate(referralTypeRequest);
        clientRobotReportTagService.resetReportTag(referralTypeRequest.getClientId(),"menu_zpInfo");//重置上报Tag
        return toAjax(rows);
    }

}
