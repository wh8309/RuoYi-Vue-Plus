package com.ruoyi.web.controller.website;

import java.util.List;
import java.util.Arrays;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.website.domain.vo.request.*;
import com.ruoyi.website.domain.vo.response.TorsNominateUnitExcelImportResponseVO;
import com.ruoyi.website.domain.vo.response.TorsNominateUnitResponseVO;
import com.ruoyi.website.service.IClientBaseInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.website.domain.TorsNominateUnit;
import com.ruoyi.website.service.ITorsNominateUnitService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 推荐单位Controller
 *
 * @author ruoyi
 * @date 2021-04-22
 */
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/tors/nominateUnit")
@Api(value = "推荐单位维护", description = "推荐单位维护")
public class TorsNominateUnitController extends BaseController {

    private final ITorsNominateUnitService iTorsNominateUnitService;

    private final IClientBaseInfoService clientBaseInfoService;

    private final TokenService tokenService;


    /**
     * 查询推荐单位列表
     */
//    @PreAuthorize("@ss.hasPermi('tors:nominateUnit:list')")
    @GetMapping("/list")
    @ApiOperation(value = "条件分页查询单位信息", notes = "条件分页查询单位信息")
    public TableDataInfo list(TorsNominateUnitQueryRequestVO torsNominateUnitQueryRequestVO) {
        TorsNominateUnit torsNominateUnit = new TorsNominateUnit();
        BeanUtils.copyProperties(torsNominateUnitQueryRequestVO, torsNominateUnit);
        startPage();
        List<TorsNominateUnitResponseVO> list = iTorsNominateUnitService.queryList(torsNominateUnit);
        return getDataTable(list);
    }

    /**
     * 导出推荐单位列表
     */
//    @PreAuthorize("@ss.hasPermi('tors:nominateUnit:export')" )
//    @Log(title = "推荐单位" , businessType = BusinessType.EXPORT)
//    @GetMapping("/export" )
//    public AjaxResult export(TorsNominateUnit torsNominateUnit) {
//        List<TorsNominateUnitResponseVO> list = iTorsNominateUnitService.queryList(torsNominateUnit);
//        ExcelUtil<TorsNominateUnit> util = new ExcelUtil<TorsNominateUnit>(TorsNominateUnit.class);
//        return util.exportExcel(list, "unit" );
//    }

    /**
     * 获取推荐单位详细信息
     */
    @ApiOperation(value = "获取推荐单位详细信息", notes = "获取推荐单位详细信息")
//    @PreAuthorize("@ss.hasPermi('tors:nominateUnit:query')" )
    @GetMapping(value = "/{nominateUnitId}")
    public AjaxResult getInfo(@PathVariable("nominateUnitId") Long nominateUnitId) {
        return AjaxResult.success(iTorsNominateUnitService.getById(nominateUnitId));
    }

    /**
     * 新增推荐单位
     */
//    @PreAuthorize("@ss.hasPermi('tors:nominateUnit:add')" )
    @ApiOperation(value = "推荐单位新增", notes = "推荐单位新增")
    @Log(title = "推荐单位", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Validated @RequestBody TorsNominateUnitAddRequestVO torsNominateUnitAddRequestVO) {
        TorsNominateUnit torsNominateUnit = new TorsNominateUnit();
        BeanUtils.copyProperties(torsNominateUnitAddRequestVO, torsNominateUnit);
        AjaxResult ajaxResult = toAjax(iTorsNominateUnitService.save(torsNominateUnit) ? 1 : 0);
        return ajaxResult;
    }

    /**
     * 修改推荐单位
     */
//    @PreAuthorize("@ss.hasPermi('tors:nominateUnit:edit')" )
    @ApiOperation(value = "编辑推荐单位信息", notes = "编辑推荐单位信息")
    @Log(title = "推荐单位", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Validated @RequestBody TorsNominateUnitEditRequestVO torsNominateUnitEditRequestVO) {
        TorsNominateUnit torsNominateUnit = new TorsNominateUnit();
        BeanUtils.copyProperties(torsNominateUnitEditRequestVO, torsNominateUnit);
        int rows=iTorsNominateUnitService.updateById(torsNominateUnit) ? 1 : 0;
        if(rows>0){//修改用户里面授权码
            clientBaseInfoService.updateByTorsNominateUnit(torsNominateUnit);
        }
        AjaxResult ajaxResult = toAjax(rows);
        return ajaxResult;
    }

    /**
     * 删除推荐单位
     */
//    @PreAuthorize("@ss.hasPermi('tors:nominateUnit:remove')" )
    @ApiOperation(value = "推荐单位批量删除", notes = "推荐单位批量删除接口")
    @Log(title = "推荐单位", businessType = BusinessType.DELETE)
    @DeleteMapping("/{nominateUnitIds}")
    public AjaxResult remove(@PathVariable Long[] nominateUnitIds) {
        return toAjax(iTorsNominateUnitService.deleteBatch(Arrays.asList(nominateUnitIds)) ? 1 : 0);
    }

    /**
     * 客户信息导出
     */
    @Log(title = "推荐单位导出", businessType = BusinessType.EXPORT)
    @GetMapping("/exportData")
//    @PreAuthorize("@ss.hasPermi('tors:nominateUnit:export')")
    @ApiOperation(value = "推荐单位导出", notes = "推荐单位导出")
    public AjaxResult exportExcel(TorsNominateUnitQueryRequestVO requestVO) {
        TorsNominateUnit torsNominateUnit = new TorsNominateUnit();
        BeanUtils.copyProperties(requestVO, torsNominateUnit);
        List<TorsNominateUnitExcelImportResponseVO> list = iTorsNominateUnitService.exportListByParam(torsNominateUnit);
        ExcelUtil<TorsNominateUnitExcelImportResponseVO> util = new ExcelUtil<>(TorsNominateUnitExcelImportResponseVO.class);
        return util.exportExcel(list, "推荐单位数据");
    }

    /**
     * 客户信息导入模板
     *
     * @return
     */
    @GetMapping("/importTemplate")
    @ApiOperation(value = "推荐单位数据导入模板下载", notes = "推荐单位数据导入模板下载")
    public AjaxResult importTemplate() {
        ExcelUtil<TorsNominateUnitExcelImportResponseVO> util = new ExcelUtil<>(TorsNominateUnitExcelImportResponseVO.class);
        return util.importTemplateExcel("推荐单位导入模板");
    }

    /**
     * 客户信息导入
     */
    @Log(title = "推荐单位导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    @ApiOperation(value = "推荐单位数据导入", notes = "推荐单位数据导入")
//    @PreAuthorize("@ss.hasPermi('tors:nominateUnit:import')")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<TorsNominateUnitExcelImportResponseVO> util = new ExcelUtil<>(TorsNominateUnitExcelImportResponseVO.class);
        List<TorsNominateUnitExcelImportResponseVO> excelData = util.importExcel(file.getInputStream());
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String operName = loginUser.getUsername();
        String message = iTorsNominateUnitService.importNominateUnit(excelData, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 状态修改
     */
    @ApiOperation(value = "修改推荐单位短信通知状态", notes = "修改推荐单位短信通知状态")
    @PutMapping("/changeSmsEnable")
    public AjaxResult changeSmsEnable(@Validated @RequestBody TorsNominateUnitSmsEnableRequestVO nominateUnitStatusUpdateRequestVO) {
        return toAjax(iTorsNominateUnitService.updateSmsEnable(nominateUnitStatusUpdateRequestVO));
    }
}
