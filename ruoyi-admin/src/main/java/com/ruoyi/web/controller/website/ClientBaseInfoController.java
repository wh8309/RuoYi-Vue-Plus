package com.ruoyi.web.controller.website;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ObjectUtils;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.website.domain.ClientBaseInfo;
import com.ruoyi.website.domain.SysTree;
import com.ruoyi.website.domain.vo.request.*;
import com.ruoyi.website.domain.vo.response.*;
import com.ruoyi.website.service.IClientBaseInfoService;
import com.ruoyi.website.service.IClientRobotReportTagService;
import com.ruoyi.website.service.ISysTreeService;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 客户基本信息Controller
 *
 * @author ruoyi
 * @date 2021-03-18
 */
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/baseInfo")
@Api(value = "基础信息维护", description = "基础信息维护")
public class ClientBaseInfoController extends BaseController {

    private final IClientBaseInfoService clientBaseInfoService;

    private final ISysTreeService sysTreeService;

    private final TokenService tokenService;

    private final IClientRobotReportTagService clientRobotReportTagService;

    /**
     * 根据ID查询基础信息详情
     */
    @GetMapping(value = "/queryDetailById")
    @ApiOperation(value = "根据ID查询基础信息详情", notes = "根据ID查询基础信息详情")
    @ApiImplicitParams(@ApiImplicitParam(name = "clientId", value = "基础信息ID", required = false, dataType = "Long", paramType = "query"))
    public AjaxResult queryDetailById(Long clientId) {
        BaseInfoResponseVO baseInfoResponse = clientBaseInfoService.queryBaseById(clientId);
        return AjaxResult.success(baseInfoResponse);
    }

    /**
     * 查询查询基础信息列表
     */
    //    @ApiOperation(value = "查询查询基础信息列表", notes = "查询查询基础信息列表")
    @GetMapping("/queryBaseList")
    @ApiResponses(@ApiResponse(code = 200, message = "OK", response = BaseInfoResponseVO.class))
    public AjaxResult queryBaseInfoList(BaseInfoQueryRequestVO baseInfoQueryRequestVO) {
        List<BaseInfoResponseVO> baseInfoResponseList = clientBaseInfoService.queryBaseInfoList(baseInfoQueryRequestVO);
        return AjaxResult.success(baseInfoResponseList);
    }

    /**
     * 新增基础信息
     */
    @PostMapping("/add")
    @ApiOperation(value = "新增基础信息", notes = "新增基础信息")
    public AjaxResult add(@Validated @RequestBody BaseInfoRequestVO baseInfoRequest) {
        try {
            ObjectUtils.setEmptyString(baseInfoRequest);//如果为NULL,设置为空字符串
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        int rows = clientBaseInfoService.insertOrUpdate(baseInfoRequest);
        clientRobotReportTagService.resetReportTag(baseInfoRequest.getClientId(),"menu_jbxx");//重置上报Tag
        return toAjax(rows);
    }

    /**
     * 根据Id修改基础信息
     */
    @Log(title = "职称申报-基本情况", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ApiOperation(value = "根据Id修改基础信息", notes = "根据Id修改基础信息")
    public AjaxResult edit(@Validated @RequestBody BaseInfoRequestVO baseInfoRequest) {
        try {
            ObjectUtils.setEmptyString(baseInfoRequest);//如果为NULL,设置为空字符串
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        if(StringUtils.isBlank(baseInfoRequest.getProfessionDate())||"null".equalsIgnoreCase(baseInfoRequest.getProfessionDate())){
            baseInfoRequest.setProfessionDate("");//持何职业资格(或一体化)证书 日期为NULL处理
        }
        baseInfoRequest.setNominateUnit(null);//推荐单位
        baseInfoRequest.setUnitAuthCode(null);//推荐单位授权码
        if("无".equals(baseInfoRequest.getNowPostName().trim())){//“现职称” 无
            baseInfoRequest.setNowPostTreeId("2695");
        }
        if(StringUtils.isBlank(baseInfoRequest.getBelongSeries()) && StringUtils.isNotBlank(baseInfoRequest.getReportSpecialty()) ){
            SysTree sysTree=sysTreeService.getById(baseInfoRequest.getReportSpecialty());
            baseInfoRequest.setBelongSeries(String.valueOf(sysTree.getParentTreeId()));
        }

        int rows = clientBaseInfoService.insertOrUpdate(baseInfoRequest);
        clientRobotReportTagService.resetReportTag(baseInfoRequest.getClientId(),"menu_jbxx");//重置上报Tag
        return toAjax(rows);
    }

    /**
     * 更新政务网账号和密码
     */
    @PostMapping("/editGovAccount")
    @ApiOperation(value = "更新政务网账号和密码", notes = "更新政务网账号和密码")
    public AjaxResult edit(@Validated @RequestBody GovAccountVO govAccountVO) {
        boolean retBoolean=clientBaseInfoService.updateGovAccount(govAccountVO);
        if(retBoolean){
            return AjaxResult.success();
        }else {
            return AjaxResult.error();
        }
    }


    /**
     * 查询重选通知列表
     */
    @GetMapping("/queryReviewNoticeList")
    @ApiOperation(value = "查询重选通知列表", notes = "查询重选通知列表")
    @ApiResponses(@ApiResponse(code = 200, message = "OK", response = ReviewNoticeResponse.class))
    public AjaxResult queryReviewNoticeList(  ReviewNoticeRequest reviewNoticeRequest) {
        return AjaxResult.success(clientBaseInfoService.queryReviewNoticeList(reviewNoticeRequest));
    }

    /**
     * 根据Id查询重选通知详情
     */
    @GetMapping("/queryReviewNoticeByTreeId")
    @ApiOperation(value = "根据treeId查询重选通知详情", notes = "根据treeId查询重选通知详情")
    @ApiImplicitParams(@ApiImplicitParam(name = "treeId", value = "评审通知ID", required = true, dataType = "Long", paramType = "query"))
    @ApiResponses(@ApiResponse(code = 200, message = "OK", response = ReviewNoticeResponse.class))
    public AjaxResult queryReviewNoticeByTreeId(Long treeId) {
        return AjaxResult.success(clientBaseInfoService.queryReviewNoticeByTreeId(treeId));
    }


    @GetMapping("/querySpecialtyTreeList")
    @ApiOperation(value = "查询所属系列-申报专业-专业名称", notes = "查询所属系列-申报专业-专业名称")
    @ApiResponses(@ApiResponse(code = 200, message = "OK", response = SysTreeResponse.class))
    public AjaxResult querySpecialtyTreeList() {
        return AjaxResult.success(clientBaseInfoService.querySpecialtyTreeList());
    }


    /**
     * 查询申报专业
     */
    @GetMapping("/queryProfessionalList")
    @ApiOperation(value = "查询申报专业", notes = "查询申报专业")
    @ApiImplicitParams(@ApiImplicitParam(name = "parentTreeId", value = "申报专业Id", required = true, dataType = "Long", paramType = "query"))
    public AjaxResult queryProfessionalList(Long parentTreeId) {
        return AjaxResult.success(clientBaseInfoService.queryProfessionalList(parentTreeId));
    }

    /**
     * 查询专业名称
     */
    @GetMapping("/queryProfessionalNameByParentTreeId")
    @ApiOperation(value = "查询专业名称", notes = "查询专业名称")
    @ApiImplicitParams(@ApiImplicitParam(name = "parentTreeId", value = "申报专业Id", required = true, dataType = "Long", paramType = "query"))
    public AjaxResult queryProfessionalNameList(Long parentTreeId) {
        return AjaxResult.success(clientBaseInfoService.queryProfessionalNameList(parentTreeId));
    }

    /**
     * 查询编码单位
     */
    @GetMapping("/queryEncodeCompanyTreeList")
    @ApiOperation(value = "查询编码单位", notes = "查询编码单位")
    @ApiResponses(@ApiResponse(code = 200, message = "OK", response = SysTreeResponse.class))
    public AjaxResult queryEncodeCompanyTreeList() {
        return AjaxResult.success(clientBaseInfoService.queryEncodeCompanyTreeList());
    }

    @GetMapping("/queryPositionalTitlesTreeList")
    @ApiOperation(value = "查询现职称", notes = "查询现职称")
    @ApiResponses(@ApiResponse(code = 200, message = "OK", response = SysTreeResponse.class))
    public AjaxResult queryPositionalTitlesTreeList() {
        return AjaxResult.success(clientBaseInfoService.queryPositionalTitlesTreeList());
    }


    /**
     * 查询菜单信息
     */
    @GetMapping("/queryMenuList")
    @ApiOperation(value = "查询菜单信息", notes = "查询菜单信息")
    @ApiImplicitParams(@ApiImplicitParam(name = "clientId", value = "用户Id", required = false, dataType = "Long", paramType = "query"))
    @ApiResponses(@ApiResponse(code = 200, message = "OK", response = MenuResponseVO.class))
    public AjaxResult queryMenuList(Long clientId) {
        return AjaxResult.success(clientBaseInfoService.queryMenuList(clientId));
    }


    /**
     * 查询客户职称申报
     */
    @GetMapping("/queryPageList")
    @ApiOperation(value = "查询客户职称申报", notes = "查询客户职称申报")
    @ApiResponses(@ApiResponse(code = 200, message = "OK", response = BasePositionApplyResponseVO.class))
    public TableDataInfo queryPageList(BasePositionApplyPageRequestVO basePositionApplyPageRequestVO) {
        ClientBaseInfo clientBaseInfo=new ClientBaseInfo();
        BeanUtils.copyProperties(basePositionApplyPageRequestVO,clientBaseInfo);
        clientBaseInfo.setReportStatus(basePositionApplyPageRequestVO.getReportStatus());
        startPage();
        List<BasePositionApplyResponseVO> list =clientBaseInfoService.queryPageList(clientBaseInfo);
        return getDataTable(list);
    }

    /**
     * 查询客户职称申报
     */
    @GetMapping("/queryFillInStatistics")
    @ApiOperation(value = "人员填报统计", notes = "人员填报统计")
    @ApiResponses(@ApiResponse(code = 200, message = "OK", response = FillInStatisticsResponseVO.class))
    public TableDataInfo queryFillInStatistics(FillInStatisticsRequestVO basePositionApplyPageRequestVO) {
        ClientBaseInfo clientBaseInfo=new ClientBaseInfo();
        BeanUtils.copyProperties(basePositionApplyPageRequestVO,clientBaseInfo);
        List<FillInStatisticsResponseVO> list =clientBaseInfoService.fillInStatistics(clientBaseInfo);
        return getDataTable(list);
    }

    /**
     * 查询客户职称申报
     */
    @GetMapping("/queryRobotReportLogByClientId")
    @ApiOperation(value = "获取客户机器人上报日志", notes = "获取客户机器人上报日志")
    @ApiResponses(@ApiResponse(code = 200, message = "OK", response = RobotReportLogResponseVO.class))
    public TableDataInfo queryRobotReportLogByClientId(RobotReportLogRequestVO robotReportLogRequestVO) {
        List<RobotReportLogResponseVO> list =clientBaseInfoService.queryRobotReportLogByClientId(robotReportLogRequestVO.getClientId());
        return getDataTable(list);
    }

    /**
     * 批量设置推荐单位
     */
    @PostMapping("/batchSetNominateUnit")
    @ApiOperation(value = "批量设置推荐单位", notes = "批量设置推荐单位")
    public AjaxResult batchSetNominateUnit(@Validated @RequestBody BatchSetNominateUnitVO batchSetNominateUnitVO) {
        for (String clientId : batchSetNominateUnitVO.getClientIds().split(",")) {
            clientRobotReportTagService.resetReportTag(Long.valueOf(clientId),"menu_jbxx");//重置上报Tag
        }
        boolean retBoolean= clientBaseInfoService.updateBatch(batchSetNominateUnitVO);
        if(retBoolean){
            return AjaxResult.success();
        }else{
            return AjaxResult.error();
        }
    }

    /**
     * 批量设置工作单位
     */
    @PostMapping("/batchSetWorkUnit")
    @ApiOperation(value = "批量设置工作单位", notes = "批量设置工作单位")
    public AjaxResult batchSetWorkUnit(@Validated @RequestBody BatchSetWorkUnitVO batchSetWorkUnitVO) {
        for (String clientId : batchSetWorkUnitVO.getClientIds().split(",")) {
            clientRobotReportTagService.resetReportTag(Long.valueOf(clientId),"menu_jbxx");//重置上报Tag
        }
        boolean retBoolean= clientBaseInfoService.updateBatch(batchSetWorkUnitVO);
        if(retBoolean){
            return AjaxResult.success();
        }else{
            return AjaxResult.error();
        }
    }

    /**
     * 批量设置填报人
     */
    @PostMapping("/batchSetFillInUser")
    @ApiOperation(value = "批量设置填报人", notes = "批量设置填报人")
    public AjaxResult batchSetFillInUser(@Validated @RequestBody BatchSetFillInUserVO batchSetFillInUserVO) {
        boolean retBoolean= clientBaseInfoService.updateBatch(batchSetFillInUserVO);
        if(retBoolean){
            return AjaxResult.success();
        }else{
            return AjaxResult.error();
        }
    }

    /**
     * 申报填写批量退回
     */
    @PostMapping("/batchBackDistribute")
    @ApiOperation(value = "批量分配退回", notes = "批量分配退回")
    public AjaxResult batchBackDistribute(@Validated @RequestBody BatchDistributeBackVO batchDistributeBackVO) {
        boolean retBoolean= clientBaseInfoService.updateBatch(batchDistributeBackVO);
        if(retBoolean){
            return AjaxResult.success();
        }else{
            return AjaxResult.error();
        }
    }

    /**
     * 批量流程退回
     */
    @PostMapping("/batchBackClientReportStatus")
    @ApiOperation(value = "批量流程退回", notes = "批量流程退回")
    public AjaxResult batchBackClientReportStatus(@Validated @RequestBody BatchBackClientReportStatusVO batchBackClientReportStatusVO) {
        boolean retBoolean= clientBaseInfoService.updateBatch(batchBackClientReportStatusVO);
        if(retBoolean){
            return AjaxResult.success();
        }else{
            return AjaxResult.error();
        }
    }

    /**
     * 批量重新上报
     */
    @PostMapping("/batchRestRobotReportStatus")
    @ApiOperation(value = "批量重报", notes = "批量重报")
    public AjaxResult batchRestRobotReport(@Validated @RequestBody BatchRestRobotReportVO batchRestRobotReportVO) {
        boolean retBoolean= clientBaseInfoService.updateBatch(batchRestRobotReportVO);
        if(retBoolean){
            return AjaxResult.success();
        }else{
            return AjaxResult.error();
        }
    }


    /**
     * 客户信息导出
     */
    @Log(title = "基础信息导出", businessType = BusinessType.EXPORT)
    @GetMapping("/exportData")
//    @PreAuthorize("@ss.hasPermi('client:baseInfo:export')")
    @ApiOperation(value = "基础信息导出", notes = "基础信息导出")
    public AjaxResult exportExcel(BasePositionApplyPageRequestVO requestVO) {
        ClientBaseInfo baseInfo=new ClientBaseInfo();
        BeanUtils.copyProperties(requestVO,baseInfo);
        List<BaseInfoExcelImportResponseVO> list = clientBaseInfoService.exportListByParam(baseInfo);
        ExcelUtil<BaseInfoExcelImportResponseVO> util = new ExcelUtil<>(BaseInfoExcelImportResponseVO.class);
        return util.exportExcel(list, "申报数据");
    }

    /**
     * 客户信息导入模板
     * @return
     */
    @GetMapping("/importTemplate")
    @ApiOperation(value = "基础信息数据导入模板下载", notes = "基础信息数据导入模板下载")
    public AjaxResult importTemplate() {
        ExcelUtil<BaseInfoExcelImportResponseVO> util = new ExcelUtil<>(BaseInfoExcelImportResponseVO.class);
        return util.importTemplateExcel("申报数据导入模板");
    }

    /**
     * 客户信息导入
     */
    @Log(title = "基础信息数据导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    @ApiOperation(value = "基础信息数据导入", notes = "基础信息数据导入")
//    @PreAuthorize("@ss.hasPermi('client:baseInfo:import')")
    public AjaxResult importData(MultipartFile file) throws Exception {
        ExcelUtil<BaseInfoExcelImportResponseVO> util = new ExcelUtil<>(BaseInfoExcelImportResponseVO.class);
        List<BaseInfoExcelImportResponseVO> excelData = util.importExcel(file.getInputStream());
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String operName = loginUser.getUsername();
        String message = clientBaseInfoService.importBaseInfo(excelData, operName);
        return AjaxResult.success(message);
    }


}
