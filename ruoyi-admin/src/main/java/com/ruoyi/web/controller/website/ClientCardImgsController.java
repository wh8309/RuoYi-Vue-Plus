package com.ruoyi.web.controller.website;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.website.domain.vo.response.CardImgsMenuResponseVO;
import com.ruoyi.website.service.IClientCardImgsService;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * 证件电子图片Controller
 *
 * @author ruoyi
 * @date 2021-03-18
 */

@RestController
@RequiredArgsConstructor(onConstructor_ = @Autowired)

@Api(value = "证件电子图片", description = "证件电子图片")
@RequestMapping("/cardImgs")
public class ClientCardImgsController extends BaseController {

    private final IClientCardImgsService clientCardImgsService;

    @GetMapping("/menu")
    @ApiOperation(value="证件电子图片",notes="获取证件电子图片菜单，目的标识哪些已经填写过")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "clientId", value = "客户Id,前端网站可以不传，默认取当前登录的用户id;管理端该参数必填", example = "", paramType = "query", dataType = "Long", required = false)
    })
    @ApiResponses({
            @ApiResponse(code = 200,message = "OK",response = CardImgsMenuResponseVO.class),
    })
    public TableDataInfo list(Long clientId)
    {
        List<CardImgsMenuResponseVO> list = clientCardImgsService.selectCardImgsMenu(clientId);
        return getDataTable(list);
    }



}
