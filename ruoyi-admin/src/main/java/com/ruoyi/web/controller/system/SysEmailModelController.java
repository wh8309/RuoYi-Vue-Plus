package com.ruoyi.web.controller.system;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.bo.SysEmailModelQueryBo;
import com.ruoyi.system.domain.vo.request.SysEmailModelAddBo;
import com.ruoyi.system.domain.vo.request.SysEmailModelEditBo;
import com.ruoyi.system.domain.vo.response.SysEmailModelQueryResponseVo;
import com.ruoyi.system.service.ISysEmailModelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;

/**
 * 邮件模板Controller
 *
 * @author ruoyi
 * @date 2021-06-24
 */

@Slf4j
@RestController
@RequestMapping("/system/emailModel")
@Api(value = "邮件模板控制器", description = "邮件模板管理")
public class SysEmailModelController extends BaseController {

    @Resource
    private ISysEmailModelService iSysEmailModelService;

    @GetMapping(value = "/queryList")
    @ApiOperation(value = "查询邮箱模板", notes = "查询邮箱模板")
    @ApiResponses(@ApiResponse(code = 200, message = "OK", response = SysEmailModelQueryResponseVo.class))
    public AjaxResult querySysEmailModelList(@Validated SysEmailModelQueryBo bo) {
        startPage();
        return  AjaxResult.success(iSysEmailModelService.queryList(bo));
    }

    /**
     * 新增邮件模板
     */

    @ApiOperation(value = "邮箱模板新增接口", notes = "邮箱模板新增接口")
    @Log(title = "邮箱模板新增", businessType = BusinessType.INSERT)
    @PostMapping("/addEmail")
   /* @ApiOperation(value = "新增邮箱模板", notes = "新增邮箱模板")*/
    public AjaxResult addEmailModel(@Validated @RequestBody SysEmailModelAddBo bo) {
        if (UserConstants.NOT_UNIQUE.equals(iSysEmailModelService.checkByColdUnique(bo)))
        {
            return AjaxResult.error("新增邮件模板code'" + bo.getEmailCode() + "'失败，模板code已存在");
        }
        return toAjax(iSysEmailModelService.insertByAddBo(bo));
    }

    /**
     * 修改邮件模板
     */
     @ApiOperation(value = "邮箱模板修改接口", notes = "邮箱模板修改接口")
     @Log(title = "邮箱模板修改", businessType = BusinessType.UPDATE)
     @PostMapping("/updateEmail")
    /* @ApiOperation(value = "修改邮箱模板", notes = "修改邮箱模板")*/
    public AjaxResult updateEmailModel(@Validated @RequestBody SysEmailModelEditBo bo) {

         if (UserConstants.NOT_UNIQUE.equals(iSysEmailModelService.checkByColdUnique(bo)))
         {
             return AjaxResult.error("修改邮件模板code'" + bo.getEmailCode() + "'失败，模板code已存在");
         }
         return toAjax(iSysEmailModelService.updateByEditBo(bo));
         }

    /**
     * 查询单条邮箱模板
     */
    @GetMapping(value = "/querySysEmailModelOne")
    @ApiOperation(value = "查询单条邮箱模板", notes = "查询单条邮箱模板")
    @ApiResponses(@ApiResponse(code = 200, message = "OK", response = SysEmailModelQueryResponseVo.class))
    public AjaxResult querySysEmailModelOne(@RequestParam(value = "id") Long id) {
        return  AjaxResult.success(iSysEmailModelService.queryById(id));
    }

    
}
