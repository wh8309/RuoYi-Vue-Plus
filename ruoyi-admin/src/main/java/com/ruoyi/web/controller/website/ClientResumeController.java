package com.ruoyi.web.controller.website;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.website.domain.vo.request.ClientEducationRequestVO;
import com.ruoyi.website.domain.vo.request.ClientResumeRequest;
import com.ruoyi.website.domain.vo.response.ClientEducationResponseVO;
import com.ruoyi.website.domain.vo.response.ClientResumeResponse;
import com.ruoyi.website.service.IClientResumeService;
import com.ruoyi.website.service.IClientRobotReportTagService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 从事专业技术工作简历Controller
 *
 * @author ruoyi
 * @date 2021-03-22
 */
@RestController
@RequestMapping("/resume" )
@Api(value = "从事专业技术工作简历", description = "从事专业技术工作简历")
public class ClientResumeController extends BaseController {

    @Autowired
    private IClientResumeService iClientResumeService;
    @Autowired
    private IClientRobotReportTagService clientRobotReportTagService;

    /**
     * 根据主键查询详情
     */
    @GetMapping(value = "/queryDetailById")
    @ApiOperation(value = "根据主键查询详情", notes = "根据主键查询详情")
    @ApiResponses(@ApiResponse(code = 200, message = "OK", response = ClientResumeResponse.class))
    @ApiImplicitParams(@ApiImplicitParam(name = "clientId", value = "主键ID", required = false, dataType = "Long", paramType = "query"))
    public AjaxResult queryDetailById(Long clientId) {
        ClientResumeResponse clientResumeResponse = iClientResumeService.queryDetailById(clientId);
        return AjaxResult.success(clientResumeResponse);
    }

    /**
     * 新增新增
     */
    @PostMapping("/add")
    @ApiOperation(value = "新增从事专业技术工作简历信息", notes = "新增从事专业技术工作简历信息")
    public AjaxResult add(@Validated @RequestBody ClientResumeRequest clientResumeRequest) {
        int rows = iClientResumeService.insertOrUpdate(clientResumeRequest);
        clientRobotReportTagService.resetReportTag(clientResumeRequest.getClientId(),"menu_experience");//重置上报Tag
        return toAjax(rows);
    }

    /**
     * 编辑
     */
    @PostMapping("/edit")
    @ApiOperation(value = "编辑从事专业技术工作简历信息", notes = "编辑从事专业技术工作简历信息")
    public AjaxResult edit(@Validated @RequestBody ClientResumeRequest clientResumeRequest) {
        int rows = iClientResumeService.insertOrUpdate(clientResumeRequest);
        clientRobotReportTagService.resetReportTag(clientResumeRequest.getClientId(),"menu_experience");//重置上报Tag
        return toAjax(rows);
    }


}
