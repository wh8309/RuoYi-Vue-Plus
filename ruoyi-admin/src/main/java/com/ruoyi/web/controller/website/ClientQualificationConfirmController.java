package com.ruoyi.web.controller.website;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.website.domain.vo.request.ClientExchangeFacultyRequest;
import com.ruoyi.website.domain.vo.request.ClientQualificationConfirmRequest;
import com.ruoyi.website.domain.vo.response.ClientExchangeFacultyResponse;
import com.ruoyi.website.domain.vo.response.ClientQualificationConfirmResponse;
import com.ruoyi.website.service.IClientQualificationConfirmService;
import com.ruoyi.website.service.IClientRobotReportTagService;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 职称资格确认Controller
 *
 * @author ruoyi
 * @date 2021-03-24
 */
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/confirm" )
@Api(value = "职称资格确认", description = "职称资格确认")
public class ClientQualificationConfirmController extends BaseController {

    @Autowired
    private IClientQualificationConfirmService iClientQualificationConfirmService;
    @Autowired
    private IClientRobotReportTagService clientRobotReportTagService;

    /**
     * 根据主键查询详情
     */
    @GetMapping(value = "/queryDetailById")
    @ApiOperation(value = "根据主键查询详情", notes = "根据主键查询详情")
    @ApiResponses(@ApiResponse(code = 200, message = "OK", response = ClientQualificationConfirmResponse.class))
    @ApiImplicitParams(@ApiImplicitParam(name = "clientId", value = "主键ID", required = false, dataType = "Long", paramType = "query"))
    public AjaxResult queryDetailById(Long clientId) {
        ClientQualificationConfirmResponse qualificationConfirmResponse = iClientQualificationConfirmService.queryDetailById(clientId);
        return AjaxResult.success(qualificationConfirmResponse);
    }

    /**
     * 新增新增
     */
    @PostMapping("/add")
    @ApiOperation(value = "新增职称资格确认", notes = "新增职称资格确认")
    public AjaxResult add(@Validated @RequestBody ClientQualificationConfirmRequest qualificationConfirmRequest) {
        int rows = iClientQualificationConfirmService.insertOrUpdate(qualificationConfirmRequest);
        clientRobotReportTagService.resetReportTag(qualificationConfirmRequest.getClientId(),"menu_msInfo4");//重置上报Tag
        return toAjax(rows);
    }

    /**
     * 编辑
     */
    @PostMapping("/edit")
    @ApiOperation(value = "编辑职称资格确认", notes = "编辑职称资格确认")
    public AjaxResult edit(@Validated @RequestBody ClientQualificationConfirmRequest qualificationConfirmRequest) {
        int rows = iClientQualificationConfirmService.insertOrUpdate(qualificationConfirmRequest);
        clientRobotReportTagService.resetReportTag(qualificationConfirmRequest.getClientId(),"menu_msInfo4");//重置上报Tag
        return toAjax(rows);
    }


}
