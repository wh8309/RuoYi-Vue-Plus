package com.ruoyi.web.controller.website;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.website.domain.vo.request.ClientSkillAwardRequest;
import com.ruoyi.website.domain.vo.response.ClientSkillAwardResponse;
import com.ruoyi.website.service.IClientRobotReportTagService;
import com.ruoyi.website.service.IClientSkillAwardService;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 任职期间奖励情况Controller
 *
 * @author ruoyi
 * @date 2021-03-24
 */
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/award" )
@Api(value = "任职期间奖励情况", description = "任职期间奖励情况")
public class ClientSkillAwardController extends BaseController {

    @Autowired
    private IClientSkillAwardService iClientSkillAwardService;
    @Autowired
    private IClientRobotReportTagService clientRobotReportTagService;

    /**
     * 根据主键查询详情
     */
    @GetMapping(value = "/queryDetailById")
    @ApiOperation(value = "根据主键查询详情", notes = "根据主键查询详情")
    @ApiResponses(@ApiResponse(code = 200, message = "OK", response = ClientSkillAwardResponse.class))
    @ApiImplicitParams(@ApiImplicitParam(name = "clientId", value = "主键ID", required = false, dataType = "Long", paramType = "query"))
    public AjaxResult queryDetailById(Long clientId) {
        ClientSkillAwardResponse skillAwardResponse = iClientSkillAwardService.queryDetailById(clientId);
        return AjaxResult.success(skillAwardResponse);
    }

    /**
     * 新增新增
     */
    @PostMapping("/add")
    @ApiOperation(value = "新增任职期间奖励情况", notes = "新增任职期间奖励情况")
    public AjaxResult add(@Validated @RequestBody ClientSkillAwardRequest clientSkillAwardRequest) {
        int rows = iClientSkillAwardService.insertOrUpdate(clientSkillAwardRequest);
        clientRobotReportTagService.resetReportTag(clientSkillAwardRequest.getClientId(),"menu_trainGuideTeacher");//重置上报Tag
        return toAjax(rows);
    }

    /**
     * 编辑
     */
    @PostMapping("/edit")
    @ApiOperation(value = "编辑任职期间奖励情况", notes = "编辑任职期间奖励情况")
    public AjaxResult edit(@Validated @RequestBody ClientSkillAwardRequest clientSkillAwardRequest) {
        int rows = iClientSkillAwardService.insertOrUpdate(clientSkillAwardRequest);
        clientRobotReportTagService.resetReportTag(clientSkillAwardRequest.getClientId(),"menu_trainGuideTeacher");//重置上报Tag
        return toAjax(rows);
    }

}
