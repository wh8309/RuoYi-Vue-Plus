package com.ruoyi.web.controller.website;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.website.domain.vo.request.ClientReportProcessSubmitRequest;
import com.ruoyi.website.domain.vo.response.ClientBaseInfoReportResponseVO;
import com.ruoyi.website.domain.vo.response.ClientReportProcessResponseVO;
import com.ruoyi.website.service.IClientReportProcessService;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 申报流程处理(流转意见)Controller
 * 
 * @author ruoyi
 * @date 2021-04-29
 */
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/tors/process" )
public class ClientReportProcessController extends BaseController {

    private final IClientReportProcessService clientReportProcessService;

    /**
     * 数据检查接口
     */
    @GetMapping(value = "/queryValidationResult")
    @ApiOperation(value = "数据校验接口", notes = "数据校验接口")
    @ApiResponses(@ApiResponse(code = 200, message = "OK", response = String.class))
    @ApiImplicitParams(@ApiImplicitParam(name = "clientId", value = "主键ID", dataType = "Long", paramType = "query"))
    public AjaxResult checkSubmitCondition(Long clientId) {
        List<String> validationResult = clientReportProcessService.checkSubmitCondition(clientId);
        return AjaxResult.success(validationResult);
    }

    /**
     * 查询基本信息数据接口
     */
    @GetMapping(value = "/queryClientBaseInfo")
    @ApiOperation(value = "根据主键查询基本详情", notes = "根据主键查询基本详情")
    @ApiResponses(@ApiResponse(code = 200, message = "OK", response = ClientBaseInfoReportResponseVO.class))
    @ApiImplicitParams(@ApiImplicitParam(name = "clientId", value = "主键ID", dataType = "Long", paramType = "query"))
    public AjaxResult queryClientBaseInfo(Long clientId) {
        ClientBaseInfoReportResponseVO responseVO = clientReportProcessService.queryBaseInfoByClientId(clientId);
        return AjaxResult.success(responseVO);
    }

    /**
     * 查询审核情况列表的数据接口
     */
    @GetMapping(value = "/queryReportProcess")
    @ApiOperation(value = "根据用户ClientId查询审核列表信息", notes = "根据用户ClientId查询审核列表信息")
    @ApiResponses(@ApiResponse(code = 200, message = "OK", response = ClientReportProcessResponseVO.class))
    @ApiImplicitParams(@ApiImplicitParam(name = "clientId", value = "主键ID", dataType = "Long", paramType = "query"))
    public AjaxResult queryReportProcess(Long clientId) {
        return AjaxResult.success(clientReportProcessService.queryListByClientId(clientId));
    }

    /**
     * 提交送审的接口
     */
    @PostMapping(value = "/submitFlow")
    @ApiOperation(value = "流程提交", notes = "流程提交")
    public AjaxResult submitFlow(@Validated @RequestBody ClientReportProcessSubmitRequest clientReportProcessSubmitRequest) {
        return AjaxResult.success(clientReportProcessService.processSubmit(clientReportProcessSubmitRequest));
    }

}
