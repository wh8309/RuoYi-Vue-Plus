package com.ruoyi.web.controller.website;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.website.domain.vo.request.ClientExchangeFacultyRequest;
import com.ruoyi.website.domain.vo.response.ClientExchangeFacultyResponse;
import com.ruoyi.website.service.IClientExchangeFacultyService;
import com.ruoyi.website.service.IClientRobotReportTagService;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 转换系列Controller
 *
 * @author ruoyi
 * @date 2021-03-24
 */
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/faculty" )
@Api(value = "转换系列", description = "转换系列")
public class ClientExchangeFacultyController extends BaseController {

    @Autowired
    private IClientExchangeFacultyService iClientExchangeFacultyService;
    @Autowired
    private IClientRobotReportTagService clientRobotReportTagService;

    /**
     * 根据主键查询详情
     */
    @GetMapping(value = "/queryDetailById")
    @ApiOperation(value = "根据主键查询详情", notes = "根据主键查询详情")
    @ApiImplicitParams(@ApiImplicitParam(name = "clientId", value = "主键ID", required = false, dataType = "Long", paramType = "query"))
    @ApiResponses(@ApiResponse(code = 200, message = "OK", response = ClientExchangeFacultyResponse.class))
    public AjaxResult queryDetailById(Long clientId) {
        ClientExchangeFacultyResponse exchangeFacultyResponse = iClientExchangeFacultyService.queryDetailById(clientId);
        return AjaxResult.success(exchangeFacultyResponse);
    }

    /**
     * 新增新增
     */
    @PostMapping("/add")
    @ApiOperation(value = "新增转换系列信息", notes = "新增转换系列信息")
    public AjaxResult add(@Validated @RequestBody ClientExchangeFacultyRequest exchangeFacultyRequest) {
        int rows = iClientExchangeFacultyService.insertOrUpdate(exchangeFacultyRequest);
        clientRobotReportTagService.resetReportTag(exchangeFacultyRequest.getClientId(),"menu_msInfo5");//重置上报Tag
        return toAjax(rows);
    }

    /**
     * 编辑
     */
    @PostMapping("/edit")
    @ApiOperation(value = "编辑转换系列信息", notes = "编辑转换系列信息")
    public AjaxResult edit(@Validated @RequestBody ClientExchangeFacultyRequest exchangeFacultyRequest) {
        int rows = iClientExchangeFacultyService.insertOrUpdate(exchangeFacultyRequest);
        clientRobotReportTagService.resetReportTag(exchangeFacultyRequest.getClientId(),"menu_msInfo5");//重置上报Tag
        return toAjax(rows);
    }

}
