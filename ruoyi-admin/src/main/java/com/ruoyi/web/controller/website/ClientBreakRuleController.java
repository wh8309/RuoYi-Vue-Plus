package com.ruoyi.web.controller.website;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.website.domain.vo.request.ClientBreakRuleRequest;
import com.ruoyi.website.domain.vo.response.ClientBreakRuleResponse;
import com.ruoyi.website.service.IClientBreakRuleService;
import com.ruoyi.website.service.IClientRobotReportTagService;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 破格条件说明Controller
 *
 * @author ruoyi
 * @date 2021-03-25
 */
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/breakRule")
@Api(value = "破格条件说明", description = "破格条件说明")

public class ClientBreakRuleController extends BaseController {

    @Autowired
    private IClientBreakRuleService iClientBreakRuleService;
    @Autowired
    private IClientRobotReportTagService clientRobotReportTagService;

    /**
     * 根据主键查询详情
     */
    @GetMapping(value = "/queryDetailById")
    @ApiOperation(value = "根据主键查询详情", notes = "根据主键查询详情")
    @ApiResponses(@ApiResponse(code = 200, message = "OK", response = ClientBreakRuleResponse.class))
    @ApiImplicitParams(@ApiImplicitParam(name = "clientId", value = "主键ID", required = false, dataType = "Long", paramType = "query"))
    public AjaxResult queryDetailById(Long clientId) {
        ClientBreakRuleResponse clientBreakRuleResponse = iClientBreakRuleService.queryDetailById(clientId);
        return AjaxResult.success(clientBreakRuleResponse);
    }

    /**
     * 新增新增
     */
    @PostMapping("/add")
    @ApiOperation(value = "新增破格条件说明", notes = "新增破格条件说明")
    public AjaxResult add(@Validated @RequestBody ClientBreakRuleRequest clientBreakRuleRequest) {
        int rows = iClientBreakRuleService.insertOrUpdate(clientBreakRuleRequest);
        clientRobotReportTagService.resetReportTag(clientBreakRuleRequest.getClientId(),"menu_pgInfo");//重置上报Tag
        return toAjax(rows);
    }

    /**
     * 编辑
     */
    @PostMapping("/edit")
    @ApiOperation(value = "编辑破格条件说明", notes = "编辑破格条件说明")
    public AjaxResult edit(@Validated @RequestBody ClientBreakRuleRequest clientBreakRuleRequest) {
        int rows = iClientBreakRuleService.insertOrUpdate(clientBreakRuleRequest);
        clientRobotReportTagService.resetReportTag(clientBreakRuleRequest.getClientId(),"menu_pgInfo");//重置上报Tag
        return toAjax(rows);
    }


}
