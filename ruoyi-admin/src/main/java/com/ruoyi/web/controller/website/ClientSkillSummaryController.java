package com.ruoyi.web.controller.website;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.website.domain.vo.request.ClientSkillSummaryRequest;
import com.ruoyi.website.domain.vo.response.ClientSkillAwardResponse;
import com.ruoyi.website.domain.vo.response.ClientSkillSummaryResponse;
import com.ruoyi.website.service.IClientRobotReportTagService;
import com.ruoyi.website.service.IClientSkillSummaryService;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 任期内专业技术业绩与成果报告Controller
 *
 * @author ruoyi
 * @date 2021-03-24
 */
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/summary")
@Api(value = "任期内专业技术业绩与成果报告", description = "任期内专业技术业绩与成果报告")
public class ClientSkillSummaryController extends BaseController {

    @Autowired
    private IClientSkillSummaryService iClientSkillSummaryService;
    @Autowired
    private IClientRobotReportTagService clientRobotReportTagService;

    /**
     * 根据主键查询详情
     */
    @GetMapping(value = "/queryDetailById")
    @ApiOperation(value = "根据主键查询详情", notes = "根据主键查询详情")
    @ApiResponses(@ApiResponse(code = 200, message = "OK", response = ClientSkillSummaryResponse.class))
    @ApiImplicitParams(@ApiImplicitParam(name = "clientId", value = "主键ID", required = false, dataType = "Long", paramType = "query"))
    public AjaxResult queryDetailById(Long clientId) {
        ClientSkillSummaryResponse clientSkillSummaryResponse = iClientSkillSummaryService.queryDetailById(clientId);
        return AjaxResult.success(clientSkillSummaryResponse);
    }

    /**
     * 新增新增
     */
    @PostMapping("/add")
    @ApiOperation(value = "新增任期内专业技术业绩与成果报告", notes = "新增任期内专业技术业绩与成果报告")
    public AjaxResult add(@Validated @RequestBody ClientSkillSummaryRequest clientSkillSummaryRequest) {
        int rows = iClientSkillSummaryService.insertOrUpdate(clientSkillSummaryRequest);
        clientRobotReportTagService.resetReportTag(clientSkillSummaryRequest.getClientId(),"menu_workSummary");//重置上报Tag
        return toAjax(rows);
    }

    /**
     * 编辑
     */
    @PostMapping("/edit")
    @ApiOperation(value = "编辑任期内专业技术业绩与成果报告", notes = "编辑任期内专业技术业绩与成果报告")
    public AjaxResult edit(@Validated @RequestBody ClientSkillSummaryRequest clientSkillSummaryRequest) {
        int rows = iClientSkillSummaryService.insertOrUpdate(clientSkillSummaryRequest);
        clientRobotReportTagService.resetReportTag(clientSkillSummaryRequest.getClientId(),"menu_workSummary");//重置上报Tag
        return toAjax(rows);
    }


}
