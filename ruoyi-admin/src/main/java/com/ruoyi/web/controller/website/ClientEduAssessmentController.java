package com.ruoyi.web.controller.website;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.website.domain.vo.request.ClientEduAssessmentRequestVO;
import com.ruoyi.website.domain.vo.response.ClientEduAssessmentResponseVO;
import com.ruoyi.website.domain.vo.response.ClientEducationResponseVO;
import com.ruoyi.website.service.IClientEduAssessmentService;
import com.ruoyi.website.service.IClientRobotReportTagService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 年度考核与继续教育Controller
 *
 * @author ruoyi
 * @date 2021-03-22
 */
@RestController
@RequestMapping("/assessment")
@Api(value = "年度考核与继续教育", description = "年度考核与继续教育")
public class ClientEduAssessmentController extends BaseController {

    @Autowired
    private IClientEduAssessmentService iClientEduAssessmentService;
    @Autowired
    private IClientRobotReportTagService clientRobotReportTagService;

    /**
     * 根据主键查询详情
     */

    @GetMapping(value = "/queryDetailById")
    @ApiOperation(value = "根据主键查询详情", notes = "根据主键查询详情")
    @ApiImplicitParams(@ApiImplicitParam(name = "clientId", value = "主键ID", required = false, dataType = "Long", paramType = "query"))
    @ApiResponses(@ApiResponse(code = 200, message = "OK", response = ClientEduAssessmentResponseVO.class))
    public AjaxResult queryDetailById( Long clientId) {
        ClientEduAssessmentResponseVO clientEduAssessmentResponse = iClientEduAssessmentService.queryDetailById(clientId);
        return AjaxResult.success(clientEduAssessmentResponse);
    }

    /**
     * 新增新增
     */
    @PostMapping("/add")
    @ApiOperation(value = "新增年度考核与继续教育信息", notes = "新增年度考核与继续教育信息")
    public AjaxResult add(@Validated @RequestBody ClientEduAssessmentRequestVO clientEducationRequest) {
        int rows = iClientEduAssessmentService.insertOrUpdate(clientEducationRequest);
        clientRobotReportTagService.resetReportTag(clientEducationRequest.getClientId(),"menu_latestCheck");//重置上报Tag
        return toAjax(rows);
    }

    /**
     * 编辑
     */
    @PostMapping("/edit")
    @ApiOperation(value = "编辑年度考核与继续教育信息", notes = "编辑年度考核与继续教育信息")
    public AjaxResult edit(@Validated @RequestBody ClientEduAssessmentRequestVO clientEducationRequest) {
        int rows = iClientEduAssessmentService.insertOrUpdate(clientEducationRequest);
        clientRobotReportTagService.resetReportTag(clientEducationRequest.getClientId(),"menu_latestCheck");//重置上报Tag
        return toAjax(rows);
    }


}
