package com.ruoyi.web.controller.website;

import java.util.List;
import java.util.Arrays;

import com.ruoyi.website.domain.SysFileInfo;
import com.ruoyi.website.service.ISysFileInfoService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;

import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 文件信息Controller
 *
 * @author ruoyi
 * @date 2021-03-18
 */
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/api/sysFileInfo" )
public class SysFileInfoController extends BaseController {

    private final ISysFileInfoService iSysFileInfoService;

    /**
     * 获取文件信息详细信息
     */
    @GetMapping(value = "/{fileId}" )
    public AjaxResult getInfo(@PathVariable("fileId" ) String fileId) {
        return AjaxResult.success(iSysFileInfoService.getById(fileId));
    }

    /**
     * 新增文件信息
     */
    @PreAuthorize("@ss.hasPermi('system:info:add')" )
    @Log(title = "文件信息" , businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysFileInfo sysFileInfo) {
        return toAjax(iSysFileInfoService.save(sysFileInfo) ? 1 : 0);
    }

    /**
     * 修改文件信息
     */
    @PreAuthorize("@ss.hasPermi('system:info:edit')" )
    @Log(title = "文件信息" , businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysFileInfo sysFileInfo) {
        return toAjax(iSysFileInfoService.updateById(sysFileInfo) ? 1 : 0);
    }

    /**
     * 删除文件信息
     */
    @PreAuthorize("@ss.hasPermi('system:info:remove')" )
    @Log(title = "文件信息" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{fileIds}" )
    public AjaxResult remove(@PathVariable String[] fileIds) {
        return toAjax(iSysFileInfoService.removeByIds(Arrays.asList(fileIds)) ? 1 : 0);
    }
}
