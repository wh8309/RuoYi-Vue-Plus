package com.ruoyi.web.controller.website;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.DropDownBoxUnit;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.website.domain.TorsWorkUnit;
import com.ruoyi.website.domain.vo.request.*;
import com.ruoyi.website.domain.vo.response.TorsWorkUnitExcelImportResponseVO;
import com.ruoyi.website.domain.vo.response.TorsWorkUnitResponseVO;
import com.ruoyi.website.service.ITorsWorkUnitService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 工作单位Controller
 *
 * @author ruoyi
 * @date 2021-05-14
 */
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/tors/workUnit")
@Api(value = "工作单位维护", description = "工作单位维护")
public class TorsWorkUnitController extends BaseController {

    private final ITorsWorkUnitService torsWorkUnitService;

    private final TokenService tokenService;

    @GetMapping("/list")
    //    @PreAuthorize("@ss.hasPermi('tors:workUnit:list')" )
    @ApiOperation(value = "条件分页查询工作单位信息", notes = "条件分页查询工作单位信息")
    public TableDataInfo list(TorsWorkUnitQueryRequestVo torsWorkUnitQueryRequestVo) {
        TorsWorkUnit torsWorkUnit = new TorsWorkUnit();
        BeanUtils.copyProperties(torsWorkUnitQueryRequestVo, torsWorkUnit);
        startPage();
        List<TorsWorkUnitResponseVO> list = torsWorkUnitService.selectWorkUnitByPage(torsWorkUnit);
        return getDataTable(list);
    }

    @ApiOperation(value = "工作单位新增", notes = "工作单位新增")
    //    @PreAuthorize("@ss.hasPermi('tors:workUnit:add')" )
    @Log(title = "工作单位", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@Validated @RequestBody TorsWorkUnitPostRequestVo torsWorkUnitPostRequestVo) {
        TorsWorkUnit torsWorkUnit = new TorsWorkUnit();
        BeanUtils.copyProperties(torsWorkUnitPostRequestVo, torsWorkUnit);
        return toAjax(torsWorkUnitService.saveWorkUnit(torsWorkUnit) ? 1 : 0);
    }

    @ApiOperation(value = "工作单位修改", notes = "工作单位修改")
    //    @PreAuthorize("@ss.hasPermi('tors:workUnit:edit')" )
    @Log(title = "工作单位", businessType = BusinessType.UPDATE)
    @PutMapping("/edit")
    public AjaxResult edit(@Validated @RequestBody TorsWorkUnitPutRequestVo torsWorkUnitPutRequestVo) {
        TorsWorkUnit torsWorkUnit = new TorsWorkUnit();
        BeanUtils.copyProperties(torsWorkUnitPutRequestVo, torsWorkUnit);
        return toAjax(torsWorkUnitService.updateWorkUnit(torsWorkUnit) ? 1 : 0);
    }

    @ApiOperation(value = "工作单位删除", notes = "工作单位删除")
    //    @PreAuthorize("@ss.hasPermi('tors:workUnit:remove')" )
    @Log(title = "工作单位", businessType = BusinessType.DELETE)
    @DeleteMapping("/{workUnitIds}")
    public AjaxResult remove(@PathVariable Long[] workUnitIds) {
        return toAjax(torsWorkUnitService.deleteTorsWorkUnitByIds(workUnitIds) ? 1 : 0);
    }

    /**
     * 获取工作位详细信息
     */
    @ApiOperation(value = "获取工作单位详细信息", notes = "获取工作单位详细信息")
    //    @PreAuthorize("@ss.hasPermi('tors:workUnit:query')" )
    @GetMapping(value = "/{workUnitId}")
    public AjaxResult getInfo(@PathVariable("workUnitId") Long workUnitId) {
        return AjaxResult.success(torsWorkUnitService.getById(workUnitId));
    }


    /**
     * 客户信息导出
     */
    @Log(title = "工作单位导出", businessType = BusinessType.EXPORT)
    @GetMapping("/exportData")
//    @PreAuthorize("@ss.hasPermi('tors:workUnit:export')")
    @ApiOperation(value = "工作单位导出", notes = "工作单位导出")
    public AjaxResult exportExcel(TorsWorkUnitQueryRequestVo requestVO) {
        TorsWorkUnit workUnit = new TorsWorkUnit();
        BeanUtils.copyProperties(requestVO, workUnit);
        List<TorsWorkUnitExcelImportResponseVO> list = torsWorkUnitService.exportListByParam(workUnit);
        ExcelUtil<TorsWorkUnitExcelImportResponseVO> util = new ExcelUtil<>(TorsWorkUnitExcelImportResponseVO.class);
        return util.exportExcel(list, "工作单位数据");
    }

    /**
     * 客户信息导入模板
     *
     * @return
     */
    @GetMapping("/importTemplate")
    @ApiOperation(value = "工作单位数据导入模板下载", notes = "工作单位数据导入模板下载")
    public AjaxResult importTemplate() {
        ExcelUtil<TorsWorkUnitExcelImportResponseVO> util = new ExcelUtil<>(TorsWorkUnitExcelImportResponseVO.class);
        return util.importTemplateExcel("工作单位导入模板");
    }

    /**
     * 客户信息导入
     */
    @Log(title = "工作单位导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    @ApiOperation(value = "工作单位数据导入", notes = "工作单位数据导入")
//    @PreAuthorize("@ss.hasPermi('tors:workUnit:import')")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<TorsWorkUnitExcelImportResponseVO> util = new ExcelUtil<>(TorsWorkUnitExcelImportResponseVO.class);
        List<TorsWorkUnitExcelImportResponseVO> excelData = util.importExcel(file.getInputStream());
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String operName = loginUser.getUsername();
        String message = torsWorkUnitService.importWorkUnit(excelData, updateSupport, operName);
        return AjaxResult.success(message);
    }
}
