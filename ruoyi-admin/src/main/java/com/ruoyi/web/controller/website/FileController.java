package com.ruoyi.web.controller.website;

import com.alibaba.fastjson.JSON;
import com.aliyun.oss.OSSException;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.exception.FileUploadException;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.file.FileUtils;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.cloud.CloudConstant;
import com.ruoyi.system.cloud.CloudStorageConfig;
import com.ruoyi.system.service.ISysConfigService;
import com.ruoyi.website.domain.SysFileInfo;
import com.ruoyi.website.domain.vo.request.FileEditRemarkRequestVO;
import com.ruoyi.website.domain.vo.request.FileEditRequestVO;
import com.ruoyi.website.domain.vo.request.FileQueryRequestVO;
import com.ruoyi.website.domain.vo.request.FileUploadRequestVO;
import com.ruoyi.website.domain.vo.response.FileResponseVO;
import com.ruoyi.website.service.IClientRobotReportTagService;
import com.ruoyi.website.service.ISysFileInfoService;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@Api(value = "文件上传,下载,预览", description = "文件上传,下载,预览")
@RequestMapping("/file")
public class FileController extends BaseController {
    @Autowired
    private TokenService tokenService;
    @Autowired
    private ISysFileInfoService fileInfoService;
    @Autowired
    private ISysConfigService sysConfigService;
    @Autowired
    private IClientRobotReportTagService clientRobotReportTagService;

    @PostMapping("/upload")
    @ApiOperation(value="文件上传接口",notes="文件上传接口")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType="__file", name = "file",value = "文件", required = true),
    })
    @ApiResponses({
            @ApiResponse(code = 200,message = "OK",response = FileResponseVO.class),
    })
    public AjaxResult fileUpload(FileUploadRequestVO fileUploadRequestVO)throws IOException {
        clientRobotReportTagService.resetReportTag(fileUploadRequestVO.getClientId(),fileUploadRequestVO.getFileType());//重置上报tag
        if (fileUploadRequestVO.getFile().isEmpty())
        {
            throw new FileUploadException("上传文件不能为空");
        }else{
            LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
            Long userId=loginUser.getUser().getUserId();
            if(fileUploadRequestVO.getClientId()!=null){
                userId=fileUploadRequestVO.getClientId();
            }
             //上传到服务器本地
            Map<String,String> fileMap=FileUploadUtils.uploadReport(RuoYiConfig.getReportPath(),String.valueOf(userId),fileUploadRequestVO.getFileType(),fileUploadRequestVO.getFile());
            SysFileInfo sysFileInfo = new SysFileInfo();
            //异步上传到云存储
            String isUploadCloud = sysConfigService.selectConfigByKey("sys.file.is.upload.cloud");
            if("1".equals(isUploadCloud)){//0默认不上传 1上传
                //异步上传云存储
                fileInfoService.uploadFileToCloud(fileMap.get("diskRelativePath"),fileMap.get("diskStoreFullPath"));
                String jsonconfig = sysConfigService.selectConfigByKey(CloudConstant.CLOUD_STORAGE_CONFIG_KEY);
                sysFileInfo.setCloudStorageType( JSON.parseObject(jsonconfig, CloudStorageConfig.class).getType());
                sysFileInfo.setCloudStoragePath(fileMap.get("diskRelativePath"));
            }
            sysFileInfo=fileInfoService.insertFile(fileMap,fileUploadRequestVO,sysFileInfo);
            FileResponseVO fileResponseVo=new FileResponseVO(sysFileInfo.getFileId(), sysFileInfo.getFilePath(), sysFileInfo.getOriginalFileName(), sysFileInfo.getFileType(),sysFileInfo.getRemark());
            AjaxResult ajax = AjaxResult.success(fileResponseVo);
            return ajax;
        }
    }

    @PostMapping("/edit")
    @ApiOperation(value="文件编辑",notes="根据文件主键重写上传文件或者修改备注", response = AjaxResult.class)
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType="__file", name = "file",value = "文件"),
    })
    public AjaxResult fileEdit(FileEditRequestVO fileEditRequestVO)throws IOException {
        SysFileInfo originalFileInfo=fileInfoService.getById(fileEditRequestVO.getFileId());
        if(originalFileInfo==null){
            return AjaxResult.error("编辑文件不存在");
        }
        clientRobotReportTagService.resetReportTag(originalFileInfo.getClientId(),originalFileInfo.getFileType());//重置上报tag
        if (fileEditRequestVO.getFile()!=null)//重新上传文件
        {
            Map<String,String> fileMap=FileUploadUtils.uploadReport(RuoYiConfig.getReportPath(),String.valueOf(originalFileInfo.getClientId()),originalFileInfo.getFileType(),fileEditRequestVO.getFile());
            //异步上传到云存储
            String isUploadCloud = sysConfigService.selectConfigByKey("sys.file.is.upload.cloud");
            if("1".equals(isUploadCloud)){//0默认不上传 1上传
                //同步上传云存储
                fileInfoService.uploadFileToCloud(fileMap.get("diskRelativePath"),fileMap.get("diskStoreFullPath"));
                String jsonconfig = sysConfigService.selectConfigByKey(CloudConstant.CLOUD_STORAGE_CONFIG_KEY);
                originalFileInfo.setCloudStorageType( JSON.parseObject(jsonconfig, CloudStorageConfig.class).getType());
                originalFileInfo.setCloudStoragePath(fileMap.get("diskRelativePath"));
            }
            originalFileInfo.setRemark(fileEditRequestVO.getRemark());
            SysFileInfo sysFileInfo=fileInfoService.updateFile(fileMap,originalFileInfo);
            FileResponseVO fileResponseVo=new FileResponseVO(sysFileInfo.getFileId(), sysFileInfo.getFilePath(), sysFileInfo.getOriginalFileName(), sysFileInfo.getFileType(),sysFileInfo.getRemark());
            AjaxResult ajax = AjaxResult.success(fileResponseVo);
            return ajax;
        }else if(StringUtils.isNotBlank(fileEditRequestVO.getRemark())){//修改备注
            originalFileInfo.setRemark(fileEditRequestVO.getRemark());
            originalFileInfo.setUpdateBy(SecurityUtils.getUsername());
            fileInfoService.updateById(originalFileInfo);
            FileResponseVO fileResponseVo=new FileResponseVO(originalFileInfo.getFileId(), originalFileInfo.getFilePath(), originalFileInfo.getOriginalFileName(), originalFileInfo.getFileType(),originalFileInfo.getRemark());
            AjaxResult ajax = AjaxResult.success(fileResponseVo);
            return ajax;
        }
        return AjaxResult.error("上传图片异常，请联系管理员");
    }

    @PostMapping("/editRemark")
    @ApiOperation(value="文件编辑备注",notes="根据文件主键修改备注", response = AjaxResult.class)
    public AjaxResult fileEditRemark(FileEditRemarkRequestVO fileEditRemarkRequestVO)throws IOException {
        SysFileInfo originalFileInfo=fileInfoService.getById(fileEditRemarkRequestVO.getFileId());
        if(originalFileInfo==null){
            return AjaxResult.error("编辑文件不存在");
        }
        clientRobotReportTagService.resetReportTag(originalFileInfo.getClientId(),originalFileInfo.getFileType());//重置上报tag
        originalFileInfo.setRemark(fileEditRemarkRequestVO.getRemark());
        originalFileInfo.setUpdateBy(SecurityUtils.getUsername());
        fileInfoService.updateById(originalFileInfo);
        FileResponseVO fileResponseVo=new FileResponseVO(originalFileInfo.getFileId(), originalFileInfo.getFilePath(), originalFileInfo.getOriginalFileName(), originalFileInfo.getFileType(),originalFileInfo.getRemark());
        AjaxResult ajax = AjaxResult.success(fileResponseVo);
        return ajax;

    }

    @GetMapping("/list")
    @ApiOperation(value="根据文件类型查询列表数据",notes="根据文件类型查询列表数据,如果传：ALLCardImgs,返回全部证件电子图片")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "fileType", value = "文件类型", example = "", paramType = "query", dataType = "string", required = true)
    })
    public TableDataInfo list(FileQueryRequestVO fileQueryRequestVO)
    {
        List<SysFileInfo> list = fileInfoService.selectFileList(fileQueryRequestVO);
        existDistFile(list);
        return getDataTable(list);
    }

    private void existDistFile(List<SysFileInfo> fileInfoList){
        for(SysFileInfo fileInfo:fileInfoList){
            String localFilePath= RuoYiConfig.getProfile()+fileInfo.getFilePath().replaceAll(Constants.RESOURCE_PREFIX,"");
            //判定本地文件是否存在，如果不存在则从云端下载
            File localFile=new File(localFilePath);
            if(!localFile.exists()){
                fileInfo.setFilePath(fileInfoService.getCloudVisitUrl(fileInfo.getCloudStoragePath()));
            }
        }
    }

    @GetMapping("/download")
    @ApiOperation(value="文件下载接口",notes="文件下载接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "fileId", value = "文件ID", example = "", paramType = "query", dataType = "string", required = true)
    })
    public void fileDownload(String fileId, HttpServletResponse response) throws Exception {
        SysFileInfo sysFileInfo = fileInfoService.getById(fileId);
        try
        {
            String resource=sysFileInfo.getFilePath();
            if (!FileUtils.checkAllowDownload(resource))
            {
                throw new Exception(StringUtils.format("资源文件({})非法，不允许下载。 ", resource));
            }
            // 本地资源路径
            String localPath = RuoYiConfig.getProfile();
            // 数据库资源地址
            String downloadPath = localPath + StringUtils.substringAfter(resource, Constants.RESOURCE_PREFIX);
            // 下载名称
            String downloadName = StringUtils.substringAfterLast(downloadPath, "/");
            response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
            FileUtils.setAttachmentResponseHeader(response, sysFileInfo.getFileName());
            FileUtils.writeBytes(downloadPath, response.getOutputStream());
        }
        catch (Exception e)
        {
            log.error("下载文件失败", e);
        }
    }

    @DeleteMapping(value = "/delete")
    @ApiOperation(value="文件删除",notes="根据文件id删除文件")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "fileId", value = "文件ID", example = "", paramType = "query", dataType = "string", required = true)
    })
    public AjaxResult delete(String fileId) {
        SysFileInfo originalFileInfo=fileInfoService.getById(fileId);
        if(originalFileInfo!=null){
            clientRobotReportTagService.resetReportTag(originalFileInfo.getClientId(),originalFileInfo.getFileType());//重置上报tag
        }
        try {
            LoginUser loginUser = SecurityUtils.getLoginUser();
            SysUser sysUser = loginUser.getUser();
            int rows = fileInfoService.deleteFileById(fileId);
            return toAjax(rows);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error(e.getMessage());
        }
    }












}


