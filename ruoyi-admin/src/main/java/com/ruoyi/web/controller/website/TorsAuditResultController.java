package com.ruoyi.web.controller.website;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.website.bo.TorsAuditResultQueryBo;
import com.ruoyi.website.service.ITorsAuditResultService;
import com.ruoyi.website.vo.TorsAuditResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * 审核未通过Controller
 * 
 * @author admin
 * @date 2021-07-07
 */
@Api(value = "资料补充控制器", tags = {"资料补充管理"})
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/tors/auditResult")
public class TorsAuditResultController extends BaseController {

    private final ITorsAuditResultService iTorsAuditResultService;

    /**
     * 查询审核未通过列表
     */
    @ApiOperation("查询资料补充列表")
    @GetMapping("/list")
    public TableDataInfo list(TorsAuditResultQueryBo bo) {
        startPage();
        List<TorsAuditResultVo> list = iTorsAuditResultService.queryList(bo);
        return getDataTable(list);
    }

    @ApiOperation(value = "推荐设置已处理", notes = "推荐设置已处理")
    @PostMapping("/batchSetDealStatus/{auditResultIds}")
    public AjaxResult batchSetDealStatus(@PathVariable Long[] auditResultIds) {
        return toAjax(iTorsAuditResultService.updateDealStatusByIds(Arrays.asList(auditResultIds)) ? 1 : 0);
    }

    /**
     * 导出审核未通过列表
     */
    @ApiOperation("导出资料补充列表")
    @GetMapping("/export")
    public AjaxResult export(TorsAuditResultQueryBo bo) {
        List<TorsAuditResultVo> list = iTorsAuditResultService.queryList(bo);
        ExcelUtil<TorsAuditResultVo> util = new ExcelUtil<TorsAuditResultVo>(TorsAuditResultVo.class);
        return util.exportExcel(list, "审核情况");
    }




}
