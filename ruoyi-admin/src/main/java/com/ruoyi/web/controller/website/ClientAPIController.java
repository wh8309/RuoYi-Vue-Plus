package com.ruoyi.web.controller.website;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginBody;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.ip.IpUtils;
import com.ruoyi.framework.web.service.SysLoginService;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.website.service.IClientProfileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import org.hibernate.validator.constraints.Range;
import org.hibernate.validator.constraints.URL;

@Slf4j
@RestController
@RequestMapping("/api")
@Api(value = "职称网上申报网站", description = "职称网上申报网站")
public class ClientAPIController {

    @Autowired
    private SysLoginService loginService;
    @Autowired
    private TokenService tokenService;
    @Autowired
    IClientProfileService clientProfileService;

    /**
     * 登录方法
     *
     * @param loginBody 登录信息
     * @return 结果
     */
    @PostMapping("/login")
    public AjaxResult login(@RequestBody LoginBody loginBody, HttpServletRequest request) {
        AjaxResult ajax = AjaxResult.success();
        LoginUser loginUser = loginService.login(loginBody.getUsername(), loginBody.getPassword(), loginBody.getCode(), loginBody.getUuid(), 2);
        // 生成令牌.
        String token = tokenService.createToken(loginUser);
        //更新用户登录的时间和登录IP
        String ipAddr = IpUtils.getIpAddr(request);
        clientProfileService.updateLogin(loginBody.getUsername(), ipAddr);
        SysUser sysUser = loginUser.getUser();
        ajax.put(Constants.TOKEN, token);
        ajax.put("loginType", "2");
        ajax.put("isEdit", sysUser.getIsEdit());//1可以编辑 0 不可以编辑
        //登录完成后创建基本信息表数据
        log.info("用户登录成功！");
        log.info("token：  {}", token);
        return ajax;
    }

    /**
     * 重置密码
     */
    @Log(title = "客户密码修改接口", businessType = BusinessType.UPDATE)
    @PutMapping("/updatePwd")
    @ApiOperation(value = "客户密码修改接口", notes = "客户密码修改接口")
    public AjaxResult updatePwd(String oldPassword, String newPassword) {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String userName = loginUser.getUser().getLoginName();
        String password = loginUser.getPassword();
        if (StringUtils.isEmpty(userName)) {
            return AjaxResult.error("为获取到用户登录名，请联系管理员");
        }
        if (!SecurityUtils.matchesPassword(oldPassword, password)) {
            return AjaxResult.error("修改密码失败，旧密码错误");
        }
        if (SecurityUtils.matchesPassword(newPassword, password)) {
            return AjaxResult.error("新密码不能与旧密码相同");
        }
        if (clientProfileService.resetUserPwd(userName, SecurityUtils.encryptPassword(newPassword)) > 0) {
            // 更新缓存用户密码
            loginUser.getUser().setPassword(SecurityUtils.encryptPassword(newPassword));
            tokenService.setLoginUser(loginUser);
            return AjaxResult.success();
        }
        return AjaxResult.error("修改密码异常，请联系管理员");
    }

}
