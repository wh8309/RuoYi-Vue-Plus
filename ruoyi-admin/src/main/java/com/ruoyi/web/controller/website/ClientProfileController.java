package com.ruoyi.web.controller.website;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.idcard.IdCardUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.website.domain.ClientProfile;
import com.ruoyi.website.domain.vo.request.*;
import com.ruoyi.website.domain.vo.response.ClientProfileExcelExportResponseVO;
import com.ruoyi.website.domain.vo.response.ClientProfileExcelImportResponseVO;
import com.ruoyi.website.domain.vo.response.ClientProfileResponseVO;
import com.ruoyi.website.service.IClientBaseInfoService;
import com.ruoyi.website.service.IClientProfileService;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * 用户信息Controller
 *
 * @author ruoyi
 * @date 2021-04-01
 */
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/tors/clientProfile")
@Api(value = "客户信息操作", description = "客户信息操作")
public class ClientProfileController extends BaseController {

    private final IClientProfileService clientProfileService;

    private final TokenService tokenService;
    /**
     * ClientProfileResponseVO
     * 用户列表分页查询
     */
    @PreAuthorize("@ss.hasPermi('tors:clientProfile:list')")
    @GetMapping("/queryPage")
    @ApiOperation(value = "条件分页查询用户信息", notes = "条件分页查询用户信息")
    public TableDataInfo queryPage(ClientProfilePageRequestVO pageRequestVO) {
        ClientProfile clientProfile=new ClientProfile();
        BeanUtils.copyProperties(pageRequestVO,clientProfile);
        startPage();
        List<ClientProfileResponseVO> list = clientProfileService.queryListByParam(clientProfile);
        return getDataTable(list);
    }

    /**
     * 删除用户
     */
    @ApiOperation(value = "客户批量删除", notes = "客户批量删除接口")
    @PreAuthorize("@ss.hasPermi('tors:clientProfile:remove')")
    @Log(title = "客户管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{clientIds}")
    public AjaxResult remove(@PathVariable Long[] clientIds) {
        return toAjax(clientProfileService.deleteClientProfileByClientIds(clientIds));
    }



    /**
     * 根据主键查询详情
     */
    @PreAuthorize("@ss.hasPermi('tors:clientProfile:query')")
    @GetMapping(value = {"/queryDetailById", "/{clientId}"})
    @ApiOperation(value = "根据主键查询详情", notes = "根据主键查询详情")
    @ApiResponses(@ApiResponse(code = 200, message = "OK", response = ClientProfileResponseVO.class))
    public AjaxResult queryDetailById(Long clientId) {
        ClientProfileResponseVO referralTypeResponse = clientProfileService.queryDetailById(clientId);
        return AjaxResult.success(referralTypeResponse);
    }

    /**
     * 新增客户
     */
    @PreAuthorize("@ss.hasPermi('tors:clientProfile:add')")
    @Log(title = "客户管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ApiOperation(value = "客户新增", notes = "客户新增")
    public AjaxResult add(@Validated @RequestBody ClientProfileAddRequestVO clientProfileRequestVO) {
        ClientProfile clientProfile = new ClientProfile();
        BeanUtils.copyProperties(clientProfileRequestVO, clientProfile);
        if (StringUtils.isNotEmpty(clientProfile.getMobile())
                && UserConstants.NOT_UNIQUE.equals(clientProfileService.checkMobileUnique(clientProfile))) {
            return AjaxResult.error("新增客户'" + clientProfile.getName() + "'失败，手机号码已存在");
        } else if (!IdCardUtils.isIDCard(clientProfile.getIdcard())) {
            return AjaxResult.error("新增客户'" + clientProfile.getName() + "'失败，身份证号不正确");
        } else if (StringUtils.isNotEmpty(clientProfile.getIdcard())
                && UserConstants.NOT_UNIQUE.equals(clientProfileService.checkIdcardUnique(clientProfile))) {
            return AjaxResult.error("新增客户'" + clientProfile.getName() + "'失败，身份证号已存在");
        }
        Map<String, String> idCardMap = IdCardUtils.getBirthdayAgeSex(clientProfile.getIdcard());
        if (!clientProfile.getSex().equals(idCardMap.get("sex"))) {
            return AjaxResult.error("新增客户'" + clientProfile.getName() + "'失败，身份证号上性别和实际选择的性别不匹配");
        }
        return toAjax(clientProfileService.insertOrUpdate(clientProfile));
    }

    /**
     * 编辑
     */
    @ApiOperation(value = "编辑用户信息", notes = "编辑用户信息")
    @PreAuthorize("@ss.hasPermi('tors:clientProfile:edit')")
    @Log(title = "客户信息修改", businessType = BusinessType.UPDATE)
    @PutMapping("/edit")
    public AjaxResult edit(@Validated @RequestBody ClientProfileRequestVO clientProfileRequestVO) {
        ClientProfile clientProfile = new ClientProfile();
        BeanUtils.copyProperties(clientProfileRequestVO, clientProfile);
        clientProfileService.checkUserAllowed(clientProfile);
        if (StringUtils.isNotEmpty(clientProfile.getMobile())
                && UserConstants.NOT_UNIQUE.equals(clientProfileService.checkMobileUniqueContainsThat(clientProfile))) {
            return AjaxResult.error("修改客户'" + clientProfile.getName() + "'失败，手机号码已存在");
        } else if (!IdCardUtils.isIDCard(clientProfile.getIdcard())) {
            return AjaxResult.error("修改客户'" + clientProfile.getName() + "'失败，身份证号不正确");
        } else if (StringUtils.isNotEmpty(clientProfile.getEmail())
                && UserConstants.NOT_UNIQUE.equals(clientProfileService.checkIdcardUniqueUnContainsThat(clientProfile))) {
            return AjaxResult.error("修改客户'" + clientProfile.getName() + "'失败，身份证号已存在");
        }
        clientProfile.setUpdateBy(SecurityUtils.getUsername());
        Map<String, String> idCardMap = IdCardUtils.getBirthdayAgeSex(clientProfile.getIdcard());
        if (!clientProfile.getSex().equals(idCardMap.get("sex"))) {
            return AjaxResult.error("新增客户'" + clientProfile.getName() + "'失败，身份证号上性别和实际选择的性别不匹配");
        }
        return toAjax(clientProfileService.insertOrUpdate(clientProfileRequestVO));
    }

    /**
     * 重置密码
     */
    @PreAuthorize("@ss.hasPermi('tors:clientProfile:resetPwd')")
    @Log(title = "重置客户密码", businessType = BusinessType.UPDATE)
    @PutMapping("/resetPwd")
    @ApiOperation(value = "重置客户密码", notes = "重置客户密码")
    @ApiImplicitParams(@ApiImplicitParam(name = "clientId", value = "主键ID", dataType = "Long", paramType = "query"))
    public AjaxResult resetPwd(Long clientId) {
        return toAjax(clientProfileService.resetPwd(clientId));
    }


    /**
     * 状态修改
     */
    @PreAuthorize("@ss.hasPermi('tors:clientProfile:edit')")
    @Log(title = "用户管理", businessType = BusinessType.UPDATE)
    @ApiOperation(value = "修改客户状态", notes = "修改客户状态")
    @PutMapping("/changeStatus")
    public AjaxResult changeStatus(@Validated @RequestBody ClientProfileStatusUpdateRequestVO clientProfileStatusUpdateRequestVO) {
        return toAjax(clientProfileService.updateUserStatus(clientProfileStatusUpdateRequestVO));
    }

    /**
     * 客户信息导出
     */
    @Log(title = "用户管理", businessType = BusinessType.EXPORT)
    @GetMapping("/exportData")
    @PreAuthorize("@ss.hasPermi('tors:clientProfile:export')")
    @ApiOperation(value = "客户信息导出", notes = "客户信息导出")
    public AjaxResult exportExcel(ClientProfilePageRequestVO requestVO) {
        ClientProfile clientProfile=new ClientProfile();
        BeanUtils.copyProperties(requestVO,clientProfile);
        List<ClientProfileExcelExportResponseVO> list = clientProfileService.exportListByParam(clientProfile);
        ExcelUtil<ClientProfileExcelExportResponseVO> util = new ExcelUtil<>(ClientProfileExcelExportResponseVO.class);
        return util.exportExcel(list, "客户数据");
    }

    /**
     * 客户信息导入模板
     * @return
     */
    @GetMapping("/importTemplate")
    @ApiOperation(value = "数据导入模板下载", notes = "数据导入模板下载")
    public AjaxResult importTemplate() {
        ExcelUtil<ClientProfileExcelImportResponseVO> util = new ExcelUtil<>(ClientProfileExcelImportResponseVO.class);
        return util.importTemplateExcel("客户数据导入模板");
    }

    /**
     * 客户信息导入
     */
    @Log(title = "客户管理", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    @ApiOperation(value = "客户信息导入", notes = "客户信息导入")
    @PreAuthorize("@ss.hasPermi('tors:clientProfile:import')")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<ClientProfileExcelImportResponseVO> util = new ExcelUtil<>(ClientProfileExcelImportResponseVO.class);
        List<ClientProfileExcelImportResponseVO> excelData = util.importExcel(file.getInputStream());
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String operName = loginUser.getUsername();
        String message = clientProfileService.importUser(excelData, updateSupport, operName);
        return AjaxResult.success(message);
    }
}
