package com.ruoyi.web.controller.website;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.website.domain.ClientSkillResearch;
import com.ruoyi.website.domain.vo.request.SkillResearchQueryRequestVO;
import com.ruoyi.website.domain.vo.request.SkillResearchRequestVO;
import com.ruoyi.website.domain.vo.response.SkillResearchResponseVO;
import com.ruoyi.website.service.IClientRobotReportTagService;
import com.ruoyi.website.service.IClientSkillResearchService;
import com.ruoyi.website.service.ISysFileInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 任期内科研成果Controller
 *
 * @author ruoyi
 * @date 2021-03-18
 */
@Slf4j
@RestController
@Api(value = "任期内科研成果", description = "任期内科研成果,文件类型：54")
@RequestMapping("/skillResearch")
public class ClientSkillResearchController extends BaseController {

    @Autowired
    private IClientSkillResearchService clientSkillResearchService;
    @Autowired
    private ISysFileInfoService fileInfoService;
    @Autowired
    private IClientRobotReportTagService clientRobotReportTagService;



    /**
     * 查询任期内科研成果列表
     */
    @GetMapping("/list")
    @ApiOperation(value="任期内科研成果列表",notes="任期内科研成果列表")
    @ApiResponses({
            @ApiResponse(code = 200,message = "OK",response = SkillResearchResponseVO.class),
    })
    public TableDataInfo list(SkillResearchQueryRequestVO skillResearchQueryRequestVO) {
        List<SkillResearchResponseVO> list = clientSkillResearchService.queryList(skillResearchQueryRequestVO);
        return getDataTable(list);
    }


    /**
     * 获取任期内科研成果详细信息
     */
    @GetMapping(value = "/getInfo/{skillResearchId}")
    @ApiOperation(value = "获取任期内科研成果详细信息", notes = "获取任期内科研成果详细信息")
    @ApiResponses({
            @ApiResponse(code = 200,message = "OK",response = SkillResearchResponseVO.class),
    })
    public AjaxResult getInfo(@PathVariable("skillResearchId") Long skillResearchId) {
        ClientSkillResearch clientSkillResearch=clientSkillResearchService.getById(skillResearchId);
        SkillResearchResponseVO skillResearchResponseVO=new SkillResearchResponseVO();
        BeanUtils.copyProperties(clientSkillResearch,skillResearchResponseVO);
        return AjaxResult.success(skillResearchResponseVO);
    }

    /**
     * 新增任期内科研成果
     */
    @PostMapping("/add")
    @ApiOperation(value="新增任期内科研成果",notes="新增任期内科研成果")
    public AjaxResult add(@Validated @RequestBody SkillResearchRequestVO skillResearchRequestVO) {
        int rows = clientSkillResearchService.insertOrUpdate(skillResearchRequestVO);
        clientRobotReportTagService.resetReportTag(skillResearchRequestVO.getClientId(),"menu_qualityProject12");//重置上报Tag
        return toAjax(rows);
    }

    /**
     * 编辑任期内科研成果
     */
    @PostMapping("/edit")
    @ApiOperation(value="编辑任期内科研成果",notes="编辑任期内科研成果")
    public AjaxResult edit(@Validated @RequestBody SkillResearchRequestVO skillResearchRequestVO) {
        int rows = clientSkillResearchService.insertOrUpdate(skillResearchRequestVO);
        clientRobotReportTagService.resetReportTag(skillResearchRequestVO.getClientId(),"menu_qualityProject12");//重置上报Tag
        return toAjax(rows);
    }


    /**
     * 删除任期内科研成果
     */
    @DeleteMapping(value = "/remove/{skillResearchId}")
    @ApiOperation(value = "根据主键删除任期内科研成果", notes = "根据主键删除任期内科研成果")
    public AjaxResult remove(@PathVariable("skillResearchId") Long skillResearchId) {
        //1.删除附件
        fileInfoService.deleteFileByBusinessId(skillResearchId,"54");
        //2.删除主数据
        ClientSkillResearch clientSkillResearch=clientSkillResearchService.getById(skillResearchId);
        clientSkillResearch.setIsDeleted(-1);
        clientSkillResearch.setUpdateBy(SecurityUtils.getUsername());
        boolean retBool = clientSkillResearchService.updateById(clientSkillResearch);
        clientRobotReportTagService.resetReportTag(clientSkillResearch.getClientId(),"menu_qualityProject12");//重置上报Tag
        return AjaxResult.success(retBool);
    }


}
