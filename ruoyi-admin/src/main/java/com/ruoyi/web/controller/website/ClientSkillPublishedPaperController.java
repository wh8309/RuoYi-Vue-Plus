package com.ruoyi.web.controller.website;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.website.domain.ClientSkillPublishedPaper;
import com.ruoyi.website.domain.vo.request.SkillPublishedQueryRequestVO;
import com.ruoyi.website.domain.vo.request.SkillPublishedRequestVO;
import com.ruoyi.website.domain.vo.response.SkillPublishedResponseVO;
import com.ruoyi.website.domain.vo.response.SkillResearchResponseVO;
import com.ruoyi.website.service.IClientRobotReportTagService;
import com.ruoyi.website.service.IClientSkillPublishedPaperService;
import com.ruoyi.website.service.ISysFileInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 任期内发论文论著情况Controller
 *
 * @author ruoyi
 * @date 2021-03-18
 */
@Slf4j
@RestController
@Api(value = "任期内发论文论著情况", description = "任期内发表论文论著情况,文件类型：53")
@RequestMapping("/skillPapers")
public class ClientSkillPublishedPaperController extends BaseController {

    @Autowired
    private IClientSkillPublishedPaperService clientSkillPublishedPapersService;
    @Autowired
    private ISysFileInfoService fileInfoService;
    @Autowired
    private IClientRobotReportTagService clientRobotReportTagService;

    /**
     * 查询任期内科研成果列表
     */
    @GetMapping("/list")
    @ApiOperation(value = "任期内发表论文论著列表", notes = "任期内发表论文论著列表")
    @ApiResponses(@ApiResponse(code = 200, message = "OK", response = SkillResearchResponseVO.class))
    public TableDataInfo list(SkillPublishedQueryRequestVO skillPublishedQueryRequestVO) {
        List<SkillPublishedResponseVO> list = clientSkillPublishedPapersService.queryList(skillPublishedQueryRequestVO);
        return getDataTable(list);
    }

    /**
     * 获取任期内科研成果详细信息
     */
    @GetMapping(value = "/getInfo/{skillPublishedPapersId}")
    @ApiOperation(value = "获取任期内论文论著详细信息", notes = "获取任期内论文论著详细信息")
    @ApiResponses(@ApiResponse(code = 200, message = "OK", response = SkillPublishedResponseVO.class))
    public AjaxResult getInfo(@PathVariable("skillPublishedPapersId") Long skillPublishedPapersId) {
        ClientSkillPublishedPaper publishedPaper = clientSkillPublishedPapersService.getById(skillPublishedPapersId);
        SkillPublishedResponseVO skillPublishedResponseVO = new SkillPublishedResponseVO();
        BeanUtils.copyProperties(publishedPaper, skillPublishedResponseVO);
        return AjaxResult.success(skillPublishedResponseVO);
    }

    /**
     * 新增任期内论文论著
     */
    @PostMapping("/add")
    @ApiOperation(value = "新增任期内论文论著", notes = "新增任期内论文论著")
    public AjaxResult add(@Validated @RequestBody SkillPublishedRequestVO skillPublishedRequestVO) {
        int rows = clientSkillPublishedPapersService.insertOrUpdate(skillPublishedRequestVO);
        clientRobotReportTagService.resetReportTag(skillPublishedRequestVO.getClientId(),"menu_paperBook");//重置上报Tag
        return toAjax(rows);
    }

    /**
     * 编辑任期内论文论著
     */
    @PostMapping("/edit")
    @ApiOperation(value = "编辑任期内论文论著", notes = "编辑任期内论文论著")
    public AjaxResult edit(@Validated @RequestBody SkillPublishedRequestVO skillPublishedRequestVO) {
        int rows = clientSkillPublishedPapersService.insertOrUpdate(skillPublishedRequestVO);
        clientRobotReportTagService.resetReportTag(skillPublishedRequestVO.getClientId(),"menu_paperBook");//重置上报Tag
        return toAjax(rows);
    }


    /**
     * 删除任期内论文论著
     */
    @DeleteMapping(value = "/remove/{publishedPapersId}")
    @ApiOperation(value = "根据主键删除任期内论文论著", notes = "根据主键删除任期内论文论著")
    public AjaxResult remove(@PathVariable("publishedPapersId") Long publishedPapersId) {
        //1.删除附件
        fileInfoService.deleteFileByBusinessId(publishedPapersId, "53");
        //2.删除主数据
        ClientSkillPublishedPaper clientSkillPublishedPaper = clientSkillPublishedPapersService.getById(publishedPapersId);
        clientSkillPublishedPaper.setIsDeleted(-1);
        clientSkillPublishedPaper.setUpdateBy(SecurityUtils.getUsername());
        boolean retBool = clientSkillPublishedPapersService.updateById(clientSkillPublishedPaper);
        clientRobotReportTagService.resetReportTag(clientSkillPublishedPaper.getClientId(),"menu_paperBook");//重置上报Tag
        return AjaxResult.success(retBool);
    }


}
